<?php

/**
 * This is the model class for table "documents".
 *
 * The followings are the available columns in table 'documents':
 * @property integer $id
 * @property integer $status_is
 * @property integer $type
 * @property string $created_on
 * @property integer $created_by
 * @property string $name
 * @property string $attachment
 * @property integer $outlet_id
 * @property integer $bakery_id
 * @property string $reference
 */
class Documents extends CActiveRecord
{
	public $document_type = array('1' => 'Purchase order', '2' => 'Invoice', '3' => 'Credit note');

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'documents';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			// array('status_is, type, created_on, created_by, name, attachment, outlet_id, bakery_id, reference', 'required'),
			array('status_is, type, created_by, outlet_id, bakery_id', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>100),
			array('attachment', 'length', 'max'=>255),
			array('reference', 'length', 'max'=>50),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, status_is, type, created_on, created_by, name, attachment, outlet_id, bakery_id, reference', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
                return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'status_is' => 'Status Is',
			'type' => 'Type',
			'created_on' => 'Created On',
			'created_by' => 'Created By',
			'name' => 'Name',
			'attachment' => 'Attachment',
			'outlet_id' => 'Outlet',
			'bakery_id' => 'Bakery',
			'reference' => 'Reference',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('status_is',$this->status_is);
		$criteria->compare('type',$this->type);
		$criteria->compare('created_on',$this->created_on,true);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('attachment',$this->attachment,true);
		$criteria->compare('outlet_id',$this->outlet_id);
		$criteria->compare('bakery_id',$this->bakery_id);
		$criteria->compare('reference',$this->reference,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Documents the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getCutomerNames($customer = NULL, $type)
    {
        $html = '<option value="" selected>All</option>';
        $connection = Yii::app()->db;
        $branch_id = Yii::app()->user->bakery;
        $branchClause = $branch_id == 0 ? "" : " WHERE `bakery_id` = '$branch_id'";
        $sql = "SELECT id, description

                FROM `outlet`

                $branchClause

                ORDER BY `description` 

                ASC";
        $command = $connection->createCommand($sql);
        $customers = $command->queryAll();
        $autocomplete=[];
        foreach($customers as $val)
        {
            $selected = $customer == $val['id'] ? 'selected' : '';
            $html .= '<option value="' . $val['id'] . '" ' . $selected . '>' . $val['description'] . '</option>';
            $autocomplete[] = "'" . str_replace("'", "", ucfirst(strtolower($val['description']))) . "'";

        }
        if($type == 'autocomplete') return implode(',', $autocomplete);
        else return $html;


    }

    public static function getDocumentsForDashboardNew($customer = '', $start = '', $end = '')
    {
        $connection = Yii::app()->db;
        $branch_id = Yii::app()->user->bakery;
        // $dateClause = " WHERE d.created_on >= '" . $start ." 00:00:00' AND d.created_on <= '" . $end . " 23:59:59'";
        $branchClause = $branch_id == 0 ? "" : " AND om.`bakery_id` = " . $branch_id;
        $customerClause = $customer == '' ? "" : " WHERE (f.`customer` LIKE '%" . $customer . "%'  OR f.account_number LIKE '%". $customer . "%' OR f.id LIKE '%". $customer . "%' OR f.reference LIKE '%". $customer . "%')";

        $documentsSql = <<<SQL
        select f.* from (
        select
            om.id,
            om.order_status_id,
            1 as status_is,
            1 as `type`,
            om.order_date as created_on,
            om.order_taker_employee_nr as created_by,
            'PURCHASE ORDER' as `name`,
            '' as attachment,
            om.account as outlet_id,
            om.bakery_id,
            om.id as reference,
            IFNULL(omh.to_description, '-') as customer,
            IFNULL(omh.to_account_number, o.account_number) as account_number
        from
            order_main om
        left join order_main_header omh ON
            omh.order_id = om.id
        join
            outlet o on o.id = om.account
        where
            om.order_type = 1
            AND om.order_date >= '{$start} 00:00:00'
            AND om.order_date <= '{$end} 23:59:59'
            AND om.order_status_id NOT IN (7, 8)
            {$branchClause}
        union all
        select
            om.id,
            om.order_status_id,
            1 as status_is,
            2 as `type`,
            om.actual_delivery_date as created_on,
            om.delivered_by as created_by,
            'INVOICE' as name,
            '' as attachment,
            om.account as outlet_id,
            om.bakery_id,
            om.id as reference,
            IFNULL(omh.to_description, '-') as customer,
            IFNULL(omh.to_account_number, o.account_number) as account_number
        from
            order_main om
        left join order_main_header omh ON
            omh.order_id = om.id
        join
            outlet o on o.id = om.account
        left join
		    (select missed.order_id from orders_missed missed where `date` >= '{$start} 00:00:00' and `date` <= '{$end} 23:59:59' group by missed.order_id) m on m.order_id = om.id
        where
            om.actual_delivery_date >= '{$start} 00:00:00'
            AND om.actual_delivery_date <= '{$end} 23:59:59'
            AND om.order_status_id NOT IN (7,8)
            AND m.order_id is null
            {$branchClause}
        union all
        select
            om.id,
            om.order_status_id,
            1 as status_is,
            3 as `type`,
            om.order_date as created_on,
            om.order_taker_employee_nr as created_by,
            'CREDIT NOTE' as name,
            '' as attachment,
            om.account as outlet_id,
            om.bakery_id,
            om.id as reference,
            IFNULL(omh.to_description, '-') as customer,
            IFNULL(omh.to_account_number, o.account_number) as account_number
        from
            order_main om
        left join order_main_header omh ON
            omh.order_id = om.id
        join
            outlet o on o.id = om.account
        where
            om.order_status_id in (13, 18)
            AND om.order_date >= '{$start} 00:00:00'
            AND om.order_date <= '{$end} 23:59:59'
            {$branchClause}
           /*------|-UM2230|=-FORM UNION QUERY ---*/
           union all
           select
           r.id,
           c.checklist_type_id as order_status_id,
           1 as status_is,
           4 as `type`,
            r.result_datetime as created_on,
            r.employee_id as created_by,
            concat(c.checklist_name, " ", r.id) as name,
            '' as attachment,
            r.outlet_id,
            r.bakery_id,
            r.id as reference,
            IF(ISNULL(o.id), e.name, o.description) as customer,
            IF(ISNULL(o.id), e.employee_number, o.account_number) as account_number
        from
            results r
           left join employee e on e.id = r.employee_id 
        left join
            outlet o on o.id = r.outlet_id
        left join
            checklist c on c.id = r.checklist_id
        where
            r.result_datetime >= '{$start} 00:00:00'
            AND r.result_datetime <= '{$end} 23:59:59'
            {$branchClause}  
           /*--------END FORM QUERY --|UM-2230|-*/ 

        ) f
        {$customerClause}
        order by f.created_on
SQL;

        $results = $connection->createCommand($documentsSql)->queryAll();
        return $results;
    }

    public static function getDocumentsForDashboard($customer = '', $start = '', $end = '')
    {
        $connection = Yii::app()->db;
        $branch_id = Yii::app()->user->bakery;
        $dateClause = " WHERE d.created_on >= '" . $start ." 00:00:00' AND d.created_on <= '" . $end . " 23:59:59'";
        $branchClause = $branch_id == 0 ? "" : " AND d.`bakery_id` = " . $branch_id;
        $customerClause = $customer == '' ? "" : " AND (c.`description` LIKE '%" . $customer . "%'  OR omh.to_account_number LIKE '%". $customer . "%' OR om.id LIKE '%". $customer . "%' OR d.reference LIKE '%". $customer . "%')";

        $documentsSql = <<<SQL
        SELECT 
            d.*,
            IFNULL(omh.to_description, '-') AS customer,
            IFNULL(omh.to_account_number, c.account_number) AS account_number
        FROM
            `documents` d
                JOIN
            `outlet` c ON c.`id` = d.`outlet_id`
                LEFT JOIN
            `order_main` om ON c.id = om.`account`
                AND om.id = d.`reference`
                LEFT JOIN
            order_main_header omh ON omh.order_id = om.id
                {$dateClause}
                {$branchClause}
                {$customerClause}
        GROUP BY
            d.`type`,
            d.reference
        ORDER BY d.`created_on` DESC;
SQL;

        $results = $connection->createCommand($documentsSql)->queryAll();
        return $results;

    }

    public function getInvoiceLinkByOrderId($id)
    {
        #Documents type : 1=Purchase order, 2=Invoice, 3=Credit note etc
        $connection = Yii::app()->db;
        $sql = "SELECT attachment, type FROM documents WHERE status_is='1' AND (attachment LIKE '%" . trim($id) . "%' OR reference ='" . trim($id) . "')";
        $command = $connection->createCommand($sql);
        $attachments = $command->queryAll();
        if(isset($attachments) && !empty($attachments))
        {
            $return['purchase'] = '';
            $return['invoice'] = '';
            foreach($attachments as $val)
            {
                if($val['type'] == 1) $return['purchase'] = $val['attachment'];
                if($val['type'] == 2) $return['invoice'] = $val['attachment'];

            }
            return $return;

        }
        else
        {
            return false;

        }

    }


    public static function logDocument($orderid, $attachment, $note)
    {
        $attachment = str_replace('\\', '/', $attachment);
        $attachment = str_replace('#', '', $attachment);
        $attachment = Yii::app()->baseUrl . '/' . $attachment;
        $logId = CommunicationLog::log('N/A', 'SAVED', $note, $attachment);
        return $attachment;

    }


    public static function log($type, $employid, $name, $attachment, $outlet, $bakery, $reference)
    {
        $insertSql = "INSERT INTO documents

                     VALUES (null, '1', '{$type}', NOW(), '{$employid}', '{$name}', '$attachment', '{$outlet}', '{$bakery}', '{$reference}')

                     ";
        $con = Yii::app()->db;
        $command = $con->createCommand($insertSql);
        $result = $command->execute();
        return Yii::app()->db->getLastInsertID();

    }

    public static function logDoc($type, $employid, $name, $attachment, $outlet, $bakery, $reference, $date)
    {
        $doc = new Documents();
        $doc->status_is = 1;
        $doc->type = $type;
        $doc->created_on = $date;
        $doc->created_by = $employid;
        $doc->name = $name;
        $doc->attachment = $attachment;
        $doc->outlet_id = $outlet;
        $doc->bakery_id = $bakery;
        $doc->reference = $reference;
        $doc->save();

        return $doc->id;
    }


    public function getDocumentById($orderId, $docType)
    {
        $name = 'PURCHASE ORDER';
        if($docType == '2')
        {
            $name = 'INVOICE';
        }
        elseif($docType == '3')
        {
            $name = 'CREDIT NOTE';
        }
        elseif($docType == '4')// ---UM2230//
        {
            $name = 'FORM';
        }
        $results = array();
        $connection = Yii::app()->db;
        $user_id = Yii::app()->user->id;
        if($name == 'FORM')
        {
            $sql = <<<SQL
        select
                r.id,
                1 as status_is,
                4 as `type`,
                    r.result_datetime as created_on,
                    r.employee_id as created_by,
                    'FORM' as name,
                    '' as attachment,
                    r.outlet_id,
                    r.bakery_id,
                    r.id as reference,
                    IFNULL(o.description, '-') as customer,
                    IFNULL(r.account, o.account_number) as account_number
                from
                    results r
                left join
                    outlet o on o.id = r.outlet_id
                left join
                    checklist c on c.id = r.checklist_id
                where
                r.id = :orderId
SQL;
        }
        else
        {
        $sql = <<<SQL
        select
            om.id,
            1 as status_is,
            {$docType} as `type`,
            om.order_date as created_on,
            om.order_taker_employee_nr as created_by,
            '{$name}' as name,
            '' as attachment,
            om.account as outlet_id,
            om.bakery_id,
            om.id as reference,
            IFNULL(omh.to_description, '-') as customer,
            IFNULL(omh.to_account_number, o.account_number) as account_number
        from
            order_main om
        left join order_main_header omh ON
            omh.order_id = om.id
        join outlet o on
            o.id = om.account
        where
            om.id = :orderId
SQL;
        }
        $command = $connection->createCommand($sql);
        $command->bindValue(':orderId', $orderId, PDO::PARAM_INT);
        $results = $command->queryAll();
        return $results[0];

    }


    public function getUserDetails()
    {
        $results = array();
        $connection = Yii::app()->db;
        $id = Yii::app()->user->id;
        $sql = "SELECT email, name

                FROM employee 

                WHERE id=$id

                ";
        $command = $connection->createCommand($sql);
        $results = $command->queryAll();
        return $results[0];

    }
    
    public static function getPrinterDocumentTemplate($bakery_id)
    {
        //Get invoice template
        $templates=array(
            'Invoice'=>Documents::getInvoiceTemplate($bakery_id),
            'Quotation'=>Documents::getQuotationTemplate($bakery_id),
            'CreditNote'=>Documents::getCreditNoteTemplate($bakery_id),
            'OpeningReceipt'=>Documents::getOpeningRTemplate($bakery_id),
            'ClosingReceipt'=>Documents::getClosingRTemplate($bakery_id),
            'CashCollect'=>Documents:: getCashCTemplate($bakery_id)
        );
        return $templates;
    }
    
    public static function getInvoiceTemplate($bakery_id)
    {
        $db = Yii::app()->db;
        $invoiceDocumentTemplate=array();
        
        //Get invoice template
        $sql="select *
        from document_template dt
        left join document_template_options dto on dt.document_template_id=dto.document_template_id
        where bakery_id= :bakeryId and document_type_id=4
        group by dto.label
        order by dto.id";
        $invoiceTemplate = $db->createCommand($sql)
                ->bindValue(':bakeryId', $bakery_id, PDO::PARAM_INT)
                ->queryAll();
        
        $title='TAX Invoice';
        $creditNoteApplied = 'CREDIT NOTE - ';
        $invoice_number='INVOICE #';
        $branch_vat='VAT #';
        $delivery_date='DELIVERY DATE';
        $from='FROM';
        $to='TO';
        $customer_vat='VAT #';
        $payment_type='PAYMENT TYPE';
        $purchase_order='PURCHASE ORDER #';
        $deliver_by='DELIVERED BY';
        $a_received='A: RECEIVED';
        $excl_vat='EXCL VAT';
        $sub_total_a='SUB-TOTAL A';
        $b_returned='B: RETURNED';
        $sub_total_b='SUB-TOTAL B';
        $sub_total_a_b='SUB-TOTAL (A - B)';
        $discount='DISCOUNT';
        $sub_total='SUB-TOTAL';
        $vat='VAT';
        $total='TOTAL';
        $cash_collected='CASH COLLECTED';
        $currency_collected='CURRENCY COLLECTED';
        $currency_collected_amount='CURRENCY COLLECTED AMOUNT';
        $customer_signature='CUSTOMER SIGNATURE';
        $driver_signature='DRIVER SIGNATURE';
        $total_items='TOTAL ITEMS';
        $ref_number="REF #";
        $document_footer_text='';
        
        if(count($invoiceTemplate)>0)
        {
            foreach($invoiceTemplate as $options)
            {
                switch($options['label'])
                {
                    case 'title' :
                        $title=$options['translation'];
                        break;
                    case 'credit_note_applied':
                        $creditNoteApplied = $options['translation'];
                        break;
                    case 'invoice_number' :
                        $invoice_number=$options['translation'];
                        break;
                   
                    case 'branch_vat' :
                        $branch_vat=$options['translation'];
                        break;
                    case 'delivery_date' :
                        $delivery_date=$options['translation'];
                        break;
                    case 'from' :
                        $from=$options['translation'];
                        break;
                    case 'to' :
                        $to=$options['translation'];
                        break;
                    case 'customer_vat' :
                        $customer_vat=$options['translation'];
                        break;
                    case 'payment_type' :
                        $payment_type=$options['translation'];
                        break;
                    case 'purchase_order' :
                        $purchase_order=$options['translation'];
                        break;
                    case 'deliver_by' :
                        $deliver_by=$options['translation'];
                        break;
                    case 'a_received' :
                        $a_received=$options['translation'];
                        break;
                    case 'excl_vat' :
                        $excl_vat=$options['translation'];
                        break;
                    case 'sub_total_a' :
                        $sub_total_a=$options['translation'];
                        break;
                    case 'b_returned' :
                        $b_returned=$options['translation'];
                        break;
                    case 'sub_total_b' :
                        $sub_total_b=$options['translation'];
                        break;
                    case 'sub_total_a_b' :
                        $sub_total_a_b=$options['translation'];
                        break;
                    case 'discount' :
                        $discount=$options['translation'];
                        break;
                    case 'sub_total' :
                        $sub_total=$options['translation'];
                        break;
                    case 'vat':
                        $vat=$options['translation'];
                        break;
                    case 'total' :
                        $total=$options['translation'];
                        break;
                    case 'cash_collected' :
                        $cash_collected=$options['translation'];
                        break;
                    case 'currency_collected' :
                        $currency_collected=$options['translation'];
                        break;
                    case 'currency_collected_amount' :
                        $currency_collected_amount=$options['translation'];
                        break;
                    case 'customer_signature' :
                        $customer_signature=$options['translation'];
                        break;
                    case 'driver_signature' :
                        $driver_signature=$options['translation'];
                        break;
                    case 'total_items' :
                        $total_items=$options['translation'];
                        break;
                    case 'document_footer_text' :
                        $document_footer_text=$options['translation'];
                        break;
                    case 'ref_number' :
                        $ref_number=$options['translation'];
                        break;
                        
                }
            }
        }
        $invoiceDocumentTemplate=array(
            'title'=>$title,
            'credit_note_applied' => $creditNoteApplied,
            'invoice_number'=>$invoice_number,
            'ref_number' => $ref_number, 
            'purchase_order'=>$purchase_order,
            'branch_vat'=>$branch_vat,
            'delivery_date'=>$delivery_date,
            'fromName'=>$from,
            'toName'=>$to,
            'customer_vat'=>$customer_vat,
            'payment_type'=>$payment_type,
            'deliver_by'=>$deliver_by,
            'a_received'=>$a_received,
            'excl_vat'=>$excl_vat,
            'sub_total_a'=>$sub_total_a,
            'b_returned'=>$b_returned,
            'sub_total_b'=>$sub_total_b,
            'sub_total_a_b'=>$sub_total_a_b,
            'discount'=>$discount,
            'sub_total'=>$sub_total,
            'vat'=>$vat,
            'total'=>$total,
            'cash_collected'=>$cash_collected,
            'currency_collected'=>$currency_collected,
            'currency_collected_amount'=>$currency_collected_amount,
            'customer_signature'=>$customer_signature,
            'driver_signature'=>$driver_signature,
            'total_items'=>$total_items,
            'document_footer_text'=>$document_footer_text
            
            
        );
        return $invoiceDocumentTemplate;
    }
    
    public static function getQuotationTemplate($bakery_id)
    {
        $db = Yii::app()->db;
        $quotationDocumentTemplate=array();
        
        //Get invoice template
        $sql="select *
        from document_template dt
        left join document_template_options dto on dt.document_template_id=dto.document_template_id
        where bakery_id= :bakeryId and document_type_id=5";
        $quotationTemplate = $db->createCommand($sql)
                ->bindValue(':bakeryId', $bakery_id, PDO::PARAM_INT)
                ->queryAll();
        
        $title='QUOTATION';
        $quotation_number='QUOTATION #';
        $branch_vat='VAT #';
        $delivery_date='DELIVERY DATE';
        $from='FROM';
        $to='TO';
        $a_received='A: RECEIVED';
        $excl_vat='EXCL VAT';
        $sub_total_a='SUB-TOTAL A';
        $b_returned='B: RETURNED';
        $sub_total_b='SUB-TOTAL B';
        $sub_total_a_b='SUB-TOTAL (A - B)';
        $discount='DISCOUNT';
        $sub_total='SUB-TOTAL';
        $vat='VAT';
        $cnApplied = 'CREDIT NOTE - ';
        $total='TOTAL';
        $purchase_order='PURCHASE ORDER #';
        $delivered_by='QUOTATION BY';
        $document_footer_text='';
        
        if(count($quotationTemplate)>0)
        {
            foreach($quotationTemplate as $options)
            {
                switch($options['label'])
                {
                    case 'title' :
                        $title=$options['translation'];
                        break;
                    case 'quotation_number' :
                        $quotation_number=$options['translation'];
                        break;
                    case 'purchase_order' :
                        $purchase_order=$options['translation'];
                        break;
                    case 'branch_vat' :
                        $branch_vat=$options['translation'];
                        break;
                    case 'delivery_date' :
                        $delivery_date=$options['translation'];
                        break;
                    case 'from' :
                        $from=$options['translation'];
                        break;
                    case 'to' :
                        $to=$options['translation'];
                        break;
                    case 'delivered_by' :
                        $delivered_by=$options['translation'];
                        break;
                        
                    case 'a_received' :
                        $a_received=$options['translation'];
                        break;
                    case 'excl_vat' :
                        $excl_vat=$options['translation'];
                        break;
                    case 'sub_total_a' :
                        $sub_total_a=$options['translation'];
                        break;
                    case 'b_returned' :
                        $b_returned=$options['translation'];
                        break;
                    case 'sub_total_b' :
                        $sub_total_b=$options['translation'];
                        break;
                    case 'sub_total_a_b' :
                        $sub_total_a_b=$options['translation'];
                        break;
                    case 'discount' :
                        $discount=$options['translation'];
                        break;
                    case 'sub_total' :
                        $sub_total=$options['translation'];
                        break;
                    case 'vat':
                        $vat=$options['translation'];
                        break;
                    case 'cn_applied':
                        $cnApplied=$options['translation'];
                        break;
                    case 'total' :
                        $total=$options['translation'];
                        break;
                    case 'document_footer_text' :
                        $document_footer_text=$options['translation'];
                        break;
                }
            }
        }
        $quotationDocumentTemplate=array(
            'title'=>$title,
            'quotation_number'=>$quotation_number,
            'purchase_order'=>$purchase_order,
            'branch_vat'=>$branch_vat,
            'delivery_date'=>$delivery_date,
            'fromName'=>$from,
            'toName'=>$to,
            'delivered_by'=>$delivered_by,
            'a_received'=>$a_received,
            'excl_vat'=>$excl_vat,
            'sub_total_a'=>$sub_total_a,
            'b_returned'=>$b_returned,
            'sub_total_b'=>$sub_total_b,
            'sub_total_a_b'=>$sub_total_a_b,
            'discount'=>$discount,
            'sub_total'=>$sub_total,
            'vat'=>$vat,
            'cn_applied' => $cnApplied,
            'total'=>$total,
            'document_footer_text'=>$document_footer_text
         );
        return $quotationDocumentTemplate;
    }
    public static function getCreditNoteTemplate($bakery_id)
    {
        $db = Yii::app()->db;
        $creditnoteDocumentTemplate=array();
        
        //Get invoice template
        $sql="select *
        from document_template dt
        left join document_template_options dto on dt.document_template_id=dto.document_template_id
        where bakery_id= :bakeryId and document_type_id=6";
        $creditnoteTemplate = $db->createCommand($sql)
                ->bindValue(':bakeryId', $bakery_id, PDO::PARAM_INT)
                ->queryAll();
        
        $title='CREDIT NOTE';
        $credit_note_number='CREDIT NOTE #';
        $branch_vat='VAT #';
        $delivery_date='ISSUED DATE';
        $from='FROM';
        $to='TO';
        $deliver_by='ISSUED BY';
        $a_received='A: RECEIVED';
        $excl_vat='EXCL VAT';
        $sub_total_a='SUB-TOTAL A';
        $b_returned='B: RETURNED';
        $sub_total_b='SUB-TOTAL B';
        $sub_total_a_b='SUB-TOTAL (A - B)';
        $sub_total='SUB-TOTAL';
        $vat='VAT';
        $total='TOTAL';
        $signature='SIGNATURE';
        $signatureTwo='DRIVER SIGNATURE';
        $purchase_order='PURCHASE ORDER #';
        $document_footer_text='';
        $ref_number="REF #";
        $linkedInvoice="LINKED INVOICE #";
        
        if(count($creditnoteTemplate)>0)
        {
            foreach($creditnoteTemplate as $options)
            {
                switch($options['label'])
                {
                    case 'title' :
                        $title=$options['translation'];
                        break;
                    case 'credit_note_number' :
                        $credit_note_number=$options['translation'];
                        break;
                    case 'purchase_order' :
                        $purchase_order=$options['translation'];
                        break;
                    case 'branch_vat' :
                        $branch_vat=$options['translation'];
                        break;
                    case 'delivery_date' :
                        $delivery_date=$options['translation'];
                        break;
                    case 'from' :
                        $from=$options['translation'];
                        break;
                    case 'to' :
                        $to=$options['translation'];
                        break;
                    case 'deliver_by' :
                        $deliver_by=$options['translation'];
                        break;
                    case 'a_received' :
                        $a_received=$options['translation'];
                        break;
                    case 'excl_vat' :
                        $excl_vat=$options['translation'];
                        break;
                    case 'sub_total_a' :
                        $sub_total_a=$options['translation'];
                        break;
                    case 'b_returned' :
                        $b_returned=$options['translation'];
                        break;
                    case 'sub_total_b' :
                        $sub_total_b=$options['translation'];
                        break;
                    case 'sub_total_a_b' :
                        $sub_total_a_b=$options['translation'];
                        break;
                    case 'sub_total' :
                        $sub_total=$options['translation'];
                        break;
                    case 'vat':
                        $vat=$options['translation'];
                        break;
                    case 'total' :
                        $total=$options['translation'];
                        break;
                    case 'signature' :
                        $signature=$options['translation'];
                        break;
                    case 'signature_two_text' :
                        $signatureTwo=$options['translation'];
                        break;
                    case 'document_footer_text' :
                        $document_footer_text=$options['translation'];
                        break;
                    case 'ref_number' :
                        $ref_number=$options['translation'];
                        break;
                    case 'linked_invoice_number' :
                        $linkedInvoice=$options['translation'];
                        break;
                    
                }
            }
        }
        $creditnoteDocumentTemplate=array(
            'title'=>$title,
            'credit_note_number'=>$credit_note_number,
            'linked_invoice_number'=>$linkedInvoice,
            'ref_number' => $ref_number,
            'purchase_order'=>$purchase_order,
            'branch_vat'=>$branch_vat,
            'delivery_date'=>$delivery_date,
            'fromName'=>$from,
            'toName'=>$to,
            'deliver_by'=>$deliver_by,
            'a_received'=>$a_received,
            'excl_vat'=>$excl_vat,
            'sub_total_a'=>$sub_total_a,
            'b_returned'=>$b_returned,
            'sub_total_b'=>$sub_total_b,
            'sub_total_a_b'=>$sub_total_a_b,
            'sub_total'=>$sub_total,
            'vat'=>$vat,
            'total'=>$total,
            'signature'=>$signature,
            'signature_two_text'=>$signatureTwo,
            'document_footer_text'=>$document_footer_text
          );
        return $creditnoteDocumentTemplate;
    }
    public static function getOpeningRTemplate($bakery_id)
    {
        $db = Yii::app()->db;
        $openingRDocumentTemplate=array();
        
        //Get invoice template
        $sql="select *
        from document_template dt
        left join document_template_options dto on dt.document_template_id=dto.document_template_id
        where bakery_id= :bakeryId and document_type_id=7";
        $openingRTemplate = $db->createCommand($sql)
                ->bindValue(':bakeryId', $bakery_id, PDO::PARAM_INT)
                ->queryAll();
        
        $title='OPENING RECEIPT';
        $branch='BRANCH';
        $date='DATE';
        $stock_location_from='STOCK LOCATION FROM';
        $driver='DRIVER';
        $shipment='SHIPMENT';
        $opening_mileage='OPENING MILEAGE';
        $qty='QTY';
        $description='DESCRIPTION';
        $total_qty='TOTAL QTY';
        $total='TOTAL';
        $signature='SIGNATURE';
        $document_footer_text='';
        $purchase_order='PURCHASE ORDER #';
        if(count($openingRTemplate)>0)
        {
            foreach($openingRTemplate as $options)
            {
                switch($options['label'])
                {
                    case 'title' :
                        $title=$options['translation'];
                        break;
                    case 'branch' :
                        $branch=$options['translation'];
                        break;
                    case 'date' :
                        $date=$options['translation'];
                        break;
                    case 'purchase_order' :
                        $purchase_order=$options['translation'];
                        break;
                    case 'stock_location_from' :
                        $stock_location_from=$options['translation'];
                        break;
                    case 'driver' :
                        $driver=$options['translation'];
                        break;
                    case 'shipment' :
                        $shipment=$options['translation'];
                        break;
                    case 'opening_mileage' :
                        $opening_mileage=$options['translation'];
                        break;
                    case 'qty' :
                        $qty=$options['translation'];
                        break;
                    case 'description' :
                        $description=$options['translation'];
                        break;
                    case 'total_qty' :
                        $total_qty=$options['translation'];
                        break;
                    case 'total' :
                        $total=$options['translation'];
                        break;
                    case 'signature' :
                        $signature=$options['translation'];
                        break;
                    case 'document_footer_text' :
                        $document_footer_text=$options['translation'];
                        break;
                }
            }
        }
        $openingRDocumentTemplate=array(
            'title'=>$title,
            'branch'=>$branch,
            'date'=>$date,
            // 'purchase_order'=>$purchase_order,
            'stock_location_from'=>$stock_location_from,
            'driver'=>$driver,
            'shipment'=>$shipment,
            'opening_mileage'=>$opening_mileage,
            'qty'=>$qty,
            'description'=>$description,
            // 'total_qty'=>$total_qty,
            // 'total'=>$total,
            'signature'=>$signature,
            'document_footer_text'=>$document_footer_text
        );
        return $openingRDocumentTemplate;
    }
    
    public static function getClosingRTemplate($bakery_id)
    {
        $db = Yii::app()->db;
        $closingRDocumentTemplate=array();
        
        //Get invoice template
        $sql="select *
        from document_template dt
        left join document_template_options dto on dt.document_template_id=dto.document_template_id
        where bakery_id= :bakeryId and document_type_id=8";
        $closingRTemplate = $db->createCommand($sql)
                ->bindValue(':bakeryId', $bakery_id, PDO::PARAM_INT)
                ->queryAll();
        
        $title='CLOSING RECEIPT';
        $date='DATE';
        $branch='BRANCH';
        $stock_location_from='STOCK LOCATION FROM';
        $driver='DRIVER';
        $shipment='SHIPMENT';
        $closing_mileage='CLOSING MILEAGE';
        $dispatched='DISPATCHED';
        $qty='QTY';
        $description='DESCRIPTION';
        $dispatched_total_qty='DISPATCHED TOTAL QTY';
        $total='TOTAL';
        $returns='RETURNS';
        $returns_qty='QTY';
        $returns_description='DESCRIPTION';
        $returns_total_qty='RETURNS TOTAL QTY';
        $returns_total='TOTAL';
        $invoice_cash_collected = 'INVOICE CASH COLLECTED';
        $follow_up_cash_collected = 'FOLLOW UP CASH COLLECTED';
        $signature='SIGNATURE';
        $document_footer_text='';
        $purchase_order='PURCHASE ORDER #';
        if(count($closingRTemplate)>0)
        {
            foreach($closingRTemplate as $options)
            {
                switch($options['label'])
                {
                    case 'title' :
                        $title=$options['translation'];
                        break;
                    case 'date' :
                        $date=$options['translation'];
                        break;
                    case 'purchase_order' :
                        $purchase_order=$options['translation'];
                        break;
                    case 'branch' :
                        $branch=$options['translation'];
                        break;
                    case 'stock_location_from' :
                        $stock_location_from=$options['translation'];
                        break;
                    case 'driver' :
                        $driver=$options['translation'];
                        break;
                    case 'shipment' :
                        $shipment=$options['translation'];
                        break;
                    case 'closing_mileage' :
                        $closing_mileage=$options['translation'];
                        break;
                    case 'dispatched' :
                        $dispatched=$options['translation'];
                        break;
                    case 'qty' :
                        $qty=$options['translation'];
                        break;
                    case 'description' :
                        $description=$options['translation'];
                        break;
                    case 'dispatched_total_qty' :
                        $dispatched_total_qty=$options['translation'];
                        break;
                    case 'total' :
                        $total=$options['translation'];
                        break;
                    case 'returns' :
                        $returns=$options['translation'];
                        break;
                    case 'returns_qty' :
                        $returns_qty=$options['translation'];
                        break;
                    case 'returns_description' :
                        $returns_description=$options['translation'];
                        break;
                    case 'returns_total_qty' :
                        $returns_total_qty=$options['translation'];
                        break;
                    case 'returns_total' :
                        $returns_total=$options['translation'];
                        break;
                    case 'invoice_cash_collected' :
                        $invoice_cash_collected=$options['translation'];
                        break;
                    case 'follow_up_cash_collected' :
                        $follow_up_cash_collected=$options['translation'];
                        break;
                    case 'signature' :
                        $signature=$options['translation'];
                        break;
                    case 'document_footer_text' :
                        $document_footer_text=$options['translation'];
                        break;
                }
            }
        }
        $closingRDocumentTemplate=array(
            'title'=>$title,
            'date'=>$date,
            // 'purchase_order'=>$purchase_order,
            'branch'=>$branch,
            'stock_location_from'=>$stock_location_from,
            'driver'=>$driver,
            'shipment'=>$shipment,
            'closing_mileage'=>$closing_mileage,
            'dispatched'=>$dispatched,
            'qty'=>$qty,
            'description'=>$description,
            'dispatched_total_qty'=>$dispatched_total_qty,
            'total'=>$total,
            'returns'=>$returns,
            'returns_qty'=>$returns_qty,
            'returns_description'=>$returns_description,
            // 'returns_total_qty'=>$returns_total_qty,
            // 'returns_total'=>$returns_total,
            'invoice_cash_collected' => $invoice_cash_collected,
            'follow_up_cash_collected' => $follow_up_cash_collected,
            'signature'=>$signature,
            'document_footer_text'=>$document_footer_text
        );
        return $closingRDocumentTemplate;
    }
    public static function getCashCTemplate($bakery_id)
    {
        $db = Yii::app()->db;
        $cashCDocumentTemplate=array();
        
        //Get invoice template
        $sql="select *
        from document_template dt
        left join document_template_options dto on dt.document_template_id=dto.document_template_id
        where bakery_id= :bakeryId and document_type_id=9";
        $cashCTemplate = $db->createCommand($sql)
                ->bindValue(':bakeryId', $bakery_id, PDO::PARAM_INT)
                ->queryAll();
        
        $title='CASH COLLECT';
        $date='DATE';
        $from='FROM';
        $to='TO';
        $ref='REF #';
        $name='NAME';
        $amount='AMOUNT';
        $currency_collected='CURRENCY COLLECTED';
        $currency_collected_amount='CURRENCY COLLECTED AMOUNT';
        $payment_type='PAYMENT TYPE';
        $signature='SIGNATURE';
        $document_footer_text='';
        $purchase_order='PURCHASE ORDER #';
        if(count($cashCTemplate)>0)
        {
            foreach($cashCTemplate as $options)
            {
                switch($options['label'])
                {
                    case 'title' :
                        $title=$options['translation'];
                        break;
                    case 'date' :
                        $date=$options['translation'];
                        break;
                    case 'purchase_order' :
                        $purchase_order=$options['translation'];
                        break;
                    case 'from' :
                        $from=$options['translation'];
                        break;
                    case 'to' :
                        $to=$options['translation'];
                        break;
                    case 'ref' :
                        $ref=$options['translation'];
                        break;
                    case 'name' :
                        $name=$options['translation'];
                        break;
                    case 'amount' :
                        $amount=$options['translation'];
                        break;
                    case 'currency_collected' :
                        $currency_collected=$options['translation'];
                        break;
                    case 'currency_collected_amount' :
                        $currency_collected_amount=$options['translation'];
                        break;
                    case 'payment_type' :
                        $payment_type=$options['translation'];
                        break;
                    case 'signature' :
                        $signature=$options['translation'];
                        break;
                    case 'document_footer_text' :
                        $document_footer_text=$options['translation'];
                        break;
                }
            }
        }
        $cashCDocumentTemplate=array(
            'title'=>$title,
            'date'=>$date,
            // 'purchase_order'=>$purchase_order,
            'fromName'=>$from,
            'toName'=>$to,
            'ref'=>$ref,
            'name'=>$name,
            'amount'=>$amount,
            'currency_collected' => $currency_collected,
            'currency_collected_amount' => $currency_collected_amount,
            'payment_type'=>$payment_type,
            'signature'=>$signature,
            'document_footer_text'=>$document_footer_text
        );
        return $cashCDocumentTemplate;
    }

    public static function sendDocumentEmail($orderId, $docType)
    {
        switch($docType)
        {
            case 1://PURCHASE ORDER
                $pdfDataFunction = 'getPurchaseOrderDetails';
                $pdfCreateFunction = 'createPurchaseOrder';
                $type = 'purchaseOrder';
            break;

            case 2://INVOICE
                $pdfDataFunction = 'getInvoiceDetails';
                $pdfCreateFunction = 'createInvoice';
                $type = 'invoice';
            break;

            case 3://CREDIT NOTE
                $pdfDataFunction = 'getCreditNoteDetails';
                $pdfCreateFunction = 'createCreditNote';
                $type = 'document';
            break;

            case 4://FORM
                $pdfDataFunction = 'getFormDetails';
                $pdfCreateFunction = 'createForm';
                $type = 'form';
            break;
        }

        $pdfData = MailFactory::$pdfDataFunction($orderId);
        $pdfFactory = new PDFFactory($pdfData);
        $pdf = $pdfFactory->$pdfCreateFunction($orderId);

        $to = $_POST['to'];
        $subject = $_POST['subject'];
        $comment = nl2br($_POST['comment']);
        
        if(strpos($to, ',') !== false)
        {
            $to = str_replace(',',';',$to);
        }
        
        if(strpos($to, ';') !== false)
        {
            $dest = explode(';',$to);
            foreach($dest as $email)
            {
                $email = new MailFactory($type, $orderId, trim($email));
                $details = $email->sendDocumentEmail($pdf, $subject, $comment);    
            }
        }
        else
        {
            $email = new MailFactory($type, $orderId, $to);
            $details = $email->sendDocumentEmail($pdf, $subject, $comment);
        }

        unlink($pdf);
    }
}
