<?php

/**
 * Created by PhpStorm.
 * User: Christo
 * Date: 2016/03/22
 * Time: 05:41 PM
 */

define('PDF_MGL', 5);
define('PDF_MGR', 5);
define('PDF_MGT', 5);
define('PDF_MGB', 15);
define('PDF_MGH', 0);
define('PDF_MGF', 5);

class PDFFactory
{
    protected $data;
    protected $pdfFolder;
    private $heading;
    private $removeTitle;
    private $orderId;

    public function __construct($pdfData, $heading = 'Purchase Order', $removeTitle = false, $showOrderId = false)
    {
        $this->data = $pdfData;
        $this->pdfFolder = Yii::app()->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'files' . DIRECTORY_SEPARATOR;
        $this->heading = $heading;
        $this->removeTitle = $removeTitle;

        if ($showOrderId)
        {
            $oId = '';
            if (isset($this->data['ordered'][0]['purchase_order_number']))
            {
                $oId = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' . $this->data['ordered'][0]['purchase_order_number'];
            }
            $this->orderId = $oId;
        }
        else
        {
            $this->orderId = '';
        }
    }

    protected function show($content, $bakeryId = null)
    {
        $connection = Yii::app()->db;
        if (Yii::app() instanceof CConsoleApplication)
        {
            $baseUrl = Yii::app()->params['base_URL'];
        }
        else
        {
            $baseUrl = Yii::app()->getBaseUrl(true);
        }
        if (is_null($bakeryId))
        {
            $logoSql = <<<SQL
            select concat('{$baseUrl}/', logo_url) from project_config order by id limit 1;
SQL;
        }
        else
        {
            $logoSql = <<<SQL
            select concat('{$baseUrl}/', image) from bakery where id = {$bakeryId};
SQL;
        }
        $command = $connection->createCommand($logoSql);
        $logoUrl = $command->queryScalar();
        if (is_null($logoUrl)) //no logo found for the branch then use the default logo
        {
            $logoSql = <<<SQL
            select concat('{$baseUrl}/', logo_url) from project_config order by id limit 1;
SQL;
            $command = $connection->createCommand($logoSql);
            $logoUrl = $command->queryScalar();
        }
        $html = <<<HTML
        <html>
            <head>
                <style>
                    html{
                        margin: 0;
                        padding: 0;
                    }

                    body{
                        font-family: Arial, Helvetica, sans-serif;
                        margin: 0;
                    }

                    .center{
                        text-align: center;
                    }

                    .right{
                        text-align: right;
                    }

                    .bold{
                        font-weight: bold;
                    }

                    .italic{
                        font-style: italic;
                    }

                    .customTable{
                        border-collapse: collapse;
                        border-spacing: 0;
                        width: 100%;
                        font-family: 'Arial', sans-serif;
                        font-size: 8px;
                        margin-bottom: 0;
                    }

                    .customTableNoHover{
                        border-collapse: collapse;
                        border-spacing: 0;
                        width: 100%;
                        font-family: 'Arial', sans-serif;
                        font-size: 8px;
                        margin-bottom: 0;
                    }

                    .customTableBlanks{
                        border-collapse: collapse;
                        border-spacing: 0;
                        width: 100%;
                        font-family: 'Arial', sans-serif;
                        font-size: 8px;
                        margin-bottom: 0;
                    }

                    .customTableHeading{
                        padding: 8px;
                        color: #212121;
                        border-bottom: 2px solid #9e9e9e;
                        font-weight: bold;
                    }

                    .customTableSubHeading{
                        background: #F5F5F5;
                        color: #212121;
                        padding: 8px;
                        border: 1px solid #9e9e9e;
                        border-bottom: 2px solid #9e9e9e;
                        font-weight: bold;
                        text-shadow: 0 1px 1px #FFFFFF;
                    }
                    .customTableFormSubHeading{
                        
                        color: #212121;
                        padding: 8px;
                        border: none;
                        font-weight: bold;
                        text-shadow: 0 1px 1px #FFFFFF;
                    }
                    .customTableForm > tr > th, td{
                        padding: 8px;
                        border: none;
                        /*border: 1px solid #9e9e9e;*/
                    }
                    .customTableSubHeading2{
                        background: #FAFAFA;
                        color: #212121;
                        padding: 8px;
                        border: 1px solid #DDDDDD;
                        font-weight: bold;
                        text-shadow: 0 1px 1px #FFFFFF;
                    }

                    .customTableSubHeadingDark{
                        background: linear-gradient(#9E9E9E, #616161);
                        color: #FFFFFF;
                        padding: 7px 5px;
                        border: 1px solid #616161;
                        font-weight: bold;
                        text-shadow: 0 1px 1px #424242;
                    }

                    .customTableHeadingDark
                    {
                        background: linear-gradient(#424242, #212121);
                        color: #FFFFFF;
                        padding: 7px 5px;
                        border: 1px solid #212121;
                        font-weight: bold;
                        text-shadow: 0 1px 1px #000000;
                    }

                    .label{
                        background: #EFEFEF;
                        padding: 5px;
                        border-radius: 3px;
                        font-weight: bold;
                        display: inline;
                        font-size: 11px;
                    }

                    .label-info{
                        background: #03a9f4;
                        color: #FFFFFF;
                        text-shadow: 0 1px 1px #01579b;
                    }

                    .label-success{
                        background: #8bc34a;
                        color: #FFFFFF;
                        text-shadow: 0 1px 1px #5f8e40;
                    }

                    .label-error{
                        background: #f44336;
                        color: #FFFFFF;
                        text-shadow: 0 1px 1px #ae322c;
                    }

                    .bgSuccess{
                        background-color: #4caf50;
                        color: #FFFFFF;
                    }

                    .green{
                        color: #4caf50;
                    }

                    .blue{
                        color: #2196f3;
                    }

                    .bgWarning{
                        background-color: #ff9800;
                    }

                    .bgDanger{
                        background-color: #f44336;
                        color: #FFFFFF;
                    }

                    .customTableData{
                        padding: 8px;
                        border: 1px solid #9e9e9e;
                    }

                    .customTableDataBlanks{
                        padding: 8px;
                        border: 0;
                        /* border: 1px solid #000000; */
                    }

                    .container{
                        background: #FFFFFF;
                    }

                    
                    table > tr > th, td{
                        padding: 10px;
                        border: 1px solid #9e9e9e;
                    }

                    .vehicle-headings{
                        border: 1px solid #E0E0E0;
                        background: #EEEEEE;
                    }

                    .house-img{
                        width:400px;
                        height:400px;
                        position:relative;
                        display:inline-block;
                        overflow:hidden;margin:0 auto;
                        border: 1px solid #E0E0E0;
                    }

                    .vehicle-img{
                        position:absolute;
                        top:50%;
                        min-height:100%;
                        display:block;
                        left:50%;
                        -webkit-transform: translate(-50%, -50%);
                        min-width:100%;
                    }
            
                    th{
                        padding: 10px;
                        border: 1px solid #9e9e9e;
                        background: #EEEEEE;
                    }

                    .headerTableData{
                        border: 0;
                        padding: 0;
                    }

                    .input{
                        border-bottom: 1px solid #9e9e9e;
                    }

                    .fit{
                        width:1%;
                        white-space:nowrap;
                    }

                    .hide{
                        display: none;
                    }

                    .highlightLinked{
                        background: #ffecb3;
                    }

                    .highlightLinked td.customTableData{
                        border: 1px solid #ffd54f;
                    }

                    .highlightNonOriginal{
                        background: #424242;
                    }

                    .highlightNonOriginal td.customTableData{
                        border: 1px solid #212121;
                        color: #f5f5f5;
                    }
            
                    .exception-true { background: #ff7f7f; }

                    .additional-col {background: #EEEEEE;font-style: italic;}
                </style>
            </head>



            <body>
                <!--mpdf
                <htmlpagefooter name="myfooter">
                <div style="border-top: 1px solid #000000; font-size: 0.9em; padding-top: 3mm; ">
                <div>Page {PAGENO} of {nbpg}{$this->orderId}<div>
                </div>
                </htmlpagefooter>
                <sethtmlpagefooter name="myfooter" value="on" />
                mpdf-->
                <div class="container">
                    <table class="customTable" style="margin-bottom: 15px;">
                        <tr>
                            <td class="headerTableData">
                                <img style="height: 50px;" src="{$logoUrl}" alt="" />
                            </td>

HTML;
        if (!$this->removeTitle)
        {
            $html .= <<<HTML
                            <td class="headerTableData right" style="vertical-align: middle;"><h3>{$this->heading}</h3></td>
HTML;
        }

        $body = $content;
        if (is_array($content))
        {
            $body = '';
            foreach ($content as $bodyContent)
            {
                $body .= $bodyContent;
            }
        }

        $html .= <<<HTML
                        </tr>
                    </table>
                    {$body}
                </div>
            </body>
        </html>
HTML;
        return $html;
    }

    public function createGoodsReceivedOrder($orderId, $html = false, $inline = false)
    {
        $bakeryId = $this->data['ordered'][0]['bakery_id'];
        $includeOrExclude = 'Excl';
        if (Yii::app()->params['vat'] == 0)
        {
            $includeOrExclude = 'Excl';
        }
        $bankingDetails = '';
        if ($this->data['ordered'][0]['banking_details'] != '')
        {
            $bankingDetails = '<br><br>' . nl2br($this->data['ordered'][0]['banking_details']);
        }
        $content = <<<HTML
            <table class="customTableBlanks" style="margin-bottom: 15px;">
                <tr>
                    <td class="customTableDataBlanks bold fit">Payment Type:</td>
                    <td class="customTableDataBlanks">{$this->data['ordered'][0]['payment_type']}</td>
                    <td class="customTableDataBlanks bold fit">Order No:</td>
                    <td class="customTableDataBlanks bold" style="font-size: 16px;">{$this->data['ordered'][0]['purchase_order_number']}</td>
                </tr>
                <tr>
                    <td class="customTableDataBlanks bold fit">GR Purchase Order #:</td>
                    <td class="customTableDataBlanks">{$this->data['ordered'][0]['po']}</td>
                    <td class="customTableDataBlanks bold fit">Delivery Date:</td>
                    <td class="customTableDataBlanks bold">{$this->data['ordered'][0]['expected_delivery_date']}</td>
                </tr>
                <tr>
                    <td class="customTableDataBlanks bold fit">Ordered By:</td>
                    <td class="customTableDataBlanks">{$this->data['ordered'][0]['ordered_by']}</td>
                    <td class="customTableDataBlanks bold fit">Order Date:</td>
                    <td class="customTableDataBlanks bold">{$this->data['ordered'][0]['order_date']}</td>
                </tr>
            </table>

            <table class="customTable" style="margin-bottom: 15px;">
                <tr>
                    <td class="customTableSubHeading">FROM</td>
                </tr>
                <tr>
                    <td class="customTableData" style="vertical-align: top;">
                        {$this->data['ordered'][0]['customer_name_from']}<br/>
                        {$this->data['ordered'][0]['customer_address_from']}<br/>
                        {$this->data['ordered'][0]['customer_postal_address_from']}<br/>
                        {$this->data['ordered'][0]['customer_tel_from']}<br/>
                        {$this->data['ordered'][0]['customer_vat_number_from']}
                        {$bankingDetails}
                    </td>
                </tr>
HTML;
        if ($this->data['ordered'][0]['comments'] !== '')
        {
            $content .= <<<HTML
            <tr>
                <td class="customTableSubHeading">Comments</td>
            </tr>
            <tr>
                <td class="customTableData">{$this->data['ordered'][0]['comments']}</td>
            </tr>
HTML;
        }

        $content .= <<<HTML
            </table>

            <table class="customTable">
                <tr>
                    <td class="customTableSubHeading center">A: Ordered</td>
                    <td class="customTableSubHeading">Qty. Ordered</td>
                    <td class="customTableSubHeading">Qty. Received</td>
                    <td class="customTableSubHeading">Batch Code</td>
                    <td class="customTableSubHeading">Exp. Date</td>
                    <td class="customTableSubHeading">Comment</td>
                </tr>
HTML;
        $orderedTotal = 0;
        $slipOrders = '';


        for ($i = 0; $i < count($this->data['ordered']); $i++)
        {
            $currentdisplayi = $i + 1;
            $itemTotal = floatval($this->data['ordered'][$i]['ordered_item_total']);
            $orderedTotal += $itemTotal;
            $formatted = FieldFormatter::format(FIELD_FORMATTER_DECIMAL_TWO, $itemTotal);
            $orderedItemVatValue = $this->data['ordered'][$i]['ordered_item_vat_value'];
            $orderedItemVatValueFormatted = FieldFormatter::format(FIELD_FORMATTER_DECIMAL_TWO, $orderedItemVatValue);
            $orderedItemTotalIncl = $this->data['ordered'][$i]['ordered_item_total_incl'];
            $orderedItemTotalInclFormatted = FieldFormatter::format(FIELD_FORMATTER_DECIMAL_TWO, $orderedItemTotalIncl);
            $at = <<<HTML
                {$this->data['ordered'][$i]['original_price']}
HTML;
            if ($this->data['ordered'][$i]['discount'] > 0)
            {
                $at = <<<HTML
                <span style="text-decoration: line-through; color: #F44336;">{$this->data['ordered'][$i]['original_price']}</span> {$this->data['ordered'][$i]['discounted_price']}
HTML;
            }
            $content .= <<<HTML
                <tr>
                    <td class="customTableData"><span class="bold">{$this->data['ordered'][$i]['uom']}</span> - [{$this->data['ordered'][$i]['sku']}] {$this->data['ordered'][$i]['product_description']}</td>
                    <td class="customTableData right">{$this->data['ordered'][$i]['qty']}</td>
                    <td class="customTableData right"></td>
                    <td class="customTableData">{$this->data['ordered'][$i]['batch_code']}</td>
                    <td class="customTableData">{$this->data['ordered'][$i]['expiry_date']}</td>
                    <td class="customTableData"></td>
                </tr>
HTML;
            $slipOrders .= <<<HTML
                <tr><td class="customTableData">{$currentdisplayi}</td><td>{$this->data['ordered'][$i]['ordered_product']}</td><td>_________</td><td>_________________</td><td>_________________</td></tr>
HTML;
        }
        $orderedTotalFormatted = FieldFormatter::format(FIELD_FORMATTER_DECIMAL_TWO, $orderedTotal);
        //returns
        $returns = <<<HTML
            <tr>
                <td class="customTableSubHeading center">B: Returned</td>
                <td class="customTableSubHeading">Qty. Delivered</td>
                <td class="customTableSubHeading">Batch Code</td>
                <td class="customTableSubHeading">Exp. Date</td>
                <td class="customTableSubHeading">Comment</td>
            </tr>
HTML;
        $slipReturns = '';
        $returnedTotal = 0;
        $returnedTotalFormatted = '0.00';
        if (!empty($this->data['returns']))
        {
            for ($i = 0; $i < count($this->data['returns']); $i++)
            {
                $orderedItemTotalIncl = $this->data['returns'][$i]['returned_item_total_incl'];
                $orderedItemTotalInclFormatted = FieldFormatter::format(FIELD_FORMATTER_DECIMAL_TWO, $orderedItemTotalIncl);
                $itemTotal = floatval($this->data['returns'][$i]['returned_item_total']);
                $returnedTotal += $itemTotal;
                $formatted = FieldFormatter::format(FIELD_FORMATTER_DECIMAL_TWO, $itemTotal);
                $orderedItemVatValue = $this->data['returns'][$i]['returned_item_vat_value'];
                $orderedItemVatValueFormatted = FieldFormatter::format(FIELD_FORMATTER_DECIMAL_TWO, $orderedItemVatValue);
                $slipReturns .= <<<HTML
                       <tr><td>{$this->data['returns'][$i]['returned_product']}</td><td>______________</td><td>_____________</td></tr>
HTML;
                $returns .= <<<HTML
                <tr>
                    <td class="customTableData">{$this->data['returns'][$i]['product_description']}</td>
                    <td class="customTableData right"></td>
                    <td class="customTableData">{$this->data['returns'][$i]['batch_code']}</td>
                    <td class="customTableData">{$this->data['returns'][$i]['expiry_date']}</td>
                    <td class="customTableData"></td>
                </tr>
HTML;
            }
            $returnedTotalFormatted = FieldFormatter::format(FIELD_FORMATTER_DECIMAL_TWO, $returnedTotal);
        }
        else
        {
            $returns .= <<<HTML
            <tr>
                <td>-</td>
                <td style="text-align: right;"></td>
            </tr>
HTML;
        }
        $aMinusB = $this->data['ordered'][0]['gross_order_total_amount'];
        $aMinusBFormatted = FieldFormatter::format(FIELD_FORMATTER_DECIMAL_TWO, $aMinusB);
        $invoiceDiscount = 0; //$this->data['ordered'][0]['invoice_discount'];
        $discountType = 0; //$this->data['ordered'][0]['invoice_discount_type'];
        $discountTypeDisplay = '';
        if (isset($this->data['ordered'][0]['invoice_discount_value']) && $this->data['ordered'][0]['invoice_discount_value'] > 0)
        {
            if ($discountType == '%')
            {
                $discountTypeDisplay = ' (-' . $this->data['ordered'][0]['invoice_discount_value'] . '%)';
            }
            else
            {
                $discountTypeDisplay = ' (-' . $this->data['ordered'][0]['invoice_discount_value'] . ')';
            }
        }
        $discountedTotal = $aMinusB - $invoiceDiscount;
        $discountedTotalFormatted = FieldFormatter::format(FIELD_FORMATTER_DECIMAL_TWO, $discountedTotal);
        $invoiceDiscountDisplay = FieldFormatter::format(FIELD_FORMATTER_DECIMAL_TWO, $invoiceDiscount);
        $vat = $this->data['ordered'][0]['vat_value'];
        $vatFormatted = FieldFormatter::format(FIELD_FORMATTER_DECIMAL_TWO, $vat);
        $total = $this->data['ordered'][0]['order_total_amount'];
        $totalFormatted = FieldFormatter::format(FIELD_FORMATTER_DECIMAL_TWO, $total);
        $vatPercentage = $this->data['ordered'][0]['vat_value'] . '%';
        $cashCollected = $this->data['ordered'][0]['cash_collected'];
        $cashCollectedFormatted = FieldFormatter::format(FIELD_FORMATTER_DECIMAL_TWO, $cashCollected);
        $finalTotal = $totalFormatted;
        $showVat = <<<HTML
        <tr>
            <td style="text-align: right;">Vat</td>
            <td style="text-align: right;">{$vatFormatted}</td>
        </tr>
HTML;

        $content .= <<<HTML
        </table>
HTML;

        if (!$html)
        {
            Yii::import('application.vendors.mpdf.*');
            $this->pdfFolder .= 'purchaseOrders/';
            if (!file_exists($this->pdfFolder)) mkdir($this->pdfFolder, 0777, true);
            $mpdf = new mpdf('win-1252', 'A4', 0, '', PDF_MGL, PDF_MGR, PDF_MGT, PDF_MGB, PDF_MGH, PDF_MGF);
            //            $mpdf->SetProtection(array('print'));
            $mpdf->SetWatermarkText('Plant Order Purchase Order');
            $mpdf->showWatermarkText = true;
            $mpdf->watermark_font = 'DejaVuSansCondensed';
            $mpdf->watermarkTextAlpha = 0.1;
            $mpdf->WriteHTML($this->show($content, $bakeryId));
            $filename = $this->pdfFolder . 'Plant Order Purchase Order ' . $orderId . '.pdf';
            if ($inline)
            {
                $mpdf->Output('', 'I');
            }
            else
            {
                file_put_contents($filename, $mpdf->Output('', 'S'));
                chmod($filename, 0777);
            }
            return $filename;
        }
        else
        {
            return $this->show($content, $bakeryId);
        }
    }

    public function createCashCollectPDF($cashCollectId, $html = false)
    {
        $bakeryId = $this->data['outlet']['bakery_id'];
        $content = <<<HTML
        <table class="customTableBlanks" style="margin-bottom: 15px;">
            <tr>
                <td class="customTableDataBlanks bold fit">Collected By:</td>
                <td class="customTableDataBlanks">{$this->data['employee']['name']} ({$this->data['employee']['employee_number']})</td>
                <td class="customTableDataBlanks bold fit">Collected Date:</td>
                <td class="customTableDataBlanks">{$this->data['cash_collected_date']}</td>
            </tr>
HTML;
        if ($this->data['status'] === '1')
        {
            $content .= <<<HTML
            <tr>
                <td class="customTableDataBlanks bold fit">Approved By</td>
                <td class="customTableDataBlanks">{$this->data['employeeApproved']['name']} ({$this->data['employeeApproved']['employee_number']})</td>
                <td class="customTableDataBlanks bold fit">Approved Date</td>
                <td class="customTableDataBlanks">{$this->data['approved_date']}</td>
            </tr>
HTML;
        }
        $amountCollectedDisplay = number_format($this->data['amount_collected'], 2, '.', ' ');
        $currencyCollected = is_null($this->data['currenciesCashCollected']['currencies']['description']) ? '-' : $this->data['currenciesCashCollected']['currencies']['description'];
        $currencyCollectedAmountDisplay = is_null($this->data['currenciesCashCollected']['selected_value']) ? '-' : number_format($this->data['currenciesCashCollected']['selected_value'], 2, '.', ' ');
        $baseUrl = Yii::app()->getBaseUrl(true);
        if ($this->data['signature_url'] != '')
        {
            $signatureUrl = $baseUrl . '/images/api/CashCollectSignatures/' . $this->data['signature_url'] . '.jpg';
            $signature = <<<HTML
            <img alt="signature not found" style="height: 50px;" src="{$signatureUrl}" />
HTML;
        }
        else
        {
            $signature = '';
        }
        $content .= <<<HTML
        </table>
        <table class="customTable">
            <tr>
                <td class="customTableSubHeading">Customer</td>
                <td class="customTableSubHeading fit">Account Number</td>
                <td class="customTableSubHeading fit">Name</td>
                <td class="customTableSubHeading fit">Reference</td>
                <td class="customTableSubHeading fit">Amount Collected</td>
                <td class="customTableSubHeading fit">Currency Collected</td>
                <td class="customTableSubHeading fit">Currency Collected Amount</td>
                <td class="customTableSubHeading fit">Inivoice#</td>
            </tr>
            <tr>
                <td class="customData">{$this->data['outlet']['description']}</td>
                <td class="customData fit">{$this->data['outlet']['account_number']}</td>
                <td class="customData fit">{$this->data['customer_name']}</td>
                <td class="customData fit">{$this->data['reference_nr']}</td>
                <td class="customData fit">{$amountCollectedDisplay}</td>
                <td class="customData fit">{$currencyCollected}</td>
                <td class="customData fit">{$currencyCollectedAmountDisplay}</td>
                <td class="customData fit">{$this->data['order_id']}</td>
            </tr>
            <tr>
                <td class="customTableData right bold">Signature:</td>
                <td class="customTableData" colspan="7">
                    {$signature}
                </td>
            </tr>
        </table>
HTML;
        if (!$html)
        {
            Yii::import('application.vendors.mpdf.*');
            $mpdf = new mpdf('win-1252', 'A4', 0, '', PDF_MGL, PDF_MGR, PDF_MGT, PDF_MGB, PDF_MGH, PDF_MGF);
            //            $mpdf->SetProtection(array('print'));
            $mpdf->SetWatermarkText('Cash Collect');
            $mpdf->showWatermarkText = true;
            $mpdf->watermark_font = 'DejaVuSansCondensed';
            $mpdf->watermarkTextAlpha = 0.1;
            $mpdf->WriteHTML($this->show($content, $bakeryId));
            $filename = $this->pdfFolder . 'Cash Collect' . $cashCollectId . '.pdf';
            $mpdf->Output('', 'I');
            return $filename;
        }
        else
        {
            return $this->show($content, $bakeryId);
        }
    }

    protected function getPDFTotals($qtyOrdersIndex = 'qty_ordered', $qtyReturnsIndex = 'qty')
    {
        $totals = [
            'brand' => [],
            'category' => [],
            'product' => [],
            'uom' => []
        ];
        $totalsReturns = [
            'brand' => [],
            'category' => [],
            'product' => [],
            'uom' => []
        ];
        foreach ($this->data['ordered'] as $s)
        {
            $qty = isset($s[$qtyOrdersIndex]) ? $s[$qtyOrdersIndex] : 0;
            if(isset($s['supplier']))
            {
                if (!isset($totals['brand'][$s['supplier']]))
                {
                    $totals['brand'][$s['supplier']] = 0;
                    $totals['brand'][$s['supplier']] += $qty;
                }
                else
                {
                    $totals['brand'][$s['supplier']] += $qty;
                }
            }

            if(isset($s['type']))
            {
                if (!isset($totals['category'][$s['type']]))
                {
                    $totals['category'][$s['type']] = 0;
                    $totals['category'][$s['type']] += $qty;
                }
                else
                {
                    $totals['category'][$s['type']] += $qty;
                }
            }

            if(isset($s['product']))
            {
                if (!isset($totals['product'][$s['product']]))
                {
                    $totals['product'][$s['product']] = 0;
                    $totals['product'][$s['product']] += $qty;
                }
                else
                {
                    $totals['product'][$s['product']] += $qty;
                }
            }

            if(isset($s['unit_of_measure']))
            {
                if (!isset($totals['uom'][$s['unit_of_measure']]))
                {
                    $totals['uom'][$s['unit_of_measure']] = 0;
                    $totals['uom'][$s['unit_of_measure']] += $qty;
                }
                else
                {
                    $totals['uom'][$s['unit_of_measure']] += $qty;
                }
            }
        }

        foreach ($this->data['returns'] as $s)
        {
            $qty = isset($s[$qtyReturnsIndex]) ?  $s[$qtyReturnsIndex] : 0;
            if (!isset($totalsReturns['brand'][$s['supplier']]))
            {
                $totalsReturns['brand'][$s['supplier']] = 0;
                $totalsReturns['brand'][$s['supplier']] += $qty;
            }
            else
            {
                $totalsReturns['brand'][$s['supplier']] += $qty;
            }

            if (!isset($totalsReturns['category'][$s['type']]))
            {
                $totalsReturns['category'][$s['type']] = 0;
                $totalsReturns['category'][$s['type']] += $qty;
            }
            else
            {
                $totalsReturns['category'][$s['type']] += $qty;
            }

            if (!isset($totalsReturns['product'][$s['product']]))
            {
                $totalsReturns['product'][$s['product']] = 0;
                $totalsReturns['product'][$s['product']] += $qty;
            }
            else
            {
                $totalsReturns['product'][$s['product']] += $qty;
            }

            if (!isset($totalsReturns['uom'][$s['unit_of_measure']]))
            {
                $totalsReturns['uom'][$s['unit_of_measure']] = 0;
                $totalsReturns['uom'][$s['unit_of_measure']] += $qty;
            }
            else
            {
                $totalsReturns['uom'][$s['unit_of_measure']] += $qty;
            }
        }

        return [$totals, $totalsReturns];
    }

    public function createPurchaseOrder($orderId, $html = false, $inline = false)
    {
        list($totalsOrdered, $totalsReturns) = $this->getPDFTotals();

        $db = Yii::app()->db;
        $bakeryId = $this->data['ordered'][0]['bakery_id'];
        $sql = <<<SQL
    select *
    from document_template dt
    left join document_template_options dto on dt.document_template_id=dto.document_template_id
    where bakery_id = {$bakeryId} and document_type_id=1
SQL;
        $purchaseOrderTemplate = $db->createCommand($sql)->queryAll();

        $title = 'Purchase Order';
        $payment_type_text = 'Payment Type';
        $order_num_text = 'Order No';
        $purchase_order_text = 'Purchase Order';
        $delivery_date_text = 'Delivery Date';
        $order_by_text = 'Ordered By';
        $order_date_text = 'Order Date';
        $to_text = 'TO';
        $from_text = 'FROM';
        $driver_signature = 'Driver Signature';
        $customer_signature = 'Customer Signature';
        $customer_name = 'Customer Name';
        $customer_contact_number = 'Customer Contact Number';
        $ordered_items_text = 'A: Ordered';
        $excl_vat_text = 'Excl Vat ';
        $vat_text = 'Vat';
        $incl_vat_text = 'Incl Vat';
        $sub_total_text = 'Sub-Total A';
        $return_items_text = 'B: Returned';
        $b_excl_vat_text = 'Excl Vat';
        $b_vat_text = 'Vat';
        $b_incl_vat_text = 'Incl Vat';
        $order_total_text = 'Order Total';
        $b_sub_total_text = 'Sub-Total B';
        $minus_sub_total_text = 'Sub-Total (A - B)';
        $discount_text = ' Discount';
        $discounted_total_text = 'Discounted Total';
        $final_vat_text = 'Vat';
        $total_text = 'Total';
        $cash_collected_text = 'Cash Collected';
        $info_vat_text = 'Vat';
        $payment_type_status = 1;
        $order_num_status = 1;
        $purchase_order_status = 1;
        $delivery_date_status = 1;
        $order_by_status = 1;
        $order_date_status = 1;
        $document_footer = "";
        $ref_number_text = "REF #";
        $ref_number_by_status = 1;

        // var_dump($purchaseOrderTemplate);
        $options = [];
        foreach ($purchaseOrderTemplate as $t)
        {
            $options[$t['label']] = $t['translation'];
        }
        $orderItemQtyTxt = (isset($options['ordered_items_qty'])) ? $options['ordered_items_qty'] : 'Qty';
        $orderItemSkuTxt = (isset($options['ordered_items_sku'])) ? $options['ordered_items_sku'] : 'Sku';
        $orderItemUnitPriceTxt = (isset($options['ordered_items_unit_price'])) ? $options['ordered_items_unit_price'] : 'Unit Price';
        $orderItemDiscountTxt = (isset($options['ordered_items_discount'])) ? $options['ordered_items_discount'] : 'Discount';
        $orderItemNetPriceTxt = (isset($options['ordered_items_net_price'])) ? $options['ordered_items_net_price'] : 'Net Price';
        $returnItemQtyTxt = (isset($options['return_items_qty'])) ? $options['return_items_qty'] : 'Qty';
        $returnItemSkuTxt = (isset($options['return_items_sku'])) ? $options['return_items_sku'] : 'Sku';
        $returnItemUnitPriceTxt = (isset($options['return_items_unit_price'])) ? $options['return_items_unit_price'] : 'Unit Price';

        if (count($purchaseOrderTemplate) > 0)
        {
            foreach ($purchaseOrderTemplate as $options)
            {
                switch ($options['label'])
                {
                    case 'title':
                        $title = $options['translation'];
                        break;

                    case 'payment_type_text':
                        $payment_type_text = $options['translation'];
                        $payment_type_status = $options['status'];
                        break;

                    case 'order_num_text':
                        $order_num_text = $options['translation'];
                        $order_num_status = $options['status'];
                        break;


                    case 'purchase_order_text':
                        $purchase_order_text = $options['translation'];
                        $purchase_order_status = $options['status'];
                        break;

                    case 'delivery_date_text':
                        $delivery_date_text = $options['translation'];
                        $delivery_date_status = $options['status'];

                        break;

                    case 'order_by_text':
                        $order_by_text = $options['translation'];
                        $order_by_status = $options['status'];

                        break;

                    case 'order_date_text':
                        $order_date_text = $options['translation'];
                        $order_date_status = $options['status'];

                        break;


                    case 'from_text':
                        $from_text = $options['translation'];
                        break;

                    case 'to_text':
                        $to_text = $options['translation'];
                        break;

                    case 'ordered_items_text':
                        $ordered_items_text = $options['translation'];
                        break;

                    case 'excl_vat_text':
                        $excl_vat_text = $options['translation'];
                        break;

                    case 'vat_text':
                        $vat_text = $options['translation'];
                        break;

                    case 'incl_vat_text':
                        $incl_vat_text = $options['translation'];
                        break;
                    case 'sub_total_text':
                        $sub_total_text = $options['translation'];
                        $subTotalStatus = $options['status'];
                        break;
                    case 'return_items_text':
                        $return_items_text = $options['translation'];
                        break;

                    case 'b_excl_vat_text':
                        $b_excl_vat_text = $options['translation'];
                        break;

                    case 'b_vat_text':
                        $b_vat_text = $options['translation'];
                        break;

                    case 'b_incl_vat_text':
                        $b_incl_vat_text = $options['translation'];
                        break;
                    case 'info_vat_text':
                        $info_vat_text = $options['translation'];
                        break;
                    case 'order_total_text':
                        $order_total_text = $options['translation'];
                        break;

                    case 'b_sub_total_text':
                        $b_sub_total_text = $options['translation'];
                        $bSubTotalStatus = $options['status'];
                        break;

                    case 'minus_sub_total_text':
                        $minus_sub_total_text = $options['translation'];
                        $minusSubTotalStatus = $options['status'];
                        break;

                    case 'discount_text':
                        $discount_text = $options['translation'];
                        $discountStatus = $options['status'];
                        break;

                    case 'discounted_total_text':
                        $discounted_total_text = $options['translation'];
                        $discountTotalStatus = $options['status'];
                        break;

                    case 'final_vat_text':
                        $final_vat_text = $options['translation'];
                        $finalVatStatus = $options['status'];
                        break;

                    case 'total_text':
                        $total_text = $options['translation'];
                        $totalStatus = $options['status'];
                        break;

                    case 'cash_collected_text':
                        $cash_collected_text = $options['translation'];
                        $cashCollectedStatus = $options['status'];
                        break;
                    case 'document_footer_text':
                        $document_footer = $options['translation'];
                        $documentFooterStatus = $options['status'];
                        break;
                    case 'driver_signature':
                        $driver_signature = $options['translation'];
                        break;
                    case 'customer_signature':
                        $customer_signature = $options['translation'];
                        break;
                    case 'customer_name':
                        $customer_name = $options['translation'];
                        break;
                    case 'customer_contact_number':
                        $customer_contact_number = $options['translation'];
                        break;
                    case 'ref_number':
                        $ref_number_text = $options['translation'];
                        break;
                }
            }
        }

        if (count($this->data['ordered']) != 0)
        {
            $includeOrExclude = 'Excl';
            if (Yii::app()->params['vat'] == 0)
            {
                $includeOrExclude = 'Excl';
            }
            $order_number = '';
            $ref_number = '';
            if ($this->data['ordered'][0]['use_document_sequencing'] == 1)
            {
                $order_number .= $this->data['ordered'][0]['po_document_prefix'] . $this->data['ordered'][0]['po_document_number'];
                $ref_number .= $this->data['ordered'][0]['purchase_order_number'];
                if ($order_number == '')
                {
                    $ref_number_by_status = 0;
                    $order_number .= $this->data['ordered'][0]['purchase_order_number'];
                }
            }
            else
            {
                $order_number .= $this->data['ordered'][0]['purchase_order_number'];
            }
            $bankingDetails = '';
            if ($this->data['ordered'][0]['banking_details'] != '')
            {
                $bankingDetails = '<br><br>' . nl2br($this->data['ordered'][0]['banking_details']);
            }
            $payment_html = '';
            if ($payment_type_status == 1)
            {
                $payment_html = <<<HTML
            <td class="customTableDataBlanks bold fit">{$payment_type_text}:</td>
            <td class="customTableDataBlanks">{$this->data['ordered'][0]['payment_type']}</td>
HTML;
            }
            $orderNo_html = '';
            if ($order_num_status == 1)
            {
                $orderNo_html = <<<HTML
            <td class="customTableDataBlanks bold fit">{$order_num_text}:</td>

            <td class="customTableDataBlanks bold" style="font-size: 16px;">{$order_number}</td>
HTML;
            }

            $purchaseOrder_html = '';
            if ($purchase_order_status == 1)
            {
                $purchaseOrder_html = <<<HTML
            <td class="customTableDataBlanks bold fit">{$purchase_order_text}:</td>
            <td class="customTableDataBlanks">{$this->data['ordered'][0]['po']}</td>      
HTML;
            }

            $deliveryDate_html = '';
            if ($delivery_date_status == 1)
            {
                $deliveryDate_html = <<<HTML
            <td class="customTableDataBlanks bold fit">{$delivery_date_text}:</td>
            <td class="customTableDataBlanks bold">{$this->data['ordered'][0]['expected_delivery_date']}</td>
HTML;
            }
            $orderBy_html = '';
            if ($order_by_status == 1)
            {
                $orderBy_html = <<<HTML
            <td class="customTableDataBlanks bold fit">{$order_by_text}:</td>
            <td class="customTableDataBlanks">{$this->data['ordered'][0]['ordered_by']}</td>
HTML;
            }
            $orderDate_Html = '';
            if ($order_date_status == 1)
            {
                $orderDate_Html = <<<HTML
            <td class="customTableDataBlanks bold fit">{$order_date_text}:</td>
            <td class="customTableDataBlanks bold">{$this->data['ordered'][0]['order_date']}</td>
HTML;
            }
            $refNumber_Html = '';
            if ($this->data['ordered'][0]['use_document_sequencing'] == 1 && $ref_number_by_status == 1)
            {
                $refNumber_Html = <<<HTML
                <td class="customTableDataBlanks bold fit">{$ref_number_text}:</td>
                    <td class="customTableDataBlanks">{$ref_number}</td>
HTML;
            }
            $vat_replace_from_string = str_replace("Vat #", $info_vat_text, $this->data['ordered'][0]['customer_vat_number_from']);
            $vat_replace_to_string = str_replace("Vat #", $info_vat_text, $this->data['ordered'][0]['customer_vat_number_to']);

            $row1 = CustomUtils::pdfRowDetails($payment_html, $orderNo_html);
            $row2 = CustomUtils::pdfRowDetails($purchaseOrder_html, $deliveryDate_html);
            $row3 = CustomUtils::pdfRowDetails($orderBy_html, $orderDate_Html);
            $row4 = CustomUtils::pdfRowDetails($refNumber_Html, '');
            $branchAddress = "";
            if ($this->data['ordered'][0]['branch_company_id'] > 0)
            {
                $vat_replace_from_string = str_replace("Vat #", $info_vat_text, "Vat #: " . $this->data['ordered'][0]['company_vat_number']);
                $branchAddress = <<<HTML
                        {$this->data['ordered'][0]['company_description']}<br/>
                        {$this->data['ordered'][0]['company_physical_address']}<br/>
                        {$this->data['ordered'][0]['company_postal_address']}<br/>
                        {$this->data['ordered'][0]['company_phone']}<br/>
                        {$vat_replace_from_string}<br/>
                        {$this->data['ordered'][0]['company_banking_details']}
HTML;
            }
            else
            {
                $branchAddress = <<<HTML
                        {$this->data['ordered'][0]['customer_name_from']}<br/>
                        {$this->data['ordered'][0]['customer_address_from']}<br/>
                        {$this->data['ordered'][0]['customer_postal_address_from']}<br/>
                        {$this->data['ordered'][0]['customer_tel_from']}<br/>
                        {$vat_replace_from_string}<br/>
                        {$bankingDetails}
HTML;
            }
            $content = <<<HTML
            <table class="customTableBlanks" style="margin-bottom: 15px;">
                {$row1}
                {$row2}
                {$row3}
                {$row4}
            </table>
            
            <table class="customTable" style="margin-bottom: 15px;">
                <tr>
                    <td class="customTableSubHeading" style="width: 50%;">{$from_text}</td>
                    <td class="customTableSubHeading" style="width: 50%;">{$to_text}</td>
                </tr>
                <tr>
                    <td class="customTableData" style="vertical-align: top;">
                        {$branchAddress}

                       
                    </td>
                    <td class="customTableData" style="vertical-align: top;">
                        {$this->data['ordered'][0]['customer_account_number_to']}<br/>
                        {$this->data['ordered'][0]['customer_name_to']}<br/>
                        {$this->data['ordered'][0]['customer_address_to']}<br/>
                        {$this->data['ordered'][0]['customer_tel_to']}<br/>
                        {$this->data['ordered'][0]['customer_cell_to']}<br/>
                        {$vat_replace_to_string}
                    </td>
                </tr>
HTML;
            if ($this->data['ordered'][0]['comments'] !== '')
            {
                $content .= <<<HTML
                <tr>
                    <td class="customTableSubHeading" colspan="2">Comments</td>
                </tr>
                <tr>
                    <td class="customTableData" colspan="2">{$this->data['ordered'][0]['comments']}</td>
                </tr>
HTML;
            }

            $content .= <<<HTML
            </table>
            
            <table class="customTable">
                <tr>
                    <td class="customTableSubHeading center fit">{$orderItemQtyTxt}</td>
                    <td class="customTableSubHeading center fit">{$orderItemSkuTxt}</td>
                    <td class="customTableSubHeading center">{$ordered_items_text}</td>
                    <td class="customTableSubHeading center fit">{$orderItemUnitPriceTxt}</td>
                    <td class="customTableSubHeading center fit">{$orderItemDiscountTxt}</td>
                    <td class="customTableSubHeading center fit">{$orderItemNetPriceTxt}</td>
                    <td class="customTableSubHeading center fit">{$excl_vat_text} </td>
                    <td class="customTableSubHeading center fit">{$vat_text}</td>
                    <td class="customTableSubHeading center fit">{$incl_vat_text}</td>
                </tr>
HTML;
            $orderedTotal = 0;
            $currentIndex = 0;
            $nextIndex = $currentIndex + 1;
            $bakeryOrderBy = isset($this->data['ordered'][0]['bakery_distribution_order_by_id']) ? $this->data['ordered'][0]['bakery_distribution_order_by_id'] : 0;
            for ($i = 0; $i < count($this->data['ordered']); $i++)
            {
                $itemTotal = floatval($this->data['ordered'][$i]['ordered_item_total']);
                $orderedTotal += $itemTotal;
                $formatted = FieldFormatter::format(FIELD_FORMATTER_DECIMAL_TWO, $itemTotal);
                $orderedItemVatValue = $this->data['ordered'][$i]['ordered_item_vat_value'];
                $orderedItemVatValueFormatted = FieldFormatter::format(FIELD_FORMATTER_DECIMAL_TWO, $orderedItemVatValue);
                $orderedItemTotalIncl = $this->data['ordered'][$i]['ordered_item_total_incl'];
                $orderedItemTotalInclFormatted = FieldFormatter::format(FIELD_FORMATTER_DECIMAL_TWO, $orderedItemTotalIncl);
                $at = <<<HTML
                {$this->data['ordered'][$i]['original_price']}
HTML;
                if ($this->data['ordered'][$i]['discount'] > 0)
                {
                    $d = number_format(round(($this->data['ordered'][$i]['discount'] / $this->data['ordered'][$i]['original_price']) * 100, 2), '2');
                    $discountPercentage = ' <span class="italic">(' . $d . '%)</span>';
                    $at = <<<HTML
                <span style="text-decoration: line-through; color: #F44336;">{$this->data['ordered'][$i]['original_price']}</span>{$discountPercentage} {$this->data['ordered'][$i]['discounted_price']}
HTML;
                }

                $batchCode = $this->data['ordered'][$i]['batch_code'];
                $batchCodeDisplay = '<br>' . $batchCode;
                if (is_null($batchCode))
                {
                    $batchCodeDisplay = '';
                }
                $expiryDate = $this->data['ordered'][$i]['expiry_date'];
                $expiryDateDisplay = '<br>' . $expiryDate;
                if (is_null($expiryDate) || $expiryDate === '0000-00-00')
                {
                    $expiryDateDisplay = '';
                }
                $content .= <<<HTML
                <tr>
                    <td class="customTableData fit">{$this->data['ordered'][$i]['qty_ordered']}</td>
                    <td class="customTableData fit">{$this->data['ordered'][$i]['product_sku']}</td>
                    <td class="customTableData">{$this->data['ordered'][$i]['ordered_product']} {$batchCodeDisplay}{$expiryDateDisplay}</td>
                    <td class="customTableData fit right">{$this->data['ordered'][$i]['original_price']}</td>
                    <td class="customTableData fit right">{$this->data['ordered'][$i]['discount']}</td>
                    <td class="customTableData fit right">{$this->data['ordered'][$i]['discounted_price']}</td>
                    <td class="customTableData right fit">{$formatted}</td>
                    <td class="customTableData right fit">{$orderedItemVatValueFormatted}</td>
                    <td class="customTableData right fit">{$orderedItemTotalInclFormatted}</td>
                </tr>
HTML;
                switch($bakeryOrderBy)
                {
                    case 2:
                        $content .= $this->getSubTotalsPDFs('ordered', $totalsOrdered, $currentIndex, $nextIndex, 'type', 'category');
                    break;

                    case 3:
                        $content .= $this->getSubTotalsPDFs('ordered', $totalsOrdered, $currentIndex, $nextIndex);
                    break;

                    case 4:
                        $content .= $this->getSubTotalsPDFs('ordered', $totalsOrdered, $currentIndex, $nextIndex, 'unit_of_measure', 'uom');
                    break;

                    case 5:
                        $content .= $this->getSubTotalsPDFs('ordered', $totalsOrdered, $currentIndex, $nextIndex, 'unit_of_measure', 'uom');
                    break;

                    case 6:
                        $content .= $this->getSubTotalsPDFs('ordered', $totalsOrdered, $currentIndex, $nextIndex, 'unit_of_measure', 'uom');
                    break;

                    default:
                        $content .= '';
                    break;
                }
                ++$currentIndex;
                ++$nextIndex;
            }
            $orderedTotalFormatted = FieldFormatter::format(FIELD_FORMATTER_DECIMAL_TWO, $orderedTotal);
            //returns
            $returns = <<<HTML
            <tr>
                <td class="customTableSubHeading center fit">{$returnItemQtyTxt}</td>
                <td class="customTableSubHeading center fit">{$returnItemSkuTxt}</td>
                <td class="customTableSubHeading center" colspan="3">{$return_items_text}</td>
                <td class="customTableSubHeading center fit">{$returnItemUnitPriceTxt}</td>
                <td class="customTableSubHeading center fit">{$b_excl_vat_text} </td>
                <td class="customTableSubHeading center fit">{$b_vat_text}</td>
                <td class="customTableSubHeading center fit">{$b_incl_vat_text}</td>
            </tr>
HTML;
            $returnedTotal = 0;
            $returnedTotalFormatted = '0.00';
            $currentIndex = 0;
            $nextIndex = $currentIndex + 1;
            if (!empty($this->data['returns']))
            {
                for ($i = 0; $i < count($this->data['returns']); $i++)
                {
                    $orderedItemTotalIncl = $this->data['returns'][$i]['returned_item_total_incl'];
                    $orderedItemTotalInclFormatted = FieldFormatter::format(FIELD_FORMATTER_DECIMAL_TWO, $orderedItemTotalIncl);
                    $itemTotal = floatval($this->data['returns'][$i]['returned_item_total']);
                    $returnedTotal += $itemTotal;
                    $formatted = FieldFormatter::format(FIELD_FORMATTER_DECIMAL_TWO, $itemTotal);
                    $orderedItemVatValue = $this->data['returns'][$i]['returned_item_vat_value'];
                    $orderedItemVatValueFormatted = FieldFormatter::format(FIELD_FORMATTER_DECIMAL_TWO, $orderedItemVatValue);

                    $batchCode = $this->data['returns'][$i]['batch_code'];
                    $batchCodeDisplay = '<br>' . $batchCode;
                    if (is_null($batchCode))
                    {
                        $batchCodeDisplay = '';
                    }
                    $expiryDate = $this->data['returns'][$i]['expiry_date'];
                    $expiryDateDisplay = '<br>' . $expiryDate;
                    if (is_null($expiryDate) || $expiryDate === '0000-00-00')
                    {
                        $expiryDateDisplay = '';
                    }

                    $returns .= <<<HTML
                <tr>
                    <td class="customTableData fit">{$this->data['returns'][$i]['qty']}</td>
                    <td class="customTableData fit">{$this->data['returns'][$i]['sku']}</td>
                    <td class="customTableData" colspan="3">{$this->data['returns'][$i]['returned_product']}{$batchCodeDisplay}{$expiryDateDisplay}</td>
                    <td class="customTableData fit right">{$this->data['returns'][$i]['price']}</td>
                    <td class="customTableData fit right">{$formatted}</td>
                    <td class="customTableData fit right">{$orderedItemVatValueFormatted}</td>
                    <td class="customTableData fit right">{$orderedItemTotalInclFormatted}</td>
                </tr>
HTML;
                    switch($bakeryOrderBy)
                    {
                        case 2:
                            $returns .= $this->getSubTotalsPDFs('returns', $totalsReturns, $currentIndex, $nextIndex, 'type', 'category');
                        break;

                        case 3:
                            $returns .= $this->getSubTotalsPDFs('returns', $totalsReturns, $currentIndex, $nextIndex);
                        break;

                        case 4:
                            $returns .= $this->getSubTotalsPDFs('returns', $totalsReturns, $currentIndex, $nextIndex, 'unit_of_measure', 'uom');
                        break;

                        case 5:
                            $returns .= $this->getSubTotalsPDFs('returns', $totalsReturns, $currentIndex, $nextIndex, 'unit_of_measure', 'uom');
                        break;

                        case 6:
                            $returns .= $this->getSubTotalsPDFs('returns', $totalsReturns, $currentIndex, $nextIndex, 'unit_of_measure', 'uom');
                        break;

                        default:
                            $returns .= '';
                        break;
                    }
                    ++$currentIndex;
                    ++$nextIndex;
                }
                $returnedTotalFormatted = FieldFormatter::format(FIELD_FORMATTER_DECIMAL_TWO, $returnedTotal);
            }
            else
            {
                $returns .= <<<HTML
            <tr>
                <td colspan="6">-</td>
                <td style="text-align: right;">0.00</td>
            </tr>
HTML;
            }

            $aMinusB = $this->data['ordered'][0]['gross_order_total_amount'];
            $aMinusBFormatted = FieldFormatter::format(FIELD_FORMATTER_DECIMAL_TWO, $aMinusB);
            $invoiceDiscount = $this->data['ordered'][0]['invoice_discount'];
            $discountType = $this->data['ordered'][0]['invoice_discount_type'];
            $discountTypeDisplay = '';
            if ($this->data['ordered'][0]['invoice_discount_value'] > 0)
            {
                if ($discountType == '%')
                {
                    $discountTypeDisplay = ' (-' . $this->data['ordered'][0]['invoice_discount_value'] . '%)';
                }
                else
                {
                    $discountTypeDisplay = ' (-' . $this->data['ordered'][0]['invoice_discount_value'] . ')';
                }
            }
            $discountedTotal = $aMinusB - $invoiceDiscount;
            $discountedTotalFormatted = FieldFormatter::format(FIELD_FORMATTER_DECIMAL_TWO, $discountedTotal);
            $invoiceDiscountDisplay = FieldFormatter::format(FIELD_FORMATTER_DECIMAL_TWO, $invoiceDiscount);
            $vat = $this->data['ordered'][0]['vat_value'];
            $vatFormatted = FieldFormatter::format(FIELD_FORMATTER_DECIMAL_TWO, $vat);
            $total = $this->data['ordered'][0]['order_total_amount'];
            $depositHtml = '';
            if($this->data['ordered'][0]['deposit_amount'] > 0)
            {
                $total += $this->data['ordered'][0]['deposit_amount'];
                $total += $this->data['ordered'][0]['deposit_vat'];
                $depositAmountFormatted = FieldFormatter::format(FIELD_FORMATTER_DECIMAL_TWO, $this->data['ordered'][0]['deposit_amount']);
                $depositVatFormatted = FieldFormatter::format(FIELD_FORMATTER_DECIMAL_TWO, $this->data['ordered'][0]['deposit_vat']);

                $net = $discountedTotal + $this->data['ordered'][0]['deposit_amount'];
                $netDisplay = FieldFormatter::format(FIELD_FORMATTER_DECIMAL_TWO, $net);
                $tax = $vat + $this->data['ordered'][0]['deposit_vat'];
                $taxDisplay = FieldFormatter::format(FIELD_FORMATTER_DECIMAL_TWO, $tax);

                $depositHtml = <<<HTML
                <tr>
                    <td colspan="6" class="customTableData right">Deposit Price</td>
                    <td class="customTableData right">{$depositAmountFormatted}</td>
                </tr>
                <tr>
                    <td colspan="6" class="customTableData right">Output Tax for Deposit</td>
                    <td class="customTableData right">{$depositVatFormatted}</td>
                </tr>
                <tr>
                    <td colspan="6" class="customTableData right">Net</td>
                    <td class="customTableData right">{$netDisplay}</td>
                </tr>
                <tr>
                    <td colspan="6" class="customTableData right">Tax</td>
                    <td class="customTableData right">{$taxDisplay}</td>
                </tr>
HTML;
            }
            $totalFormatted = FieldFormatter::format(FIELD_FORMATTER_DECIMAL_TWO, $total);
            $cashCollected = $this->data['ordered'][0]['cash_collected'];
            $cashCollectedFormatted = FieldFormatter::format(FIELD_FORMATTER_DECIMAL_TWO, $cashCollected);
            $finalTotal = $totalFormatted;

            // if($final_vat_text!='' && $vat>0)
            // {
            if ($finalVatStatus == 1)
            {
                $showVat = <<<HTML
                <tr>
                    <td colspan="6" class="customTableData right">{$final_vat_text}</td>
                    <td class="customTableData right">{$vatFormatted}</td>
                </tr>
HTML;
            }
            else
            {
                $showVat = '';
            }

            if ($subTotalStatus == 1)
            {
                $content .= <<<HTML
            <tr>
                <td colspan="6" class="customTableData right">{$sub_total_text}</td>
                <td class="customTableData right">{$orderedTotalFormatted}</td>
            </tr>
HTML;
            }

            if ($returnedTotal > 0)
            {
                $content .= <<<HTML
                {$returns}
                <tr>
                    <td colspan="6" class="customTableSubHeading center">{$order_total_text}</td>
                    <td class="customTableSubHeading"></td>
                </tr>
HTML;

                if ($bSubTotalStatus == 1)
                {
                    $content .= <<<HTML
    <tr>
        <td colspan="6" class="customTableData right">{$b_sub_total_text}</td>
        <td class="customTableData right">{$returnedTotalFormatted}</td>
    </tr>
HTML;
                }
                if ($minusSubTotalStatus == 1)
                {
                    $content .= <<<HTML
    <tr>
        <td colspan="6" class="customTableData right">{$minus_sub_total_text}</td>
        <td class="customTableData right">{$aMinusBFormatted}</td>
    </tr>
HTML;
                }
            }

            if ($discountStatus == 1)
            {
                $content .= <<<HTML
    <tr>
        <td colspan="6" class="customTableData right">{$discount_text}</td>
        <td class="customTableData right">{$invoiceDiscountDisplay}</td>
    </tr>
HTML;
            }

            if ($discountTotalStatus == 1)
            {
                $content .= <<<HTML
    <tr>
        <td colspan="6" class="customTableData right">{$discounted_total_text}</td>
        <td class="customTableData right">{$discountedTotalFormatted}</td>
    </tr>
HTML;
            }

            $content .= <<<HTML
            {$showVat}
            {$depositHtml}
HTML;
            if($totalStatus == 1)
            {
                $content .= <<<HTML
                <tr>
                    <td colspan="6" class="customTableData right bold">{$total_text}</td>
                    <td class="customTableData right bold">{$finalTotal}</td>
                </tr>
HTML;
            }
            // if($cash_collected_text!='' && $cashCollected>0){
            if($cashCollectedStatus == 1)
            {
                $content .= <<<HTML
                <tr>
                    <td colspan="6" class="customTableData right">{$cash_collected_text}</td>
                    <td class="customTableData right">{$cashCollectedFormatted}</td>
                </tr>
HTML;
            }

            $image_folder = Yii::app()->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'api' . DIRECTORY_SEPARATOR . 'PreSalesSignatures' . DIRECTORY_SEPARATOR;
            if(Yii::app() instanceof CConsoleApplication)
            {
                $image_url = Yii::app()->params['base_URL'] . '/images/api/PreSalesSignatures/';
            }
            else
            {
                $image_url = Yii::app()->getBaseUrl(true) . '/images/api/PreSalesSignatures/';
            }
            if ($this->data['ordered'][0]['presales_driver_signature_url'] != '' or $this->data['ordered'][0]['presales_signature_url'] != '')
            {
                $driver_image = "<img height='125' src='{$image_url}{$this->data['ordered'][0]['presales_driver_signature_url']}'>";
                $customer_image = "<img height='125' src='{$image_url}{$this->data['ordered'][0]['presales_signature_url']}'>";
                if (file_exists($image_folder . $this->data['ordered'][0]['presales_driver_signature_url']) && $this->data['ordered'][0]['presales_driver_signature_url'] != '')
                {
                    $content .= <<<HTML
                            <br/>
                            <tr>
                                <td colspan="6" class="customTableData right">{$driver_signature}</td>
                                <td class=" center"  colspan="3">{$driver_image}</td>
                            </tr>
HTML;
                }
                if (file_exists($image_folder . $this->data['ordered'][0]['presales_signature_url']) && $this->data['ordered'][0]['presales_signature_url'] != '')
                {
                    $content .= <<<HTML
                            <tr>
                                <td colspan="6" class="customTableData right">{$customer_signature} </td>
                                <td class=" center" colspan="3">{$customer_image}  <br/> {$this->data['ordered'][0]['presale_customer_name']} <br/> {$this->data['ordered'][0]['presale_customer_contact_number']} </td>
                            </tr>
HTML;
                }else if($this->data['ordered'][0]['presale_customer_name'] != ''){
                    $content .= <<<HTML
                            <tr>
                                <td colspan="6" class="customTableData right">{$customer_signature} </td>
                                <td class=" center" colspan="3">{$this->data['ordered'][0]['presale_customer_name']} <br/> {$this->data['ordered'][0]['presale_customer_contact_number']} </td>
                            </tr>
HTML;
                }
            }else{
                $content .= <<<HTML
                        <tr>
                            <td colspan="6" class="customTableData right">{$customer_signature} </td>
                            <td class=" center" colspan="3">{$this->data['ordered'][0]['presale_customer_name']} <br/> {$this->data['ordered'][0]['presale_customer_contact_number']} </td>
                        </tr>
HTML;
                }
            $content .= "</table>";
            if ($documentFooterStatus == 1)
            {
                $content .= '<p align="center" class="" style="font-size:9px !important">' . $document_footer . '</p>';
            }
            if (!$html)
            {
                Yii::import('application.vendors.mpdf.*');
                $this->pdfFolder .= 'tmp' . DIRECTORY_SEPARATOR;
                if (!file_exists($this->pdfFolder)) mkdir($this->pdfFolder, 0777, true);
                $mpdf = new mpdf('win-1252', 'A4', 0, '', PDF_MGL, PDF_MGR, PDF_MGT, PDF_MGB, PDF_MGH, PDF_MGF);
                //                $mpdf->SetProtection(array('print'));
                $mpdf->SetWatermarkText($title);
                $this->heading = $title;
                $mpdf->showWatermarkText = true;
                $mpdf->watermark_font = 'DejaVuSansCondensed';
                $mpdf->watermarkTextAlpha = 0.1;
                $mpdf->WriteHTML($this->show($content, $bakeryId));
                $filename = $this->pdfFolder . 'Purchase Order ' . $orderId . '.pdf';
                if ($inline)
                {
                    $mpdf->Output('', 'I');
                }
                else
                {
                    file_put_contents($filename, $mpdf->Output('', 'S'));
                    chmod($filename, 0777);
                }
                return $filename;
            }
            else
            {
                return $this->show($content, $bakeryId);
            }
        }
    }

   //=========|UM-2230| ====//
   public function createForm($orderId, $html = false, $inline = false)
   {
        $db = Yii::app()->db;
        $bakeryId = $this->data['ordered'][0]['bakery_id'];
      
              
              $FormTemplate = $this->data['ordered'];
              //var_dump($FormTemplate);
            // die;
               $title = $this->data['ordered'][0]['checklist_name'];
               $form = "Form : ".$this->data['ordered'][0]['checklist_name'];
               $form_number = "Form #: ".$this->data['ordered'][0]['form_number'];
               $customer_name ="Customer Name: ".$this->data['ordered'][0]['customer_name'];
               $customer = "Customer #: ".$this->data['ordered'][0]['customer_number'];
               $submitted_by = "Submitted By: ".$this->data['ordered'][0]['submitted_by'];
               $submission_date = "Submitted Date: ".$this->data['ordered'][0]['submission_date'];

              /* $content = <<<HTML
               <table>
               <tr>
                    <td class="customTableSubHeading center fit">
                    {$form_number}
                    </td>
                    <td class="customTableSubHeading center fit">
                    </td>
                    <td class="customTableSubHeading center fit">
                    {$submitted_by}
                    </td>
                </tr>
               <tr>
                    <td class="customTableSubHeading center fit">
                    {$form}
                    </td>
                    <td class="customTableSubHeading center fit">
                    </td>
                    <td class="customTableSubHeading center fit">
                    {$customer}
                    </td>
                </tr>
                 <tr>
                    <td class="customTableSubHeading center fit">
                    {$submission_date}
                    </td>
                    <td class="customTableSubHeading center fit">
                    </td>
                    <td class="customTableSubHeading center fit">
                    {$customer_name}
                    </td>
                </tr>
                </table>                
               HTML; */
               $content ='<table class="customTableForm">
               <tr>
                    <td class="customTableFormSubHeading left fit">
                    '.$form_number.'
                    </td>
                    
                    <td class="customTableFormSubHeading right fit">
                    '.$submitted_by.'
                    </td>
                </tr>
               <tr>
                    <td class="customTableFormSubHeading left fit">
                    '.$form.'
                    </td>
                    
                    <td class="customTableFormSubHeading right fit">
                    '.$customer.'
                    </td>
                </tr>
                 <tr>
                    <td class="customTableFormSubHeading left fit">
                    '.$submission_date.'
                    </td>
                    
                    <td class="customTableFormSubHeading right fit">
                    '.$customer_name.'
                    </td>
                </tr>
                </table><br><br>';
                $content .='<table class="customTable" style="width: 100%; margin-bottom: 15px;">
                <tr>
                    <th class="customTableHeading" style="width: 10%;">#</th>
                    <th class="customTableHeading" style="width: 40%;">Questions</th>
                    <th class="customTableHeading" style="width: 40%;">Answers</th>
                </tr>
                <tbody>';
                
               /* $content .= <<<HTML
               <table  class="customTable" style="margin-bottom: 15px;">
                   <thead><th class="customTableDataBlanks bold fit" style="width: 10%;">#</th><th class="customTableDataBlanks bold fit" style="width: 40%;">Question</th><th class="customTableDataBlanks bold fit" style="width: 40%;">Answer</th></thead>
                   <tbody>
                HTML;
                        */
                      foreach ($FormTemplate as $c)
                      {
                      /* $content .= <<<HTML
                       <tr><td style="width: 10%;">{$c['question_number']}</td><td style="width: 40%;">>{$c['question']}</td><td style="width: 50%;">>{$c['answer']}</td></tr>
                       HTML;*/
                       $search = 'photo';
                       $format = '.jpg';
                       $image_folder = Yii::app()->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'api' . DIRECTORY_SEPARATOR . 'checklistpictureupload' . DIRECTORY_SEPARATOR;
                       if(Yii::app() instanceof CConsoleApplication)
                       {
                           $image_url = Yii::app()->params['base_URL'] . '/images/api/checklistpictureupload/';
                       }
                       else
                       {
                           $image_url = Yii::app()->getBaseUrl(true) . '/images/api/checklistpictureupload/';
                       }
                                  
                       
                       if((preg_match("/{$format}/i", $c['answer']) || $c['question_type']=='photo') && $c['answer'] !='')
                       {        
                        
                            if(preg_match("/{$format}/i", $c['answer']))
                            {
                                $image = $image_url.''.$c['answer'];
                            }
                            else
                            {
                                $image = $image_url.''.$c['answer'].'.jpg';
                            }
                            $answer = '<img height="125" src="'.$image.'"><br><a href="'.$image.'" >View Image</a>';
                       }
                       else
                       {
                               $answer = $c['answer'];
                       }
                       $question = $c['question'];
                       $question_number =$c['question_number'];
                       $content .='<tr><td class="customTableData" style="width: 10%;">'.$question_number.'</td><td class="customTableData"  style="width: 40%;">'.$question.'</td><td class="customTableData"  style="width: 50%;">'.$answer.'</td></tr>';
                   
                            //----- Additional --//
                            if( $c['additional_answer'] != NULL)
                            {
                               
                                if(preg_match("/{$search}/i", $c['additional_question']) || preg_match("/{$format}/i", $c['additional_answer']))
                                {
                                    if(preg_match("/{$format}/i", $c['additional_answer']))
                                    {
                                        $image = $image_url.''.$c['additional_answer'];
                                    }
                                    else
                                    {
                                        $image = $image_url.''.$c['additional_answer'].'.jpg';
                                    }
                                    $answer = '<img height="125" src="'.$image.'"><br><a href="'.$image.'" >View Image</a>';
                                }
                                else
                                {
                                        $answer = $c['additional_answer'];
                                }
                                $question = '<i>'.$c['additional_question'].'</i>';
                                $question_number = '';//$c['question_number'];
                                $content .='<tr><td class="customTableData"  style="width: 10%;">'.$question_number.'</td><td class="customTableData"  style="width: 40%;">'.$question.'</td><td class="customTableData" style="width: 50%;">'.$answer.'</td></tr>';
                            }

                            
                      }
                      $content .='</tbody>
                      </table>';
                     /* 
                      $content.=<<<HTML
                      </tbody>
                      </table>
                      HTML;
              */
 
                    if (!$html)
                    {
                        Yii::import('application.vendors.mpdf.*');
                        $this->pdfFolder .= 'tmp' . DIRECTORY_SEPARATOR;
                        if (!file_exists($this->pdfFolder)) mkdir($this->pdfFolder, 0777, true);
                        $mpdf = new mpdf('win-1252', 'A4', 0, '', PDF_MGL, PDF_MGR, PDF_MGT, PDF_MGB, PDF_MGH, PDF_MGF);
                        //            $mpdf->SetProtection(array('print'));
                        $mpdf->SetWatermarkText($title);
                        $this->heading = "MACmobile Field Force -".$title;
                        $mpdf->showWatermarkText = true;
                        $mpdf->watermark_font = 'DejaVuSansCondensed';
                        $mpdf->watermarkTextAlpha = 0.1;
                        $mpdf->SetFooter("footer");
                        $mpdf->WriteHTML($this->show($content, $bakeryId));
                        $filename = $this->pdfFolder . 'Form ' . $orderId . '.pdf';
                        if ($inline)
                        {
                            $mpdf->Output('', 'I');
                        }
                        else
                        {
                            file_put_contents($filename, $mpdf->Output('', 'S'));
                            chmod($filename, 0777);
                        }
                        return $filename;
                    }
                    else
                    {
                        return $this->show($content, $bakeryId);
                    }
}
   //=========END|UM-2230 ===// 

    public function createInvoice($orderId, $html = false, $inline = false)
    {
        list($totalsOrdered, $totalsReturned) = $this->getPDFTotals('qty');

        $db = Yii::app()->db;
        $bakeryId = $this->data['ordered'][0]['bakery_id'];
        $sql = "select *
    from document_template dt
left join document_template_options dto on dt.document_template_id=dto.document_template_id
    where bakery_id= :bakeryId and document_type_id=2";
        $invoiceTemplate = $db->createCommand($sql)
            ->bindValue(':bakeryId', $bakeryId, PDO::PARAM_INT)
            ->queryAll();

        $title = 'Invoice';
        $payment_type_text = ' Payment Type';
        $order_num_text = 'Order No';
        $purchase_order_text = 'Purchase Order';
        $delivery_date_text = 'Delivery Date';
        $order_by_text = 'Ordered By';
        $order_date_text = 'Order Date';
        $to_text = 'TO';
        $from_text = 'FROM';
        $ordered_items_text = 'A: Ordered';
        $excl_vat_text = 'Excl Vat ';
        $vat_text = 'Vat';
        $incl_vat_text = 'Incl Vat';
        $sub_total_text = 'Sub-Total A';
        $return_items_text = 'B: Returned';
        $b_excl_vat_text = 'Excl Vat';
        $b_vat_text = 'Vat';
        $b_incl_vat_text = 'Incl Vat';
        $order_total_text = 'Order Total';
        $b_sub_total_text = 'Sub-Total B';
        $minus_sub_total_text = 'Sub-Total (A - B)';
        $discount_text = ' Discount';
        $discounted_total_text = 'Discounted Total';
        $final_vat_text = 'Vat';
        $credit_note_applied_text = 'Credit Note - ';
        $total_text = 'Total';
        $cash_collected_text = 'Cash Collected';
        $delivered_by_text = 'Delivered By';
        $receiver_sig_text = 'Receiver Signature';
        $driver_sig_text = 'Driver Signature';
        $payment_term_text = 'Payment Term (days)';
        $info_vat_text = 'Vat';
        $payment_type_status = 1;
        $order_num_status = 1;
        $purchase_order_status = 1;
        $delivery_date_status = 1;
        $order_by_status = 1;
        $order_date_status = 1;
        $delivered_by_status = 1;
        $document_footer = "";
        $ref_number_text = "REF #";
        $ref_number_by_status = 1;
        $credit_note_applied_status = 1;

        $options = [];
        foreach ($invoiceTemplate as $t)
        {
            $options[$t['label']] = $t['translation'];
        }

        $orderItemQtyTxt = (isset($options['ordered_items_qty'])) ? $options['ordered_items_qty'] : 'Qty';
        $orderItemSkuTxt = (isset($options['ordered_items_sku'])) ? $options['ordered_items_sku'] : 'Sku';
        $orderItemUnitPriceTxt = (isset($options['ordered_items_unit_price'])) ? $options['ordered_items_unit_price'] : 'Unit Price';
        $orderItemDiscountTxt = (isset($options['ordered_items_discount'])) ? $options['ordered_items_discount'] : 'Discount';
        $orderItemNetPriceTxt = (isset($options['ordered_items_net_price'])) ? $options['ordered_items_net_price'] : 'Net Price';
        $returnItemQtyTxt = (isset($options['return_items_qty'])) ? $options['return_items_qty'] : 'Qty';
        $returnItemSkuTxt = (isset($options['return_items_sku'])) ? $options['return_items_sku'] : 'Sku';
        $returnItemUnitPriceTxt = (isset($options['return_items_unit_price'])) ? $options['return_items_unit_price'] : 'Unit Price';

        if (count($invoiceTemplate) > 0)
        {
            foreach ($invoiceTemplate as $options)
            {
                switch ($options['label'])
                {
                    case 'title':
                        $title = $options['translation'];
                        break;

                    case 'payment_type_text':
                        $payment_type_text = $options['translation'];
                        $payment_type_status = $options['status'];
                        break;

                    case 'order_num_text':
                        $order_num_text = $options['translation'];
                        $order_num_status = $options['status'];
                        break;


                    case 'purchase_order_text':
                        $purchase_order_text = $options['translation'];
                        $purchase_order_status = $options['status'];
                        break;

                    case 'delivery_date_text':
                        $delivery_date_text = $options['translation'];
                        $delivery_date_status = $options['status'];

                        break;

                    case 'order_by_text':
                        $order_by_text = $options['translation'];
                        $order_by_status = $options['status'];

                        break;

                    case 'order_date_text':
                        $order_date_text = $options['translation'];
                        $order_date_status = $options['status'];

                        break;

                    case 'delivered_by_text':
                        $delivered_by_text = $options['translation'];
                        $delivered_by_status = $options['status'];

                        break;
                    case 'from_text':
                        $from_text = $options['translation'];
                        break;

                    case 'to_text':
                        $to_text = $options['translation'];
                        break;
                    case 'payment_term_text':
                        $payment_term_text = $options['translation'];
                        break;

                    case 'ordered_items_text':
                        $ordered_items_text = $options['translation'];
                        break;

                    case 'excl_vat_text':
                        $excl_vat_text = $options['translation'];
                        break;

                    case 'vat_text':
                        $vat_text = $options['translation'];
                        break;

                    case 'incl_vat_text':
                        $incl_vat_text = $options['translation'];
                        break;
                    case 'sub_total_text':
                        $sub_total_text = $options['translation'];
                        $subTotalStatus = $options['status'];
                        break;

                    case 'return_items_text':
                        $return_items_text = $options['translation'];
                        break;

                    case 'b_excl_vat_text':
                        $b_excl_vat_text = $options['translation'];
                        break;

                    case 'b_vat_text':
                        $b_vat_text = $options['translation'];
                        $bVatStatus = $options['status'];
                        break;

                    case 'b_incl_vat_text':
                        $b_incl_vat_text = $options['translation'];
                        break;

                    case 'order_total_text':
                        $order_total_text = $options['translation'];
                        break;

                    case 'b_sub_total_text':
                        $b_sub_total_text = $options['translation'];
                        $bSubTotalStatus = $options['status'];
                        break;

                    case 'minus_sub_total_text':
                        $minus_sub_total_text = $options['translation'];
                        $minusSubTotalStatus = $options['status'];
                        break;

                    case 'discount_text':
                        $discount_text = $options['translation'];
                        $discountStatus = $options['status'];
                        break;

                    case 'discounted_total_text':
                        $discounted_total_text = $options['translation'];
                        $discountTotalStatus = $options['status'];
                        break;

                    case 'final_vat_text':
                        $final_vat_text = $options['translation'];
                        $finalVatStatus = $options['status'];
                        break;

                    case 'total_text':
                        $total_text = $options['translation'];
                        $totalStatus = $options['status'];
                        break;

                    case 'cash_collected_text':
                        $cash_collected_text = $options['translation'];
                        $cashCollectedStatus = $options['status'];
                        break;
                    case 'currency_collected_text':
                        $currency_collected_text = $options['translation'];
                        $currencyCollectedStatus = $options['status'];
                        break;

                    case 'currency_collected_amount_text':
                        $currency_collected_amount_text = $options['translation'];
                        $currencyCollectedAmountStatus = $options['status'];
                        break;
                    case 'info_vat_text':
                        $info_vat_text = $options['translation'];
                        break;
                    case 'receiver_sig_text':
                        $receiver_sig_text = $options['translation'];
                        $receiverSigStatus = $options['status'];
                        break;
                    case 'document_footer_text':
                        $document_footer = $options['translation'];
                        $documentFooterStatus = $options['status'];
                        break;
                    case 'driver_signature':
                        $driver_sig_text = $options['translation'];
                        break;
                    case 'customer_signature':
                        $receiver_sig_text = $options['translation'];
                        break;
                    case 'ref_number':
                        $ref_number_text = $options['translation'];
                        $ref_number_by_status = $options['status'];
                        break;
                    case 'credit_note_applied_text':
                        $credit_note_applied_text = $options['translation'];
                        $credit_note_applied_status = $options['status'];
                        break;
                }
            }
        }
        $creditNoteOrderId = isset($this->data['ordered'][0]['credit_note_order_id']) ? $this->data['ordered'][0]['credit_note_order_id'] : '0';
        $creditNoteValue = isset($this->data['ordered'][0]['credit_note_value']) ? $this->data['ordered'][0]['credit_note_value'] : '0';
        $creditNoteApplied = '';
        if($creditNoteOrderId != '0' && $credit_note_applied_status == '1')
        {
            $creditNoteAppliedText = $credit_note_applied_text . $creditNoteOrderId;
            $creditNoteOrderValueFormatted = number_format($creditNoteValue, 2);
            $creditNoteApplied = <<<HTML
            <tr>
                <td colspan="6" class="customTableData right">{$creditNoteAppliedText}</td>
                <td class="customTableData right">{$creditNoteOrderValueFormatted}</td>
            </tr>
HTML;
        }
        $includeOrExclude = 'Excl';
        if (Yii::app()->params['vat'] == 0)
        {
            $includeOrExclude = 'Excl';
        }

        $bankingDetails = '';
        if ($this->data['ordered'][0]['banking_details'] != '')
        {
            $bankingDetails = '<br><br>' . nl2br($this->data['ordered'][0]['banking_details']);
        }

        $paymentTerm = '';
        if ($this->data['ordered'][0]['customer_payment_term_to'] !== null)
        {
            $paymentTerm = '<br>' . $payment_term_text . ': ' . $this->data['ordered'][0]['customer_payment_term_to'];
        }
        $payment_html = '';
        if ($payment_type_status == 1 && $payment_type_text != '')
        {
            $payment_html = <<<HTML
                   <td class="customTableDataBlanks bold fit">{$payment_type_text}:</td>
                    <td class="customTableDataBlanks">{$this->data['ordered'][0]['payment_type']}</td>
HTML;
        }
        $order_number = '';
        $ref_number = '';
        if ($this->data['ordered'][0]['use_document_sequencing'] == 1)
        {
            $order_number .= $this->data['ordered'][0]['inv_document_prefix'] . $this->data['ordered'][0]['inv_document_number'];
            $ref_number .= $this->data['ordered'][0]['purchase_order_number'];
            if ($order_number == '')
            {
                $ref_number_by_status = 0;
                $order_number .= $this->data['ordered'][0]['purchase_order_number'];
            }
        }
        else
        {
            $order_number .= $this->data['ordered'][0]['purchase_order_number'];
        }
        $orderNo_html = '';
        if ($order_num_status == 1)
        {
            $orderNo_html = <<<HTML
              <td class="customTableDataBlanks bold fit">{$order_num_text}:</td>
                    <td class="customTableDataBlanks bold" style="font-size: 16px;">{$order_number}</td>
                         
HTML;
        }

        $purchaseOrder_html = '';
        if ($purchase_order_status == 1 && $purchase_order_text != '')
        {
            $purchaseOrder_html = <<<HTML
                     <td class="customTableDataBlanks bold fit">{$purchase_order_text}:</td>
                    <td class="customTableDataBlanks">{$this->data['ordered'][0]['po']}</td>
HTML;
        }

        $deliveryDate_html = '';
        if ($delivery_date_status == 1 && $delivery_date_text != '')
        {
            $deliveryDate_html = <<<HTML
                         <td class="customTableDataBlanks bold fit">{$delivery_date_text}:</td>
                    <td class="customTableDataBlanks bold">{$this->data['ordered'][0]['expected_delivery_date']}</td>
HTML;
        }
        $orderBy_html = '';
        if ($order_by_status == 1 && $order_by_text != '')
        {
            $orderBy_html = <<<HTML
                  <td class="customTableDataBlanks bold fit">{$order_by_text}:</td>
                    <td class="customTableDataBlanks">{$this->data['ordered'][0]['ordered_by']}</td>
HTML;
        }
        $orderDate_Html = '';
        if ($order_date_status == 1 && $order_date_text != '')
        {
            $orderDate_Html = <<<HTML
                 <td class="customTableDataBlanks bold fit">{$order_date_text}:</td>
                    <td class="customTableDataBlanks bold">{$this->data['ordered'][0]['order_date']}</td>
HTML;
        }

        $deliverBy_Html = '';
        if ($delivered_by_status == 1 && $delivered_by_text != '')
        {
            $deliverBy_Html = <<<HTML
                <td class="customTableDataBlanks bold fit">{$delivered_by_text}:</td>
                    <td class="customTableDataBlanks">{$this->data['ordered'][0]['delivered_by']}</td>
HTML;
        }
        $refNumber_Html = '';
        if ($this->data['ordered'][0]['use_document_sequencing'] == 1 && $ref_number_by_status == 1)
        {
            $refNumber_Html = <<<HTML
                <td class="customTableDataBlanks bold fit">{$ref_number_text}:</td>
                    <td class="customTableDataBlanks">{$ref_number}</td>
HTML;
        }
        $vat_replace_from_string = str_replace("Vat #", $info_vat_text, $this->data['ordered'][0]['customer_vat_number_from']);
        $vat_replace_to_string = str_replace("Vat #", $info_vat_text, $this->data['ordered'][0]['customer_vat_number_to']);

        $row1 = CustomUtils::pdfRowDetails($payment_html, $orderNo_html);
        $row2 = CustomUtils::pdfRowDetails($purchaseOrder_html, $deliveryDate_html);
        $row3 = CustomUtils::pdfRowDetails($orderBy_html, $orderDate_Html);
        $row4 = CustomUtils::pdfRowDetails($deliverBy_Html, $refNumber_Html);

        $branchAddress = "";
        if ($this->data['ordered'][0]['branch_company_id'] > 0)
        {
            $vat_replace_from_string = str_replace("Vat #", $info_vat_text, "Vat #: " . $this->data['ordered'][0]['company_vat_number']);
            $branchAddress = <<<HTML
                        {$this->data['ordered'][0]['company_description']}<br/>
                        {$this->data['ordered'][0]['company_physical_address']}<br/>
                        {$this->data['ordered'][0]['company_postal_address']}<br/>
                        {$this->data['ordered'][0]['company_phone']}<br/>
                        {$vat_replace_from_string}<br/>
                        {$this->data['ordered'][0]['company_banking_details']}
HTML;
        }
        else
        {
            $branchAddress = <<<HTML
                        {$this->data['ordered'][0]['customer_name_from']}<br/>
                        {$this->data['ordered'][0]['customer_address_from']}<br/>
                        {$this->data['ordered'][0]['customer_postal_address_from']}<br/>
                        {$this->data['ordered'][0]['customer_tel_from']}<br/>
                        {$vat_replace_from_string}<br/>
                        {$bankingDetails}
HTML;
        }

        $content = <<<HTML
            <table class="customTableBlanks" style="margin-bottom: 15px;">
                {$row1}
                {$row2}
                {$row3}
                {$row4}
            </table>
            
            <table class="customTable" style="margin-bottom: 15px;">
                <tr>
                   <td class="customTableSubHeading" style="width: 50%;">{$from_text}</td>
                    <td class="customTableSubHeading" style="width: 50%;">{$to_text}</td>
                </tr>
                <tr>
                    <td class="customTableData" style="vertical-align: top;">
                        {$branchAddress}
                        
                    </td>
                    <td class="customTableData" style="vertical-align: top;">
                        {$this->data['ordered'][0]['customer_account_number_to']}<br/>
                        {$this->data['ordered'][0]['customer_name_to']}<br/>
                        {$this->data['ordered'][0]['customer_address_to']}<br/>
                        {$this->data['ordered'][0]['customer_tel_to']}<br/>
                        {$this->data['ordered'][0]['customer_cell_to']}<br/>
                        {$vat_replace_to_string}
                        {$paymentTerm}
                    </td>
                </tr>
HTML;
        if ($this->data['ordered'][0]['comments'] !== '')
        {
            $content .= <<<HTML
                <tr>
                    <td class="customTableSubHeading" colspan="2">Comments</td>
                </tr>
                <tr>
                    <td class="customTableData" colspan="2">{$this->data['ordered'][0]['comments']}</td>
                </tr>
HTML;
        }

        $content .= <<<HTML
        </table>
        
        <table class="customTable">
            <tr>
            <td class="customTableSubHeading center fit">{$orderItemQtyTxt}</td>
            <td class="customTableSubHeading center fit">{$orderItemSkuTxt}</td>
              <td class="customTableSubHeading center">{$ordered_items_text}</td>
              <td class="customTableSubHeading center fit">{$orderItemUnitPriceTxt}</td>
              <td class="customTableSubHeading center fit">{$orderItemDiscountTxt}</td>
              <td class="customTableSubHeading center fit">{$orderItemNetPriceTxt}</td>
                    <td class="customTableSubHeading center fit">{$excl_vat_text} </td>
                    <td class="customTableSubHeading center fit">{$vat_text}</td>
                    <td class="customTableSubHeading center fit">{$incl_vat_text}</td>
            </tr>
HTML;
        $orderedTotal = 0;
        $currentIndex = 0;
        $nextIndex = $currentIndex + 1;
        $bakeryOrderBy = isset($this->data['ordered'][0]['bakery_distribution_order_by_id']) ? $this->data['ordered'][0]['bakery_distribution_order_by_id'] : 0;
        for ($i = 0; $i < count($this->data['ordered']); $i++)
        {
            if ($this->data['ordered'][$i]['qty'] > 0)
            {
                $itemTotal = floatval($this->data['ordered'][$i]['ordered_item_total']);
                $orderedTotal += $itemTotal;
                $formatted = FieldFormatter::format(FIELD_FORMATTER_DECIMAL_TWO, $itemTotal);
                $orderedItemVatValue = $this->data['ordered'][$i]['ordered_item_vat_value'];
                $orderedItemVatValueFormatted = FieldFormatter::format(FIELD_FORMATTER_DECIMAL_TWO, $orderedItemVatValue);
                $orderedItemTotalIncl = $this->data['ordered'][$i]['ordered_item_total_incl'];
                $orderedItemTotalInclFormatted = FieldFormatter::format(FIELD_FORMATTER_DECIMAL_TWO, $orderedItemTotalIncl);
                $at = <<<HTML
                {$this->data['ordered'][$i]['original_price']}
HTML;
                if ($this->data['ordered'][$i]['discount'] > 0)
                {
                    $d = number_format(round(($this->data['ordered'][$i]['discount'] / $this->data['ordered'][$i]['original_price']) * 100, 2), '2');
                    $discountPercentage = ' <span class="italic">(' . $d . '%)</span>';
                    $at = <<<HTML
                <span style="text-decoration: line-through; color: #F44336;">{$this->data['ordered'][$i]['original_price']}</span>{$discountPercentage} {$this->data['ordered'][$i]['discounted_price']}
HTML;
                }
                $batchCode = $this->data['ordered'][$i]['batch_code'];
                $batchCodeDisplay = '<br>' . $batchCode;
                if (is_null($batchCode))
                {
                    $batchCodeDisplay = '';
                }
                $expiryDate = $this->data['ordered'][$i]['expiry_date'];
                $expiryDateDisplay = '<br>' . $expiryDate;
                if (is_null($expiryDate) || $expiryDate === '0000-00-00')
                {
                    $expiryDateDisplay = '';
                }
                $content .= <<<HTML
                <tr>
                    <td class="customTableData fit">{$this->data['ordered'][$i]['qty']}</td>
                    <td class="customTableData fit">{$this->data['ordered'][$i]['product_sku']}</td>
                    <td class="customTableData">{$this->data['ordered'][$i]['ordered_product']}{$batchCodeDisplay}{$expiryDateDisplay}</td>
                    <td class="customTableData fit right">{$this->data['ordered'][$i]['original_price']}</td>
                    <td class="customTableData fit right">{$this->data['ordered'][$i]['discount']}</td>
                    <td class="customTableData fit right">{$this->data['ordered'][$i]['discounted_price']}</td>
                    <td class="customTableData fit right">{$formatted}</td>
                    <td class="customTableData fit right">{$orderedItemVatValueFormatted}</td>
                    <td class="customTableData fit right">{$orderedItemTotalInclFormatted}</td>
                </tr>
HTML;
                switch($bakeryOrderBy)
                {
                    case 2:
                        $content .= $this->getSubTotalsPDFs('ordered', $totalsOrdered, $currentIndex, $nextIndex, 'type', 'category');
                    break;

                    case 3:
                        $content .= $this->getSubTotalsPDFs('ordered', $totalsOrdered, $currentIndex, $nextIndex);
                    break;

                    case 4:
                        $content .= $this->getSubTotalsPDFs('ordered', $totalsOrdered, $currentIndex, $nextIndex, 'unit_of_measure', 'uom');
                    break;

                    case 5:
                        $content .= $this->getSubTotalsPDFs('ordered', $totalsOrdered, $currentIndex, $nextIndex, 'unit_of_measure', 'uom');
                    break;

                    case 6:
                        $content .= $this->getSubTotalsPDFs('ordered', $totalsOrdered, $currentIndex, $nextIndex, 'unit_of_measure', 'uom');
                    break;

                    default:
                        $content .= '';
                    break;
                }
            }

            ++$currentIndex;
            ++$nextIndex;
        }
        $orderedTotalFormatted = FieldFormatter::format(FIELD_FORMATTER_DECIMAL_TWO, $orderedTotal);
        //returns
        $returns = <<<HTML
            <tr>
                <td class="customTableSubHeading center fit">{$returnItemQtyTxt}</td>
                <td class="customTableSubHeading center fit">{$returnItemSkuTxt}</td>
                <td class="customTableSubHeading center" colspan="3">{$return_items_text}</td>
                <td class="customTableSubHeading center fit">{$returnItemUnitPriceTxt}</td>
                <td class="customTableSubHeading center fit">{$b_excl_vat_text} </td>
                <td class="customTableSubHeading center fit">{$b_vat_text}</td>
                <td class="customTableSubHeading center fit">{$b_incl_vat_text}</td>
            </tr>
HTML;
        $returnedTotal = 0;
        $returnedTotalFormatted = '0.00';
        if (!empty($this->data['returns']))
        {
            $currentIndex = 0;
            $nextIndex = $currentIndex + 1;
            for ($i = 0; $i < count($this->data['returns']); $i++)
            {
                $orderedItemTotalIncl = $this->data['returns'][$i]['returned_item_total_incl'];
                $orderedItemTotalInclFormatted = FieldFormatter::format(FIELD_FORMATTER_DECIMAL_TWO, $orderedItemTotalIncl);
                $itemTotal = floatval($this->data['returns'][$i]['returned_item_total']);
                $returnedTotal += $itemTotal;
                //$returnedTotal += $orderedItemTotalIncl;
                $formatted = FieldFormatter::format(FIELD_FORMATTER_DECIMAL_TWO, $itemTotal);
                $orderedItemVatValue = $this->data['returns'][$i]['returned_item_vat_value'];
                $orderedItemVatValueFormatted = FieldFormatter::format(FIELD_FORMATTER_DECIMAL_TWO, $orderedItemVatValue);

                $batchCode = $this->data['returns'][$i]['batch_code'];
                $batchCodeDisplay = '<br>' . $batchCode;
                if (is_null($batchCode))
                {
                    $batchCodeDisplay = '';
                }
                $expiryDate = $this->data['returns'][$i]['expiry_date'];
                $expiryDateDisplay = '<br>' . $expiryDate;
                if (is_null($expiryDate) || $expiryDate === '0000-00-00')
                {
                    $expiryDateDisplay = '';
                }

                $returns .= <<<HTML
                <tr>
                    <td class="customTableData fit">{$this->data['returns'][$i]['qty']}</td>
                    <td class="customTableData fit">{$this->data['returns'][$i]['sku']}</td>
                    <td class="customTableData" colspan="3">{$this->data['returns'][$i]['returned_product']}{$batchCodeDisplay}{$expiryDateDisplay}</td>
                    <td class="customTableData fit right">{$this->data['returns'][$i]['price']}</td>
                    <td class="customTableData fit right">{$formatted}</td>
                    <td class="customTableData fit right">{$orderedItemVatValueFormatted}</td>
                    <td class="customTableData fit right">{$orderedItemTotalInclFormatted}</td>
                </tr>
HTML;
                switch($bakeryOrderBy)
                {
                    case 2:
                        $returns .= $this->getSubTotalsPDFs('returns', $totalsReturned, $currentIndex, $nextIndex, 'type', 'category');
                    break;

                    case 3:
                        $returns .= $this->getSubTotalsPDFs('returns', $totalsReturned, $currentIndex, $nextIndex);
                    break;

                    case 4:
                        $returns .= $this->getSubTotalsPDFs('returns', $totalsReturned, $currentIndex, $nextIndex, 'unit_of_measure', 'uom');
                    break;

                    case 5:
                        $returns .= $this->getSubTotalsPDFs('returns', $totalsReturned, $currentIndex, $nextIndex, 'unit_of_measure', 'uom');
                    break;

                    case 6:
                        $returns .= $this->getSubTotalsPDFs('returns', $totalsReturned, $currentIndex, $nextIndex, 'unit_of_measure', 'uom');
                    break;

                    default:
                        $returns .= '';
                    break;
                }
                ++$currentIndex;
                ++$nextIndex;
            }
            $returnedTotalFormatted = FieldFormatter::format(FIELD_FORMATTER_DECIMAL_TWO, $returnedTotal);
        }
        else
        {
            $returns .= <<<HTML
            <tr>
                <td colspan="6">-</td>
                <td style="text-align: right;">0.00</td>
            </tr>
HTML;
        }

        $aMinusB = $this->data['ordered'][0]['gross_order_total_amount'];
        $aMinusBFormatted = FieldFormatter::format(FIELD_FORMATTER_DECIMAL_TWO, $aMinusB);
        $invoiceDiscount = $this->data['ordered'][0]['invoice_discount'];
        $discountType = $this->data['ordered'][0]['invoice_discount_type'];
        $discountTypeDisplay = '';
        if ($this->data['ordered'][0]['invoice_discount_value'] > 0)
        {
            if ($discountType == '%')
            {
                $discountTypeDisplay = ' (-' . $this->data['ordered'][0]['invoice_discount_value'] . '%)';
            }
            else
            {
                $discountTypeDisplay = ' (-' . $this->data['ordered'][0]['invoice_discount_value'] . ')';
            }
        }
        $discountedTotal = $aMinusB - $invoiceDiscount;
        $discountedTotalFormatted = FieldFormatter::format(FIELD_FORMATTER_DECIMAL_TWO, $discountedTotal);
        $invoiceDiscountDisplay = FieldFormatter::format(FIELD_FORMATTER_DECIMAL_TWO, $invoiceDiscount);
        $vat = $this->data['ordered'][0]['vat_value'];
        $vatFormatted = FieldFormatter::format(FIELD_FORMATTER_DECIMAL_TWO, $vat);
        //        $total = $aMinusB+$vat;
        $total = $this->data['ordered'][0]['order_total_amount'];
        $totalFormatted = FieldFormatter::format(FIELD_FORMATTER_DECIMAL_TWO, $total);
        $vatPercentage = $this->data['ordered'][0]['vat_value'] . '%';
        $cashCollected = $this->data['ordered'][0]['cash_collected'];
        $cashCollectedFormatted = FieldFormatter::format(FIELD_FORMATTER_DECIMAL_TWO, $cashCollected);
        $currencyCollectedAmount = $this->data['ordered'][0]['currency_collected_amount'];
        $currencyCollectedAmountFormatted = ($currencyCollectedAmount == '-') ? '-' : FieldFormatter::format(FIELD_FORMATTER_DECIMAL_TWO, $currencyCollectedAmount);
        $finalTotal = $totalFormatted;

        //if($final_vat_text!='' && $vat > 0){
        if ($finalVatStatus == 1)
        {
            $showVat = <<<HTML
                            <tr>
                                <td colspan="6" class="customTableData right">{$final_vat_text}</td>
                                <td class="customTableData right">{$vatFormatted}</td>
                            </tr>
HTML;
        }
        else
        {
            $showVat = '';
        }


        if (Yii::app() instanceof CConsoleApplication)
        {
            $baseUrl = Yii::app()->params['base_URL'];
        }
        else
        {
            $baseUrl = Yii::app()->getBaseUrl(true);
        }

        if ($this->data['ordered'][0]['order_type'] === '1') //pre-sale
        {
            $folder = 'PreSalesSignatures';
        }
        else
        {
            $folder = 'VanSalesSignatures';
        }
        $imageUrl = $baseUrl . '/images/api/' . $folder . '/' . $this->data['ordered'][0]['signature_url'];
        $imageDriverUrl = $baseUrl . '/images/api/' . $folder . '/' . $this->data['ordered'][0]['driver_signature_url'];

        $signature = '';
        $signature1 = '';
        $customer_details = '';
        if($this->data['ordered'][0]['delivery_customer_name']!='')
        {    
            $customer_details = <<<HTML
            <br/>        
            {$this->data['ordered'][0]['delivery_customer_name']}<br/>
            {$this->data['ordered'][0]['delivery_customer_contact_number']}
HTML;
        }
        
        if ($receiverSigStatus == 1)
        {
            $customer_signature = <<<HTML
        <tr>
            <td colspan="6" class="customTableData right">{$receiver_sig_text}</td>
            <td class="customTableData center" colspan="3" style="padding: 40px 0;">{$customer_details}</td>
        </tr>
HTML;
            $driver_signature = <<<HTML
        <tr>
            <td colspan="6" class="customTableData right">{$driver_sig_text}</td>
            <td class="customTableData center" colspan="3" style="padding: 40px 0;"></td>
        </tr>
HTML;
            if ($this->data['ordered'][0]['signature_url'] !== '' && file_exists(Yii::app()->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'api' . DIRECTORY_SEPARATOR . $folder . DIRECTORY_SEPARATOR . $this->data['ordered'][0]['signature_url']))
            {
                $customer_signature = <<<HTML
        <tr>
            <td colspan="6" class="customTableData right">{$receiver_sig_text}</td>
            <td class="customTableData center"><img alt="signature not found" style="height: 50px;" src="{$imageUrl}" /> {$customer_details}</td>
        </tr>
HTML;
            }
            if ($this->data['ordered'][0]['driver_signature_url'] !== '' && file_exists(Yii::app()->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'api' . DIRECTORY_SEPARATOR . $folder . DIRECTORY_SEPARATOR . $this->data['ordered'][0]['driver_signature_url']))
            {
                $driver_signature = $signature1 . <<<HTML
        <tr>
            <td colspan="6" class="customTableData right">{$driver_sig_text}</td>
            <td class="customTableData center"><img alt="signature not found" style="height: 50px;" src="{$imageDriverUrl}" /></td>
        </tr>
HTML;
            }
            $signature = $customer_signature . $driver_signature;
        }

        if ($subTotalStatus == 1)
        {
            $content .= <<<HTML
            <tr>
                <td colspan="6" class="customTableData right">{$sub_total_text}</td>
                <td class="customTableData right">{$orderedTotalFormatted}</td>
            </tr>
HTML;
        }
        if ($returnedTotal > 0)
        {
            $content .= <<<HTML
        {$returns}
        <tr>
            <td colspan="6" class="customTableSubHeading center">{$order_total_text}</td>
            <td class="customTableSubHeading"></td>
        </tr>
HTML;

            if ($bSubTotalStatus == 1)
            {
                $content .= <<<HTML
            <tr>
                <td colspan="6" class="customTableData right">{$b_sub_total_text}</td>
                <td class="customTableData right">{$returnedTotalFormatted}</td>
            </tr>
HTML;
            }

            if ($minusSubTotalStatus == 1)
            {
                $content .= <<<HTML
            <tr>
                <td colspan="6" class="customTableData right">{$minus_sub_total_text}</td>
                <td class="customTableData right">{$aMinusBFormatted}</td>
            </tr>
HTML;
            }
        }

        // if($discount_text != '' && $invoiceDiscount>0){
        if ($discountStatus == 1)
        {
            $content .= <<<HTML
        <tr>
            <td colspan="6" class="customTableData right">{$discount_text}</td>
            <td class="customTableData right">{$invoiceDiscountDisplay}</td>
        </tr>
HTML;
        }

        // if($discounted_total_text != '' && $invoiceDiscount>0){ // $invoiceDiscount
        if ($discountTotalStatus == 1)
        {
            $content .= <<<HTML
        <tr>
            <td colspan="6" class="customTableData right">{$discounted_total_text}</td>
            <td class="customTableData right">{$discountedTotalFormatted}</td>
        </tr>
HTML;
        }

        $content .= <<<HTML
        {$showVat}
        {$creditNoteApplied}
HTML;
        // if($total_text!='' && $total > 0){
        if ($totalStatus == 1)
        {
            $content .= <<<HTML
        <tr>
            <td colspan="6" class="customTableData right bold">{$total_text}</td>
            <td class="customTableData right bold">{$finalTotal}</td>
        </tr>
HTML;
        }

         
        if ($cashCollectedStatus == 1)
        {
            $content .= <<<HTML
        <tr>
            <td colspan="6" class="customTableData right">{$cash_collected_text}</td>
            <td class="customTableData right">{$cashCollectedFormatted}</td>
        </tr>
HTML;
        }
        if (isset($currencyCollectedStatus) && $currencyCollectedStatus == 1)
        {
            $content .= <<<HTML
        <tr>
            <td colspan="6" class="customTableData right">{$currency_collected_text}</td>
            <td class="customTableData right">{$this->data['ordered'][0]['currency_collected']}</td>
        </tr>
HTML;
        }
        if (isset($currencyCollectedAmountStatus) && $currencyCollectedAmountStatus == 1)
        {
            $content .= <<<HTML
        <tr>
            <td colspan="6" class="customTableData right">{$currency_collected_amount_text}</td>
            <td class="customTableData right">{$currencyCollectedAmountFormatted}</td>
        </tr>
HTML;
        }
        $content .= <<<HTML
        {$signature}
 <tr>
        </tr>
    </table>
HTML;

        if ($documentFooterStatus == 1)
        {
            $content .= <<<HTML
    <p class="center" style="font-size:9px !important">{$document_footer}</p>
HTML;
        }
        if (!$html)
        {
            Yii::import('application.vendors.mpdf.*');
            $this->pdfFolder .= 'tmp' . DIRECTORY_SEPARATOR;
            if (!file_exists($this->pdfFolder)) mkdir($this->pdfFolder, 0777, true);
            $mpdf = new mpdf('win-1252', 'A4', 0, '', PDF_MGL, PDF_MGR, PDF_MGT, PDF_MGB, PDF_MGH, PDF_MGF);
            //            $mpdf->SetProtection(array('print'));
            $mpdf->SetWatermarkText($title);
            $this->heading = $title;
            $mpdf->showWatermarkText = true;
            $mpdf->watermark_font = 'DejaVuSansCondensed';
            $mpdf->watermarkTextAlpha = 0.1;
            $mpdf->SetFooter("footer");
            $mpdf->WriteHTML($this->show($content, $bakeryId));
            $filename = $this->pdfFolder . 'Invoice ' . $orderId . '.pdf';
            if ($inline)
            {
                $mpdf->Output('', 'I');
            }
            else
            {
                file_put_contents($filename, $mpdf->Output('', 'S'));
                chmod($filename, 0777);
            }
            return $filename;
        }
        else
        {
            return $this->show($content, $bakeryId);
        }
    }

    public function createLoadedSheet($tripSheetId, $html = false)
    {
        //        var_dump($this->data);
        $printedBy = Yii::app()->user->name;

        $summarySql = <<<SQL
        SELECT
            p.id AS product_id,
            p.sku AS product_code,
            CONCAT(uom.description, ' - ', p.description) AS product,
            p.batch_code,
            p.expiry_date,
            SUM(o.qty_picked) AS qty_picked
        FROM
            trip_sheets ts
                INNER JOIN
            trip_sheets_items tsi ON tsi.trip_sheet_id = ts.id
                INNER JOIN
            order_main om ON om.id = tsi.order_id
                INNER JOIN
            orders o ON o.order_id = om.id
                INNER JOIN
            products p ON p.id = o.product_id
                INNER JOIN
            units_of_measure uom ON uom.id = p.units_of_measure_id
        WHERE
            tsi.trip_sheet_id = :tripSheetId
        GROUP BY p.id WITH ROLLUP;
SQL;
        $summaryCommand = Yii::app()->db->createCommand($summarySql);
        $summaryCommand->bindValue(':tripSheetId', $tripSheetId, PDO::PARAM_INT);
        $summary = $summaryCommand->queryAll();

        $content = <<<HTML
        <h2>Loaded Shipment {$this->data[0]['shipment_no']}</h2>
        <table class="customTable" style="width: 100%; border-spacing: 0; border-collapse: collapse;">
            <tr>
                <td class="customTableSubHeading" colspan="6">{$this->data[0]['branch']}</td>
            </tr>
            <tr>
                <td class="customTableData bold">Shipment #:</td><td class="customTableData">{$this->data[0]['shipment_no']}</td>
                <td class="customTableData bold">Print Date:</td><td class="customTableData">{$this->data[0]['print_date']}</td>
                <td class="customTableData bold">Delivery Date:</td><td class="customTableData">{$this->data[0]['expected_delivery_date']}</td>
            </tr>
            <tr>
                <td class="customTableData bold">Vehicle:</td><td class="customTableData">{$this->data[0]['fleet']}</td>
                <td class="customTableData bold">Printed by:</td><td class="customTableData">{$printedBy}</td>
                <td class="customTableData bold">Employee:</td><td class="customTableData">{$this->data[0]['employee']}</td>
            </tr>
            <tr>
                <td class="customTableData bold">Date:</td><td colspan="5" class="customTableData bold">&nbsp;</td>
            </tr>
            <tr>
                <td class="customTableData bold">Loaded by:</td><td colspan="2" class="customTableData">&nbsp;</td>
                <td class="customTableData bold">Signature:</td><td colspan="2" class="customTableData">&nbsp;</td>
            </tr>
            <tr>
                <td class="customTableData bold">Accepted by:</td><td colspan="2" class="customTableData">&nbsp;</td>
                <td class="customTableData bold">Signature:</td><td colspan="2" class="customTableData">&nbsp;</td>
            </tr>
        </table>
HTML;
        $content .= <<<HTML
        <h2>Shipment {$tripSheetId} Summary</h2>
        <table class="customTable" style="width: 100%; border-spacing: 0; border-collapse: collapse;">
            <tr>
                <td class="customTableSubHeading">Product Code</td>
                <td class="customTableSubHeading">Product</td>
                <td class="customTableSubHeading">Batch Code</td>
                <td class="customTableSubHeading">Expiry Date</td>
                <td class="customTableSubHeading">Qty Picked</td>
            </tr>
HTML;
        foreach ($summary as $sum)
        {
            if (is_null($sum['product_id']))
            {
                $content .= <<<HTML
                <tr>
                    <td class="customTableData" colspan="4" style="text-align: right;">Total</td>
                    <td class="customTableData">{$sum['qty_picked']}</td>
                </tr>
HTML;
            }
            else
            {
                $content .= <<<HTML
                <tr>
                    <td class="customTableData">{$sum['product_code']}</td>
                    <td class="customTableData">{$sum['product']}</td>
                    <td class="customTableData">{$sum['batch_code']}</td>
                    <td class="customTableData">{$sum['expiry_date']}</td>
                    <td class="customTableData">{$sum['qty_picked']}</td>
                </tr>
HTML;
            }
        }

        $content .= <<<HTML
        </table>
        <h2>Shipment {$tripSheetId} Details</h2>
        <table class="customTable" style="width: 100%; border-spacing: 0; border-collapse: collapse;">
            <tr>
                <td class="customTableSubHeading">Product Code</td>
                <td class="customTableSubHeading">Product</td>
                <td class="customTableSubHeading">Batch Code</td>
                <td class="customTableSubHeading">Expiry Date</td>
                <td class="customTableSubHeading">Qty Picked</td>
            </tr>
HTML;
        $currentIndex = 0;
        $previousIndex = $currentIndex - 1;
        foreach ($this->data as $data)
        {
            $currentOrderId = $this->data[$currentIndex]['order_id'];
            $header = '';
            if ($currentIndex === 0 || (isset($this->data[$previousIndex]['order_id']) && $this->data[$previousIndex]['order_id'] !== $currentOrderId && $currentOrderId != ''))
            {
                $header = <<<HTML
                <tr>
                    <td class="customTableHeading" colspan="5">Order # {$data['order_id']}</td>
                </tr>
HTML;
            }

            $content .= $header;
            if (is_null($data['product_id']))
            {
                $content .= <<<HTML
                <tr><td class="customTableData" style="text-align: right;" colspan="4">Total</td><td class="customTableData">{$data['qty_picked']}</td></tr>
HTML;
            }
            else
            {
                $content .= <<<HTML
                <tr>
                    <td class="customTableData">{$data['product_code']}</td>
                    <td class="customTableData">{$data['product']}</td>
                    <td class="customTableData">{$data['batch_code']}</td>
                    <td class="customTableData">{$data['expiry_date']}</td>
                    <td class="customTableData">{$data['qty_picked']}</td>
                </tr>
HTML;
            }

            ++$currentIndex;
            ++$previousIndex;
        }
        $content .= <<<HTML
        </table>
HTML;

        if (!$html)
        {
            Yii::import('application.vendors.mpdf.*');
            $mpdf = new mpdf('win-1252', 'A4');
            $mpdf->setAutoBottomMargin = 'pad';
            //            $mpdf->SetProtection(array('print'));
            $mpdf->SetDefaultBodyCSS('font-family', 'Arial');
            $mpdf->SetDisplayMode('fullpage');
            $mpdf->WriteHTML($this->show($content));
            $mpdf->Output('Loaded Shipment ' . $tripSheetId . '.pdf', 'I');
            die();
        }
        else
        {
            return $this->show($content);
        }
    }

    public function createCreditNote($orderId, $outputHtml = false, $inline = false)
    {
        list($totalsOrdered, $totalsReturns) = $this->getPDFTotals();

        $db = Yii::app()->db;
        if (isset($this->data['returns'][0]['bakery_id']))
        {
            $bakeryId = $this->data['returns'][0]['bakery_id'];
        }
        else
        {
            $bakeryId = Order_main::model()->findByPk($orderId)->bakery_id;
        }

        $sql = "select *
    from document_template dt
left join document_template_options dto on dt.document_template_id=dto.document_template_id
    where bakery_id=" . $bakeryId . " and document_type_id=3";
        $creditNoteTemplate = $db->createCommand($sql)->queryAll();

        $title = 'Credit Note';
        $issued_by_text = ' Issued By';
        $credit_note_num_text = 'Credit Note No';
        $issued_date_text = 'Issued Date';
        $cn_date_text = 'Date';
        $to_text = 'TO';
        $from_text = 'FROM';
        $comments_text = 'Comments';
        $cn_total_text = 'Total';
        $return_items_text = 'Return';
        $b_excl_vat_text = 'Excl Vat';
        $b_vat_text = 'Vat';
        $b_incl_vat_text = 'Incl Vat';
        $final_vat_text = 'Vat';
        $info_vat_text = 'Vat';
        $issued_by_status = 0;
        $credit_note_num_status = 0;
        $issued_date_status = 0;
        $cn_date_status = 0;
        $document_footer = "";
        $qtyTxt = 'Qty';
        $skuTxt = 'Sku';
        $unitPriceText = 'Unit Price';
        $signatureText = 'Signature';
        $signatureStatus = 1;
        $signatureTwoText = 'Driver Signature';
        $signatureTwoStatus = 1;
        $ref_number_text = "REF #";
        $ref_number_by_status = 1;
        $finalVatStatus = 1;
        $cnTotalStatus = 1;
        $documentFooterStatus = 1;
        $customerNameText = 'Customer Name';
        $customerContactsText = 'Customer Contact Number';
        if (count($creditNoteTemplate) > 0)
        {
            foreach ($creditNoteTemplate as $options)
            {
                switch ($options['label'])
                {
                    case 'signature_text':
                        $signatureText = $options['translation'];
                        $signatureStatus = $options['status'];
                        break;

                    case 'signature_two_text':
                        $signatureTwoText = $options['translation'];
                        $signatureTwoStatus = $options['status'];
                        break;

                    case 'return_items_qty':
                        $qtyTxt = $options['translation'];
                        break;

                    case 'return_items_sku':
                        $skuTxt = $options['translation'];
                        break;

                    case 'return_items_unit_price':
                        $unitPriceText = $options['translation'];
                        break;

                    case 'title':
                        $title = $options['translation'];
                        break;

                    case 'issued_by_text':
                        $issued_by_text = $options['translation'];
                        $issued_by_status = $options['status'];
                        break;

                    case 'credit_note_num_text':
                        $credit_note_num_text = $options['translation'];
                        $credit_note_num_status = $options['status'];
                        break;


                    case 'issued_date_text':
                        $issued_date_text = $options['translation'];
                        $issued_date_status = $options['status'];
                        break;

                    case 'cn_date_text':
                        $cn_date_text = $options['translation'];
                        $cn_date_status = $options['status'];

                        break;

                    case 'from_text':
                        $from_text = $options['translation'];
                        break;

                    case 'to_text':
                        $to_text = $options['translation'];
                        break;

                    case 'comments_text':
                        $comments_text = $options['translation'];
                        break;

                    case 'info_vat_text':
                        $info_vat_text = $options['translation'];
                        break;
                    case 'return_items_text':
                        $return_items_text = $options['translation'];
                        break;

                    case 'b_excl_vat_text':
                        $b_excl_vat_text = $options['translation'];
                        break;

                    case 'b_vat_text':
                        $b_vat_text = $options['translation'];
                        $bVatStatus = $options['status'];
                        break;

                    case 'b_incl_vat_text':
                        $b_incl_vat_text = $options['translation'];
                        break;


                    case 'final_vat_text':
                        $final_vat_text = $options['translation'];
                        $finalVatStatus = $options['status'];
                        break;

                    case 'cn_total_text':
                        $cn_total_text = $options['translation'];
                        $cnTotalStatus = $options['status'];
                        break;
                    case 'document_footer_text':
                        $document_footer = $options['translation'];
                        $documentFooterStatus = $options['status'];
                        break;
                    case 'ref_number':
                        $ref_number_text = $options['translation'];
                        $ref_number_by_status = $options['status'];
                        break;
                    case 'customer_name_text':
                        $customerNameText = $options['translation'];
                        break;
                    case 'customer_contacts_text':
                        $customerContactsText = $options['translation'];
                        break;
                }
            }
        }

        $includeOrExclude = 'Excl';
        if (Yii::app()->params['vat'] == 0)
        {
            $includeOrExclude = 'Excl';
        }

        $issued_by_status_html = '';
        if ($issued_by_status == 1)
        {
            $issued_by_status_html = <<<HTML
                   <td class="customTableDataBlanks bold fit">{$issued_by_text}:</td>
                    <td class="customTableDataBlanks">{$this->data['ordered'][0]['ordered_by']}</td>
HTML;
        }
        $order_number = '';
        $ref_number = '';
        if ($this->data['ordered'][0]['use_document_sequencing'] == 1)
        {
            $order_number .= $this->data['ordered'][0]['cn_document_prefix'] . $this->data['ordered'][0]['cn_document_number'];
            $ref_number .= $this->data['ordered'][0]['purchase_order_number'];
            if ($order_number == '')
            {
                $ref_number_by_status = 0;
                $order_number .= $this->data['ordered'][0]['purchase_order_number'];
            }
        }
        else
        {
            $order_number .= $this->data['ordered'][0]['purchase_order_number'];
        }
        $credit_note_num_status_html = '';
        if ($credit_note_num_status == 1)
        {
            $credit_note_num_status_html = <<<HTML
                     <td class="customTableDataBlanks bold fit">{$credit_note_num_text}:</td>
                    <td class="customTableDataBlanks bold" style="font-size: 16px;">{$order_number}</td>
HTML;
        }

        $issued_date_status_html = '';
        if ($issued_date_status == 1)
        {
            $issued_date_status_html = <<<HTML
              <td class="customTableDataBlanks bold fit">{$issued_date_text}:</td>
                    <td class="customTableDataBlanks">{$this->data['ordered'][0]['order_date']}</td>
HTML;
        }

        $cn_date_status_html = '';
        if ($cn_date_status == 1)
        {
            $cn_date_status_html = <<<HTML
              <td class="customTableDataBlanks bold fit">{$cn_date_text}:</td>
                    <td class="customTableDataBlanks">{$this->data['ordered'][0]['expected_delivery_date']}</td>
HTML;
        }
        $refNumber_Html = '';
        if ($this->data['ordered'][0]['use_document_sequencing'] == 1 && $ref_number_by_status == 1)
        {
            $refNumber_Html = <<<HTML
                <td class="customTableDataBlanks bold fit">{$ref_number_text}:</td>
                    <td class="customTableDataBlanks">{$ref_number}</td>
HTML;
        }
        $vat_replace_from_string = str_replace("Vat #", $info_vat_text, $this->data['ordered'][0]['customer_vat_number_from']);
        $vat_replace_to_string = str_replace("Vat #", $info_vat_text, $this->data['ordered'][0]['customer_vat_number_to']);

        $row1 = CustomUtils::pdfRowDetails($issued_by_status_html, $credit_note_num_status_html);
        $row2 = CustomUtils::pdfRowDetails($issued_date_status_html, $cn_date_status_html);
        $row3 = CustomUtils::pdfRowDetails($refNumber_Html, '');
        $branchAddress = "";
        if ($this->data['ordered'][0]['branch_company_id'] > 0)
        {
            $vat_replace_from_string = str_replace("Vat #", $info_vat_text, "Vat #: " . $this->data['ordered'][0]['company_vat_number']);
            $branchAddress = <<<HTML
                        {$this->data['ordered'][0]['company_description']}<br/>
                        {$this->data['ordered'][0]['company_physical_address']}<br/>
                        {$this->data['ordered'][0]['company_postal_address']}<br/>
                        {$this->data['ordered'][0]['company_phone']}<br/>
                        {$vat_replace_from_string}
HTML;
        }
        else
        {
            $branchAddress = <<<HTML
                        {$this->data['ordered'][0]['customer_name_from']}<br/>
                        {$this->data['ordered'][0]['customer_address_from']}<br/>
                        {$this->data['ordered'][0]['customer_postal_address_from']}<br/>
                        {$this->data['ordered'][0]['customer_tel_from']}<br/>
                        {$vat_replace_from_string}
HTML;
        }
        $content = <<<HTML
            <table class="customTableBlanks" style="margin-bottom: 15px;">
                {$row1}
                {$row2}
                {$row3}
            </table>
            
            <table class="customTable" style="margin-bottom: 15px;">
                <tr>
                    <td class="customTableSubHeading" style="width: 50%;">{$from_text}</td>
                    <td class="customTableSubHeading" style="width: 50%;">{$to_text}</td>
                </tr>
                <tr>
                    <td class="customTableData" style="vertical-align: top;">
                        
                        {$branchAddress}
                    </td>
                    <td class="customTableData" style="vertical-align: top;">
                        {$this->data['ordered'][0]['customer_account_number_to']}<br/>
                        {$this->data['ordered'][0]['customer_name_to']}<br/>
                        {$this->data['ordered'][0]['customer_address_to']}<br/>
                        {$this->data['ordered'][0]['customer_tel_to']}<br/>
                        {$this->data['ordered'][0]['customer_cell_to']}<br/>
                        {$vat_replace_to_string}
                    </td>
                </tr>
HTML;
        if (isset($this->data['returns'][0]['comments']) && $this->data['returns'][0]['comments'] !== '')
        {
            $content .= <<<HTML
            <tr>
                <td class="customTableSubHeading" colspan="2">{$comments_text}</td>
            </tr>
            <tr>
                <td class="customTableData" colspan="2">{$this->data['returns'][0]['comments']}</td>
            </tr>
HTML;
        }
        elseif (isset($this->data['ordered'][0]['comments']) && $this->data['ordered'][0]['comments'] !== '')
        {
            $content .= <<<HTML
            <tr>
                <td class="customTableSubHeading" colspan="2">{$comments_text}</td>
            </tr>
            <tr>
                <td class="customTableData" colspan="2">{$this->data['ordered'][0]['comments']}</td>
            </tr>
HTML;
        }
        $content .= <<<HTML
        </table>
        
        <table class="customTable">
HTML;
        //returns
        $returnedTotal = 0;
        $returns = '';
        if (isset($this->data['returns'][0]['returned_product']) && !is_null($this->data['returns'][0]['returned_product']))
        {
            $returns .= <<<HTML
            <tr>
                <td class="customTableSubHeading center fit">{$qtyTxt}</td>
                <td class="customTableSubHeading center fit">{$skuTxt}</td>
                <td class="customTableSubHeading center">{$return_items_text}</td>
                <td class="customTableSubHeading center fit">{$unitPriceText}</td>
                <td class="customTableSubHeading center fit">{$b_excl_vat_text} </td>
                <td class="customTableSubHeading center fit">{$b_vat_text}</td>
                <td class="customTableSubHeading center fit">{$b_incl_vat_text}</td>
            </tr>
HTML;
            $returnedTotalFormatted = '0.00';
            if (!empty($this->data['returns']))
            {
                $currentIndex = 0;
                $nextIndex = $currentIndex + 1;
                $bakeryOrderBy = isset($this->data['returns'][0]['bakery_distribution_order_by_id']) ? $this->data['returns'][0]['bakery_distribution_order_by_id'] : 0;
                for ($i = 0; $i < count($this->data['returns']); $i++)
                {
                    $batchCode = $this->data['returns'][$i]['batch_code'];
                    $batchCodeDisplay = '<br>' . $batchCode;
                    if (is_null($batchCode))
                    {
                        $batchCodeDisplay = '';
                    }
                    $expiryDate = $this->data['returns'][$i]['expiry_date'];
                    $expiryDateDisplay = '<br>' . $expiryDate;
                    if (is_null($expiryDate) || $expiryDate === '0000-00-00')
                    {
                        $expiryDateDisplay = '';
                    }
                    $tradeReplacementReason = ($this->data['returns'][$i]['trade_replacement_reason'] != '') ? '<br>' . $this->data['returns'][$i]['trade_replacement_reason'] : '';
                    $orderedItemTotalIncl = $this->data['returns'][$i]['returned_item_total_incl'];
                    $orderedItemTotalInclFormatted = FieldFormatter::format(FIELD_FORMATTER_DECIMAL_TWO, $orderedItemTotalIncl);
                    $itemTotal = floatval($this->data['returns'][$i]['returned_item_total']);
                    $returnedTotal += $itemTotal;
                    //$returnedTotal += $orderedItemTotalIncl;
                    $formatted = FieldFormatter::format(FIELD_FORMATTER_DECIMAL_TWO, $itemTotal);
                    $orderedItemVatValue = $this->data['returns'][$i]['returned_item_vat_value'];
                    $orderedItemVatValueFormatted = FieldFormatter::format(FIELD_FORMATTER_DECIMAL_TWO, $orderedItemVatValue);
                    $returns .= <<<HTML
                <tr>
                    <td class="customTableData fit">{$this->data['returns'][$i]['qty']}</td>
                    <td class="customTableData fit">{$this->data['returns'][$i]['product_sku']}</td>
                    <td class="customTableData">{$this->data['returns'][$i]['returned_product']}{$batchCodeDisplay}{$expiryDateDisplay}{$tradeReplacementReason}</td>
                    <td class="customTableData right fit">{$this->data['returns'][$i]['price']}</td>
                    <td class="customTableData right fit">{$formatted}</td>
                    <td class="customTableData right fit">{$orderedItemVatValueFormatted}</td>
                    <td class="customTableData right fit">{$orderedItemTotalInclFormatted}</td>
                </tr>
HTML;
                    switch($bakeryOrderBy)
                    {
                        case 2:
                            $returns .= $this->getSubTotalsPDFs('returns', $totalsReturns, $currentIndex, $nextIndex, 'type', 'category', 6);
                        break;

                        case 3:
                            $returns .= $this->getSubTotalsPDFs('returns', $totalsReturns, $currentIndex, $nextIndex, 'supplier', 'brand', 6);
                        break;

                        case 4:
                            $returns .= $this->getSubTotalsPDFs('returns', $totalsReturns, $currentIndex, $nextIndex, 'unit_of_measure', 'uom', 6);
                        break;

                        case 5:
                            $returns .= $this->getSubTotalsPDFs('returns', $totalsReturns, $currentIndex, $nextIndex, 'unit_of_measure', 'uom', 6);
                        break;

                        case 6:
                            $returns .= $this->getSubTotalsPDFs('returns', $totalsReturns, $currentIndex, $nextIndex, 'unit_of_measure', 'uom', 6);
                        break;

                        default:
                            $returns .= '';
                        break;
                    }
                    ++$currentIndex;
                    ++$nextIndex;
                }
                $returnedTotalFormatted = FieldFormatter::format(FIELD_FORMATTER_DECIMAL_TWO, $returnedTotal);
            }
            else
            {
                $returns .= <<<HTML
            <tr>
                <td colspan="4">-</td>
                <td style="text-align: right;">0.00</td>
            </tr>
HTML;
            }
        }

        $vat = $this->data['ordered'][0]['vat_value'];
        $vatFormatted = FieldFormatter::format(FIELD_FORMATTER_DECIMAL_TWO, $vat);
        $total = $returnedTotal + $vat;
        if ((int)$total === 0) //credit note generated from backend, it will have no returns
        {
            $total = $this->data['ordered'][0]['order_total_amount'];
        }
        $totalFormatted = FieldFormatter::format(FIELD_FORMATTER_DECIMAL_TWO, $total);
        $vatPercentage = $this->data['ordered'][0]['vat_value'] . '%';
        $cashCollected = $this->data['ordered'][0]['cash_collected'];
        $finalTotal = $totalFormatted;
        $showVat = '';
        
        if ($finalVatStatus == 1)
        {
            $showVat = <<<HTML
                            <tr>
                                <td colspan="4" class="customTableData right">{$final_vat_text}</td>
                                <td class="customTableData right">{$vatFormatted}</td>
                            </tr>
HTML;
        }
        $content .= <<<HTML
            {$returns}
            {$showVat}
HTML;

        if ($cnTotalStatus == 1)
        {
            $content .= <<<HTML
    <tr>
        <td colspan="4" class="customTableData right bold">{$cn_total_text}</td>
        <td class="customTableData right bold">{$finalTotal}</td>
    </tr>
HTML;
        }


        if (Yii::app() instanceof CConsoleApplication)
        {
            $baseUrl = Yii::app()->params['base_URL'];
        }
        else
        {
            $baseUrl = Yii::app()->getBaseUrl(true);
        }

        $signature = '';
        $customer_info = '';
        if($this->data['ordered'][0]['delivery_customer_name'] !== '')
        {
                $customer_info .= <<<HTML
            <br/>            
            {$this->data['ordered'][0]['delivery_customer_name']}<br/>
            {$this->data['ordered'][0]['delivery_customer_contact_number']}
HTML;

        }
        if (isset($this->data['returns'][0]['presales_signature_url']))
        {
            $folder = 'VanSalesSignatures';

            $imageUrl = $baseUrl . '/images/api/' . $folder . '/' . $this->data['returns'][0]['presales_signature_url'];

            if ($signatureStatus == 1)
            {
                if ($this->data['returns'][0]['presales_signature_url'] !== '' && file_exists(Yii::app()->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'api' . DIRECTORY_SEPARATOR . $folder . DIRECTORY_SEPARATOR . $this->data['returns'][0]['presales_signature_url'] . '.jpg'))
                {
                    $signature = <<<HTML
            <tr>
                <td colspan="4" class="customTableData right">{$signatureText}</td>
                <td class="customTableData center" colspan="3"><img alt="signature not found" style="height: 50px;" src="{$imageUrl}.jpg" />{$customer_info}</td>
            </tr>
HTML;
                    $customer_info = ''; // Reset
                }
            }
            

        }
            
        if($customer_info != ''){ // No Signature, but has customer details.s
                    $signature = <<<HTML
            <tr>
                <td colspan="4" class="customTableData right">{$signatureText}</td>
                <td class="customTableData center" colspan="3">{$customer_info}</td>
            </tr>
HTML;
        }
        $signatureTwo = '';
        if (isset($this->data['returns'][0]['presales_driver_signature_url']))
        {
            $folder = 'VanSalesSignatures';

            $imageUrl = $baseUrl . '/images/api/' . $folder . '/' . $this->data['returns'][0]['presales_driver_signature_url'];

            if ($signatureTwoStatus == 1)
            {
                if ($this->data['returns'][0]['presales_driver_signature_url'] !== '' && file_exists(Yii::app()->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'api' . DIRECTORY_SEPARATOR . $folder . DIRECTORY_SEPARATOR . $this->data['returns'][0]['presales_driver_signature_url'] . '.jpg'))
                {
                    $signatureTwo = <<<HTML
            <tr>
                <td colspan="4" class="customTableData right">{$signatureTwoText}</td>
                <td class="customTableData center" colspan="3"><img alt="signature not found" style="height: 50px;" src="{$imageUrl}.jpg" /></td>
            </tr>
HTML;
                }
            }
        }

        $content .= <<<HTML
            {$signature}
            {$signatureTwo}
        </table>
HTML;

        if ($documentFooterStatus == 1)
        {
            $content .= <<<HTML
    <p class="center" style="font-size:9px !important">{$document_footer}</p>
HTML;
        }
        if ($outputHtml)
        {
            return $this->show($content, $bakeryId);
        }
        Yii::import('application.vendors.mpdf.*');
        $this->pdfFolder .= 'tmp' . DIRECTORY_SEPARATOR;
        if (!file_exists($this->pdfFolder)) mkdir($this->pdfFolder, 0777, true);
        $mpdf = new mpdf('win-1252', 'A4', 0, '', PDF_MGL, PDF_MGR, PDF_MGT, PDF_MGB, PDF_MGH, PDF_MGF);
        //        $mpdf->SetProtection(array('print'));
        $mpdf->SetWatermarkText($title);
        $this->heading = $title;
        $mpdf->showWatermarkText = true;
        $mpdf->watermark_font = 'DejaVuSansCondensed';
        $mpdf->watermarkTextAlpha = 0.1;
        $mpdf->WriteHTML($this->show($content, $bakeryId));
        $filename = $this->pdfFolder . 'Credit Note ' . $orderId . '.pdf';
        if ($inline)
        {
            $mpdf->Output('', 'I');
        }
        else
        {
            file_put_contents($filename, $mpdf->Output('', 'S'));
            chmod($filename, 0777);
        }
        return $filename;
    }

    public function createCompletedLoad($tripId, $content)
    {
        Yii::import('application.vendors.mpdf.*');
        $mpdf = new mpdf('win-1252', 'A4');
        $mpdf->useOnlyCoreFonts = true;    // false is default
        //        $mpdf->SetProtection(array('print'));
        $mpdf->SetDefaultBodyCSS('font-family', 'Arial');
        $mpdf->SetDisplayMode('fullpage');
        $mpdf->WriteHTML($this->show($content));
        $mpdf->Output('completed_load_' . $tripId . '.pdf', 'D');
        die();
    }

    public function createCashSettlement($tripId, $content)
    {
        Yii::import('application.vendors.mpdf.*');
        $mpdf = new mpdf('win-1252', 'A4');
        $mpdf->useOnlyCoreFonts = true;    // false is default
        //        $mpdf->SetProtection(array('print'));
        $mpdf->SetDefaultBodyCSS('font-family', 'Arial');
        $mpdf->SetDisplayMode('fullpage');
        $mpdf->WriteHTML($this->show($content));
        $mpdf->Output('cash_settlement_' . $tripId . '.pdf', 'D');
        die();
    }

    public function createPlannedLoadPickingSheet($tripId, $content)
    {
        Yii::import('application.vendors.mpdf.*');
        $mpdf = new mpdf('win-1252', 'A4-L');
        $mpdf->useOnlyCoreFonts = true;    // false is default
        //        $mpdf->SetProtection(array('print'));
        $mpdf->SetDefaultBodyCSS('font-family', 'Arial');
        $mpdf->SetDisplayMode('fullpage');
        $mpdf->WriteHTML($this->show($content));
        $mpdf->Output('planned_load_picking_sheet_' . $tripId . '.pdf', 'D');
        die();
    }

    public function createStatement($content, $bakeryId, $title, $html = false)
    {
        if (!$html)
        {
            Yii::import('application.vendors.mpdf.*');
            $mpdf = new mpdf('win-1252', 'A4', 0, '', PDF_MGL, PDF_MGR, PDF_MGT, PDF_MGB, PDF_MGH, PDF_MGF);
            //            $mpdf->SetProtection(array('print'));
            $mpdf->SetTitle($title);
            $mpdf->SetWatermarkText('Statement');
            $mpdf->showWatermarkText = true;
            $mpdf->watermark_font = 'DejaVuSansCondensed';
            $mpdf->watermarkTextAlpha = 0.1;
            $mpdf->WriteHTML($this->show($content, $bakeryId));
            $mpdf->Output('', 'I');
        }
        else
        {
            echo $this->show($content, $bakeryId);
        }
    }

    public function getCreatedStatement($content, $bakeryId, $title, $html = false)
    {
        if (!$html)
        {
            Yii::import('application.vendors.mpdf.*');
            $this->pdfFolder .= 'tmp' . DIRECTORY_SEPARATOR;
            if (!file_exists($this->pdfFolder)) mkdir($this->pdfFolder, 0777, true);
            $mpdf = new mpdf('win-1252', 'A4', 0, '', PDF_MGL, PDF_MGR, PDF_MGT, PDF_MGB, PDF_MGH, PDF_MGF);
            //                $mpdf->SetProtection(array('print'));
            $mpdf->SetWatermarkText($title);
            $this->heading = $title;
            $mpdf->SetWatermarkText('Statement');
            $mpdf->watermark_font = 'DejaVuSansCondensed';
            $mpdf->watermarkTextAlpha = 0.1;
            $mpdf->WriteHTML($this->show($content, $bakeryId));
            $filename = $this->pdfFolder . 'Statement ' . date("YmdHis") . '.pdf';
            file_put_contents($filename, $mpdf->Output('', 'S'));
            chmod($filename, 0777);
            return $filename;
        }
        else
        {
            echo $this->show($content, $bakeryId);
        }
    }

    public function createImageGalleryPDF($bakeryId, $outputHtml = false, $inline = false)
    {
        $title = 'Image Gallery Export';
        $imagesArray = $this->data;
        $formscategory = '';
        $countBreak = 0;

        $content = "";
        foreach ($imagesArray as $key => $value)
        {

            foreach ($value as $image)
            {

                $URLProcessed = str_replace($_SERVER['SERVER_NAME'] . 'https', 'https', $image['image_path']);
                $str = '';
                if (strpos($image['image_path'], "/") > 0)
                {
                    $str = $URLProcessed;
                }
                else
                {
                    $str = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME'] . $URLProcessed;
                }
                $image['image_path'] = $str;
                $path = parse_url($str, PHP_URL_PATH);
                $path = $_SERVER['DOCUMENT_ROOT'] . $path;
                if (@file_get_contents($path))
                {

                    $result_datetime = isset($image['result_datetime']) ? $image['result_datetime'] : "0000-00-00";
                    $comments = "";
                    if ($image['type'] != 'Additional Answers' && $image['type'] != 'Form')
                    {
                        $comments = <<<HTML
                            <span ><b>Comments:</b></span><br/>
                                <span>{$image['comments']}</span><br/>
HTML;
                    }
                    
                    $content .= <<<HTML
                        <table class="customTable" style="page-break-inside: avoid;">
HTML;
                    
                    $imageUrlHtml = '';
                    if(isset($image['logo_url']) && !empty($image['logo_url']))
                    {
                        $imageUrlHtml = '<img src="'.$image["logo_url"].'" style="max-height:50px; float:left;"><br>';
                    }
                    if(isset($image['category_name_original']) && $formscategory !== $image['category_name_original'])
                    {
                        $titleOnly = $image['category_name_original'] . ' - ' . $image['checklist_name'];
                         $content .= <<<HTML
                            <tr style="page-break-inside: avoid;">
                            <td class="customTableSubHeading">{$imageUrlHtml}<h1>{$titleOnly}</h2></td>
                        </tr>
HTML;
                        $formscategory = $image['category_name_original'];
                    }
                    
                    $content .= <<<HTML
                <tr style="page-break-inside: avoid;">
                    <td class="customTableSubHeading"><h2>{$key}</h2></td>
                </tr>
                <tr style="page-break-inside: avoid;">
                    <td class="customTableData" style="vertical-align: top;">
                    <table style="width:100%">

                        <tr>
                            <td style="border: 1px solid #FFFFFF;vertical-align: top;"><img style="height: 400px;" src="{$image['image_path']}" alt="" /></td>
                            <td style="border: 1px solid #FFFFFF;vertical-align: top;font-size:12px;padding-left:15px"><span><b>{$result_datetime}</b></span><br/>
                                <span >{$image['employee_name']} ({$image['customer']}) </span><br/><br/>
                                <span >{$image['category_name']}</span><br/><br/>
                                {$comments}
                            </td>
                        </tr>
                    </table>
                        
                    </td>
                    
                </tr>
</table>
HTML;
                }
                
            }


            /**/
        }
        $content .= <<<HTML

HTML;

        Yii::import('application.vendors.mpdf.*');
        $this->pdfFolder .= 'tmp' . DIRECTORY_SEPARATOR;
        if (!file_exists($this->pdfFolder)) mkdir($this->pdfFolder, 0777, true);
        $mpdf = new mpdf('win-1252', 'A4', 0, '', PDF_MGL, PDF_MGR, PDF_MGT, PDF_MGB, PDF_MGH, PDF_MGF);
        //        $mpdf->SetProtection(array('print'));
        $mpdf->SetWatermarkText($title);
        $this->heading = $title;
        $mpdf->showWatermarkText = false;
        $mpdf->watermark_font = 'DejaVuSansCondensed';
        $mpdf->watermarkTextAlpha = 0.1;
        $mpdf->SetTitle('Image gallery' . date("Y-m-d H:i:s"));
        $mpdf->WriteHTML($this->show($content, $bakeryId));
        $filename = $this->pdfFolder . 'Image gallery' . date("Y-m-d H:i:s") . '.pdf';
        if ($inline)
        {
            // $mpdf->Output('', 'I');
            $mpdf->Output('Image gallery ' . date("Y-m-d H:i:s") . '.pdf', 'I');
        }
        else
        {
            $mpdf->Output('Image gallery ' . date("Y-m-d H:i:s") . '.pdf', 'I');
            file_put_contents($filename, $mpdf->Output('', 'S'));
            chmod($filename, 0777);
        }
        // return $filename;
    }

    protected function getSubTotalsPDFs($ordersReturns, $totals, $cIndex, $nIndex, $itemIndex = 'supplier', $totalsIndex = 'brand', $colspan = 8)
    {
        $html = '';
        $orderBySubTotals = isset($this->data[$ordersReturns][0]['order_by_qty_subtotals']) ? $this->data[$ordersReturns][0]['order_by_qty_subtotals'] : 0;
        if($orderBySubTotals > 0)
        {
            $currentCategory = $this->data[$ordersReturns][$cIndex][$itemIndex];
            if(isset($totals[$totalsIndex][$currentCategory]) && isset($this->data[$ordersReturns][$nIndex][$itemIndex]) && $this->data[$ordersReturns][$nIndex][$itemIndex] != $currentCategory)
            {
                $html .= <<<HTML
                <tr>
                    <td class="customTableHeading center">{$totals[$totalsIndex][$currentCategory]}</td><td colspan="{$colspan}" class="customTableHeading left">{$currentCategory} Sub Total</td>
                </tr>
HTML;
            }
            else if(isset($totals[$totalsIndex][$currentCategory]) && !isset($this->data[$ordersReturns][$nIndex][$itemIndex]))
            {
                $html .= <<<HTML
                <tr>
                    <td class="customTableHeading center">{$totals[$totalsIndex][$currentCategory]}</td><td colspan="{$colspan}" class="customTableHeading left">{$currentCategory} Sub Total</td>
                </tr>
HTML;
            }
        }

        return $html;
    }

    protected function getSubTotalsPickingSheet($stock, $totals, $cIndex, $nIndex, $colspan, $itemIndex = 'supplier', $totalsIndex = 'brand')
    {
        $html = '';
        $currentCategory = $stock[$cIndex][$itemIndex];
        if (isset($totals[$totalsIndex][$currentCategory]) && isset($stock[$nIndex][$itemIndex]) && $stock[$nIndex][$itemIndex] != $currentCategory)
        {
            $html .= <<<HTML
            <tr>
                <td colspan="{$colspan}" class="customTableHeading right">{$currentCategory} Sub Total</td><td class="customTableHeading center">{$totals[$totalsIndex][$currentCategory]}</td><td class="customTableHeading"></td>
            </tr>
HTML;
        }
        else if (isset($totals[$totalsIndex][$currentCategory]) && !isset($stock[$nIndex][$itemIndex]))
        {
            $html .= <<<HTML
            <tr>
                <td colspan="{$colspan}" class="customTableHeading right">{$currentCategory} Sub Total</td><td class="customTableHeading center">{$totals[$totalsIndex][$currentCategory]}</td><td class="customTableHeading"></td>
            </tr>
HTML;
        }

        return $html;
    }

    protected function getSubTotalsPickingSheetSuggested($orders, $totals, $currenIndex, $nextIndex, $colspan, $itemIndex = 'supplier', $totalsIndex = 'brand')
    {
        $html = '';
        if (
            isset($totals[$totalsIndex][$orders[$currenIndex]['products'][$itemIndex]['description']])
            && isset($orders[$nextIndex]['products'][$itemIndex]['description'])
            && $orders[$nextIndex]['products'][$itemIndex]['description'] != $orders[$currenIndex]['products'][$itemIndex]['description']
        )
        {
            $html .= <<<HTML
            <tr><td class="customTableHeading right" colspan="{$colspan}">{$orders[$currenIndex]['products'][$itemIndex]['description']} Sub Total</td><td class="customTableHeading center">{$totals[$totalsIndex][$orders[$currenIndex]['products'][$itemIndex]['description']]}</td><td class="customTableHeading center"></td></tr>
HTML;
        }
        else if (
            isset($totals[$totalsIndex][$orders[$currenIndex]['products'][$itemIndex]['description']])
            && !isset($orders[$nextIndex]['products'][$itemIndex]['description'])
        )
        {
            $html .= <<<HTML
            <tr><td class="customTableHeading right" colspan="{$colspan}">{$orders[$currenIndex]['products'][$itemIndex]['description']} Sub Total</td><td class="customTableHeading center">{$totals[$totalsIndex][$orders[$currenIndex]['products'][$itemIndex]['description']]}</td><td class="customTableHeading center"></td></tr>
HTML;
        }

        return $html;
    }

    protected function getSubTotalsDispatchSheet($totals, $stockMovementItems, $currentIndex, $nextIndex, $itemIndex = 'supplier', $totalsIndex = 'brand')
    {
        $html = '';

        $theItem = $stockMovementItems[$currentIndex]['product'];
        if (
            isset($totals[$totalsIndex][$theItem->$itemIndex->description])
            && isset($stockMovementItems[$nextIndex]['product']->$itemIndex->description)
            && $stockMovementItems[$nextIndex]['product']->$itemIndex->description != $theItem->$itemIndex->description
        )
        {
            $html .= <<<HTML
            <tr>
                <td class="customTableHeading right" colspan="5">{$theItem->$itemIndex->description} Sub Total</td>
                <td class="customTableHeading right">{$totals[$totalsIndex][$theItem->$itemIndex->description]}</td>
            </tr>
HTML;
        }
        else if (isset($totals[$totalsIndex][$theItem->$itemIndex->description]) && !isset($stockMovementItems[$nextIndex]['product']->$itemIndex->description))
        {
            $html .= <<<HTML
            <tr>
                <td class="customTableHeading right" colspan="5">{$theItem->$itemIndex->description} Sub Total</td>
                <td class="customTableHeading right">{$totals[$totalsIndex][$theItem->$itemIndex->description]}</td>
            </tr>
HTML;
        }

        return $html;
    }

    protected function getSubTotalsReturnsSheet($totals, $stockMovementItems, $currentIndex, $nextIndex, $itemIndex = 'supplier', $totalsIndex = 'brand')
    {
        $html = '';

        $theItem = $stockMovementItems[$currentIndex]['product'];
        if (
            isset($totals[$totalsIndex][$theItem->$itemIndex->description])
            && isset($stockMovementItems[$nextIndex]['product']->$itemIndex->description)
            && $stockMovementItems[$nextIndex]['product']->$itemIndex->description != $theItem->$itemIndex->description
        )
        {
            $html .= <<<HTML
            <tr>
                <td class="customTableHeading right" colspan="4">{$theItem->$itemIndex->description} Sub Total</td>
                <td class="customTableHeading center">{$totals[$totalsIndex][$theItem->$itemIndex->description]}</td>
            </tr>
HTML;
        }
        else if (isset($totals[$totalsIndex][$theItem->$itemIndex->description]) && !isset($stockMovementItems[$nextIndex]['product']->$itemIndex->description))
        {
            $html .= <<<HTML
            <tr>
                <td class="customTableHeading right" colspan="4">{$theItem->$itemIndex->description} Sub Total</td>
                <td class="customTableHeading center">{$totals[$totalsIndex][$theItem->$itemIndex->description]}</td>
            </tr>
HTML;
        }

        return $html;
    }

    public function createTradeReplacementPDFforDriver($id, $bakeryId, $html = false)
    {
        $orderBy = Bakery::getDistributionProductsOrderBy($bakeryId, 'sql');
        $criteria = new CDbCriteria();
        $criteria->condition = 't.id = ' . $id;
        $criteria->order = $orderBy;
        $tradeReplacement = TradeReturnsMain::model()->with('customer', 'signature_url', 'collected_by', 'items.products.unitOfMeasure', 'items.products.supplier', 'items.products.type')->find($criteria);
        $bakery = Bakery::model()->findByPk($tradeReplacement['customer']['bakery_id']);
        //var_dump($tradeReplacement['customer']); die();
        
  
        $branchAddress = <<<HTML

                        {$bakery['description']}<br/>
                        {$bakery['physical_address']}<br/><br />
                        {$bakery['postal_address']}<br/><br />
                        {$bakery['phone']}<br/>
                     
HTML;
        
        
    
        $content = <<<HTML
           
            <table class="customTable">
            <tr>
                <td class="customTableDataBlanks bold" >Reference #:</td>
                <td class="customTableDataBlanks ">{$id}</td>
            </tr>
            <tr>
                <td class="customTableDataBlanks bold"  style="width: 25%;">Created By:</td>
                <td class="customTableDataBlanks"  style="width: 25%;">{$tradeReplacement['collected_by']['name']}</td>
            
                <td class="customTableDataBlanks bold"  style="width: 25%;">Created Date:</td>
                <td class="customTableDataBlanks"  style="width: 25%;">{$tradeReplacement['returns_collected_date']}</td>
            </tr>
        </table>
            <table class="customTable" style="margin-bottom: 15px;">
                <tr>
                    <td class="customTableSubHeading" style="width: 50%;">FROM</td>
                    <td class="customTableSubHeading" style="width: 50%;">TO</td>
                </tr>
                <tr>
                    <td class="customTableData" style="vertical-align: top;">
                        {$branchAddress}
                        
                        
                    </td>
                    <td class="customTableData" style="vertical-align: top;">
                        {$tradeReplacement['customer']['account_number']}<br/>
                        {$tradeReplacement['customer']['description']}<br/>
                        {$tradeReplacement['customer']['address']}<br/>
                        {$tradeReplacement['customer']['contact_tel']}<br/>
                        {$tradeReplacement['customer']['contact_cell']}
                    </td>

            </table>

        
        <table class="customTable">
            <tr>
                <td class="customTableSubHeading fit">Sku</td>
                <td class="customTableSubHeading">Product</td>
                <td class="customTableSubHeading fit">Batch Code</td>
                <td class="customTableSubHeading fit">Expiry Date</td>
                <td class="customTableSubHeading center" style="width: 75px;">Qty</td>
            </tr>
HTML;
        
        //var_dump($tradeReplacement['items']); die();
        foreach($tradeReplacement['items'] as $data)
        {
            
            $reason = TradeReturnsReasons::model()->findByPk($data['reason_id']);
           
            $product = $data['products'];
            $content .= <<<HTML
            <tr>
                <td class="customTableData fit">{$product['sku']}</td>
                <td class="customTableData">{$product['unitOfMeasure']['description']} - {$product['description']}<br />{$reason['description']}</td>
                <td class="customTableData fit">{$product['batch_code']}</td>
                <td class="customTableData fit">{$product['expiry_date']}</td>
                <td class="customTableData center">{$data['qty_collected']}</td>
            </tr>
HTML;
        }
        
        if ($tradeReplacement['signature_url'] != '')
        {
            $baseUrl = Yii::app()->getBaseUrl(true);
            $signatureUrl = $baseUrl . '/images/api/TradeReturnsSignatures/' . $tradeReplacement['signature_url'] . '.jpg';
            $signature = <<<HTML
            <img alt="signature not found" style="height: 50px;" src="{$signatureUrl}" />
HTML;
        }
        else
        {
            $signature = '';
        }
        
        $content .= <<<HTML
            <tr>
                <td class="customTableData right bold" colspan="2">Signature</td>
                <td class="customTableData center" colspan="3">{$signature}
</td>
            </tr>
        </table>
HTML;
        if (!$html)
        {
            $title = 'Trade Replacement';
            Yii::import('application.vendors.mpdf.*');
            $mpdf = new mpdf('win-1252', 'A4', 0, '', PDF_MGL, PDF_MGR, PDF_MGT, PDF_MGB, PDF_MGH, PDF_MGF);
            //                $mpdf->SetProtection(array('print'));
            $mpdf->SetWatermarkText($title);
            $this->heading = $title;
            $mpdf->showWatermarkText = true;
            $mpdf->watermark_font = 'DejaVuSansCondensed';
            $mpdf->watermarkTextAlpha = 0.1;
            $mpdf->WriteHTML($this->show($content, $bakeryId));

            $mpdf->Output('', 'I');
        }
        else
        {
            return $this->show($content, $bakeryId);
        }
    }
}
