<?php

class DebtorCreditorDashboardController extends Controller
{

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view'),
                'users'   => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update', 'addPayment', 'allocatePayments', 'savePaymentAllocation', 'openStatement', 'transactionInfo', 'creditNote', 'creditNoteSubmit', 'listPDFs', 'creditNoteResult', 'invoiceSkuItems','EmailStatemetForm','emailStatement'),
                'users'   => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete', 'addPayment', 'allocatePayments', 'savePaymentAllocation', 'openStatement', 'creditNote', 'creditNoteSubmit', 'listPDFs', 'creditNoteResult', 'invoiceSkuItems','emailStatement'),
                'users'   => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @param bool $reversal whether the payment came from the reversal form
     */
    public function actionCreate($reversal = false)
    {
        $model = new OutletTransactions;
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);
        if(isset($_POST['OutletTransactions']))
        {
            $creditAmount = $_POST['OutletTransactions']['credit_amount'];
            $_POST['OutletTransactions']['payment_allocation_status_id'] = 1;
            if($reversal)
            {
                $_POST['OutletTransactions']['description'] = 'REVERSAL - ' . $_POST['OutletTransactions']['description'];
            }
            else
            {
                $_POST['OutletTransactions']['description'] = 'Ref #' . $_POST['OutletTransactions']['description'];
            }
            $_POST['OutletTransactions']['create_employee_id'] = Yii::app()->user->id;
            $amount = floatval($_POST['OutletTransactions']['credit_amount']);
            if($amount < 0)
            {
                unset($_POST['OutletTransactions']['credit_amount']);
                $_POST['OutletTransactions']['debit_amount'] = $amount * (-1);//to get a positive number
            }
            $details = OutletTransactions::doAgaAnalysisLogic($_POST['OutletTransactions']['outlet_id'], 0, $creditAmount);
            $term = $details['term'];
            $amountDue = $details['due_amount'];
            $offset = $details['offset'];
            $reversalReasonId = null;
            $outletTransactionsId = null;
            if($reversal)
            {
                if(isset($_POST['OutletTransactions']['due_date']))
                {
                    $dueDate = $_POST['OutletTransactions']['due_date'];
                }
                else
                {
                    $dueDate = null;
                    $amountDue = 0;
                    $offset = 0;
                }
                $reversalReasonId = $_POST['OutletTransactions']['reversal_reasons_id'];
                $outletTransactionsId = $_POST['OutletTransactions']['outlet_transactions_id'];
            }
            else
            {
                $dueDate = $details['due_date'];
            }
            $balance = $details['balance'];
            $allocatedDate = $details['allocated_date'];
            $userId = $details['user_id'];
            $model->attributes = $_POST['OutletTransactions'];
            $model->created = date('Y-m-d H:i:s');
            $model->term = $term;
            $model->due_amount = $amountDue;
            $model->due_date = $dueDate;
            $model->invoice_number = isset($_POST['OutletTransactions']['invoice_number'])?$_POST['OutletTransactions']['invoice_number']:'';
            if($reversal)
            {
                $model->reversal_reasons_id = $reversalReasonId;
                $model->outlet_transactions_id = $outletTransactionsId;
            }
            $model->offset = $offset;
            $model->balance_prior_to_allocation = $balance;
            $model->allocated_date = $allocatedDate;
            $model->allocated_by = $userId;
            if($model->save())
            {
                // Update outlet available credit
                $outletCredit = new OutletCredit();
                $availableCredit = $outletCredit->updateAvailableCredit($_POST['OutletTransactions']['outlet_id']);
                $this->redirect(array('index', 'id' => $model->id));
               
            }
            else
            {
                //print_r($model->errors);
            }
        }
        $this->render('create', array(
            'model' => $model,
        ));
    }

//    public function actionInvoiceSkuItems($invoiceId)
//    {
//        $data = MailFactory::getInvoiceDetails($invoiceId);
//        $this->renderPartial('_againstSkuItems', ['data' => $data]);
//    }
    public function actionInvoiceSkuItems($invoiceId, $productIds = NULL)
    {
        $data = MailFactory::getInvoiceDetails($invoiceId, $productIds);
        $this->renderPartial('_againstSkuItems', ['data' => $data]);
    }

    public function actionCreditNote($outletId)
    {
        $data = [];
        $outlet = Outlet::model()->findByPk($outletId);
        $data['outlet'] = $outlet;
        $invoicesData = Order_main::model()->findAll([
            'condition' => 'order_status_id = 5 AND account = ' . $outletId . ' AND date(actual_delivery_date) >= date_sub(curdate(), interval 6 month)',
            'order'     => 'actual_delivery_date DESC']);
        $invoices = [];
        foreach($invoicesData as $inv)
        {
            $invoices[$inv['id']] = 'INV# ' . $inv['id'] . ' - ' . $inv['actual_delivery_date'] . ' (' . number_format($inv['delivered_order_total_amount'], 2, '.', ' ') . ')';
        }
        $data['invoices'] = $invoices;
        $this->renderPartial('_creditNote', ['data' => $data]);
    }

    public function actionCreditNoteSubmit()
    {
        $creditNoteComments = isset($_POST['reference']) ? $_POST['reference'] : '';
        if(isset($_POST['invoice_number']) && $_POST['invoice_number'] !== '')
        {
            $creditNoteComments = ' INV # ' . $_POST['invoice_number'] . '. ' . $_POST['reference'];
        }
        
        $vat = 0;
        $totalVatValue = 0.00;
        $ordersReturns = [];
        $orderTotalAmount = $_POST['amount'];
        $grossOrderTotalAmount = $_POST['amount'];

        if(isset($_POST['against_skus']) && $_POST['against_skus'] == 1)
        {
            if(isset($_POST['againstSkusItem']))
            {               
                $orderedItemsIds = implode(',', $_POST['againstSkusItem']);               
                $orderedItems = Orders::model()->findAll('id IN (' . $orderedItemsIds . ') AND order_id = ' . $_POST['invoice_number']);               
                $orderProductsArray = [];
                foreach($orderedItems as $item)
                {
                    $total_excluding_vat=$item['price']*$_POST['qtyAgainstSkusItemQty'][$item['id']];
                    $total_vat=$total_excluding_vat*($item['vat_percentage']/100);
                   
//                    DO NOT update orders Already placed
//                    $con = Yii::app()->db;
//                    $sql='UPDATE orders set vat_value="'.$total_vat.'", delivered_vat_value="'.$total_vat.'",qty_delivered='. $_POST['qtyAgainstSkusItemQty'][$item['id']].' where id='.$item['id'];
//                    $command = $con->createCommand($sql);
//                    $command->execute();
                    
                /*  $updateOrder= new Orders();
                    $updateOrder->id = $item['id'];
                    $updateOrder->vat_value = $total_vat;
                    $updateOrder->delivered_vat_value = $total_vat;
                    $updateOrder->qty_delivered = $_POST['qtyAgainstSkusItemQty'][$item['id']];
                    $updateOrder->save();*/
                    $orderProductsArray[] = $item['product_id'];
                    $ordersReturns[] = [
                        'product_id' => $item['product_id'],
                        'qty' => $_POST['qtyAgainstSkusItemQty'][$item['id']],
                        'return_type_id' => 0,
                        'vat' => $item['vat_percentage'],
                        'vat_value' => $total_vat,
                        'gross_price' => $item['price'],
                        'price' => $item['price'],
                        'discount' => 0
                    ];
                    
                    $totalVatValue += $total_vat;
                }
//                Non order SKUs
                $orderProductsString = implode(",", $orderProductsArray);
                $orderProductsString = !empty($orderProductsString) ? $orderProductsString : '0';
                $NonOrderedItems = Products::model()->findAll('id IN (' . $orderedItemsIds . ') AND id NOT IN (' . $orderProductsString . ')');               
                foreach($NonOrderedItems as $item)
                {
                    $total_excluding_vat = $item['price'] * $_POST['qtyAgainstSkusItemQty'][$item['id']];
                    $total_vat = $total_excluding_vat * ($item['vat']/100);
                    $total_price = $total_excluding_vat + $total_vat;
                    
                    if($total_price != $_POST['ItemTotalInc'][$item['id']])
                    {   
                        $total_excluding_vat = (($_POST['ItemTotalInc'][$item['id']] /($item['vat'] + 100)) * 100) ;// Rounding down 2 decimals;
                        $total_vat = ($_POST['ItemTotalInc'][$item['id']]  - $total_excluding_vat) ;
                        $total_price = $_POST['ItemTotalInc'][$item['id']];

                        $ordersReturns[] = [
                            'product_id' => $item['id'],
                            'qty' => $_POST['qtyAgainstSkusItemQty'][$item['id']],
                            'return_type_id' => 0,
                            'vat' => $item['vat'],
                            'vat_value' => $total_vat,
                            'gross_price' => ($_POST['ItemTotalInc'][$item['id']]  - $total_vat) / $_POST['qtyAgainstSkusItemQty'][$item['id']],
                            'price' => ($_POST['ItemTotalInc'][$item['id']] - $total_vat) / $_POST['qtyAgainstSkusItemQty'][$item['id']],
                            'discount' => 0
                        ];
                    }
                    else{
                        $ordersReturns[] = [
                            'product_id' => $item['id'],
                            'qty' => $_POST['qtyAgainstSkusItemQty'][$item['id']],
                            'return_type_id' => 0,
                            'vat' => $item['vat'],
                            'vat_value' => $total_vat,
                            'gross_price' => $item['price'],
                            'price' => $item['price'],
                            'discount' => 0
                        ];
                    }
//                        echo "<pre>";var_dump($ordersReturns);echo "</pre>";die;
                    $totalVatValue += $total_vat;
                }
                $totalVatValue = round($totalVatValue, 2);
                $grossOrderTotalAmount = round(($orderTotalAmount-$totalVatValue), 2);
            }
        }

        $employeeId = Yii::app()->user->id;
        $documentNumber = APIUtils::generateDocumentNumber($_POST['bakery_id'], $employeeId, 'credit_note');
        $requestData = [];
        $requestData['ignoreDeviceCheck'] = 1;
        $requestData['employee_id'] = $employeeId;
        $requestData['bakery_id'] = $_POST['bakery_id'];
        $requestData['outlet_id'] = $_POST['outlet_id'];
        $requestData['payment_type'] = 6;
        $requestData['comments'] = $creditNoteComments;
        $requestData['po'] = '';
        $requestData['invoice_number'] = date('YmdHis') . $employeeId;
        $requestData['document_number_prefix'] = $documentNumber['prefix'];
        $requestData['document_number'] = $documentNumber['number'];
        $requestData['vat'] = $vat;
        $requestData['total_vat_value'] = $totalVatValue;
        $requestData['invoice_discount'] = '';
        $requestData['order_status'] = 13;
        $requestData['order_type'] = 4;
        $requestData['tripsheetId'] = 0;
        $requestData['order_total_amount'] = $orderTotalAmount;
        $requestData['gross_order_total_amount'] = $grossOrderTotalAmount;
        $requestData['contact_email'] = $_POST['email'];
        $requestData['orders'] = [];
        $requestData['order_returns'] = $ordersReturns;
        $requestData['delivery_date'] = date('Y-m-d');
        $requestData['cash_collected'] = 0;
        $requestData['delivery_signature'] = '';
        $requestData['datetime_stamp'] = date('Y-m-d H:i:s');
        $requestData['DeviceID'] = '';
        $requestData['REG_KEY_VALUE'] = '';
        $requestData['RequestRef'] = date('YmdHis') . $employeeId;
        $requestData['Dt'] = date('Y-m-d H:i:s');
        $requestData['Type'] = 'saveOrder';
        $requestData['order_source'] = 2;
        $request = [
            'Request' => $requestData
        ];
        $apiUrl = Yii::app()->getBaseUrl(true) . '/index.php/api/central/webservice';
        $response = APIUtils::requestPOST($apiUrl, $request);
        if($response['http_code'] === 200)
        {
            $responseInfo = json_decode($response['message'], true);
            $orderId = $responseInfo['Data']['order_id'];
            try
            {
                CustomUtils::createOrderPDFs($orderId);
            }
            catch(Exception $e)
            {

            }
            //auto allocate the credit payment
            $transaction = OutletTransactions::model()->find('order_id = ' . $orderId . ' AND outlet_id = ' . $_POST['outlet_id']);
            $transactionData = [
                'id'                           => $transaction->id,
                'debit_amount'                 => $transaction->debit_amount,
                'credit_amount'                => $transaction->credit_amount,
                'payment_allocation_status_id' => 1,
                'outlet_id'                    => $_POST['outlet_id'],
            ];
            echo json_encode($transactionData);
        }
        else
        {
            echo 0;
        }
    }

    public function actionCreditNoteResult($transactionId)
    {
        $orderId = OutletTransactions::model()->findByPk($transactionId)->order_id;
        echo $this->renderPartial('_creditNoteResponse', ['orderId' => $orderId], true);
    }

    public function actionListPDFs($outletId)
    {
        $orders = Order_main::model()->with('outlet', 'employee', 'order_status')->findAll(['condition' => 'account = ' . $outletId . ' AND date(order_date) >= date_sub(curdate(), interval 3 month)', 'order' => 'order_date desc']);
        $this->renderPartial('_listPDFs', ['orders' => $orders]);
    }

    public function actionAddPayment($outletId)
    {
        $model = Outlet::model()->findByPk($outletId);
        $invoicesData = Order_main::model()->findAll([
            'condition' => 'order_status_id = 5 AND account = ' . $outletId . ' AND date(actual_delivery_date) >= date_sub(curdate(), interval 3 month)',
            'order'     => 'actual_delivery_date DESC']);
        $invoices = [];
        foreach($invoicesData as $inv)
        {
            $invoices[$inv['id']] = 'INV# ' . $inv['id'] . ' - ' . $inv['actual_delivery_date'] . ' (' . number_format($inv['delivered_order_total_amount'], 2, '.', ' ') . ')';
        }
        if($model === null)
            throw new CHttpException(404, 'The requested outlet does not exist.');
        $this->renderPartial('addPayment', array(
            'model' => $model,
            'invoices'=>$invoices
        ));
    }

    public function actionSavePaymentAllocation($outletId)
    {
        //--- UM-2221 ---//
       //var_dump($_POST);

        if(isset($_POST['what']) && $_POST['what'] === '0')
        {
            $_POST['credit_amount'] = 0;
            $_POST['debit_amount'] = 0;
        }
        $response = OutletTransactions::savePaymentAllocation($outletId);
        echo $response;
    }
    //---END UM-2221 ---//
 

    public function actionAllocatePayments($outletId, $dateFrom, $dateTo)
    {
        $sql = "SELECT
                   e.name, ot.*, IFNULL(pt.description, '-') AS payment_type,om.pdf_status,om.order_status_id,ocn.credit_note_invoice_number AS cn_invoice_number,
                    IFNULL(tsi.trip_sheet_id, '-') as trip_sheet_id
                FROM 
                    outlet_transactions ot
                LEFT JOIN
                    payment_type pt ON pt.id = ot.payment_type_id
                LEFT JOIN 
                    order_main om ON ot.order_id= om.id
                LEFT JOIN
                    trip_sheets_items tsi ON tsi.order_id = om.id
                JOIN
                    employee e ON  ot.create_employee_id = e.id
                LEFT JOIN orders_credit_notes ocn ON ot.order_id=ocn.order_id 
                WHERE 
                    `outlet_id` = '" . addslashes($outletId) . "' AND
                    date(`created`) BETWEEN '" . addslashes($dateFrom) . "' AND '" . addslashes($dateTo) . "' AND
                    `payment_allocation_status_id` = 0
                ORDER BY
                    `created` ASC";
       
        $rawData = Yii::app()->db->createCommand($sql);
        $count = Yii::app()->db->createCommand('SELECT COUNT(*) FROM (' . $sql . ') AS count_alias')->queryScalar();
        $model = new CSqlDataProvider($rawData, array(
            'keyField'       => 'id',
            'totalItemCount' => $count,
            'sort'           => array(
                'attributes'   => array(
                    'outlet_id',
                    'created'
                ),
                'defaultOrder' => array(
                    'outlet' => CSort::SORT_ASC //default sort value
                ),
            ),
            'pagination'     => array(
                'pageSize' => PHP_INT_MAX
            ),
        ));

        $dataProvider = $model;
        //--- UM-2221 - GET PAYMENT_TYPES
        $criteType = new CDbCriteria();
        $criteType->condition="status_id=1 Order By description ASC";
        //$dataPaymentTypes = $this->actionPaymentType();
        $dataPaymentTypes = PaymentType::model()->paymentTypesList();
        //END UM-2221:  GET PAYMENT_TYPES
        
        
        $this->renderPartial('allocatePayments', array(
            'dataProvider' => $dataProvider,
           'dataPaymentTypes'=>$dataPaymentTypes,
            'dateFrom'     => $dateFrom,
            'dateTo'       => $dateTo,
            'outletId'     => $outletId
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'index' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);
        if(isset($_POST['OutletTransactions']))
        {
            $model->attributes = $_POST['OutletTransactions'];
            if($model->save())
                $this->redirect(array('index', 'id' => $model->id));
        }
        // Get outlet id
        $outletTransactions = new OutletTransactions();
        $outletId = $outletTransactions->getOutletIdFromPK($id);
        // Update outlet available credit
        $outletCredit = new OutletCredit();
        $availableCredit = $outletCredit->updateAvailableCredit($outletId);
        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();
        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if(!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex($customerAccountNumber = null, $dateFrom = null, $dateTo = null, $dueDate = null)
    {
        $criteType = new CDbCriteria();
        $criteType->condition="status_id=1 Order By description ASC";
        //$dataPaymentTypes = PaymentType::model()->paymentTypesAll();
       // $dataPaymentTypes = $this->actionPaymentType();
        //print_r($dataPaymentTypes);

        $payment_type_updated_by = Yii::app()->user->id;
       // echo "User_ID:";
        //var_dump($payment_type_by_user_id);
        $extraWhere = '';
        if(!is_null($customerAccountNumber))
        {
            $customerAccountNumber = addslashes($customerAccountNumber);
            $extraWhere = <<<SQL
            AND (outlet.description LIKE '%{$customerAccountNumber}%' OR outlet.account_number LIKE '%{$customerAccountNumber}%')
SQL;

        }
        if(!isset($dateFrom))
        {
            $dateFrom = date('Y-m-01');
        }
        if(!isset($dateTo))
        {
            $dateTo = date("Y-m-t", strtotime($dateFrom));
        }
        $dateFrom = addslashes($dateFrom);
        $dateTo = addslashes($dateTo);
        $dateWhere = <<<SQL
        date(OT1.created) BETWEEN '{$dateFrom}' AND '{$dateTo}'
SQL;
        $dateWhere2 = <<<SQL
        date(OT5.created) BETWEEN '{$dateFrom}' AND '{$dateTo}'
SQL;
        if(!is_null($dueDate))
        {
            $dateWhere = <<<SQL
            OT1.due_date = '{$dueDate}'
SQL;
        }
        // Select Data for Datagrid
        $sql = "SELECT
                    OT1.outlet_id AS ol_id,
                    outlet.description AS outlet,
                    outlet.account_number,
                    IF (COALESCE((SELECT (SUM(OT2.credit_amount) - SUM(OT2.debit_amount))
                        FROM 
                            outlet_transactions AS OT2
                        WHERE OT2.outlet_id = OT1.outlet_id AND OT2.payment_allocation_status_id = 1),0) >= 0,CONCAT('CR',COALESCE((SELECT (SUM(OT2.credit_amount) - SUM(OT2.debit_amount)) 
                        FROM 
                            outlet_transactions AS OT2
                        WHERE OT2.outlet_id = OT1.outlet_id AND OT2.payment_allocation_status_id = 1),0)),COALESCE((SELECT (SUM(OT2.credit_amount) - SUM(OT2.debit_amount))
                        FROM 
                            outlet_transactions AS OT2
                        WHERE OT2.outlet_id = OT1.outlet_id AND OT2.payment_allocation_status_id = 1),0)) AS balance_due,
                    COALESCE((SELECT MAX(OT3.created) 
                        FROM 
                            outlet_transactions AS OT3 
                        WHERE OT3.debit_amount > 0 AND OT3.outlet_id = OT1.outlet_id AND OT3.payment_allocation_status_id = 1),'No debit history') AS last_debit_date,
                    COALESCE((SELECT MAX(OT4.created) 
                        FROM 
                            outlet_transactions AS OT4 
                        WHERE OT4.credit_amount > 0 AND OT4.outlet_id = OT1.outlet_id AND OT4.payment_allocation_status_id = 1),'No credit history') AS last_credit_date,
                    (SELECT 
                        COUNT(IF(OT5.payment_allocation_status_id = 0,1,0))
                    FROM 
                        outlet_transactions AS OT5 
                    WHERE 
                        OT5.outlet_id = OT1.outlet_id AND
                        OT5.payment_allocation_status_id = 0
                        AND {$dateWhere2}) AS pending_allocations,
                    outlet_credit.available_credit,
                    outlet_credit.credit_limit,
                    outlet_credit.payment_terms_days,
                    IFNULL(outlet_credit.block, 0) as block
                FROM 
                    outlet_transactions AS OT1
                LEFT JOIN
                    outlet
                    ON
                        OT1.outlet_id = outlet.id
                LEFT JOIN 
                    outlet_credit
                    ON
                        OT1.outlet_id = outlet_credit.outlet_id
                WHERE 
                    outlet.status_id = 1 AND {$dateWhere}";
        $sql .= $extraWhere;
        if($_SESSION['_adminbakery'] > 0)
        {
            $sql .= " AND outlet.bakery_id = " . addslashes($_SESSION['_adminbakery']);
        }
        $sql .= " GROUP BY
                    OT1.outlet_id";
        $rawData = Yii::app()->db->createCommand($sql);
        $count = Yii::app()->db->createCommand('SELECT COUNT(*) FROM (' . $sql . ') AS count_alias')->queryScalar();
        // Create use selected data to create a dataProvider for the dataGrid
        $model = new CSqlDataProvider($rawData, array(
            'keyField'       => 'ol_id',
            'totalItemCount' => $count,
            'sort'           => array(
                'attributes'   => array(
                    'outlet',
                    'balance_due',
                    'last_debit_date',
                    'last_credit_date'
                ),
                'defaultOrder' => array(
                    'outlet' => CSort::SORT_ASC, //default sort value
                ),
            ),
            'pagination'     => array(
                'pageSize' => 50,
            ),
        ));
        $dataProvider = $model;
        // Select Data for the top summary
        $bakeryWhere = '';
        if($_SESSION['_adminbakery'] > 0)
        {
            $bakeryWhere = " AND outlet_transactions.bakery_id = " . addslashes($_SESSION['_adminbakery']);
        }
        $sql = "SELECT
                    SUM(`credit_amount`) - SUM(`debit_amount`) AS balance_brought_forward,
                    (SELECT 
                        SUM(`debit_amount`) 
                    FROM
                        outlet_transactions
                        inner join outlet on outlet.id = outlet_transactions.outlet_id
                    WHERE
                        date(created) BETWEEN '" . addslashes($dateFrom) . "' AND '" . addslashes($dateTo) . "' AND outlet_transactions.payment_allocation_status_id = 1 {$extraWhere} {$bakeryWhere}) AS net_debit_amount,

                    (SELECT 
                        SUM(`credit_amount`) 
                    FROM
                        outlet_transactions
                        inner join outlet on outlet.id = outlet_transactions.outlet_id
                    WHERE
                        date(created) BETWEEN '" . addslashes($dateFrom) . "' AND '" . addslashes($dateTo) . "' AND outlet_transactions.payment_allocation_status_id = 1 {$extraWhere} {$bakeryWhere}) AS net_credit_amount
                    FROM
                        outlet_transactions
                        inner join outlet on outlet.id = outlet_transactions.outlet_id
                    WHERE
                        date(created) < '" . addslashes($dateFrom) . "' AND
                        payment_allocation_status_id = 1 {$extraWhere} {$bakeryWhere}";

        $stmt = Yii::app()->db->createCommand($sql);
        $summary = $stmt->queryRow();
        if(!isset($summary['balance_brought_forward']) || empty($summary['balance_brought_forward']))
        {
            $summary['balance_brought_forward'] = '0.00';
        }
        //get the overview of who owe's what on what date
        //build up the union statements
        $maxDays = 7;
        $unionFuture = '';
        for($i = 1; $i <= $maxDays; $i++)
        {
            $unionFuture .= <<<SQL
            UNION ALL SELECT
                DATE_FORMAT(DATE_ADD(NOW(), INTERVAL {$i} DAY), '%D<br>%b') AS date_formatted,
                    DATE_FORMAT(DATE_ADD(NOW(), INTERVAL {$i} DAY), '%Y-%m-%d') AS raw_date
SQL;
        }
        $unionPast = '';
        for($i = $maxDays; $i >= 1; $i--)
        {
            if($i == $maxDays)
            {
                $unionPast .= <<<SQL
                SELECT
                DATE_FORMAT(DATE_SUB(NOW(), INTERVAL {$i} DAY), '%D<br>%b') AS date_formatted,
                    DATE_FORMAT(DATE_SUB(NOW(), INTERVAL {$i} DAY), '%Y-%m-%d') AS raw_date
SQL;
            }
            else
            {
                $unionPast .= <<<SQL
                UNION ALL SELECT
                DATE_FORMAT(DATE_SUB(NOW(), INTERVAL {$i} DAY), '%D<br>%b') AS date_formatted,
                    DATE_FORMAT(DATE_SUB(NOW(), INTERVAL {$i} DAY), '%Y-%m-%d') AS raw_date
SQL;
            }
        }
        $bakeryId = Yii::app()->user->bakery;
        $bakeryWhere = '';
        if($bakeryId > 0)
        {
            $bakeryWhere = ' AND outlet_transactions.bakery_id = ' . $bakeryId;
        }
        $overviewSql = <<<SQL
        SELECT
            tmp.date_formatted, tmp.raw_date, IFNULL(tmp2.tot, 0) AS tot
        FROM
            (
            {$unionPast}
            UNION ALL SELECT
                DATE_FORMAT(NOW(), '%D<br>%b') AS date_formatted,
                    DATE_FORMAT(NOW(), '%Y-%m-%d') AS raw_date
             {$unionFuture}
            ) tmp
                LEFT JOIN
            (SELECT
                due_date, SUM(`offset`) AS tot
            FROM
                outlet_transactions
            INNER JOIN outlet ON outlet.id = outlet_transactions.outlet_id
            WHERE 1=1 {$bakeryWhere}
            {$extraWhere}
            GROUP BY due_date) tmp2 ON tmp2.due_date = tmp.raw_date
        GROUP BY tmp.date_formatted, tmp.raw_date
        ORDER BY tmp.raw_date;
SQL;
        $command = Yii::app()->db->createCommand($overviewSql);
        $overview = $command->queryAll();
        $this->render('index', array(
            'dataProvider'          => $dataProvider,
            'summary'               => (object)$summary,
            'customerAccountNumber' => $customerAccountNumber,
            'dateFrom'              => $dateFrom,
            'dateTo'                => $dateTo,
            'overview'              => $overview
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model = new OutletTransactions('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['OutletTransactions']))
            $model->attributes = $_GET['OutletTransactions'];
        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return OutletTransactions the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = OutletTransactions::model()->findByPk($id);
        if($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    public function actionOpenStatement($type, $outletId, $dateFrom = '', $dateTo = '')
    {
        if($dateFrom === '')
        {
            $dateFrom = date('Y-m-01') . ' 00:00:00';
        }
        if($dateTo === '')
        {
            $dateTo = date("Y-m-t", strtotime($dateFrom)) . ' 23:59:59';
        }
        $dateFrom .= ' 00:00:00';
        $dateTo .= ' 23:59:59';
        // Get all Row Data
        $sql = <<<SQL
        SELECT
            outlet_transactions.id AS transaction_id,
            outlet_transactions.outlet_id AS ol_id,
            outlet_transactions.payment_allocation_status_id,
            outlet_transactions.description AS old_paymentRef,
            case
                when om.order_status_id = 5 then
                    case
                        when b.statement_display_invoice_information = 1 then outlet_transactions.description
                        when b.statement_display_invoice_information = 2 then concat(om.inv_document_prefix, om.inv_document_number)
                        else concat(outlet_transactions.description, ' [', concat(om.inv_document_prefix, om.inv_document_number), ']')
                    end
                when om.order_status_id = 13 then
                    case
                        when b.statement_display_credit_note_information = 1 then outlet_transactions.description
                        when b.statement_display_credit_note_information = 2 then concat(om.cn_document_prefix, om.cn_document_number)
                        else concat(outlet_transactions.description, ' [', concat(om.cn_document_prefix, om.cn_document_number), ']')
                    end
                else outlet_transactions.description
            end as paymentRef,
            outlet_transactions.created AS paymentDate,
            outlet_transactions.credit_amount,
            outlet_transactions.debit_amount,
            outlet.description AS outlet,
            outlet.account_number,
            om.pdf_status,
            om.order_status_id,
            om.id as order_id,
            outlet_transactions.credit_amount - outlet_transactions.debit_amount AS balance_due

        FROM
            outlet_transactions
        LEFT JOIN
            order_main om ON outlet_transactions.order_id= om.id
        JOIN
            outlet ON outlet.id = outlet_transactions.outlet_id
        LEFT JOIN
	        bakery b on b.id = om.bakery_id AND b.id = outlet.bakery_id
        WHERE
            outlet.status_id = 1 AND
            outlet_transactions.payment_allocation_status_id = 1 AND
            outlet_transactions.outlet_id = {$outletId} AND
            date(created) BETWEEN '{$dateFrom}' AND '{$dateTo}'
        ORDER BY
            created ASC;
SQL;
        $stmt = Yii::app()->db->createCommand($sql);
        $dataRows = $stmt->queryAll();
        // Get The Outlet Data
        $sql = <<<SQL
        SELECT
            o.*,
            b.description AS branch,
            b.physical_address,
            b.postal_address,
            b.phone,
            b.vat_number AS branch_vat,
            b.banking_details,
            oc.payment_terms_days
        FROM
            outlet o
        INNER JOIN
          bakery b on b.id = o.bakery_id
        LEFT JOIN
          outlet_credit oc ON oc.outlet_id = o.id
        WHERE
            o.id = {$outletId};
SQL;

        $stmt = Yii::app()->db->createCommand($sql);
        $outlet = $stmt->queryRow();
        // Select Data for the top summary
        $sql = <<<SQL
        SELECT
            SUM(`credit_amount`) - SUM(`debit_amount`) AS balance_brought_forward
        FROM
            outlet_transactions
        WHERE
            outlet_id = {$outletId} AND
            payment_allocation_status_id = 1 AND
            date(created) < '{$dateFrom}'
SQL;

        $stmt = Yii::app()->db->createCommand($sql);
        $summary = $stmt->queryRow();
        $summary['balance_brought_forward_int'] = $summary['balance_brought_forward'];

        if(empty($summary['balance_brought_forward']))
        {
            $summary['balance_brought_forward'] = 'CR 0.00';
        }
        elseif($summary['balance_brought_forward'] >= 0)
        {
            $summary['balance_brought_forward'] = 'CR ' . $summary['balance_brought_forward'];
        }
        elseif($summary['balance_brought_forward'] < 0)
        {
            $summary['balance_brought_forward'] = $summary['balance_brought_forward'] * -1;
        }
        $logoSql = 'SELECT logo_url FROM project_config';
        $command = Yii::app()->db->createCommand($logoSql);
        $logoUrl = $command->queryScalar();
        $processOutput = false;
        if($type == 'pdf')
        {
            $processOutput = true;
        }
        $ageAnalysisSql = <<<SQL
        SELECT
            due_date,
            CASE
                WHEN DATEDIFF(due_date, NOW()) = 0 THEN '(TODAY)'
                WHEN DATEDIFF(due_date, NOW()) = 1 THEN '(1 day from now)'
                ELSE CONCAT('(', DATEDIFF(due_date, NOW()), ' days from now)')
            END AS days_from_now,
            SUM(`offset`) AS amount_due
        FROM
            outlet_transactions
        WHERE
            outlet_id = :a AND `offset` > 0
                AND date(created) BETWEEN :b AND :c
        GROUP BY due_date, DATEDIFF(due_date, NOW());
SQL;
        $command = Yii::app()->db->createCommand($ageAnalysisSql);
        $command->bindParam(':a', $outletId, PDO::PARAM_INT);
        $command->bindParam(':b', $dateFrom, PDO::PARAM_STR);
        $command->bindParam(':c', $dateTo, PDO::PARAM_STR);
        $ageAnalysis = $command->queryAll();
        $reportData = array(
            'rows'        => (object)$dataRows,
            'outlet'      => (object)$outlet,
            'summary'     => (object)$summary,
            'dateFrom'    => $dateFrom,
            'dateTo'      => $dateTo,
            'logoUrl'     => $logoUrl,
            'type'        => $type,
            'ageAnalysis' => $ageAnalysis
        );

        $body = $this->renderPartial($type . 'Statement', $reportData, true);

        if($type === 'pdf')
        {
            $pdfFactory = new PDFFactory([], 'Statement');
            $title = $outlet['description'] . ' - Statement. ' . $dateFrom . ' to ' . $dateTo;
            $pdfFactory->createStatement($body, $outlet['bakery_id'], $title, false);
        }
        else
        {
            echo $body;
        }
    }

    public function actionTransactionInfo($id, $outletId, $dateFrom, $dateTo)
    {
        $model = Outlet::model()->findByPk($outletId);
        if($model === null)
        {
            throw new CHttpException(404, 'The requested outlet does not exist.');
        }
        $sql = <<<SQL
        SELECT
            p.description AS payment_type,
            t.allocated_date,
            case when t.allocated_by = 0 then '-' else e.`name` end AS allocated_by,
            o.description AS outlet_name,
            o.account_number,
            t.credit_amount,
            t.description AS reference,
            t.invoice_number,
            r.description AS reversal_reason,
            tmp.transaction_id,
            c.available_credit,
            CASE
                WHEN c.available_credit - t.credit_amount >= 0 THEN 0
                ELSE 1
            END AS display_due_date
        FROM
            outlet_transactions t
                INNER JOIN
            payment_type p ON p.id = t.payment_type_id
                INNER JOIN
            employee e ON e.id = t.allocated_by
                INNER JOIN
            outlet o ON o.id = t.outlet_id
                LEFT JOIN
            reversal_reasons r ON r.id = t.reversal_reasons_id
                LEFT JOIN
            (SELECT
                outlet_transactions_id AS transaction_id
            FROM
                outlet_transactions
            WHERE
                outlet_transactions_id = :a) tmp ON tmp.transaction_id = t.id
                LEFT JOIN
            outlet_credit c ON c.outlet_id = t.outlet_id
        WHERE
            t.id = :b;
SQL;
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(':a', $id, PDO::PARAM_INT);
        $command->bindParam(':b', $id, PDO::PARAM_INT);
        $transactionData = $command->queryRow();
        $data = [
            'model'           => $model,
            'id'              => $id,
            'transactionData' => $transactionData,
            'outletId'        => $outletId,
            'dateFrom'        => $dateFrom,
            'dateTo'          => $dateTo
        ];
        $body = $this->renderPartial('transactionInfo', $data);
    }
    public function actionEmailStatemetForm($outletId, $dateFrom = '', $dateTo = '')
    {
        $outlet= $model = Outlet::model()->findByPk($outletId);
        $this->renderPartial('emailStatementForm', array('outlet'=>$outlet,'dateFrom'=>$dateFrom,'dateTo'=>$dateTo));
    }
    public function actionEmailStatement($outletId, $dateFrom = '', $dateTo = '')
    {
        $data=$_POST['form'];
        $type='pdf';
        $dateFrom=$data['start_date'];
        $dateTo=$data['end_date'];
        $outletId=$data['id'];
        if($dateFrom === '')
        {
            $dateFrom = date('Y-m-01') . ' 00:00:00';
        }
        if($dateTo === '')
        {
            $dateTo = date("Y-m-t", strtotime($dateFrom)) . ' 23:59:59';
        }
        $dateFrom .= ' 00:00:00';
        $dateTo .= ' 23:59:59';
        // Get all Row Data
        $sql = <<<SQL
        SELECT
            outlet_transactions.id AS transaction_id,
            outlet_transactions.outlet_id AS ol_id,
            outlet_transactions.payment_allocation_status_id,
            outlet_transactions.description AS paymentRef,
            outlet_transactions.created AS paymentDate,
            outlet_transactions.credit_amount,
            outlet_transactions.debit_amount,
            outlet.description AS outlet,
            outlet.account_number,
            om.pdf_status,
            om.order_status_id,
            om.id as order_id,
            outlet_transactions.credit_amount - outlet_transactions.debit_amount AS balance_due
            
        FROM
            outlet_transactions
        LEFT JOIN
                    order_main om ON outlet_transactions.order_id= om.id
        JOIN
            outlet  ON outlet.id = outlet_transactions.outlet_id
        WHERE
            status_id = 1 AND
            payment_allocation_status_id = 1 AND
            outlet_id = {$outletId} AND
            date(created) BETWEEN '{$dateFrom}' AND '{$dateTo}'
        ORDER BY
            created ASC;
SQL;
        $stmt = Yii::app()->db->createCommand($sql);
        $dataRows = $stmt->queryAll();
        // Get The Outlet Data
        $sql = <<<SQL
        SELECT
            o.*,
            b.description AS branch,
            b.physical_address,
            b.postal_address,
            b.phone,
            b.vat_number AS branch_vat,
            b.banking_details,
            oc.payment_terms_days
        FROM
            outlet o
        INNER JOIN
          bakery b on b.id = o.bakery_id
        LEFT JOIN
          outlet_credit oc ON oc.outlet_id = o.id
        WHERE
            o.id = {$outletId};
SQL;
        
        $stmt = Yii::app()->db->createCommand($sql);
        $outlet = $stmt->queryRow();
        // Select Data for the top summary
        $sql = <<<SQL
        SELECT
            SUM(`credit_amount`) - SUM(`debit_amount`) AS balance_brought_forward
        FROM
            outlet_transactions
        WHERE
            outlet_id = {$outletId} AND
            payment_allocation_status_id = 1 AND
            date(created) < '{$dateFrom}'
SQL;
        
        $stmt = Yii::app()->db->createCommand($sql);
        $summary = $stmt->queryRow();
        $summary['balance_brought_forward_int'] = $summary['balance_brought_forward'];
        
        if(empty($summary['balance_brought_forward']))
        {
            $summary['balance_brought_forward'] = 'CR 0.00';
        }
        elseif($summary['balance_brought_forward'] >= 0)
        {
            $summary['balance_brought_forward'] = 'CR ' . $summary['balance_brought_forward'];
        }
        elseif($summary['balance_brought_forward'] < 0)
        {
            $summary['balance_brought_forward'] = $summary['balance_brought_forward'] * -1;
        }
        $logoSql = 'SELECT logo_url FROM project_config';
        $command = Yii::app()->db->createCommand($logoSql);
        $logoUrl = $command->queryScalar();
        $processOutput = false;
        if($type == 'pdf')
        {
            $processOutput = true;
        }
        $ageAnalysisSql = <<<SQL
        SELECT
            due_date,
            CASE
                WHEN DATEDIFF(due_date, NOW()) = 0 THEN '(TODAY)'
                WHEN DATEDIFF(due_date, NOW()) = 1 THEN '(1 day from now)'
                ELSE CONCAT('(', DATEDIFF(due_date, NOW()), ' days from now)')
            END AS days_from_now,
            SUM(`offset`) AS amount_due
        FROM
            outlet_transactions
        WHERE
            outlet_id = :a AND `offset` > 0
                AND date(created) BETWEEN :b AND :c
        GROUP BY due_date, DATEDIFF(due_date, NOW());
SQL;
        $command = Yii::app()->db->createCommand($ageAnalysisSql);
        $command->bindParam(':a', $outletId, PDO::PARAM_INT);
        $command->bindParam(':b', $dateFrom, PDO::PARAM_STR);
        $command->bindParam(':c', $dateTo, PDO::PARAM_STR);
        $ageAnalysis = $command->queryAll();
        $reportData = array(
            'rows'        => (object)$dataRows,
            'outlet'      => (object)$outlet,
            'summary'     => (object)$summary,
            'dateFrom'    => $dateFrom,
            'dateTo'      => $dateTo,
            'logoUrl'     => $logoUrl,
            'type'        => $type,
            'ageAnalysis' => $ageAnalysis
        );
        
        $body = $this->renderPartial($type . 'Statement', $reportData, true);
        $pdfFactory = new PDFFactory([], 'Statement');
        $title = $outlet['description'] . ' - Statement. ' . $dateFrom . ' to ' . $dateTo;
        $pdf=$pdfFactory->getCreatedStatement($body, $outlet['bakery_id'], $title, false);
        $email = new MailFactory("Statement", 0, trim($data['email']));
        $details = $email->sendDocumentEmail($pdf, "Statement", $data['description']);
        echo 'Email sent successfully!';
      }

    /**
     * Performs the AJAX validation.
     * @param OutletTransactions $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if(isset($_POST['ajax']) && $_POST['ajax'] === 'outlet-transactions-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
