<div class="alert alert-danger allocatePaymentError" style="display:none">Error occurred.please try again</div>
<?php
$bakery_id = Yii::app()->user->bakery;
//var_dump($dataPaymentTypes);

if($bakery_id == 0)
{
     $readOnlyDebitAmountsStatus = 1;   // If the bakery_id = 0 Then , Use it as Blocking debit Amounts ON [1]
}
else{
//    var_dump(Bakery::model()->findAllByAttributes(['id' => $bakery_id])[0]['debtors_dashboard_lock_debit_amounts']);die;
    $readOnlyDebitAmountsStatus = Bakery::model()->findAllByAttributes(['id' => $bakery_id])[0]['debtors_dashboard_lock_debit_amounts'];
}

if($readOnlyDebitAmountsStatus)
{
    $debitAmountHtmlOption = array(
                        'class'               => 'DataColumn',
                        'header'              => 'Debit Amount',
                        'name'                => 'debit_amount',
                        'evaluateHtmlOptions' => true,
                        'filter'              => false,
                        'htmlOptions'         => array(
                                'evaluateExpression' => true,
                                'class'              => 'amountCellHidden',
                                'transId'            => '$data[\'id\']'
                        ));

}
else{
    $debitAmountHtmlOption = array(
                        'class'               => 'DataColumn',
                        'header'              => 'Debit Amount',
                        'name'                => 'debit_amount',
                        'evaluateHtmlOptions' => true,
                        'filter'              => false,
                        'htmlOptions'         => array(
                                'evaluateExpression' => true,
                                'class'              => 'amountCell',
                                'transId'            => '$data[\'id\']'
                        ));
}

$this->widget('zii.widgets.grid.CGridView', array(
        'id'                       => 'form-paymentAllocation-grid',
        'dataProvider'             => $dataProvider,
        'enableSorting'            => false,
        'ajaxUpdate'               => false,
        'rowHtmlOptionsExpression' => 'array("id"=>"row_".$data[\'id\'])',
        'pager'                    => array('cssFile' => Yii::app()->request->baseUrl . '/gridviewstyles/pager.css'),
        'cssFile'                  => Yii::app()->request->baseUrl . '/gridviewstyles/styles.css',
        'columns'                  => array(
                array(
                        'headerHtmlOptions' => GridViewStyles::fit()['headerHtmlOptions'],
                        'htmlOptions'       => GridViewStyles::fit()['htmlOptions'],
                        'header'            => 'Date',
                        'name'              => 'created'
                ),
                array(
                        'headerHtmlOptions' => GridViewStyles::fit()['headerHtmlOptions'],
                        'htmlOptions'       => GridViewStyles::fit()['htmlOptions'],
                        'header'            => 'Reference',
                        'name'              => 'description'
                ),
                array(
                    'headerHtmlOptions' => GridViewStyles::fit()['headerHtmlOptions'],
                    'htmlOptions'       => GridViewStyles::fit()['htmlOptions'],
                    'header'            => 'Shipment #',
                    'name'              => 'trip_sheet_id'
                ),
                array(
                        'headerHtmlOptions' => GridViewStyles::fit()['headerHtmlOptions'],
                        'htmlOptions'       => GridViewStyles::fit()['htmlOptions'],
                        'header'            => 'Created By',
                        'name'              => 'name'
                ),
                array(
                        'headerHtmlOptions' => GridViewStyles::fit()['headerHtmlOptions'],
                        'htmlOptions'       => GridViewStyles::fit()['htmlOptions'],
                        'header'            => 'Current Payment Type',
                        'name'              => 'payment_type'
                       
                ),
                
                //---UM-2221
                
                   array(
                        'header'        => 'Payment Type',
			            'class'         =>'CButtonColumn',
                        'template'      =>'{down}',
                        'buttons'       => array(
                                        'down'  =>array(
                                                'options'   =>array(
                                                'title'     =>'Click to Change Type'),
                                                'label'     =>CHtml::dropDownList('payment_type', 'payment_type', $dataPaymentTypes, array('options' => array(/*payment_type should contain the value*/'payment_type'=>array('selected'=>true)))),     //Text label of the button.
                                                'visible'=>'true', 
                ),

			),

		), //---END UM-2221
                            


//                array(
//                        'headerHtmlOptions' => GridViewStyles::fit()['headerHtmlOptions'],
//                        'htmlOptions'       => GridViewStyles::fit()['htmlOptions'],
//                        'header'            => 'Order Status',
//                        'name'              => 'order_status_id'
//                ),
            
                array(
                        'class'               => 'DataColumn',
                        'header'              => 'Credit Amount',
                        'name'                => 'credit_amount',
                        'evaluateHtmlOptions' => true,
                        'htmlOptions'         => array(
                                'evaluateExpression' => true,
                                'class'              => 'creditAmountCell',
                                'transId'            => '$data[\'id\']'
                        ),
                ),
                $debitAmountHtmlOption,
                array(
                        'headerHtmlOptions' => GridViewStyles::button()['headerHtmlOptions'],
                        'htmlOptions'       => GridViewStyles::button()['htmlOptions'],
                        'class'             => 'CButtonColumn',
                        'header'            => 'Action',
                        'template'          => '{confirm}{decline}{invoice}{credit_note}',
                        'buttons'           => array(
                            'confirm' => array(
                                'options'  => array('data-what' => '1', 'data-toggle' => 'tooltip', 'title' => 'allocate', 'class' => 'allocateCheck', 'rel' => $outletId),
                                'imageUrl' => false,
                                'url'      => '$data[\'id\']',
                                'label'    => ' <i class="fa fa-check-circle text-success"></i> ',
                                
                            ),
                            'decline' => array(
                                'options'  => array('data-what' => '0', 'data-toggle' => 'tooltip', 'title' => 'decline', 'class' => 'allocateCheck', 'rel' => $outletId),
                                'imageUrl' => false,
                                'url'      => '$data[\'id\']',
                                'label'    => ' <i class="fa fa-ban text-danger"></i> ',
                                
                            ),
                            'invoice' => array(
                                'options'  => array('data-toggle' => 'tooltip', 'title' => 'invoice pdf','target'=>'_blank'),
                                'imageUrl' => false,
//                                'url'      => '$data[\'id\']',
                                'label'    => ' <i class="fa fa-file-pdf-o text-danger"></i> ',
                                'visible'=>'$data[\'order_id\']>0 && $data[\'pdf_status\']=="INVOICE" &&  $data[\'order_status_id\']=="5"',
                                // 'url'      => 'Yii::app()->createUrl("admin/distribution/showPDF/", array("orderStatusId"=>$data[\'order_status_id\'],"orderId"=>$data[\'order_id\']))',
                                'url'      => 'Yii::app()->createUrl("admin/distribution/showPDF/", array("orderStatusId"=>$data[\'order_status_id\'],"orderId"=>$data[\'order_id\'], "hash" => FieldFormatter::format(FIELD_FORMATTER_CREATE_HASH, $data[\'order_id\'])))',
                                
                            ),
                            'credit_note' => array(
                                'options'  => array('data-toggle' => 'tooltip', 'title' => 'credit note pdf','target'=>'_blank'),
                                'imageUrl' => false,
//                                'url'      => '$data[\'id\']',
                                'label'    => ' <i class="fa fa-file-pdf-o text-danger"></i> ',
                                'visible'=>'$data[\'order_id\']>0  &&  $data[\'order_status_id\']=="13"',
                                // 'url'      => 'Yii::app()->createUrl("admin/distribution/showPDF/", array("orderStatusId"=>$data[\'order_status_id\'],"orderId"=>$data[\'order_id\']))',
                                'url'      => 'Yii::app()->createUrl("admin/distribution/showPDF/", array("orderStatusId"=>$data[\'order_status_id\'],"orderId"=>$data[\'order_id\'], "hash" => FieldFormatter::format(FIELD_FORMATTER_CREATE_HASH, $data[\'order_id\'])))',

                            )
                        )
                )
        ),
));
$imagePath = Yii::app()->baseUrl;
?>
<script>
    $(document).ready(function () {
        $(".ui-dialog-buttonpane button:last-child,.ui-icon-closethick,.ui-dialog-titlebar-close").hide();
        $(".creditAmountCell").each(function (i, elem) {
            var amount = $(elem).html();
            var transId = $(elem).attr('transid');
            $(elem).html('<input value="' + amount + '" ref="' + transId + '" id="credit_transaction_' + transId + '" type="number" step="0.01" />');
        });
        $(".amountCell").each(function (i, elem) {
            var amount = $(elem).html();
            var transId = $(elem).attr('transid');
            $(elem).html('<input value="' + amount + '" ref="' + transId + '" id="debit_transaction_' + transId + '" type="number" step="0.01" />');
        });
        $(".amountCellHidden").each(function (i, elem) {
            var amount = $(elem).html();
            var transId = $(elem).attr('transid');
            $(elem).html(' ' + amount + '<input value="' + amount + '" ref="' + transId + '" id="debit_transaction_' + transId + '" type="number"  style="display:none;"/>');
        });
        $(".allocateCheck").click(function (event) {
            event.preventDefault();
            hideMessages();
            $btnClicked = $(this);
            var what = $(this).attr('data-what');
            var transactionId = $(this).attr('href');
            if($("#debit_transaction_" + transactionId).val() == '' || $("#credit_transaction_" + transactionId).val() == '')
            {
                showMessage('error', 'Please supply values for credit and debit!', 2000, false);
                return false;
            }
            var extra = '';
            if(what == 0)
            {
                extra = 'decline and ';
            }
            var r = confirm("Are you sure you want to "+ extra +"allocate this?");
            //-----NEW PAYMENT TYPE VALUE FROM SELECT---//
            //console.log($('#payment_type').val());
           // return false;
            if (r == true) {
                showBodyLoader();

                if ($("#debit_transaction_" + transactionId).val().length > 0 && $("#credit_transaction_" + transactionId).val().length > 0) {
                    var data = {
                        id: transactionId,
                        payment_type: $('#payment_type').val(),
                        debit_amount: $("#debit_transaction_" + transactionId).val(),
                        credit_amount: $("#credit_transaction_" + transactionId).val(),
                        payment_allocation_status_id: 1,
                        outlet_id: $(this).attr('rel'),
                        what: what
                    };
                    $.ajax({
                        type: 'POST',
                        url: '<?php echo Yii::app()->createAbsoluteUrl("admin/DebtorCreditorDashboard/savePaymentAllocation/outletId/" . $outletId); ?>',
                        data: data,
                        dataType: 'json',
                        success: function (json) {
                            $("#row_" + json.id).hide();
                            if (json.available_credit <= 0) {
                                if(json.error == 'true')
                                {
                                    showMessage('error', json.msg, 3000);
                                }
                                else
                                {
                                    showMessage('warning', 'This outlet\'s credit limit has been reached', 3000);
                                }
                            } else {
                                showMessage('success', 'Payment allocated. Available Credit: ' + json.available_credit, 5000);
                            }
                            hideBodyLoader();
                        },
                        error: function (data) { // if error occured
                            showMessage('error', 'An error occured, please try again later', 5000);
                            hideBodyLoader();
                        }
                    });
                } else {
                    $(".allocatePaymentError").html('Please enter a valid debit amount');
                    $(".allocatePaymentError").show();
                    setTimeout(function () {
                        $(".allocatePaymentError").fadeOut(1000);
                    }, 3000);
                }
            }
        });
    });
</script>