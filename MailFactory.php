<?php
/**
 * Created by PhpStorm.
 * User: Christo
 * Date: 2016/03/22
 * Time: 01:22 PM
 */

class MailFactory
{
    protected $view;
    private $orderId;
    protected $footerImage;
    protected $toEmail;

    public function __construct($view, $orderId, $toEmail)
    {
        $this->view = $view;
        $this->orderId = $orderId;
        $this->footerImage = '';//Yii::app()->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'logo' . DIRECTORY_SEPARATOR . 'SIH_header_logo.png';
        $this->toEmail = $toEmail;
    }
    
    public function sendDocumentEmail($pdf, $subject, $msg)
    {
        $message = new YiiMailMessage();
        $message->view = 'document';
        $img = '';//$message->embed(Swift_EmbeddedFile::fromPath($this->footerImage));
        $params = array('contentBody'=>$msg, 'footerImage' => $img);
        $message->subject = Yii::app()->name . ' - ' . $subject;
        $message->setBody($params, 'text/html');
        $message->addTo($this->toEmail);
        $message->from = Yii::app()->params['adminEmail'];
        $filePath = Yii::app()->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'files';
        if(file_exists($pdf))
        {
            $message->attach(Swift_Attachment::fromPath($pdf));
        }
        $attachment = str_replace($filePath, 'files/', $pdf);
        $sent = Yii::app()->mail->send($message);
        return ['status' => $sent, 'attachment' => $attachment];
    }

    public function sendPurchaseOrderEmail($pdf)
    {
        $message = new YiiMailMessage();
        $message->view = $this->view;
        $img = '';//$message->embed(Swift_EmbeddedFile::fromPath($this->footerImage));
        $params = array('footerImage' => $img);
        $message->subject = Yii::app()->name . ' - Purchase Order #' . $this->orderId;
        $message->setBody($params, 'text/html');
        $message->addTo($this->toEmail);
        $message->from = Yii::app()->params['adminEmail'];
        $filePath = Yii::app()->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'files';

        if(file_exists($pdf))
        {
            $message->attach(Swift_Attachment::fromPath($pdf));
        }

        $attachment = str_replace($filePath, 'files', $pdf);
        try{
            $msg = null;
            $sent = Yii::app()->mail->send($message);
        }catch(Exception $e){
            $sent = 'FAILED';
            $msg = $e->getMessage();
        }

        return ['status' => $sent, 'attachment' => $attachment, 'message' => $msg];
    }

    public function sendInvoiceEmail($pdf)
    {
        $message = new YiiMailMessage();
        $message->view = $this->view;
        $img = '';//$message->embed(Swift_EmbeddedFile::fromPath($this->footerImage));
        $params = array('footerImage' => $img);
        $message->subject = Yii::app()->name . ' - Invoice #' . $this->orderId;
        $message->setBody($params, 'text/html');
        $message->addTo($this->toEmail);
        $message->from = Yii::app()->params['adminEmail'];//'noreply@solutions-in-hand.com';
        $filePath = Yii::app()->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'files';

        if(file_exists($pdf))
        {
            $message->attach(Swift_Attachment::fromPath($pdf));
        }

        $attachment = str_replace($filePath, 'files', $pdf);
        try{
            $msg = null;
            $sent = Yii::app()->mail->send($message);
        }catch(Exception $e){
            $sent = 'FAILED';
            $msg = $e->getMessage();
        }

        return ['status' => $sent, 'attachment' => $attachment, 'message' => $msg];
    }

    public function sendCreditNoteEmail($pdf)
    {
        $message = new YiiMailMessage();
        $message->view = $this->view;
        $img = '';//$message->embed(Swift_EmbeddedFile::fromPath($this->footerImage));
        $params = array('footerImage' => $img);
        $message->subject = Yii::app()->name . ' - Credit Note #' . $this->orderId;
        $message->setBody($params, 'text/html');
        $message->addTo($this->toEmail);
        $message->from = Yii::app()->params['adminEmail'];//'noreply@solutions-in-hand.com';
        $filePath = Yii::app()->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'files';

        if(file_exists($pdf))
        {
            $message->attach(Swift_Attachment::fromPath($pdf));
        }

        $attachment = str_replace($filePath, 'files/', $pdf);
        $sent = Yii::app()->mail->send($message);

        return ['status' => $sent, 'attachment' => $attachment];
    }
    public function sendFormEmail($pdf)
    {
        $message = new YiiMailMessage();
        $message->view = $this->view;
        $img = '';//$message->embed(Swift_EmbeddedFile::fromPath($this->footerImage));
        $params = array('footerImage' => $img);
        $message->subject = Yii::app()->name . ' - Form #' . $this->orderId;
        $message->setBody($params, 'text/html');
        $message->addTo($this->toEmail);
        $message->from = Yii::app()->params['adminEmail'];//'noreply@solutions-in-hand.com';
        $filePath = Yii::app()->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'files';

        if(file_exists($pdf))
        {
            $message->attach(Swift_Attachment::fromPath($pdf));
        }

        $attachment = str_replace($filePath, 'files/', $pdf);
        $sent = Yii::app()->mail->send($message);

        return ['status' => $sent, 'attachment' => $attachment];
    }

    public static function getPurchaseOrderDetails($orderId, $preview = false, $edit = false, $recurring = 'false')
    {
        $tableSuffix = '';
        $orderMain = 'Order_main';
        if($recurring == 'true')
        {
            $orderMain = 'OrderMainRecurring';
            $tableSuffix = '_recurring';
        }
        $bakeryId = $orderMain::model()->findByPk($orderId)->bakery_id;
        $orderBy = Bakery::getDistributionProductsOrderBy($bakeryId, 'sql_two');
        if($preview)
        {
            $tableSuffix = '_preview';
        }

        $skuDescription = <<<SQL
        CONCAT(i.uom_description, ' - ', i.product_description) AS ordered_product,
SQL;
        if($edit)
        {
            $skuDescription = <<<SQL
            CONCAT('[', i.product_sku, '] ', i.uom_description, ' - ', i.product_description, ' ', ' @ ') AS ordered_product,
SQL;
        }

        $sql = <<<SQL
        SELECT
            m.manufacturer_id,
            m.deposit_amount,
            m.deposit_vat,
            man.description as manufacturer,
            man.email as manufacturer_email,
            m.vat_value,
            m.cash_collected,
            m.id AS purchase_order_number,
            m.comments,
            m.purchase_order_nr as po,
            m.presale_customer_name,
            m.presale_customer_contact_number,
            CONCAT(pt.payment_code, ' - ', pt.description) AS payment_type,
            case when m.order_taker_employee_nr = 0 then concat(e.name, ' - ', concat(c.first_name, ' ' , c.last_name))
            else e.name end as ordered_by,
            m.order_date,
            m.expected_delivery_date,
            mh.from_description AS customer_name_from,
            IFNULL(mh.from_physical_address, '-') AS customer_address_from,
            IFNULL(mh.from_postal_address, '-') AS customer_postal_address_from,
            IFNULL(mh.from_phone, '-') AS customer_tel_from,
            CONCAT('Vat #: ', IFNULL(mh.from_vat_number, '')) AS customer_vat_number_from,
            m.account as outlet_id,
            -- IFNULL(presales_signature_url, '') presales_signature_url, 
            -- IFNULL(presales_driver_signature_url, '') presales_driver_signature_url,
            case
                when presales_signature_url != '' then
                    case when instr(presales_signature_url, '.jpg') = 0 then concat(presales_signature_url, '.jpg')
                    else presales_signature_url end
                else ''
            end as presales_signature_url,
            case
                when presales_driver_signature_url != '' then
                    case when instr(presales_driver_signature_url, '.jpg') = 0 then concat(presales_driver_signature_url, '.jpg')
                    else presales_driver_signature_url end
                else ''
            end as presales_driver_signature_url,
            mh.to_account_number AS customer_account_number_to,
            mh.to_description AS customer_name_to,
            mh.to_address AS customer_address_to,
            mh.to_contact_tel AS customer_tel_to,
            mh.to_contact_cell AS customer_cell_to,
            CONCAT('Vat #: ', IFNULL(mh.to_vat_number, '')) AS customer_vat_number_to,
            {$skuDescription}
            (i.qty_ordered * i.price) AS ordered_item_total,
            i.vat_value as ordered_item_vat_value,
            ((i.qty_ordered * i.price)+i.vat_value) AS ordered_item_total_incl,
            i.vat_percentage as ordered_item_vat_percentage,
            i.price as discounted_price,
            i.gross_price as original_price,
            i.discount,
            m.order_total_amount,
            m.gross_order_total_amount,
            left(m.discount, 1) as invoice_discount_type,
            SUBSTRING(m.discount, 2) AS invoice_discount_value,
            m.discount_value as invoice_discount,
            b.id as bakery_id,
            i.product_batch_code as batch_code,
            i.product_expiry_date as expiry_date,
            mh.from_banking_details banking_details,
            i.qty_ordered,
            i.id as item_id,
            i.product_id,
            m.purchase_order_nr,
            m.comments,
            m.payment_type_id,
            t.description as `type`,
            s.description as `supplier`,
            i.uom_description as unit_of_measure,
            i.product_description as product,
            pe.discount_type,
            pe.discount_type_value,
            i.qty_ordered,
            i.product_sku,
            b.use_document_sequencing,
            m.po_document_prefix,
            m.po_document_number,
            o.branch_company_id,
            bc.description as company_description,
            bc.physical_address as company_physical_address,
            bc.postal_address as company_postal_address,
            bc.vat_number as company_vat_number,
            bc.phone as company_phone,
            bc.banking_details as company_banking_details,
            b.bakery_distribution_order_by_id,
            b.order_by_qty_subtotals
        FROM
            order_main{$tableSuffix} m
                LEFT JOIN
            manufacturers man on man.id = m.manufacturer_id
                LEFT JOIN
            outlet o on m.account = o.id
                LEFT JOIN
            bakery_companies bc on o.branch_company_id = bc.id
                LEFT JOIN
            contacts c on c.id = m.contact_id
                INNER JOIN
            payment_type pt ON pt.id = m.payment_type_id
                INNER JOIN
            employee e ON e.id = m.order_taker_employee_nr
              INNER JOIN
            order_main_header mh ON m.id = mh.order_id
                INNER JOIN
            bakery b ON b.id = m.bakery_id
                INNER JOIN
            orders{$tableSuffix} i ON i.order_id = m.id
                INNER JOIN
            products p ON p.id = i.product_id
                INNER JOIN
            product_type t ON t.id = p.product_type_id
                INNER JOIN
            product_supplier s on s.id = p.supplier_id
                LEFT JOIN
			pricing_engine pe on pe.id = i.pricing_engine_id
                -- INNER JOIN
			-- units_of_measure uom on uom.id = p.units_of_measure_id
        WHERE
            m.id = {$orderId}
        ORDER BY
          {$orderBy};
            -- AND i.qty_ordered > 0;
SQL;
        $connection = Yii::app()->db;
        $command = $connection->createCommand($sql);
        $result = $command->queryAll();

        $returned = [];
        $returned['ordered'] = $result;

        $skuReturnDescription = <<<SQL
        CONCAT(m.uom_description, ' - ', m.product_description) AS returned_product,
SQL;
        $returnsWhere = 'AND m.qty > 0';
        if($edit)
        {
            $skuReturnDescription = <<<SQL
            CONCAT('[', m.product_sku, '] ', m.uom_description, ' - ', m.product_description, ' ', ' @ ', ROUND(fn_checkPricing(p.price, om.account, o.outlet_price_group_id, p.id), 2)) AS returned_product,
SQL;
            $returnsWhere = '';
        }

        //get the returns
        $returnsSql = <<<SQL
        SELECT
            {$skuReturnDescription}
            (m.qty * ROUND(fn_checkPricing(p.price, om.account, o.outlet_price_group_id, p.id), 2)) AS returned_item_total,
            m.vat_value AS returned_item_vat_value,
            ((m.qty * ROUND(fn_checkPricing(p.price, om.account, o.outlet_price_group_id, p.id), 2)) + m.vat_value) AS returned_item_total_incl,
            m.vat_percentage AS returned_item_vat_percentage,
            m.product_batch_code as batch_code,
            m.product_expiry_date as expiry_date,
            m.qty,
            m.id as item_id,
            m.product_id,
            m.return_types_id,
            m.price AS standard_price,            
            ROUND(fn_checkPricing(p.price, om.account, o.outlet_price_group_id, p.id), 2) AS price,
            p.vat,
            pt.description as `type`,
            s.description as `supplier`,
            m.uom_description as unit_of_measure,
            m.product_description as product,
            p.sku,
            b.bakery_distribution_order_by_id,
            b.order_by_qty_subtotals
        FROM
            orders_returns{$tableSuffix} m
                INNER JOIN
            order_main om on om.id = m.order_id
                INNER JOIN
            outlet o ON o.id = om.account
                INNER JOIN
             products p ON p.id = m.product_id
                INNER JOIN
            return_types t ON t.id = m.return_types_id
                -- INNER JOIN
			-- units_of_measure uom on uom.id = p.units_of_measure_id
                INNER JOIN
            product_type pt ON pt.id = p.product_type_id
                INNER JOIN
            product_supplier s on s.id = p.supplier_id
                INNER JOIN
            bakery b ON b.id = om.bakery_id
        WHERE
            m.order_id = {$orderId}
            {$returnsWhere}
        ORDER BY
          {$orderBy};
SQL;

        $command = $connection->createCommand($returnsSql);
        $returnsResult = $command->queryAll();

        $returned['returns'] = $returnsResult;

        return $returned;
    }
    
    public static function getGoodsReceivedOrderDetails($orderId)
    {
        $bakeryId = GrOrderMain::model()->findByPk($orderId)->bakery_id;
        $orderBy = Bakery::getDistributionProductsOrderBy($bakeryId, 'sql_two');
        
        $sql = <<<SQL
        SELECT
            m.vat_value,
           0 AS cash_collected,
            m.id AS purchase_order_number,
            m.purchase_order_nr as po,
            m.comments,
            CONCAT(pt.payment_code, ' - ', pt.description) AS payment_type,
            e.name as ordered_by,
            m.order_date,
            m.expected_delivery_date,
            b.description AS customer_name_from,
            IFNULL(b.physical_address, '-') AS customer_address_from,
            IFNULL(b.postal_address, '-') AS customer_postal_address_from,
            IFNULL(b.phone, '-') AS customer_tel_from,
            CONCAT('Vat #: ', IFNULL(b.vat_number, '')) AS customer_vat_number_from,
		'' AS customer_account_number_to,
		'' AS customer_name_to,
		'' AS customer_address_to,
		'' AS customer_tel_to,
		'' AS customer_cell_to,
        '' AS customer_vat_number_to,
                o.qty_ordered as qty,
                p.description as product_description,
            CONCAT(o.qty_ordered,
                    ' x ',
                    t.description,
                    ' ',
                    p.description,
                    ' [', p.sku, '] ',
                    ' - ',
                    uom.description,
                    ' @ ') AS ordered_product,
            (o.qty_ordered * o.price) AS ordered_item_total,
            o.vat_value as ordered_item_vat_value,
            ((o.qty_ordered * o.price)+o.vat_value) AS ordered_item_total_incl,
            0 as ordered_item_vat_percentage,
            o.price as discounted_price,
            p.price as original_price,
            0 AS discount,
            m.order_total_amount,
            m.gross_order_total_amount,
            1 as invoice_discount_type,
           0 AS invoice_discount_value,
            0 as invoice_discount,
            bakery_id_stock_location_id,
            b.id as bakery_id,
            case when trim(p.batch_code) = '' then '-' else ifnull(p.batch_code, '-') end as batch_code, p.expiry_date,
            p.sku,
            uom.description as uom,
            b.banking_details,
            uom.description as unit_of_measure,
            t.description as `type`,
            s.description as supplier,
            p.description as product
        FROM
            gr_order_main m
                INNER JOIN
            payment_type pt ON pt.id = m.payment_type_id
                INNER JOIN
            employee e ON e.id = m.order_taker_employee_nr
             INNER JOIN
            bakery b ON b.id =  m.bakery_id
                INNER JOIN
           gr_orders o ON o.order_id = m.id
                INNER JOIN
            products p ON p.id = o.product_id
                INNER JOIN
            product_type t ON t.id = p.product_type_id
                INNER JOIN
            product_supplier s on s.id = p.supplier_id
                INNER JOIN
			units_of_measure uom on uom.id = p.units_of_measure_id
        WHERE
            m.id =  :orderId
        ORDER BY {$orderBy};
SQL;
        $connection = Yii::app()->db;
        $result = $connection->createCommand($sql)
                ->bindValue(':orderId', $orderId, PDO::PARAM_INT)
                ->queryAll();

        $returned = [];
        $returned['ordered'] = $result;

        //get the returns
        $returnsSql = <<<SQL
        SELECT
            m.qty,
                p.description AS product_description,
            CONCAT(m.qty,
                    ' x ',
                    t.description,
                    ' ',
                    p.description,
                    ' [', p.sku, '] ',
                    ' - ',
                    uom.description,
                    ' @ ',
                    m.price) AS returned_product,
            (m.qty * m.price) AS returned_item_total,
            m.vat_value AS returned_item_vat_value,
            ((m.qty * m.price) + m.vat_value) AS returned_item_total_incl,
            0 AS returned_item_vat_percentage,
            p.batch_code,
            p.expiry_date
        FROM
            gr_orders_returns m
                INNER JOIN
            products p ON p.id = m.product_id
                LEFT JOIN
            gr_returns_reasons t ON t.id = m.return_types_id
                LEFT JOIN
			units_of_measure uom on uom.id = p.units_of_measure_id
        WHERE
            m.order_id = :orderId;
SQL;

        $returnsResult = $connection->createCommand($returnsSql)
                ->bindValue(':orderId', $orderId, PDO::PARAM_INT)
                ->queryAll();

        $returned['returns'] = $returnsResult;

        return $returned;
    }

    public static function getInvoiceDetails($orderId, $addedProductIds = NULL)
    {
        $bakeryId = Order_main::model()->findByPk($orderId)->bakery_id;
        $orderBy = Bakery::getDistributionProductsOrderBy($bakeryId, 'sql_two');
        
        $sql = <<<SQL
        SELECT
            m.bakery_id as bakery_id,
            m.credit_note_value,
            m.credit_note_order_id,
            o.id as outlet_id,
            o.outlet_price_group_id,
            case when m.order_status_id = 5 then m.delivered_vat_value else m.picked_vat_value end as vat_value,
            m.cash_collected,
            IFNULL(c2.description, '-') as currency_collected,
            IFNULL(ccc.selected_value, '-') as currency_collected_amount,
            m.id AS purchase_order_number,
            m.purchase_order_nr as po,
            m.comments,
            m.delivery_customer_name,
            m.delivery_customer_contact_number,
            CONCAT(pt.payment_code, ' - ', pt.description) AS payment_type,
            e.name as ordered_by,
            IFNULL(e2.name, '-') AS delivered_by,
            m.order_date,
            CASE WHEN m.order_status_id IN (5,6,8,9) THEN m.actual_delivery_date ELSE NOW() END as expected_delivery_date,
            mh.from_description AS customer_name_from,
            IFNULL(mh.from_physical_address, '-') AS customer_address_from,
            IFNULL(mh.from_postal_address, '-') AS customer_postal_address_from,
            IFNULL(mh.from_phone, '-') AS customer_tel_from,
            CONCAT('Vat #: ', IFNULL(mh.from_vat_number, '')) AS customer_vat_number_from,
            mh.to_account_number AS customer_account_number_to,
            mh.to_description AS customer_name_to,
            oc.payment_terms_days AS customer_payment_term_to,
            mh.to_address AS customer_address_to,
            mh.to_contact_tel  AS customer_tel_to,
            mh.to_contact_cell AS customer_cell_to,
            p.vat AS vatPercentage,
            CONCAT('Vat #: ', IFNULL(mh.to_vat_number, '')) AS customer_vat_number_to,
            CONCAT(
                    i.uom_description,
                    ' - ',
                    i.product_description) AS ordered_product,
          CASE WHEN m.order_status_id = 5 THEN i.qty_delivered ELSE i.qty_picked END AS qty,
            CASE WHEN m.order_status_id = 5 THEN (i.qty_delivered * i.price) ELSE (i.qty_picked * i.price) END AS ordered_item_total,
            CASE WHEN m.order_status_id = 5 THEN i.delivered_vat_value ELSE (i.vat_value/i.qty_ordered)*i.qty_picked END AS ordered_item_vat_value,
            CASE WHEN m.order_status_id = 5
				THEN (((i.qty_delivered * i.price)+i.delivered_vat_value))
				ELSE ((i.qty_picked * i.price)+((i.vat_value/i.qty_ordered)*i.qty_picked))
			END AS ordered_item_total_incl,
            i.vat_percentage as ordered_item_vat_percentage,
            i.price as discounted_price,
            i.gross_price as original_price,
            i.discount,
            i.product_sku,
            i.id as item_id,
            CASE WHEN m.order_status_id = 5 THEN m.delivered_order_total_amount ELSE m.picked_order_total_amount END AS order_total_amount,
            CASE WHEN m.order_status_id = 5 THEN m.delivered_gross_order_total_amount ELSE m.picked_gross_order_total_amount END AS gross_order_total_amount,
            left(m.discount, 1) as invoice_discount_type,
            SUBSTRING(m.discount, 2) AS invoice_discount_value,
            CASE WHEN m.order_status_id = 5 THEN m.delivered_discount_value ELSE m.picked_discount_value END AS invoice_discount,
            i.product_batch_code as batch_code,
            i.product_expiry_date as expiry_date,
            mh.from_banking_details banking_details,
            m.order_type,
            p.sku as product_code,
            case
                when signature_url != '' then
                    case when instr(signature_url, '.jpg') = 0 then concat(signature_url, '.jpg')
                    else signature_url end
                else ''
            end as signature_url,
            case
                when driver_signature_url != '' then
                    case when instr(driver_signature_url, '.jpg') = 0 then concat(driver_signature_url, '.jpg')
                    else driver_signature_url end
                else ''
            end as driver_signature_url,
            t.description as `type`,
            s.description as `supplier`,
            i.uom_description as unit_of_measure,
            i.product_description as product,
            pe.discount_type,
            pe.discount_type_value,
            b.use_document_sequencing,
            m.inv_document_prefix,
            m.inv_document_number,
            o.branch_company_id,
            bc.description as company_description,
            bc.physical_address as company_physical_address,
            bc.postal_address as company_postal_address,
            bc.vat_number as company_vat_number,
            bc.phone as company_phone,
			bc.banking_details as company_banking_details,
            b.bakery_distribution_order_by_id,
            b.order_by_qty_subtotals
        FROM
            order_main m
                INNER JOIN
            payment_type pt ON pt.id = m.payment_type_id
                INNER JOIN
            employee e ON e.id = m.order_taker_employee_nr
              LEFT JOIN
            employee e2 ON e2.id = m.delivered_by
                INNER JOIN
            outlet o ON o.id = m.account
                LEFT JOIN
            bakery_companies bc on o.branch_company_id = bc.id
                INNER JOIN
            order_main_header mh ON m.id = mh.order_id
                LEFT JOIN
            outlet_credit oc ON oc.outlet_id = o.id
                INNER JOIN
            bakery b ON b.id = m.bakery_id
                INNER JOIN
            orders i ON i.order_id = m.id
                INNER JOIN
             products p ON p.id = i.product_id
                INNER JOIN
            product_type t ON t.id = p.product_type_id
                INNER JOIN
            product_supplier s on s.id = p.supplier_id
                LEFT JOIN
			pricing_engine pe on pe.id = i.pricing_engine_id
                LEFT JOIN
			cash_collected_currencies ccc on ccc.order_id = m.id
				LEFT JOIN
			currencies c2 on c2.id = ccc.selected_currency_id
        WHERE
            m.id = :orderId
        ORDER BY
            {$orderBy};
            -- AND CASE WHEN m.order_status_id = 5 THEN i.qty_delivered ELSE i.qty_picked END > 0;
SQL;
        $connection = Yii::app()->db;
        $result = $connection->createCommand($sql)
                ->bindValue(':orderId', $orderId, PDO::PARAM_INT)
                ->queryAll();
#debug( array($sql, $result), 1);
        if(count($result) == 0)
        {
            return [];
        }
        $returned = [];
        $returned['ordered'] = $result;
        $outletId = $result[0]['outlet_id'];
        $outletPriceGroupId = $result[0]['outlet_price_group_id'];
// We add in Credit Note products when they are not included in the Original Order placed but needs to be credited
        $sql = <<<SQL
        SELECT            
            p.vat/100 * p.price as vat_value,          
            p.vat AS vatPercentage,
            CONCAT(
                    uom.description,
                    ' - ',
                    p.description) AS ordered_product,
            1 AS qty,
            ROUND(fn_checkPricing(p.price, {$outletId}, {$outletPriceGroupId}, p.id), 2) AS ordered_item_total,
            -- p.price AS ordered_item_total,
            p.vat/100 * ROUND(fn_checkPricing(p.price, {$outletId}, {$outletPriceGroupId}, p.id), 2) AS ordered_item_vat_value,
            (1 + (p.vat/100)) * ROUND(fn_checkPricing(p.price, {$outletId}, {$outletPriceGroupId}, p.id), 2) AS ordered_item_total_incl,
            -- p.price AS original_price, 
            ROUND(fn_checkPricing(p.price, {$outletId}, {$outletPriceGroupId}, p.id), 2) AS original_price,
            p.sku,
            p.id as item_id,
            p.sku as product_code,
            t.description as `type`,
            s.description as `supplier`,
            uom.description as unit_of_measure,
            p.description as product,
            0 AS discount,
            -- p.price AS discounted_price
             ROUND(fn_checkPricing(p.price, {$outletId}, {$outletPriceGroupId}, p.id), 2) AS discounted_price
                
           --  pe.discount_type,
           --  pe.discount_type_value
        FROM
            products p 
                INNER JOIN
            units_of_measure uom ON uom.id = p.units_of_measure_id
                INNER JOIN
            product_type t ON t.id = p.product_type_id
                INNER JOIN
            product_supplier s on s.id = p.supplier_id
            --    LEFT JOIN
			-- pricing_engine pe on pe.id = i.pricing_engine_id
        WHERE
            FIND_IN_SET(p.id, :addedProductIds )    
        AND     
			p.id NOT IN  (SELECT product_id from orders WHERE order_id =  :orderId);
SQL;
//        echo $sql;
        $resultAdded = $connection->createCommand($sql)
                ->bindValue(':orderId', $orderId, PDO::PARAM_INT)
                ->bindValue(':addedProductIds', $addedProductIds, PDO::PARAM_STR)
                ->queryAll();
        $returned['addedCreditNoteProducts'] = $resultAdded;

        //get the returns
        $returnsSql = <<<SQL
        SELECT
            CASE WHEN om.order_status_id = 5 THEN m.qty_collected ELSE m.qty END AS qty,
            CONCAT(
                    m.uom_description,
                    ' - ',
                    m.product_description) AS returned_product,
            CASE WHEN om.order_status_id = 5 THEN ((m.qty_collected * m.price)) ELSE (m.qty * m.price) END AS returned_item_total,
            CASE WHEN om.order_status_id = 5 THEN m.delivered_vat_value ELSE m.vat_value END AS returned_item_vat_value,
            CASE WHEN om.order_status_id = 5 THEN ((m.qty_collected * m.price) + m.vat_value) ELSE ((m.qty * m.price) + m.vat_value) END AS returned_item_total_incl,
            m.vat_percentage AS returned_item_vat_percentage,
            m.product_batch_code AS batch_code,
            m.product_expiry_date AS expiry_date,
            pt.description as `type`,
            s.description as `supplier`,
            m.uom_description as unit_of_measure,
            m.product_description as product,
            p.sku,
            -- p.price,
             ROUND(fn_checkPricing(p.price, {$outletId}, {$outletPriceGroupId}, p.id), 2) AS price,
            b.bakery_distribution_order_by_id,
            b.order_by_qty_subtotals
        FROM
          order_main om
          INNER JOIN
            orders_returns m on m.order_id = om.id
                INNER JOIN
            products p ON p.id = m.product_id
                INNER JOIN
            return_types t ON t.id = m.return_types_id
                INNER JOIN
            bakery b ON b.id = om.bakery_id
                -- INNER JOIN
			-- units_of_measure uom on uom.id = p.units_of_measure_id
                INNER JOIN
            product_type pt ON pt.id = p.product_type_id
                INNER JOIN
            product_supplier s on s.id = p.supplier_id
        WHERE
            m.order_id = :orderId
            AND case when om.order_status_id = 5 then m.qty_collected > 0 else m.qty > 0 end
        ORDER BY
            {$orderBy}
SQL;

        $returnsResult = $connection->createCommand($returnsSql)
                ->bindValue(':orderId', $orderId, PDO::PARAM_INT)
                ->queryAll();

        $returned['returns'] = $returnsResult;

        return $returned;
    }

    public static function getCreditNoteDetails($orderId)
    {
        $bakeryId = Order_main::model()->findByPk($orderId)->bakery_id;
        $orderBy = Bakery::getDistributionProductsOrderBy($bakeryId, 'sql_two');
        
        $sql = <<<SQL
        SELECT
            m.vat_value,
            m.cash_collected,
            m.comments,
            m.id AS purchase_order_number,
            m.purchase_order_nr as po,
            CONCAT(pt.payment_code, ' - ', pt.description) AS payment_type,
           e.name AS ordered_by,
            e.name AS delivered_by,
            m.order_date,
            m.order_date AS expected_delivery_date,
            mh.from_description AS customer_name_from,
            m.delivery_customer_name,
            m.delivery_customer_contact_number,
            IFNULL(mh.from_physical_address, '-') AS customer_address_from,
            IFNULL(mh.from_postal_address, '-') AS customer_postal_address_from,
            IFNULL(mh.from_phone, '-') AS customer_tel_from,
            CONCAT('Vat #: ', IFNULL(mh.from_vat_number, '')) AS customer_vat_number_from,
            mh.to_account_number AS customer_account_number_to,
            mh.to_description AS customer_name_to,
            mh.to_address AS customer_address_to,
            mh.to_contact_tel AS customer_tel_to,
            mh.to_contact_cell AS customer_cell_to,
            CONCAT('Vat #: ', IFNULL(mh.to_vat_number, '')) AS customer_vat_number_to,
            CONCAT(i.qty_delivered,
                    ' x ',
                    ' [', i.product_sku, '] ',
                    i.uom_description,
                    ' - ',
                    i.product_description,
                    ' @ ') AS ordered_product,
            (i.qty_delivered * i.price) AS ordered_item_total,
            i.vat_value as ordered_item_vat_value,
            ((i.qty_ordered * i.price)+i.vat_value) AS ordered_item_total_incl,
            i.vat_percentage as ordered_item_vat_percentage,
            i.price as discounted_price,
            i.price as original_price,
            i.discount,
            m.order_total_amount,
            m.gross_order_total_amount,
            left(m.discount, 1) as invoice_discount_type,
            substring(m.discount, 2) as invoice_discount,
            b.use_document_sequencing,
            m.cn_document_prefix,
            m.cn_document_number,
            o.branch_company_id,
            bc.description as company_description,
            bc.physical_address as company_physical_address,
            bc.postal_address as company_postal_address,
            bc.vat_number as company_vat_number,
            bc.phone as company_phone,
            b.bakery_distribution_order_by_id,
            b.order_by_qty_subtotals
        FROM
            order_main m
                LEFT JOIN
            outlet o on m.account = o.id
                LEFT JOIN
            bakery_companies bc on o.branch_company_id = bc.id
                LEFT JOIN
            payment_type pt ON pt.id = m.payment_type_id
                LEFT JOIN
            employee e ON e.id = m.order_taker_employee_nr
              LEFT JOIN
            employee e2 ON e2.id = m.delivered_by
                LEFT JOIN
            order_main_header mh ON m.id = mh.order_id
                LEFT JOIN
            bakery b ON b.id = m.bakery_id
                LEFT JOIN
            orders i ON i.order_id = m.id
                LEFT JOIN
            products p ON p.id = i.product_id
                LEFT JOIN
            product_type t ON t.id = p.product_type_id
                LEFT JOIN
			units_of_measure uom on uom.id = p.units_of_measure_id
        WHERE
            m.id = {$orderId};
SQL;
        $connection = Yii::app()->db;
        $command = $connection->createCommand($sql);
        $result = $command->queryAll();

        $returned = [];
        $returned['ordered'] = $result;

        //get the returns
        $returnsSql = <<<SQL
        SELECT
            IFNULL(trr.description, '') as trade_replacement_reason,
            om.comments,
            om.bakery_id,
            CONCAT(
                    m.uom_description,
                    ' - ',
                    m.product_description) AS returned_product,
            case when om.order_status_id = 13 then (m.qty_collected * m.price) else (m.qty * m.price) end AS returned_item_total,
            m.vat_value AS returned_item_vat_value,
            case when om.order_status_id = 13 then ((m.qty_collected * m.price) + m.vat_value) else ((m.qty * m.price) + m.vat_value) end AS returned_item_total_incl,
            m.vat_percentage AS returned_item_vat_percentage,
            m.product_description as product,
            m.uom_description as unit_of_measure,
            pt.description as `type`,
            s.description as supplier,
            m.product_batch_code as batch_code,
            m.product_expiry_date as expiry_date,
            case when om.order_status_id = 13 then m.qty_collected else m.qty end as qty,
            m.product_sku,
            m.price,
            om.presales_signature_url,
            om.presales_driver_signature_url,
            b.bakery_distribution_order_by_id,
            b.order_by_qty_subtotals
        FROM
            order_main om
                LEFT JOIN
            orders_returns m on m.order_id = om.id
                LEFT JOIN
            products p ON p.id = m.product_id
                LEFT JOIN
            return_types t ON t.id = m.return_types_id
                INNER JOIN
            product_type pt ON pt.id = p.product_type_id
                INNER JOIN
            product_supplier s on s.id = p.supplier_id
                INNER JOIN
            bakery b on b.id = om.bakery_id
                LEFT JOIN
                    trade_returns_main trm on trm.order_id = om.id
                LEFT JOIN
                    trade_returns_items tri on tri.trade_return_main_id = trm.id AND tri.product_id = m.product_id
                LEFT JOIN
                    trade_returns_reasons trr on trr.id = tri.reason_id
        WHERE
            om.id = {$orderId}
        ORDER BY {$orderBy};
SQL;

        $command = $connection->createCommand($returnsSql);
        $returnsResult = $command->queryAll();

        $returned['returns'] = $returnsResult;

        return $returned;
    }

   //----|UM-2230|-----//
   public static function getFormDetails($orderId)
   {
       $bakeryId = Results::model()->findByPk($orderId)->bakery_id;
       //var_dump($orderId);
       //$orderBy = Results::getDistributionProductsOrderBy($bakeryId, 'sql_two');
     // echo "Bakery ID: ".$bakeryId;
      // die; 
       $sql = <<<SQL
        select
            c.id,
            r.bakery_id,
            r.id as form_number,
            c.checklist_name,
            e.name as submitted_by,
            o.description as customer_name,
            r.account as customer_number,
            r.result_datetime as submission_date,
            ci.`ordering` as question_number,
            t.description as question_type,
            ci.description as question,
            case
                when t.description = 'select' then co.description
                when t.description = 'product' then concat(p.sku, ' - ', p.description)
                when t.description = 'product_barcode' then concat(pb.sku, ' - ', pb.description)
                else ri.text_input
            end as answer,
            cao.description as additional_question,
            rai.text_input as additional_answer
        from
            checklist c
        left join
            results r on c.id = r.checklist_id
        left join
            employee e on e.id = r.employee_id
        left join
            outlet o on o.id = r.outlet_id
        left join
            results_items ri on ri.results_id = r.id
        left join
            checklist_items ci on ci.checklist_id = c.id and ci.id = ri.checklist_item_id
        left join
            checklist_options co on co.id = ri.checklist_option_id and co.checklist_items_id = ci.id
        left join
            checklist_option_types t on t.id = co.option_type_id
        left join
            products p on p.id = ri.text_input and t.description = 'product' and r.bakery_id = p.bakery_id
        left join
            results_products rp on rp.results_id = r.id and rp.results_item_id = ri.id
        left join
            products pb on pb.id = rp.product_id and pb.bakery_id = r.bakery_id
        left join
            checklist_additional_options cao on
                cao.checklist_id = c.id
                and cao.checklist_item_id = ci.id
        left join
            result_additional_items rai
                on rai.results_id = r.id
                and rai.checklist_item_id = ci.id
        where
           r.id = {$orderId}
           and ci.status_id = 1
           group by ci.`ordering`,ci.description, ri.text_input
SQL;
       $connection = Yii::app()->db;
       $command = $connection->createCommand($sql);
       $result = $command->queryAll();

       $returned = [];
       $returned['ordered'] = $result;
       //var_dump($returned);
         return $returned;
   }
   //----END |UM - 2230|-------// 
    public static function sendBranchActivityEmail($bId, $activity = 'CREATED')
    {
        $branch = Bakery::model()->findByPk($bId);
        $employeeCreated = Employee::model()->findByPk($branch->created_by);
        $employeeUpdated = Employee::model()->findByPk($branch->updated_by);
        $type = FieldFormatter::format(FIELD_FORMATTER_BRANCH_TYPE_TXT, $branch->ccba_customer_id . '|' . $branch->ccba_sales_office);

        $message = new YiiMailMessage();
        $message->view = 'branchActivity';
        $message->subject = Yii::app()->name . ' - Branch ' . $activity;
        $params = ['appName' => Yii::app()->name, 'branch' => $branch, 'activity' => $activity, 'createdBy' => isset($employeeCreated->name) ? $employeeCreated->name : 'System', 'updatedBy' => isset($employeeUpdated->name) ? $employeeUpdated->name : 'System', 'type' => $type];
        $message->setBody($params, 'text/plain');
        $message->addTo(Yii::app()->params['supportEmail']);
        $message->from = Yii::app()->params['adminEmail'];//'noreply@solutions-in-hand.com';
        try{
            $sent = Yii::app()->mail->send($message);
            $msg = 'SUCCESS';
        }catch(Exception $e){
            $sent = 'FAILED';
            $msg = $e->getMessage();
        }

        return ['status' => $sent, 'message' => $msg];
    }
}