<?php

class DistributionController extends AdminController
{
    public $layout = '//layouts/column2';

    // public function actionTestEmail()
    // {
    //     $orderId = '202107221318021';
    //     $toEmail = 'christostan@gmail.com';
    //     $pdfData = MailFactory::getPurchaseOrderDetails($orderId);
    //     $pdfFactory = new PDFFactory($pdfData);
    //     $pdf = $pdfFactory->createPurchaseOrder($orderId);
    //     $email = new MailFactory('purchaseOrder', $orderId, $toEmail);
    //     $details = $email->sendPurchaseOrderEmail($pdf);
    //     if($details['status'] === 'FAILED')
    //     {
    //         echo('Order ' . $orderId . ' not sent: ' . $details['message']);
    //     }
    //     else
    //     {
    //         echo('Order ' . $orderId . ' sent to ' . $toEmail);
    //     }
    // }

    public function filters()
    {
        return array(
            'accessControl',
        );
    }

	public function accessRules()
    {
        return array(
            array(
                'allow',
                'actions' => array('showPDF'),
                'users' => array('*')
            )
        );
	}

    private function pickingSheetDetails($tripSheetId)
    {
        //get the trip sheet's bakery_id
        $tripSheet = TripSheets::model()->with('fleet')->findByPk($tripSheetId);
        $bakeryId = $tripSheet->bakery_id;
//        $order = 'products.created_at, products.expiry_date, products.units_of_measure_id';
        //$order = 'products.description , products.units_of_measure_id';
        $order = Bakery::getDistributionProductsOrderBy($bakeryId);
        $order .= ', products.batch_code, products.expiry_date';
        $orders = TripSheetsItems::model()->with('orderMain.outlet', 'orderMain.employee', 'orderMain.orders.products.type', 'orderMain.orders.products.supplier', 'orderMain.orders.products.productsParent.hasParentProduct', 'orderMain.orders.products.unitOfMeasure', 'orderMain.orders.products.linked', 'orderMain.ordersReturns.productsReturns')->findAll(['condition' => 't.trip_sheet_id = ' . $tripSheetId . ' AND orderMain.order_status_id != 19', 'order' => $order]);
//        var_dump($orders[0]['orderMain']['orders'][1]['products']['productsParent']['hasParentProduct']);
        $orderWeights = [];
        for($i = 0; $i < count($orders); $i++)
        {
            $orderWeights[$orders[$i]['order_id']] = 0;
            foreach($orders[$i]['orderMain']['orders'] as $order)
            {
                $orderWeights[$orders[$i]['order_id']] += $order['qty_picked']*$order['products']['weight'];
            }
        }
        $vsStock = TripSheetsStock::checkVanSaleStock($tripSheetId);
    //    var_dump($vsStock);
        if(!empty($vsStock))
        {
            $orderMainObj = [
                'orders' => []
            ];
            $orderWeight = 0;
            foreach($vsStock as $stock)
            {
                $qtyOrdered = $stock['qty'] - $stock['ps_qty_ordered'];
                $orderWeight += $qtyOrdered*$stock['weight'];
                $stockObj = [
                    'order_id' => 'VAN SALE',
                    'product_id' => $stock['product_id'],
                    'qty_ordered' => $qtyOrdered,
                    //'line_weight' => $qtyOrdered*$stock['weight'],
                    'products' => [
                        'id' => $stock['product_id'],
                        'sku' => $stock['sku'],
                        'description' => $stock['description'],
                        'units_of_measure_id' => $stock['units_of_measure_id'],
                        'batch_code' => $stock['batch_code'],
                        'expiry_date' => $stock['expiry_date'],
                        'productsParent' => [
                            'hasParentProduct' => [
                                'units' => $stock['parent_product_id']
                            ]
                        ],
                        'supplier' => [
                            'description' => $stock['product_supplier']
                        ],
                        'type' => [
                            'description' => $stock['product_type']
                        ],
                        'unitOfMeasure' => [
                            'description' => $stock['unit_of_measure']
                        ],
                        'linked' => []
                    ]
                ];
                $orderMainObj['orders'][] = $stockObj;
            }
            $vsOrderArr = [];
            $vsOrderArr['trip_sheet_id'] = $tripSheetId;
            $vsOrderArr['order_id'] = 'VAN SALE';
            $vsOrderArr['orderMain'] = $orderMainObj;
            $orders[] = $vsOrderArr;

            $orderWeights['VAN SALE'] = $orderWeight;
        }

        $linked = [];
        foreach($orders as $order)
        {
            foreach($order['orderMain']['orders'] as $products)
            {
                if($order['order_id'] === 'VAN SALE')
                {
                    $linkedProductId = ProductsLinked::model()->find([
                        'select' => 'linked_product_id',
                        'condition' => 'product_id = ' . $products['products']['id'] . ' AND status_id = 1'
                    ]);
                    if(!in_array($linkedProductId['linked_product_id'], $linked))
                    {
                        array_push($linked, $linkedProductId['linked_product_id']);
                    }
                }
                foreach($products['products']['linked'] as $linkedProduct)
                {
                    if(!in_array($linkedProduct['linked_product_id'], $linked) && $linkedProduct['status_id'] === '1')
                    {
                        array_push($linked, $linkedProduct['linked_product_id']);
                    }
                }
            }
        }

        $stock = [];
        $i = 0;
        foreach($orders as $order)
        {
            $orderId = $order['order_id'];
            foreach($order['orderMain']['orders'] as $products)
            {
                $orderBy = 'warehouse.description, zone.description, bay.description, slot.description';
                // AND t.qty > 0
                $condition = 't.product_id = ' . $products['product_id'] . ' AND t.employee_id = 0 AND t.location_id = 0 AND t.slot_id > 0 and slot.status_id = 1 and slot.trade_replacement_slot = 0 and bay.status_id = 1 and zone.status_id = 1 and warehouse.status_id = 1';
                // $condition = 't.product_id = ' . $products['product_id'] . ' AND t.employee_id = 0 AND t.location_id = 0 AND t.slot_id > 0';
                $stockLocations = AsStock::model()->with('slot.bay.zone.warehouse')->findAll(['condition' => $condition, 'order' => $orderBy]);
                $stock[$orderId][$products['product_id']] = $stockLocations;
            }

            ++$i;
        }

        $asStock = [];

        foreach($orders as $order)
        {
            foreach($stock[$order['order_id']] as $productId => $s)
            {
                if(empty($s))
                {
                    $asStock[$order['order_id']][$productId][null] = 0;
                }
                else
                {
                    foreach($s as $st)
                    {
                        $asStock[$order['order_id']][$st['product_id']][$st['id']] = $st['qty'];
                    }
                }
            }
        }
        $asProducts = [];
        $asProductsStatic = [];
        foreach($orders as $order)
        {
            foreach($order['orderMain']['orders'] as $products)
            {
                $asProducts[$products['order_id']][$products['product_id']] = $products['qty_ordered'];
                $asProductsStatic[$products['order_id']][$products['product_id']] = $products['qty_ordered'];
            }
        }
        $qtyMap = [];
        foreach($asStock as $orderId => $arr)
        {
            foreach($arr as $productId => $values)
            {
                asort($values);
                $asStock[$orderId][$productId] = $values;
                foreach($values as $id => $qty)
                {
                    $qtyMap[$productId][$id] = $qty;
                }
            }
        }

        $suggested = [];
        foreach($asStock as $orderId => $arr)
        {
            foreach($arr as $productId => $values)
            {
                foreach($values as $id => $qty)
                {
                    if(isset($asProducts[$orderId][$productId]) && $asProducts[$orderId][$productId] >= $qtyMap[$productId][$id] && $asProducts[$orderId][$productId] > 0)
                    {
//                        var_dump('a');
                        $suggested[$orderId][$productId][$id]['qty_to_pick'] = $qtyMap[$productId][$id];
                        $suggested[$orderId][$productId][$id]['qty_left'] = $asProducts[$orderId][$productId] - $qtyMap[$productId][$id];
                        $qtyMap[$productId][$id] = $qtyMap[$productId][$id] - $qty;
                        $asProducts[$orderId][$productId] = $asProducts[$orderId][$productId] - $suggested[$orderId][$productId][$id]['qty_to_pick'];
                    }
                    if(isset($asProducts[$orderId][$productId]) && $asProducts[$orderId][$productId] <= $qtyMap[$productId][$id] && $asProducts[$orderId][$productId] > 0)
                    {
//                        var_dump('b');
                        $suggested[$orderId][$productId][$id]['qty_to_pick'] = $asProducts[$orderId][$productId];
                        $suggested[$orderId][$productId][$id]['qty_left'] = $asProducts[$orderId][$productId] - $suggested[$orderId][$productId][$id]['qty_to_pick'];
                        $asProducts[$orderId][$productId] = $asProducts[$orderId][$productId] = $suggested[$orderId][$productId][$id]['qty_left'];
                        $qtyMap[$productId][$id] = $qtyMap[$productId][$id] - $suggested[$orderId][$productId][$id]['qty_to_pick'];
                    }
                    if($id === '')
                    {
//                        var_dump('c');
                        $suggested[$orderId][$productId][$id]['qty_to_pick'] = $asProducts[$orderId][$productId];
                        $suggested[$orderId][$productId][$id]['qty_left'] = 0 - $asProducts[$orderId][$productId];
                    }

                    if(!isset($suggested[$orderId]) && isset($asProducts[$orderId][$productId]) && $asProducts[$orderId][$productId] >= 0)//means suggested can't be set
                    {
//                        var_dump('d');
                        $suggested[$orderId][$productId][$id]['qty_to_pick'] = $asProducts[$orderId][$productId];
                        $suggested[$orderId][$productId][$id]['qty_left'] = 0;
                    }
                }
            }
        }

//        echo '<pre>';
//        var_dump($asProducts['155377845757']);
//        echo '</pre>';
//        echo "<pre>";
//        var_dump($qtyMap[2781]);
//        echo "</pre>";
//        die();
//        foreach($asStock as $orderId => $arr)
//        {
//            foreach($arr as $productId => $values)
//            {
//                foreach($values as $id => $qty)
//                {
//                    if(!isset($suggested[$orderId][$productId][$id]) && isset($asProducts[$orderId][$productId]))
//                    {
//                        $suggested[$orderId][$productId][$id] = [
//                            'qty_to_pick' => $asProducts[$orderId][$productId],
//                            'qty_left' => $qtyMap[$productId][$id],
//                            'flag' => true
//                        ];
//                    }
//                }
//            }
//        }
        //get the dispatch area for the warehouse
        // $bakeryLocationWhere = CustomUtils::warehouseLocationWhereClause();
        $tripSheetBakeryId = TripSheets::model()->findByPk($tripSheetId)->bakery_id;
        $bakeryLocationWhere = ' AND warehouse.bakery_id = ' .$tripSheetBakeryId;
        $stockLocations = AsStockLocations::model()->with('warehouse')->findAll(['condition' => 't.location_type_id = 2 and t.status_id = 1' . $bakeryLocationWhere, 'order' => 'warehouse.description, t.description']);
        $dispatchList = [];
        foreach($stockLocations as $loc)
        {
            $dispatchList[$loc['warehouse']['description']][$loc['id']] = $loc['description'];
        }

        $dispatchEmployees = Employee::model()->getEmployees($tripSheet->bakery_id);
        $fleet = Fleet::model()->getFleet($tripSheet->bakery_id);

        $orderWeights['total'] = 0;
        foreach($orderWeights as $weights)
        {
            $orderWeights['total'] += $weights;
        }

        return [
            'dispatchList' => $dispatchList,
            'tripSheetId' => $tripSheetId,
            'orders' => $orders,
            'stock' => $stock,
            'suggested' => $suggested,
            'asProductsStatic' => $asProductsStatic,
            'dispatchEmployees' => $dispatchEmployees,
            'dispatchFleet' => $fleet,
            'bakeryId' => $bakeryId,
            'orderWeights' => $orderWeights,
            'fleet' => $tripSheet['fleet'],
            'linked' => $linked
        ];
    }

    public function actionCheckPickingSheetSOHBlock()
    {
        $block = isset($_POST['block']) ? $_POST['block'] : 0;
        $bakeryId = $_POST['bakeryId'];
        $return = [
            'valid' => true,
            'message' => ''
        ];

        if($block > 0)
        {
            $con = Yii::app()->db;
            $productsQty = [];
            foreach($_POST['as_qty'] as $orderNumber)
            {
                foreach($orderNumber as $slot => $products)
                {
                    foreach($products as $productId => $qty)
                    {
                        if(!isset($productsQty[$productId]))
                        {
                            $productsQty[$productId] = $qty;
                        }
                        else
                        {
                            $productsQty[$productId] += $qty;
                        }
                    }
                }
            }
            $productsInString = '';
            $i = 0;
            foreach($productsQty as $productId => $qty)
            {
                ($i === 0) ? $productsInString .= $productId : $productsInString .= ','.$productId;
                ++$i;
            }

            $sql = <<<SQL
            SELECT
                s.product_id,
                CONCAT(uom.description, ' ', p.description, ' (', p.sku, ') - ', p.batch_code, ' [', p.expiry_date, ']') AS product_name,
                (SUM(s.qty) + IFNULL(parent.parent_stock,
                0)) AS soh
            FROM
                as_stock s
            INNER JOIN as_stock_slots sl ON
                sl.id = s.slot_id
            INNER JOIN as_stock_bays b ON
                b.id = sl.bay_id
            INNER JOIN as_stock_zones z ON
                z.id = b.zone_id
            INNER JOIN as_stock_warehouses w ON
                w.id = z.warehouse_id
            INNER JOIN products p ON
                p.id = s.product_id
            INNER JOIN units_of_measure uom ON
                uom.id = p.units_of_measure_id
            LEFT JOIN (
                select
                    pp.product_id,
                    SUM(s.qty*p.units) as parent_stock
                from
                    products_parent pp
                inner join products p on
                    p.id = pp.parent_product_id
                inner join as_stock s on
                    s.product_id = pp.parent_product_id
                INNER JOIN as_stock_slots sl ON
                    sl.id = s.slot_id
                INNER JOIN as_stock_bays b ON
                    b.id = sl.bay_id
                INNER JOIN as_stock_zones z ON
                    z.id = b.zone_id
                INNER JOIN as_stock_warehouses w ON
                    w.id = z.warehouse_id
                where
                    pp.product_id in ({$productsInString})
                    and pp.status_id = 1
                    and p.status_id = 1
                    and s.slot_id > 0
                    AND s.employee_id = 0
                    AND s.location_id = 0
                    AND w.bakery_id = {$bakeryId}
                    AND s.qty > 0
                GROUP BY
                    pp.product_id ) parent on
                parent.product_id = s.product_id
            WHERE
                s.slot_id > 0
                AND s.employee_id = 0
                AND s.location_id = 0
                AND w.bakery_id = {$bakeryId}
                AND s.product_id IN ({$productsInString})
            GROUP BY
                s.product_id;
SQL;
            $sohQuantities = $con->createCommand($sql)->queryAll();
            $soh = [];
            foreach($sohQuantities as $data)
            {
                $soh[$data['product_id']] = [
                    'soh' => $data['soh'],
                    'product_name' => $data['product_name']
                ];
            }
            $message = '';
            foreach($productsQty as $pId => $qty)
            {
                if(isset($soh[$pId]['soh']) && $soh[$pId]['soh'] < $qty)
                {
                    $message .= 'Product : ' . $soh[$pId]['product_name'] . ' - Picked Qty <span class="bold">' . $qty . '</span>,  SOH Qty <span class="bold">' . $soh[$pId]['soh'] . '</span><br>';
                }
                else if(isset($soh[$pId]['soh']) && $soh[$pId]['soh'] == 0 && $qty > 0)
                {
                    $message .= 'Product : ' . $soh[$pId]['product_name'] . ' - Has <span class="bold">0</span> stock<br>';
                }
                else if(!isset($soh[$pId]['soh']))//means that a stock take has never been done on a product or there is no data in the as_stock_table for a product
                {
                    $p = Products::model()->with('unitOfMeasure')->findByPk($pId);
                    $pDescription = $p['unitOfMeasure']->description . ' ' . $p->description . ' (' . $p->sku . ') - ' . $p->batch_code . ' [' . $p->expiry_date . ']';
                    $message .= 'Product : ' . $pDescription . ' - <span class="bold">Can\'t continue, no stock found. Please do a stock take for this product</span><br>';
                }
            }
            if($message !== '')
            {
                $return = [
                    'valid' => false,
                    'message' => $message
                ];
            }
        }

        $tripSheetId = $_POST['trip_sheet_id'];
        $deduct = [];
        $stockCheckProductIds = [];
        foreach($_POST['as_qty'] as $orderId => $detail)
        {
            foreach($detail as $what => $lineItem)
            {
                foreach($lineItem as $productId => $qty)
                {
                    if(!in_array($productId, $stockCheckProductIds))
                    {
                        array_push($stockCheckProductIds, $productId);
                    }

                    if(!isset($deduct[$orderId][$productId]))
                    {

                        $deduct[$orderId][$productId] = $qty;
                    }
                    else
                    {
                        $deduct[$orderId][$productId] += $qty;
                    }
                }
            }
        }
        $productIds = implode(',', $stockCheckProductIds);
        $stockSql = <<<SQL
        select
            s.product_id,
            sum(s.qty) as soh
        from
            as_stock s
        inner join as_stock_slots sl on sl.id = s.slot_id
        inner join as_stock_bays b on b.id = sl.bay_id
        inner join as_stock_zones z on z.id = b.zone_id
        inner join as_stock_warehouses w on w.id = z.warehouse_id
        where
            s.product_id in ({$productIds})
            and s.slot_id > 0
            and s.location_id = 0
            -- and s.qty > 0
            and w.bakery_id = {$bakeryId}
        group by
            s.product_id;
SQL;
        $stockRaw = Yii::app()->db->createCommand($stockSql)->queryAll();
        $stock = [];
        foreach($stockRaw as $raw)
        {
            $stock[$raw['product_id']] = $raw['soh'];
        }

        //check if there is any eaches on this order with parents (with stock)
        //so that we can ask the user where to subtract the stock from
        //first delete from as_stock_parent_deduct
        Yii::app()->db->createCommand('delete from as_stock_parent_deduct where trip_sheet_id = ' . $tripSheetId)->execute();
        $entryCount = Yii::app()->db->createCommand('select count(*) from as_stock_parent_deduct where trip_sheet_id = ' . $tripSheetId . ' and processed = 0;')->queryScalar();
        if($entryCount == 0)
        {
            foreach($deduct as $orderId => $item)
            {
                foreach($item as $productId => $qty)
                {
                    if(isset($stock[$productId]) && $stock[$productId] < $qty)
                    {
                        $sql = <<<SQL
                        insert into as_stock_parent_deduct
                        select
                            '{$orderId}' as order_id,
                            tss.trip_sheet_id,
                            {$qty} as child_qty,
                            pp.product_id,
                            pp.parent_product_id,
                            s.id as stock_id,
                            s.slot_id,
                            s.qty as parent_soh,
                            0 as processed
                        from
                            trip_sheets_stock tss
                        inner join
                            products_parent pp on pp.product_id = tss.product_id
                        inner join
                            as_stock s on s.product_id = pp.parent_product_id
                        where
                            tss.trip_sheet_id = {$tripSheetId}
                            and tss.product_id = {$productId}
                            and s.slot_id != 0
                            and s.qty > 0;
SQL;
                        Yii::app()->db->createCommand($sql)->execute();
                    }
                }
            }
        }

        $select = Yii::app()->db->createCommand('select * from as_stock_parent_deduct where trip_sheet_id = ' . $tripSheetId . ' and processed = 0;')->queryAll();
        if(count($select) > 0)
        {
            $return['parent_stock_deduct'] = true;
        }
        else
        {
            $return['parent_stock_deduct'] = false;
        }

        echo json_encode($return);
    }

    public function actionParentStockDeduct($tripSheetId)
    {
        $sql = <<<SQL
        select
            d.order_id,
            d.trip_sheet_id,
            d.child_qty,
            floor(d.child_qty/p.units) as parent_qty_to_deduct,
            p.units as parent_units,
            d.product_id,
            d.parent_product_id,
            d.stock_id,
            d.slot_id,
            s.description as slot,
            b.description as bay,
            z.description as `zone`,
            w.description as warehouse,
            d.parent_soh,
            p.description as parent_product,
            p.sku,
            p.batch_code,
            p.expiry_date,
            uom.description as uom,
            o.description as outlet,
            o.account_number,
            e.`name` as order_taker,
            e.employee_number
        from
            as_stock_parent_deduct d
        inner join
            products p on p.id = d.parent_product_id
        inner join
            units_of_measure uom on uom.id = p.units_of_measure_id
        left join
            order_main om on om.id = d.order_id
        left join
            outlet o on o.id = om.account
        left join
            employee e on e.id = om.order_taker_employee_nr
        inner join
            as_stock_slots s on s.id = d.slot_id
        inner join
            as_stock_bays b on b.id = s.bay_id
        inner join
            as_stock_zones z on z.id = b.zone_id
        inner join
            as_stock_warehouses w on w.id = z.warehouse_id
        where
            d.trip_sheet_id = {$tripSheetId}
            and d.processed = 0;
SQL;
        $select = Yii::app()->db->createCommand($sql)->queryAll();
        $data = [];
        $products = [];
        foreach($select as $detail)
        {
            $data[$detail['order_id']] = [
                'outlet' => $detail['outlet'] . ' (' . $detail['account_number'] . ')',
                'order_taker' => $detail['order_taker'] . ' (' . $detail['employee_number'] . ')',
                'ordered_by' => $detail['order_taker'],
                'product_details' => []
            ];
        }

        foreach($select as $detail)
        {
            if(isset($data[$detail['order_id']]))
            {
                $data[$detail['order_id']]['product_details'][] = [
                    'product_id' => $detail['product_id'],
                    'sku' => $detail['sku'],
                    'parent_product' => $detail['parent_product'],
                    'batch_code' => $detail['batch_code'],
                    'expiry_date' => $detail['expiry_date'],
                    'uom' => $detail['uom'],
                    'slots' => [
                        'slot_id' => $detail['slot_id'],
                        'stock_id' => $detail['stock_id'],
                        'parent_soh' => $detail['parent_soh'],
                        'child_qty' => $detail['child_qty'],
                        'parent_qty_to_deduct' => $detail['parent_qty_to_deduct'],
                        'parent_units' => $detail['parent_units'],
                        'warehouse' => $detail['warehouse'],
                        'zone' => $detail['zone'],
                        'bay' => $detail['bay'],
                        'slot' => $detail['slot']
                    ]
                ];
            }
        }

        $this->renderPartial('_parentStockDeduct', ['data' => $data]);
    }

    public function actionAdvancedStockPickingSheet($tripSheetId, $pickingMode)
    {
        set_time_limit(0);
        if(!empty($_POST))
        {
            $status = AsStockMovement::moveStockToDispatch($_POST, 15);
            $select = Yii::app()->db->createCommand('update as_stock_parent_deduct set processed = 1 where trip_sheet_id = ' . $tripSheetId)->execute();
            Yii::app()->user->setFlash($status['what'], $status['msg']);
            $this->redirect(Yii::app()->getBaseUrl(true) . '/index.php/admin/' . $this->id . '?show=planned');
            die();
        }

        switch($pickingMode)
        {
            case 2:

                $data = $this->pickingSheetDetails($tripSheetId);
                $stock = TripSheets::getPickingSheetModeDetails($tripSheetId);
                $bakery = Bakery::model()->findByPk($data['bakeryId']);

                $this->renderPartial('_submitPickingSheetSelect', ['pickingMode' => $pickingMode, 'data' => $data, 'stock' => $stock, 'bakery' => $bakery, 'tripSheetId' => $tripSheetId]);

            break;

            default:

                $data = $this->pickingSheetDetails($tripSheetId);
                $dispatchList = $data['dispatchList'];
                $orders = $data['orders'];
                $stock = $data['stock'];
                $suggested = $data['suggested'];
                $asProductsStatic = $data['asProductsStatic'];
                $dispatchEmployees = $data['dispatchEmployees'];
                $fleet = $data['dispatchFleet'];

                $bakery = Bakery::model()->findByPk($data['bakeryId']);

                $this->renderPartial('_submitPickingSheet', ['pickingMode' => $pickingMode, 'linked' => $data['linked'], 'bakery' => $bakery, 'weights' => $data['orderWeights'], 'fleet' => $data['fleet'], 'dispatchList' => $dispatchList, 'tripSheetId' => $tripSheetId, 'orders' => $orders, 'stock' => $stock, 'suggested' => $suggested, 'asProductsStatic' => $asProductsStatic,'dispatchEmployees' => $dispatchEmployees, 'dispatchFleet' => $fleet]);

            break;
        }
    }

     public function actionAdvancedStockPickingSlip($tripSheetId, $pickingMode)
    {
        switch($pickingMode)
        {
            case 2:

                $data = $this->pickingSheetDetails($tripSheetId);
                $stock = TripSheets::getPickingSheetModeDetails($tripSheetId);
                $bakery = Bakery::model()->findByPk($data['bakeryId']);

                $pdf = new DistributionPDFFactory('Picking Sheet for Shipment #' . $tripSheetId, $tripSheetId);
                echo $pdf->createPickingSheetSelf($pickingMode, $data, $stock, $bakery, $tripSheetId, false);

            break;

            default:

                $data = $this->pickingSheetDetails($tripSheetId);
                $dispatchList = $data['dispatchList'];
                $orders = $data['orders'];
                $stock = $data['stock'];
                $suggested = $data['suggested'];
                $asProductsStatic = $data['asProductsStatic'];

                $pdf = new DistributionPDFFactory('Picking Sheet for Shipment #' . $tripSheetId, $tripSheetId);
                echo $pdf->createPickingSheet($dispatchList, $tripSheetId, $orders, $stock, $suggested, $asProductsStatic, $data['bakeryId'], $data['orderWeights'], $data['fleet'], $data['linked'], false);

            break;
        }
    }

    public function actionAdvancedShipmentStockPickingSlip($tripSheetId)
    {
        $data = $this->pickingSheetDetails($tripSheetId);
        $tripSheet = TripSheets::model()->findByPk($tripSheetId);
        $requestData = [
            'Request' => [
                'ignoreDeviceCheck' => 1,
                'just_view' => 1,
                'employee_id' => $tripSheet->driver_employee_id,
                'bakery_id' => $tripSheet->bakery_id,
                'Dt' => date('Y-m-d H:i:s'),
                'trip_sheet_id' => $tripSheetId,
                'DeviceID' => '',
                'REG_KEY_VALUE' => '',
                'RequestRef' => 'closingstock_'.$tripSheet->driver_employee_id.'_'.date('YmdHis'),
                'Type' => 'checkclosingstockbyadmin',
                'from' => 'planned'
            ]
        ];
        $tss = APIUtils::requestPOST(Yii::app()->getBaseUrl(true).'/index.php/api/central/webservice' ,$requestData);
        $tssData = json_decode($tss['message']);

        $pdf = new DistributionPDFFactory('Shipment Stock for Shipment #' . $tripSheetId, $tripSheetId);
        echo $pdf->createShipmentPickingSheet($tssData, $tripSheetId, $data['orderWeights'], $data['fleet'], false);
    }

    public function actionAdvancedStockMovement($stockMovementId)
    {
        if(isset($_POST['asStockMovementBtn']))
        {
            $status = AsStockMovement::moveStockToEmployee($stockMovementId, 4);
            Yii::app()->user->setFlash($status['what'], $status['msg']);
            $this->redirect(Yii::app()->getBaseUrl(true) . '/index.php/admin/' . $this->id . '?show=in_dispatch');
            die();
        }

        //get the bakery id
        $bakeryId = AsStockMovementItems::getBakeryIdFromMovementId($stockMovementId);
        $orderBy = Bakery::getDistributionProductsOrderBy($bakeryId, 'sql_three');

        $stockMovementItems = AsStockMovementItems::model()
            ->with('stockMovement.tripSheet.driver', 'stockMovement.tripSheet.fleet', 'product.unitOfMeasure', 'product.type', 'product.supplier')
            ->findAll([
                'condition' => 'as_stock_movement_id = ' . $stockMovementId,
                'order' => $orderBy
            ]);
        $criteria = new CDbCriteria();
        $criteria->condition = 'asStockMovement.id = ' . $stockMovementId;
        $criteria->select = 't.notes, t.comments';
        $tripSheet = TripSheets::model()->with('asStockMovement')->find($criteria);
        $this->renderPartial('_asStockMovement', ['tripSheet' => $tripSheet, 'stockMovementItems' => $stockMovementItems, 'stockMovementId' => $stockMovementId]);
    }

    public function actionAdvancedStockMovementPDF($stockMovementId)
    {
        //get the bakery id
        $bakeryId = AsStockMovementItems::getBakeryIdFromMovementId($stockMovementId);
        $orderBy = Bakery::getDistributionProductsOrderBy($bakeryId, 'sql_three');

        $stockMovementItems = AsStockMovementItems::model()
            ->with('stockMovement.tripSheet.driver', 'stockMovement.tripSheet.fleet', 'product.unitOfMeasure', 'product.linked', 'product.type', 'product.supplier')
            ->findAll([
                'condition' => 'as_stock_movement_id = ' . $stockMovementId,
                'order' => $orderBy
            ]);

        $linked = [];
        foreach($stockMovementItems as $items)
        {
            foreach($items['product']['linked'] as $productsLinked)
            {
                if(!in_array($productsLinked['linked_product_id'], $linked) && $productsLinked['status_id'] === '1')
                {
                    array_push($linked, $productsLinked['linked_product_id']);
                }
            }
        }

        $orderBySummary = Bakery::getDistributionProductsOrderBy($bakeryId, 'sql_two');

        $summarySql = <<<SQL
        SELECT
            om.id AS order_id,
            c.description AS customer,
            c.address,
            e.`name` AS order_created_by,
            om.order_date,
            om.expected_delivery_date,
            om.order_total_amount,
            om.picked_order_total_amount,
            p.id as product_id,
            p.sku,
            p.description AS product,
            p.batch_code,
            p.expiry_date,
            uom.description unit_of_measure,
            t.description as `type`,
            s.description as supplier,
            SUM(o.qty_ordered) AS total_qty_ordered,
            SUM(o.qty_picked) AS total_qty_picked
        FROM as_stock_movement_items i
        INNER JOIN
        as_stock_movement m ON m.id = i.as_stock_movement_id
        INNER JOIN
        trip_sheets_items tsi ON tsi.trip_sheet_id = m.trip_sheet_id
        INNER JOIN
        order_main om ON om.id = tsi.order_id
        INNER JOIN
        outlet c ON c.id = om.`account`
        INNER JOIN
        employee e ON e.id = om.order_taker_employee_nr
        INNER JOIN
        orders o ON o.order_id = om.id AND o.product_id = i.product_id
        INNER JOIN
        products p ON p.id = o.product_id
        INNER JOIN
        units_of_measure uom ON uom.id = p.units_of_measure_id
        INNER JOIN
        product_type t on t.id = p.product_type_id
        INNER JOIN
        product_supplier s on s.id = p.supplier_id
        WHERE i.as_stock_movement_id = {$stockMovementId}
        AND i.product_indicator = 1
        GROUP BY om.id, p.sku, p.description, uom.description
        ORDER BY om.order_date, uom.description, p.sku, p.description;
SQL;
        $summary = Yii::app()->db->createCommand($summarySql)->queryAll();

        $summaryOrders = [];
        foreach($summary as $s)
        {
            $summaryOrders[$s['order_id']] = [];
        }
        foreach($summary as $s)
        {
            foreach($summaryOrders as $key => $arr)
            {
                if($s['order_id'] == $key)
                {
                    $summaryOrders[$key][] = $s;
                }
            }
        }

        $location = AsStockLocations::model()->findByPk($stockMovementItems[0]['stockMovement']->location_id);

        $tripSheetId = $stockMovementItems[0]['stockMovement']['trip_sheet_id'];

        $vsStock = TripSheetsStock::checkVanSaleStock($tripSheetId, '', true);

        $criteria = new CDbCriteria();
        $criteria->select = 't.notes, t.comments';
        $tripSheet = TripSheets::model()->findByPk($tripSheetId);
        $notes = '-';
        if($tripSheet->notes !== null)
        {
            $notes = nl2br($tripSheet->notes);
        }

        $comments = '-';
        if($tripSheet->comments !== null)
        {
            $comments = nl2br($tripSheet->comments);
        }

        $pdf = new DistributionPDFFactory('Dispatch Sheet for Trip Sheet #' . $tripSheetId . ' <barcode code="' .$tripSheetId . '" type="C128B" />', $tripSheetId);
        echo $pdf->createDispatchSheet($linked, $summary, $summaryOrders, $stockMovementItems, $stockMovementId, $location, $tripSheetId, $vsStock, $notes, $comments, $bakeryId, false);
    }

    public function actionAdvancedStockCheckIn($tripSheetId, $cancel = 'false', $from = '')
    {
        if(isset($_POST['asCheckInSubmitBtn']))
        {
            if($cancel === 'true')
            {
                $stockMovementId = AsStockMovement::model()->find('trip_sheet_id = :tripSheetId', [':tripSheetId' => $tripSheetId])->id;
                if($from == 'in_dispatch')
                {
                    $status = AsStockMovement::moveStockToEmployeeFromDispatch($stockMovementId, 4);

                    TripSheets::model()->updateByPk($tripSheetId, ['cancelled' => 1], 'id = :id', [':id' => $tripSheetId]);

                    $tripSheetsCancelled = new TripSheetsCancelled();
                    $tripSheetsCancelled->trip_sheet_id = $tripSheetId;
                    $tripSheetsCancelled->trip_sheets_cancelled_reason_id =$_POST['trip_sheets_cancelled_reason_id'];
                    $tripSheetsCancelled->comments = $_POST['comment'];
                    $tripSheetsCancelled->cancelled_by = Yii::app()->user->id;
                    $tripSheetsCancelled->cancelled_date = date('Y-m-d H:i:s');
                    $tripSheetsCancelled->save();
                }
                else
                {
                    $status = AsStockMovement::moveStockToEmployee($stockMovementId, 4);
                }
            }

            CustomUtils::closeShipmentFromBackEnd($from, $cancel);

            
            if(isset($_POST['stock_uplift_shipment']) && $_POST['stock_uplift_shipment'] == '1')
            {
                Yii::app()->user->setFlash('success', 'Stock uplift shipment successfully copmleted');
                $this->redirect(Yii::app()->getBaseUrl(true) . '/index.php/admin/' . $this->id . '?show=completed');
            }
            else
            {
                $stockMovementArea = TripSheetStatus::model()->findByPk(4)->description;
                Yii::app()->user->setFlash('success', 'Shipment successfully checked-in to ' . $stockMovementArea);
                $this->redirect(Yii::app()->getBaseUrl(true) . '/index.php/admin/' . $this->id . '?show=checkin');
            }
        }
        //get the settlement areas for the warehouse
        // $bakeryLocationWhere = CustomUtils::warehouseLocationWhereClause();
        $tripSheet = TripSheets::model()->findByPk($tripSheetId);
        $cancelReasonsData = TripSheetsCancelReasons::model()->findAllByAttributes(array('status_id'=>1));
        $cancelReasons = [];
        $cancelReasons[0] = "Please select a cancel reason";
        foreach($cancelReasonsData as $cancelR)
        {
            $cancelReasons[$cancelR['id']] = $cancelR['description'];
        }

        //means that this trip is partially accepted and that the driver needs to fully close before the admin
        //can cancel the trip from the dispatch tab
        if($tripSheet->partially_accept == 1 && $tripSheet->accepted == 1 && $tripSheet->partially_closed == 0 && $cancel == 'true' && $from == 'in_dispatch')
        {
            $this->renderPartial('_advancedStockCheckIn', ['notAllowed' => true,'cancelReasons'=>$cancelReasons]);
        }
        else
        {
            $tripSheetBakeryId = $tripSheet->bakery_id;
            $bakeryLocationWhere = ' AND warehouse.bakery_id = ' .$tripSheetBakeryId;
            $stockLocations = AsStockLocations::model()->with('warehouse')->findAll(['condition' => 't.location_type_id = 3 and t.status_id = 1' . $bakeryLocationWhere, 'order' => 'warehouse.description, t.description']);
            $settlementAreaList = [];
            foreach($stockLocations as $loc)
            {
                $settlementAreaList[$loc['warehouse']['description']][$loc['id']] = $loc['description'];
            }
            $tripSheetInv = TripSheets::model()->with('fleet', 'driver', 'asStockMovement', 'tripSheetItems.orderMain.outlet')->findByPk($tripSheetId, 'orderMain.order_status_id not in (13,19)');
            $stats = [];
            if(count($tripSheetInv['tripSheetItems']) > 0)
            {
                $unDelivered = 0;
                $unDeliveredIds = [];
                $unDeliveredCustomers = [];
                foreach($tripSheetInv['tripSheetItems'] as $items)
                {
                    if($items['orderMain']['order_status_id'] !== '5' &&
                            $items['orderMain']['order_status_id'] !== '13'  &&
                            $items['orderMain']['order_status_id'] !== '19'  &&
                            $items['orderMain']['order_status_id'] !== '8')
                    {
                        ++$unDelivered;
                        $unDeliveredIds[] = $items['orderMain']['id'];
                        $unDeliveredCustomers[] = $items['orderMain']['outlet']->description;
                    }
                }
                $stats = [
                    'trips' => count($tripSheetInv['tripSheetItems']),
                    'un_delivered' => $unDelivered,
                    'un_delivered_orders' => $unDeliveredIds,
                    'un_delivered_customers' => $unDeliveredCustomers
                ];
            }
            $tripSheetCn = TripSheets::model()->with('fleet', 'driver', 'asStockMovement', 'tripSheetItems.orderMain.outlet')->findByPk($tripSheetId, 'orderMain.order_status_id in (19)');
            $statsCn = [];
            if(count($tripSheetCn['tripSheetItems']) > 0)
            {
                $unDelivered = 0;
                $unDeliveredIds = [];
                $unDeliveredCustomers = [];
                foreach($tripSheetCn['tripSheetItems'] as $items)
                {
                    if($items['orderMain']['order_status_id'] !== '13')
                    {
                        ++$unDelivered;
                        $unDeliveredIds[] = $items['orderMain']['id'];
                        $unDeliveredCustomers[] = $items['orderMain']['outlet']->description;
                    }
                }
                $statsCn = [
                    'trips' => count($tripSheetCn['tripSheetItems']),
                    'un_delivered' => $unDelivered,
                    'un_delivered_orders' => $unDeliveredIds,
                    'un_delivered_customers' => $unDeliveredCustomers
                ];
            }

            //check the we are allowed partial closing
            $bakery = Bakery::model()->findByPk($tripSheetBakeryId);
            $partialClose = CHtml::hiddenField('partial_close', 0);
            if($bakery->partial_shipment_close == 1)
            {
                $selected = $bakery->partial_shipment_close;
                $hidden = '';
                if($from == 'in_dispatch')
                {
                    $selected = '0';
                    $hidden = 'style="display: none;"';
                }
                $htmlOptions = ['class' => 'form-control', 'onchange' => 'partialFullValues(this);'];
                $partialCloseDropdown = CHtml::dropDownList('partial_close', $selected, ['0' => 'No', '1' => 'Yes'], $htmlOptions);
                $partialClose = <<<HTML
                <div {$hidden} class="form-group">
                    <label for="partial_close">Partially Close Shipment</label>
                    {$partialCloseDropdown}
                </div>
HTML;
            }
            //get the drop off locations
            $criteria = new CDbCriteria();
            $criteria->condition = 't.location_type_id = 7 and t.status_id = 1 and warehouse.bakery_id = ' . $bakery->id . ' and warehouse.status_id = 1';
            $criteria->order = 'warehouse.description, t.description';
            $locations = AsStockLocations::model()->with('warehouse')->findAll($criteria);
            $dropOffLocations = [];
            foreach($locations as $l)
            {
                $dropOffLocations[$l['id']] = $l['warehouse']['description'] . ' - ' . $l['description'];
            }
            $response['Data']['drop_off_locations'] = $dropOffLocations;

            $noDeliveryReasonsList = CHtml::listData(MissedDeliveryReasons::model()->findAll(['condition' => 'status = 1', 'order' => 'description']) ,'id', 'description');

            //get the stock for this shipment
            $requestData = [
                'Request' => [
                    'ignoreDeviceCheck' => 1,
                    'just_view' => 1,
                    'employee_id' => $tripSheet->driver_employee_id,
                    'bakery_id' => $tripSheet->bakery_id,
                    'Dt' => date('Y-m-d H:i:s'),
                    'trip_sheet_id' => $tripSheetId,
                    'DeviceID' => '',
                    'REG_KEY_VALUE' => '',
                    'RequestRef' => 'closingstock_'.$tripSheet->driver_employee_id.'_'.date('YmdHis'),
                    'Type' => 'checkclosingstockbyadmin',
                    'from' => $from
                ]
            ];
            $tss = APIUtils::requestPOST(Yii::app()->getBaseUrl(true).'/index.php/api/central/webservice' ,$requestData);
            $tssData = json_decode($tss['message']);

            $this->renderPartial('_advancedStockCheckIn', ['tssData' => $tssData, 'from' => $from, 'tripSheetId' => $tripSheetId, 'settlementAreaList' => $settlementAreaList, 'tripSheet' => $tripSheet, 'stats' => $stats, 'statsCn' => $statsCn, 'noDeliveryReasonsList' => $noDeliveryReasonsList, 'cancel' => $cancel, 'partialCloseDropDown' => $partialClose, 'dropOffLocations' => $dropOffLocations,'cancelReasons'=>$cancelReasons]);
        }
    }

    public function actionAdvancedStockCheckInFullClose($tripSheetId, $cancel = false, $from = '')
    {
        if(isset($_POST['asCheckInSubmitBtn']))
        {
            if($cancel === 'true')
            {
                $stockMovementId = AsStockMovement::model()->find('trip_sheet_id = :tripSheetId', [':tripSheetId' => $tripSheetId])->id;
                if($from == 'in_dispatch')
                {
                    $status = AsStockMovement::moveStockToEmployeeFromDispatchFullClose($stockMovementId, 4);
                }


                // else
                // {
                //     $status = AsStockMovement::moveStockToEmployee($stockMovementId, 4);
                // }
            }

            CustomUtils::closeShipmentFromBackEndFull($from);

            $stockMovementArea = TripSheetStatus::model()->findByPk(4)->description;
            Yii::app()->user->setFlash('success', 'Shipment successfully checked-in to ' . $stockMovementArea);
            $this->redirect(Yii::app()->getBaseUrl(true) . '/index.php/admin/' . $this->id . '?show=checkin');
        }
        //get the settlement areas for the warehouse
        // $bakeryLocationWhere = CustomUtils::warehouseLocationWhereClause();
        $tripSheetBakeryId = TripSheets::model()->findByPk($tripSheetId)->bakery_id;
        $bakeryLocationWhere = ' AND warehouse.bakery_id = ' .$tripSheetBakeryId;
        $stockLocations = AsStockLocations::model()->with('warehouse')->findAll(['condition' => 't.location_type_id = 3 and t.status_id = 1' . $bakeryLocationWhere, 'order' => 'warehouse.description, t.description']);
        $settlementAreaList = [];
        foreach($stockLocations as $loc)
        {
            $settlementAreaList[$loc['warehouse']['description']][$loc['id']] = $loc['description'];
        }
        $tripSheet = TripSheets::model()->with('fleet', 'driver', 'asStockMovement', 'tripSheetItems.orderMain.outlet')->findByPk($tripSheetId);
        $stats = [];
        if(count($tripSheet['tripSheetItems']) > 0)
        {
            $unDelivered = 0;
            $unDeliveredIds = [];
            $unDeliveredCustomers = [];
            foreach($tripSheet['tripSheetItems'] as $items)
            {
                if($items['orderMain']['order_status_id'] !== '5' &&
                        $items['orderMain']['order_status_id'] !== '13'  &&
                        $items['orderMain']['order_status_id'] !== '8')
                {
                    ++$unDelivered;
                    $unDeliveredIds[] = $items['orderMain']['id'];
                    $unDeliveredCustomers[] = $items['orderMain']['outlet']->description;
                }
            }
            $stats = [
                'trips' => count($tripSheet['tripSheetItems']),
                'un_delivered' => $unDelivered,
                'un_delivered_orders' => $unDeliveredIds,
                'un_delivered_customers' => $unDeliveredCustomers
            ];
        }

        //check the we are allowed partial closing
        $bakery = Bakery::model()->findByPk($tripSheetBakeryId);
        $partialClose = CHtml::hiddenField('partial_close', 0);
        if($bakery->partial_shipment_close == 1)
        {
            $selected = $bakery->partial_shipment_close;
            $hidden = '';
            if($from == 'in_dispatch')
            {
                $selected = '0';
                $hidden = 'style="display: none;"';
            }
            $htmlOptions = ['class' => 'form-control', 'onchange' => 'partialFullValues(this);'];
            $partialCloseDropdown = CHtml::dropDownList('partial_close', $selected, ['0' => 'No', '1' => 'Yes'], $htmlOptions);
            $partialClose = <<<HTML
            <div {$hidden} class="form-group">
                <label for="partial_close">Partially Close Shipment</label>
                {$partialCloseDropdown}
            </div>
HTML;
        }
        //get the drop off locations
        $criteria = new CDbCriteria();
        $criteria->condition = 't.location_type_id = 7 and t.status_id = 1 and warehouse.bakery_id = ' . $bakery->id . ' and warehouse.status_id = 1';
        $criteria->order = 'warehouse.description, t.description';
        $locations = AsStockLocations::model()->with('warehouse')->findAll($criteria);
        $dropOffLocations = [];
        foreach($locations as $l)
        {
            $dropOffLocations[$l['id']] = $l['warehouse']['description'] . ' - ' . $l['description'];
        }
        $response['Data']['drop_off_locations'] = $dropOffLocations;

        $noDeliveryReasonsList = CHtml::listData(MissedDeliveryReasons::model()->findAll(['condition' => 'status = 1', 'order' => 'description']) ,'id', 'description');

        //get the stock for this shipment
        $requestData = [
            'Request' => [
                'ignoreDeviceCheck' => 1,
                'just_view' => 1,
                'employee_id' => $tripSheet->driver_employee_id,
                'bakery_id' => $tripSheet->bakery_id,
                'Dt' => date('Y-m-d H:i:s'),
                'trip_sheet_id' => $tripSheetId,
                'DeviceID' => '',
                'REG_KEY_VALUE' => '',
                'RequestRef' => 'closingstock_'.$tripSheet->driver_employee_id.'_'.date('YmdHis'),
                'Type' => 'checkclosingstockbyadmin',
                'from' => $from
            ]
        ];
        $tss = APIUtils::requestPOST(Yii::app()->getBaseUrl(true).'/index.php/api/central/webservice' ,$requestData);
        $tssData = json_decode($tss['message']);
        $cancelReasonsData = TripSheetsCancelReasons::model()->findAllByAttributes(array('status_id'=>1));
        $cancelReasons = [];
        $cancelReasons[0] = "Please select a cancel reason";
        
        foreach($cancelReasonsData as $cancelR)
        {
            $cancelReasons[$cancelR['id']] = $cancelR['description'];
        }
        $this->renderPartial('_advancedStockCheckInFullClose', ['tssData' => $tssData, 'from' => $from, 'tripSheetId' => $tripSheetId, 'settlementAreaList' => $settlementAreaList, 'tripSheet' => $tripSheet, 'stats' => $stats, 'noDeliveryReasonsList' => $noDeliveryReasonsList, 'cancel' => $cancel, 'partialCloseDropDown' => $partialClose, 'dropOffLocations' => $dropOffLocations,'cancelReasons'=>$cancelReasons]);
    }

    public function actionStockSettlementMode($tripSheetId, $settlementId)
    {
        $tripSheet = TripSheets::model()->with('bakery')->findByPk($tripSheetId);
        if($tripSheet->settlement_mode_id === null)
        {
            $settlementMode = $tripSheet['bakery']->settlement_mode_id;
        }
        else
        {
            $settlementMode = $tripSheet->settlement_mode_id;
        }

        $where = '';
        if($tripSheet->partially_closed == '1')//can only settle per shipment period
        {
            $where = 't.id = 1';
            $settlementMode = '1';
        }

        $sql = <<<SQL
        select * from trip_sheets_items tsi
        left join order_main om on om.id = tsi.order_id
        where tsi.trip_sheet_id = {$tripSheetId}
        and om.order_type = 2 and om.order_status_id = 5
SQL;
        $vanSaleOrders = Yii::app()->db->createCommand($sql)->queryAll();
        $vanSaleOrdersCount = count($vanSaleOrders);
        if($vanSaleOrdersCount > 0)
        {
            $where = 't.id = 1';
            $settlementMode = '1';
        }

        $settlementModeDropDownData = SettlementMode::model()->findAll($where);
        $settlementModeDropDownList = CHtml::listData($settlementModeDropDownData, 'id', 'description');

        $settlment = TripSheetsSettlement::model()->findByPk($settlementId);

        $this->renderPartial('_settlementCheck', ['settlment' => $settlment, 'settlementId' => $settlementId, 'tripSheet' => $tripSheet, 'tripSheetId' => $tripSheetId, 'settlementMode' => $settlementMode, 'settlementModeDropDownList' => $settlementModeDropDownList]);
    }

    public function actionStockSettlement($tripSheetId, $settlementId)
    {
        // $vanSales = new Vansales();
        $tripSheetSettlement = new TripSheetsSettlement();
        $s = TripSheetsSettlement::model()->findByPk($settlementId);
        $settlementStatus = $s->level_status;

        $vanSales = new Vansales();
        //check if the trip sheet blind check has already been done
        $status = $vanSales->checkTripSheetLevelStatus($settlementId, $tripSheetId, 'variance');
        if($status['block'])
        {
            $msg = end($status['messages']);
            echo <<<HTML
            <div class="alert alert-info">{$msg}</div>
HTML;
            die();
        }

        $tripSheet = TripSheets::model()->with('settlementMode')->findByPk($tripSheetId);
        // $tripSheetStatus = $tripSheet->level_status;

        if($settlementStatus === '1')
        {
            $data = $tripSheetSettlement->getVarianceTripProducts($tripSheetId, $settlementId);
            $this->renderPartial('_variance', ['tripSheet' => $tripSheet, 'data' => $data, 'tripSheetId' => $tripSheetId, 'settlementId' => $settlementId, 'print' => '0']);
        }
        else
        {
            $data = $tripSheetSettlement->getBlindCheckTripProducts($tripSheetId, $settlementId);
            //check here if the branch should see what the driver captured

            $bakery = Bakery::model()->findByPk($tripSheet->bakery_id);
            $canSee = isset($bakery->show_driver_captured_closed_load_values) ? $bakery->show_driver_captured_closed_load_values : '0';
            if($canSee === '0')
            {
                $canSeeDriverCapturedQty = false;
            }
            else
            {
                $canSeeDriverCapturedQty = true;
            }

            $this->renderPartial('_blindcheck', ['data' => $data, 'settlementId' => $settlementId, 'tripSheetId' => $tripSheetId, 'canSeeDriverCapturedQty' => $canSeeDriverCapturedQty]);
        }
    }

    public function actionStockSettlementOrder($tripSheetId, $settlementId)
    {
        $tripSheet = TripSheets::model()->with('settlementMode')->findByPk($tripSheetId);
        $bakeryId = $tripSheet->bakery_id;
        $tripSheetStatus = $tripSheet->level_status;

        $tripDetails = TripSheets::model()->with('driver', 'fleet')->find('t.id = ' . $tripSheetId);

        $orderBy = Bakery::getDistributionProductsOrderBy($bakeryId);

        if($tripSheetStatus === '1')
        {
            //variance magic here...
            $order = 'tripSheetsReturnsOrders.order_id DESC, ' . $orderBy;
            $varianceReasons = CheckingVarianceReasons::model()->findAll(['select' => 'id, description','condition' => 't.status = 1', 'order' => 't.description']);
            $data = TripSheets::model()->with('tripSheetsReturnsOrders.orderMain.outlet', 'tripSheetsReturnsOrders.products.supplier', 'tripSheetsReturnsOrders.products.unitOfMeasure', 'tripSheetsReturnsOrders.products.type')->find(['condition' => 't.id = ' . $tripSheetId . ' AND tripSheetsReturnsOrders.product_indicator = 1', 'order' => $order]);
            $dataReturns = TripSheets::model()->with('tripSheetsReturnsOrders.orderMain.outlet', 'tripSheetsReturnsOrders.products.supplier', 'tripSheetsReturnsOrders.products.unitOfMeasure', 'tripSheetsReturnsOrders.products.type')->find(['condition' => 't.id = ' . $tripSheetId . ' AND tripSheetsReturnsOrders.product_indicator = 2', 'order' => $order]);
            $this->renderPartial('_varianceOrder', ['settlementId' => $settlementId, 'dataReturns' => $dataReturns, 'varianceReasons' => $varianceReasons, 'tripSheet' => $tripSheet, 'tripDetails' => $tripDetails, 'data' => $data]);
        }
        else
        {
            // $cash = new Vansales();
            // $cash->getBlindCheckTripProducts($tripSheetId);

            $data = TripSheetsItems::model()
                ->with('orderMain.orders.products.unitOfMeasure', 'orderMain.orders.products.type', 'orderMain.orders.products.supplier', 'orderMain.outlet')
                ->findAll(['condition' => 't.trip_sheet_id = ' . $tripSheetId . ' AND orderMain.order_type IN (1, 5)', 'order' => $orderBy]);

            $orderByReturns = str_replace('products', 'productsReturns', $orderBy);
            $dataReturns = TripSheetsItems::model()
                ->with([
                    'orderMain.ordersReturns.productsReturns.unitOfMeasure' => ['joinType' => 'INNER JOIN'],
                    'orderMain.ordersReturns.productsReturns.type' => ['joinType' => 'INNER JOIN'],
                    'orderMain.ordersReturns.productsReturns.supplier' => ['joinType' => 'INNER JOIN']
                    ]
                    , 'orderMain.outlet')
                ->findAll(['condition' => 't.trip_sheet_id = ' . $tripSheetId, 'order' => $orderByReturns]);

            $vsStock = TripSheetsStock::checkVanSaleStockSettlement($tripSheetId, 'AND asml.product_indicator = 1', true, $settlementId);

            $vsStockReturns = TripSheetsStock::checkVanSaleStockSettlement($tripSheetId, 'AND asml.product_indicator = 2', true, $settlementId);

            //check here if the branch should see what the driver captured
            $bakery = Bakery::model()->findByPk($tripSheet->bakery_id);
            $canSee = isset($bakery->show_driver_captured_closed_load_values) ? $bakery->show_driver_captured_closed_load_values : '0';
            if($canSee === '0')
            {
                $canSeeDriverCapturedQty = false;
            }
            else
            {
                $canSeeDriverCapturedQty = true;
            }

            $this->renderPartial('_blindCheckOrder', ['settlementId' => $settlementId, 'tripDetails' => $tripDetails, 'vsStockReturns' => $vsStockReturns,'dataReturns' => $dataReturns, 'vsStock' => $vsStock, 'data' => $data, 'tripSheetId' => $tripSheetId, 'canSeeDriverCapturedQty' => $canSeeDriverCapturedQty]);
        }
    }

    public function actionStockSettlementBlindCheck($splitted = null)
    {
        $settlement = new TripSheetsSettlement();
        echo $settlement->performBlindCheck($splitted);
    }

    public function actionStockSettlementBlindCheckOrders()
    {
        $vanSales = new Vansales();
        echo $vanSales->performBlindCheckOrders();
    }

    public function actionStockSettlementVariance($splitted = null)
    {
        $settlement = new TripSheetsSettlement();
        echo $settlement->performVariance($splitted);
    }

    public function actionstockSettlementVarianceOrders()
    {
        $vanSales = new Vansales();
        echo $vanSales->performVarianceOrders();
    }

    public function actionCancelShipment($tripSheetId)
    {
        if(isset($_POST['option']) && $_POST['option'] === '1')
        {
            //move any orders on this shipment to NEW
            $tripSheetItems = TripSheetsItems::model()->findAll('trip_sheet_id = ' . $_POST['tripSheetId']);
            foreach($tripSheetItems as $items)
            {
                $om = Order_main::model()->findByPk($items['order_id']);
                if($om->order_status_id == 2)
                {
                    $om->order_status_id = 1;
                    $om->linked_to_van_sale = 0;
                    $om->collection_order = 0;
                    $om->save();
                }
                elseif($om->order_status_id == 19) //delete the credit note and reset the associated trade replacement
                {
                    $tr = TradeReturnsMain::model()->find('order_id = :orderId', [':orderId' => $items['order_id']]);
                    if(isset($tr->id))
                    {
                        $tr->order_id = null;
                        $tr->order_created_by = null;
                        $tr->order_created_date = null;
                        $tr->status = 0;
                        $tr->approved_by = null;
                        $tr->approved_date = null;
                        $tr->save();
                    }
                    //delete all the returns for this credit note as well
                    OrdersReturns::model()->deleteAll('order_id = :orderId', [':orderId' => $items['order_id']]);
                    $om->delete();
                }
            }
            //delete from trip_sheets_items
            TripSheetsItems::model()->deleteAll('trip_sheet_id = ' . $_POST['tripSheetId']);
            //delete from trip sheets
            //TripSheets::model()->deleteByPk($_POST['tripSheetId']);

            TripSheets::model()->updateByPk($_POST['tripSheetId'], ['cancelled' => 1], 'id = :id', [':id' => $_POST['tripSheetId']]);
            
            $tripSheetsCancelled = new TripSheetsCancelled();
            $tripSheetsCancelled->trip_sheet_id = $_POST['tripSheetId'];
            $tripSheetsCancelled->trip_sheets_cancelled_reason_id = $_POST['trip_sheets_cancelled_reason_id'];
            $tripSheetsCancelled->comments = $_POST['comment'];
            $tripSheetsCancelled->cancelled_by = Yii::app()->user->id;
            $tripSheetsCancelled->cancelled_date = date('Y-m-d H:i:s');
            $tripSheetsCancelled->save();

            echo 1;
            die();
        }
        $cancelReasonsData = TripSheetsCancelReasons::model()->findAllByAttributes(array('status_id'=>1));
        $cancelReasons = [];
        $cancelReasons[0] = "Please select a cancel reason";
        foreach($cancelReasonsData as $cancelR)
        {
            $cancelReasons[$cancelR['id']] = $cancelR['description'];
        }
        $tripSheet = TripSheets::model()->with('tripSheetItems.orderMain.outlet')->findByPk($tripSheetId);
        $this->renderPartial('_cancelShipment', ['tripSheet' => $tripSheet, 'tripSheetId' => $tripSheetId,'cancelReasons'=>$cancelReasons]);
        die();
    }

    public function actionCancelOrder($orderId, $orderCancelReasonId)
    {
        $cancelDate = date('Y-m-d H:i:s');

        Order_main::model()->updateByPk($orderId, [
            'order_status_id' => 7,
            'cancel_date' => $cancelDate,
            'cancel_user' => Yii::app()->user->id
        ]);

        $ordersCancelled = new OrdersCancelled();
        $ordersCancelled->order_id = $orderId;
        $ordersCancelled->orders_cancelled_reason_id = $orderCancelReasonId;
        $ordersCancelled->date_created = $cancelDate;

        $ordersCancelled->save();

        echo <<<HTML
        <div class="alert alert-success">Successful, please wait...</div>
HTML;
    }

    public function actionCancelOrdersSubmit()
    {
        $cancelDate = date('Y-m-d H:i:s');

        $orders = $_POST['orders'];
        $cancelOrderCount = count($orders);
        $successfulCancelOrderCount = 0;
        foreach($orders as $order)
        {
            $orderId = $order['order_id'];
            $orderCancelReasonId = $order['reason'];

            Order_main::model()->updateByPk($orderId, [
                'order_status_id' => 7,
                'cancel_date' => $cancelDate,
                'cancel_user' => Yii::app()->user->id
            ]);
//
            $ordersCancelled = new OrdersCancelled();
            $ordersCancelled->order_id = $orderId;
            $ordersCancelled->orders_cancelled_reason_id = $orderCancelReasonId;
            $ordersCancelled->date_created = $cancelDate;
//
            if($ordersCancelled->save())
            {
                ++$successfulCancelOrderCount;
            }
        }
        if($successfulCancelOrderCount === $cancelOrderCount)
        {
            echo 1;
        }
        else
        {
            echo 0;
        }
    }

    public function actionCancelOrders()
    {
        $reasons = OrdersCancelledReasons::orderCancelledReasons();
        $list = CHtml::listData($reasons, 'id', 'description');

        $orders = [];
        foreach($_POST['orders'] as $orderId)
        {
            $orderMainDetail = Order_main::model()
                ->with([
                    'outlet' => [
                        'select' => 'outlet.description'
                    ],
                    'employee' => [
                        'select' => 'employee.name'
                    ]
                ])
            ->findByPk($orderId, ['select' => 't.id, t.order_date, t.expected_delivery_date']);

            $orders[$orderMainDetail['id']] = [
                'outlet' => $orderMainDetail['outlet']->description,
                'created_by' => $orderMainDetail['employee']->name,
                'order_date' => $orderMainDetail->order_date,
                'expected_delivery_date' => $orderMainDetail->expected_delivery_date
            ];
        }

        $this->renderPartial('_cancelOrders', ['list' => $list, 'orders' => $orders]);
    }

    public function actionMergeOrders($orderStatusId = '1')
    {
        $orders = Order_main::getMergableOrders($orderStatusId);

        $this->renderPartial('_mergeOrders', ['orders' => $orders, 'orderStatusId' => $orderStatusId]);
    }

    public function actionMergeOrdersValidate($orderStatusId = '1')
    {
        // var_dump($_POST);
        $valid = true;
        $return  = [
            'errors' => '',
            'valid' => $valid
        ];
        foreach($_POST['order_id'] as $account => $orderIds)
        {
            if(count($orderIds) == 1)
            {
                $outlet = Outlet::model()->findByPk($account);
                $return['errors'] .= 'Please select more than 1 order to merge for ' . $outlet->description . '<br>';
                $return['valid'] = false;
            }
        }

        echo json_encode($return);
        die();
    }

    public function actionMergeOrdersPreview($orderStatusId = '1')
    {
        $outletIds = array_keys($_POST['order_id']);
        $paymentTypeForOutlet = [];
        foreach($outletIds as $outletId)
        {
            if(isset($_POST['order_id'][$outletId]))
            {
                $ids = array_keys($_POST['order_id'][$outletId]);
                $orderIdsIn = implode(',', $ids);
                $criteria = new CDbCriteria();
                $criteria->select = 'DISTINCT payment_type_id';
                $criteria->order = 'order_date';
                $criteria->condition = 'id IN (' . $orderIdsIn . ')';
                $paymentTypes = Order_main::model()->findAll($criteria);
                $paymentTypeForOutlet[$outletId] = isset($paymentTypes[0]['payment_type_id']) ? $paymentTypes[0]['payment_type_id'] : 0;
            }
        }

        $fpData = $_POST;
        $final = ($orderStatusId == '1') ? CustomUtils::mergerOrders() : CustomUtils::mergerCreditNoteOrders();

        $c = new CDbCriteria();
        $c->select = 'id, description';
        $c->condition = 'status_id = 1';
        $c->order = 'description';
        $paymentTypesData = PaymentType::model()->findAll($c);
        $paymentTypesList = CHtml::listData($paymentTypesData, 'id', 'description');

        echo $this->renderPartial('_mergeOrdersPreview', ['final' => $final, 'fpData' => $fpData, 'paymentTypeForOutlet' => $paymentTypeForOutlet, 'paymentTypesList' => $paymentTypesList, 'orderStatusId' => $orderStatusId], true);

        die();
    }

    public function actionMergeOrdersProcess($orderStatusId = '1')
    {
        $mergedDate = date('Y-m-d H:i:s');
        //we need to mark orders as MERGED status and insert into the merged_order_id column in order_main
        $final = ($orderStatusId == '1') ? CustomUtils::mergerOrders() : CustomUtils::mergerCreditNoteOrders();

        $mergedAll = true;
        $newOrders = [];
        $requestUrl = Yii::app()->getBaseUrl(true) . '/index.php/api/central/webservice';
        $message = 'Failed';
        foreach($final as $outletId => $request)
        {
            $data = [
                'Request' => $request
            ];

            if(isset($_POST['order_id'][$outletId]) && $orderStatusId == '18')
            {
                $orderIds = array_keys($_POST['order_id'][$outletId]);
                $data['Request']['comments'] .= ' AND order ID\'s ' . implode(',', $orderIds);
            }

            $result = APIUtils::requestPOST($requestUrl, $data);
            if($result['http_code'] == 200)
            {
                $response = json_decode($result['message']);
                $message = (isset($response->Data->Message)) ? $response->Data->Message : 'FAILED';
                $newOrderId = (isset($response->Data->order_id)) ? $response->Data->order_id : 'FAILED';
                $newOrders[$outletId] = [
                    'outlet' => $message . ' for customer ' . $request['outlet_description'],
                    'order_id' => $newOrderId
                ];

                if(isset($_POST['order_id'][$outletId]))
                {
                    $orders = array_keys($_POST['order_id'][$outletId]);
                    foreach($orders as $order)
                    {
                        Order_main::model()->updateByPk($order, ['merged_order_id' => $newOrderId, 'merged_by' => Yii::app()->user->id, 'merged_date' => $mergedDate, 'order_status_id' => 17]);
                        if($orderStatusId == '18')
                        {
                            //update the associated trade replacement as well with the new order id
                            $trm = TradeReturnsMain::model()->find('order_id = :orderId', [':orderId' => $order]);
                            $trm->order_id = $newOrderId;
                            $trm->order_created_by = Yii::app()->user->id;
                            $trm->order_created_date = $mergedDate;
                            $trm->save();
                        }
                    }
                }
            }
            else
            {
                $newOrders[$outletId] = [
                    'outlet' => $message . ' for customer ' . $request['outlet_description'],
                    'order_id' => 'FAILED'
                ];
                $mergedAll = false;
            }
        }

        echo $this->renderPartial('_mergeOrdersResult', ['newOrders' => $newOrders, 'mergedAll' => $mergedAll, 'orderStatusId' => $orderStatusId], true);
        die();
    }

    public function actionImportBulkShipmentsFilePreview()
    {
        $errors = false;
        $details = [];
        $name = $_FILES['bulkShipmentsImportFile']['name'];
        $fileDetailsArr = explode('.', $name);
        $fileDetailsArrSize = count($fileDetailsArr);
        $fileTypeIndex = $fileDetailsArrSize - 1;
        $fileType = $fileDetailsArr[$fileTypeIndex];
        if(strtolower($fileType) !== 'xlsx' && $name !== '')
        {
            $errors = true;
            $details['type'] = 'error';
            $details['message'] = 'Oh oh! Only .xlxs files are excepted';
        }

        if(!$errors)
        {
            $filename = 'Bulk Shipments Import File ' . date('YmdHis') . '.xlsx';
            $destination = Yii::app()->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'files' . DIRECTORY_SEPARATOR . 'bulkShipments' . DIRECTORY_SEPARATOR . $filename;
            if(move_uploaded_file($_FILES['bulkShipmentsImportFile']['tmp_name'], $destination))
            {
                $dataErrors = false;
                //do the validation of the contents of the file here...
                $phpExcelPath = Yii::getPathOfAlias('ext.phpexcel.Classes');
                include($phpExcelPath . DIRECTORY_SEPARATOR . 'PHPExcel.php');

                $columns = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
                $errorDetailsArray = [
                    'Shipment Date' => []
                ];

                $objReader = new PHPExcel_Reader_Excel2007();
                $objReader->setLoadSheetsOnly(array('Details'));
                $objPHPExcel = $objReader->load($destination);
                $activeSheet = $objPHPExcel->setActiveSheetIndex(0);
                $highestDataRow = $activeSheet->getHighestDataRow();
                $activeSheetData = $activeSheet->rangeToArray('A1:P'.$highestDataRow);
                $headerData = array_shift($activeSheetData);
                $row = 2;
                $errorDetailsArray = [];

                for($i = 0; $i < count($activeSheetData); ++$i)
                {
                    $dataRow = $activeSheetData[$i];

                    $shipmentDate = isset($dataRow[0]) ? $dataRow[0] : '';
                    $rowDetails = CustomUtils::validateShipmentDate($shipmentDate, $columns[0], $row);
                    foreach($rowDetails as $detail)
                    {
                        if(!empty($detail))
                        {
                            $errorDetailsArray['Shipment Date'][] = $detail;
                            $dataErrors = true;
                        }
                    }

                    $orderId = isset($dataRow[1]) ? $dataRow[1] : '';
                    $rowDetails = CustomUtils::validateOrderId($orderId, $columns[1], $row);
                    foreach($rowDetails as $detail)
                    {
                        if(!empty($detail))
                        {
                            $errorDetailsArray['Order ID'][] = $detail;
                            $dataErrors = true;
                        }
                    }

                    $customerName = isset($dataRow[2]) ? $dataRow[2] : '';
                    $rowDetails = CustomUtils::validateEmpty($customerName, $columns[2], $row);
                    if(!empty($rowDetails))
                    {
                        $errorDetailsArray['Customer'][] = $rowDetails;
                        $dataErrors = true;
                    }

                    $productId = isset($dataRow[5]) ? $dataRow[5] : '';
                    $rowDetails = CustomUtils::validateProductId($productId, $columns[5], $row);
                    foreach($rowDetails as $detail)
                    {
                        if(!empty($detail))
                        {
                            $errorDetailsArray['Product ID'][] = $detail;
                            $dataErrors = true;
                        }
                    }

                    $qty = isset($dataRow[14]) ? $dataRow[14] : '';
                    $rowDetails = CustomUtils::validateQty($qty, $columns[14], $row);
                    foreach($rowDetails as $detail)
                    {
                        if(!empty($detail))
                        {
                            $errorDetailsArray['Qty'][] = $detail;
                            $dataErrors = true;
                        }
                    }

                    $employeeId = isset($dataRow[15]) ? $dataRow[15] : '';
                    $rowDetails = CustomUtils::validateEmployeeId($employeeId, $columns[15], $row);
                    foreach($rowDetails as $detail)
                    {
                        if(!empty($detail))
                        {
                            $errorDetailsArray['Employee ID'][] = $detail;
                            $dataErrors = true;
                        }
                    }

                    ++$row;
                }

                if($dataErrors)
                {
                    $details['type'] = 'error';
                    $details['message'] = 'The uploaded file contains errors, please have a look';
                    $details['details'] =  $errorDetailsArray;
                }
            }
            else
            {
                $details['type'] = 'error';
                $details['message'] = 'Well this is embarrasing, file could not be uploaded';
            }
        }

        if(empty($details))
        {
            //import into preview table here
            $rowChunk = [];
            for($i = 0; $i < count($activeSheetData); ++$i)
            {
                $dataRow = $activeSheetData[$i];
                $row = ($i+1)+1;
                $rowChunk[] = '("'.$dataRow[0].'", '.$dataRow[1].', '.$dataRow[5].', '.$dataRow[14].', '.$dataRow[15].', ' . $row . ')';
            }

            Yii::app()->db->createCommand('truncate table trip_sheets_bulk_preview;')->execute();

            $insertChunk = array_chunk($rowChunk, 5);
            foreach($insertChunk as $chunk)
            {
                $rowValues = implode(',', $chunk);
                $sql = <<<SQL
                insert into trip_sheets_bulk_preview (shipment_date, order_id, product_id, qty, employee_id, `row`) values {$rowValues};
SQL;
                Yii::app()->db->createCommand($sql)->execute();
            }

            //now do some validation
            //validate whether the order id is alreay against a shipment
            $ordersVsShipments = CustomUtils::validateOrderVsShipment();
            foreach($ordersVsShipments as $detail)
            {
                if(!empty($detail))
                {
                    $errorDetailsArray['Order ID\'s'][] = $detail;
                    $dataErrors = true;
                }
            }

            //validate whether there is 1 employee against an order
            $orderVsEmployee = CustomUtils::validateOrderVsEmployeeId();
            foreach($orderVsEmployee as $detail)
            {
                if(!empty($detail))
                {
                    $errorDetailsArray['Order ID & Employee ID'][] = $detail;
                    $dataErrors = true;
                }
            }
            //validate whether there are more than on shipment date against an order
            $orderVsShipmentDate = CustomUtils::validateOrderVsShipmentDate();
            foreach($orderVsShipmentDate as $detail)
            {
                if(!empty($detail))
                {
                    $errorDetailsArray['Shipment Date & Order ID'][] = $detail;
                    $dataErrors = true;
                }
            }

            //validate whether there are duplicate product id's against an order
            $productIdsVsOrder = CustomUtils::validateDuplicateProductsVsOrder();
            foreach($productIdsVsOrder as $detail)
            {
                if(!empty($detail))
                {
                    $errorDetailsArray['Column Order ID & Product ID'][] = $detail;
                    $dataErrors = true;
                }
            }

            //validate whether returns exceeds orders
            $totalOrderValue = CustomUtils::validateTotalOrderValues();
            foreach($totalOrderValue as $detail)
            {
                if(!empty($detail))
                {
                    $errorDetailsArray['Total Values'][] = $detail;
                    $dataErrors = true;
                }
            }

            // validate the employee already has a shipment for the dates provided
            $employeeShipmentDate = CustomUtils::validateEmployeeShipmentDate();
            foreach($employeeShipmentDate as $detail)
            {
                if(!empty($detail))
                {
                    $errorDetailsArray['Shipment Date & Employee ID'][] = $detail;
                    $dataErrors = true;
                }
            }

            if($dataErrors)
            {
                $details['type'] = 'error';
                $details['message'] = 'The uploaded file contains errors, please have a look';
                $details['details'] =  $errorDetailsArray;
            }
            else
            {
                $infoDetailsArray = CustomUtils::getOrderAdjustmentInfo();

                $details['type'] = 'success';
                $details['message'] = 'The uploaded file is correct, please have a look at the preview below';
                $details['details'] =  $infoDetailsArray;
            }
        }

        $this->renderPartial('_bulkShipmentImportPreview', ['details' => $details]);
        die();
    }

    public function actionImportBulkShipmentsPreview()
    {
        $results = TripSheetsBulkPreview::getBulkTripSheetSummary();

        $fleetList = Fleet::getFleetOptions();

        $this->renderPartial('_bulkShipmentImportPreviewShipment', ['results' => $results, 'fleetList' => $fleetList]);
    }

    public function actionShowBulkShipmentDetail()
    {
        $shipmentDate = $_POST['shipDate'];
        $employeeId = $_POST['eId'];
        $results = TripSheetsBulkPreview::getOrdersForBulkTripSheets($shipmentDate, $employeeId);

        echo $this->renderPartial('_bulkShipmentDetail', ['results' => $results]);
    }

    public function actionImportBulkShipments()
    {
        $newTripSheetIds = [];
        $createdDate = date('Y-m-d H:i:s');
        foreach($_POST['bulkShipments'] as $shipment)
        {
            $shipmentDate = $shipment['expected_delivery_date'];
            $driverEmployeeId = $shipment['driver_employee_id'];
            //create trip_sheet
            $tripSheet = new TripSheets();
            $tripSheet->fleet_id = $shipment['fleet_id'];
            $tripSheet->bakery_id = Yii::app()->user->bakery;
            $tripSheet->opening_branch_id = Yii::app()->user->bakery;
            $tripSheet->opening_location_id = 0;
            $tripSheet->created_employee_id = Yii::app()->user->id;
            $tripSheet->created_date = $createdDate;
            $tripSheet->driver_employee_id = $driverEmployeeId;
            $tripSheet->expected_delivery_date = $shipmentDate;
            $tripSheet->level_status = 0;
            $tripSheet->trip_completed = 0;
            $tripSheet->combined_load = 1;
            $tripSheet->accepted = 0;
            $tripSheet->trip_sheet_status_id = 1;
            $tripSheet->trip_type = 1;
            $tripSheet->opening_mileage = 0;
            if($shipment['notes'] !== '')
            {
                $tripSheet->notes = $shipment['notes'];
            }
            $tripSheet->save();

            $tripSheetId = $tripSheet->id;
            $newTripSheetIds[$tripSheetId] = [];

            $results = TripSheetsBulkPreview::getOrdersForBulkTripSheets($shipmentDate, $driverEmployeeId);
            //create trip_sheets_items
            foreach($results as $result)
            {
                $orderId = $result['order_id'];
                $tripSheetItem = new TripSheetsItems();
                $tripSheetItem->order_id = $orderId;
                $tripSheetItem->trip_sheet_id = $tripSheetId;
                $tripSheetItem->stock_location_id = 0;
                $tripSheetItem->is_active = 1;

                $tripSheetItem->save();

                Order_main::model()->updateByPk($orderId, [
                    'linked_to_van_sale' => 1,
                    'order_status_id' => 2
                ]);

                $newTripSheetIds[$tripSheetId][] = $orderId;
            }
        }

        TripSheetsBulkPreview::updateBulkShipmentsImportOrders();

        foreach($newTripSheetIds as $tripId => $orderArr)
        {
            foreach($orderArr as $orderId)
            {
                $sql = <<<SQL
                CALL sp_update_order_details({$orderId});
SQL;
                Yii::app()->db->createCommand($sql)->execute();
            }
        }

        //insert into trip_sheets_stock
        $sql = <<<SQL
        insert into trip_sheets_stock (trip_sheet_id, product_id, opening_qty, variance_id, product_indicator, opening_qty_product_price)
        SELECT
            i.trip_sheet_id,
            o.product_id,
            SUM(o.qty_ordered) AS opening_qty,
            0 AS variance_id,
            1 AS product_indicator,
            p.price AS opening_qty_product_price
        FROM
            trip_sheets_items i
                INNER JOIN
            order_main om ON om.id = i.order_id
                INNER JOIN
            orders o ON o.order_id = om.id
                INNER JOIN
            trip_sheets_bulk_preview pr ON pr.order_id = i.order_id
                AND pr.product_id = o.product_id
                INNER JOIN
            products p ON p.id = o.product_id
        GROUP BY i.trip_sheet_id , o.product_id;
SQL;
        Yii::app()->db->createCommand($sql)->execute();

        //post update trip_sheets opening_qty_value column
        foreach($newTripSheetIds as $tripId => $orderArr)
        {
            $sql = <<<SQL
            UPDATE trip_sheets s
                    INNER JOIN
                (SELECT
                    s.trip_sheet_id,
                        ROUND(SUM(s.opening_qty * s.opening_qty_product_price)) AS val
                FROM
                    trip_sheets_stock s
                GROUP BY s.trip_sheet_id) tmp ON tmp.trip_sheet_id = s.id
            SET
                s.opening_qty_value = tmp.val
            WHERE
                s.id = {$tripId};
SQL;
            Yii::app()->db->createCommand($sql)->execute();

            $data['trip_sheet_id'] = $tripId;
            TripSheetsPlannedLoadLog::log(TripSheetsPlannedLoadLog::$LOG_CREATED, $data);
        }

        echo 1;
        die();
    }

    public function actionBulkShipment()
    {
        $this->renderPartial('_bulkShipment');
    }

    public function actionEmployeeGroupDropdownFromRoute()
    {
        $bakeryId = Yii::app()->user->bakery;
        $sql = <<<SQL
        SELECT
            eg.id, eg.`description` AS employee_group
        FROM
            employee e
                INNER JOIN
            employee_group eg ON eg.id = e.employee_group_id
                INNER JOIN
            `role` er ON er.id = eg.role_id
                INNER JOIN
            (SELECT
                o.route_id
            FROM
                order_main om
            INNER JOIN outlet o ON o.id = om.`account`
            WHERE
                om.order_status_id = 1
                    AND om.high_risk = 0
                    AND om.bakery_id = {$bakeryId}
            GROUP BY o.route_id) r ON r.route_id = e.route_id
        WHERE
            er.id > 1
        GROUP BY eg.id
        ORDER BY eg.`description`;
SQL;

        $data = Yii::app()->db->createCommand($sql)->queryAll();

        $list = [];
        foreach($data as $d)
        {
            $list[$d['id']] = $d['employee_group'];
        }

        $this->renderPartial('_bulkShipmentEmployeeGroupDropdown', ['data' => $list]);
    }

    public function actionDownloadBulkShipmentsImportFile()
    {
        if(!empty($_POST))
        {
            setcookie('file_downloaded', 0, time()+20);
            $_SESSION['BULK_SHIPMENTS']['SHIPMENT_DATE'] = $_POST['shipment_date'];
            $_SESSION['BULK_SHIPMENTS']['EMPLOYEE_GROUP'] = isset($_POST['employee_group']) ? $_POST['employee_group'] : 0;
            echo 1;
        }
        else
        {
            $templatePath = Yii::app()->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR . 'bulk_shipments.xlsx';
            $phpExcelPath = Yii::getPathOfAlias('ext.phpexcel.Classes');
            include($phpExcelPath . DIRECTORY_SEPARATOR . 'PHPExcel.php');

            $objReader = new PHPExcel_Reader_Excel2007();
            $objPHPExcel = $objReader->load($templatePath);
            $activeSheet = $objPHPExcel->setActiveSheetIndex(0);

            $bakeryId = Yii::app()->user->bakery;
            $shipmentDate = $_SESSION['BULK_SHIPMENTS']['SHIPMENT_DATE'];
            $employeeGroupId = $_SESSION['BULK_SHIPMENTS']['EMPLOYEE_GROUP'];
            $sql = <<<SQL
            SELECT
                om.id,
                c.`description` AS customer,
                IFNULL(oc.`block`, 0) AS `block`,
                o.product_id,
                p.sku,
                uom.`description` AS uom,
                p.`description` AS product,
                om.purchase_order_nr AS po,
                om.comments,
                p.batch_code,
                p.expiry_date,
                pt.`description` AS category,
                ps.`description` AS brand,
                o.qty_ordered AS qty,
                driver.employee_id AS driver_id,
                driver.`name` AS driver,
                p.price
            FROM
                order_main om
                    INNER JOIN
                outlet c ON c.id = om.`account`
                    LEFT JOIN
                outlet_credit oc ON oc.outlet_id = c.id
                    INNER JOIN
                orders o ON o.order_id = om.id
                    INNER JOIN
                products p ON p.id = o.product_id
                    INNER JOIN
                units_of_measure uom ON uom.id = p.units_of_measure_id
                    INNER JOIN
                product_supplier ps ON ps.id = p.supplier_id
                    INNER JOIN
                product_type pt ON pt.id = p.product_type_id
                    LEFT JOIN
                (SELECT
                    e.id AS employee_id, e.`name`, e.route_id
                FROM
                    employee e
                WHERE
                    employee_group_id = {$employeeGroupId} AND bakery_id = {$bakeryId}
                GROUP BY e.route_id) driver ON driver.route_id = c.route_id
            WHERE
                om.order_status_id = 1
                    AND om.high_risk = 0
                    AND om.merged_order_id IS NULL
                    AND om.bakery_id = {$bakeryId}
                    AND IFNULL(oc.`block`, 0) = 0
            ORDER BY om.id , pt.`description` , ps.`description`;
SQL;
            $orderMain = Yii::app()->db->createCommand($sql)->queryAll();
            $row = 2;
            foreach($orderMain as $main)
            {
                $activeSheet->setCellValueByColumnAndRow(0, $row, $shipmentDate);
                $activeSheet->setCellValueByColumnAndRow(1, $row, $main['id']);
                $activeSheet->setCellValueByColumnAndRow(2, $row, $main['customer']);
                $activeSheet->setCellValueByColumnAndRow(3, $row, $main['po']);
                $activeSheet->setCellValueByColumnAndRow(4, $row, $main['comments']);
                $activeSheet->setCellValueByColumnAndRow(5, $row, $main['product_id']);
                $activeSheet->setCellValueByColumnAndRow(6, $row, $main['sku']);
                $activeSheet->setCellValueByColumnAndRow(7, $row, $main['product']);
                $activeSheet->setCellValueByColumnAndRow(8, $row, $main['uom']);
                $activeSheet->setCellValueByColumnAndRow(9, $row, $main['batch_code']);
                $activeSheet->setCellValueByColumnAndRow(10, $row, $main['expiry_date']);
                $activeSheet->setCellValueByColumnAndRow(11, $row, $main['category']);
                $activeSheet->setCellValueByColumnAndRow(12, $row, $main['brand']);
                $activeSheet->setCellValueByColumnAndRow(13, $row, $main['price']);
                $activeSheet->setCellValueByColumnAndRow(14, $row, $main['qty']);
                $activeSheet->setCellValueByColumnAndRow(15, $row, $main['driver_id']);
                $activeSheet->setCellValueByColumnAndRow(16, $row, $main['driver']);

                ++$row;
            }

            $activeSheet = $objPHPExcel->setActiveSheetIndex(1);

            $employeeGroupWhere = '';
            if($employeeGroupId > 0)
            {
                $employeeGroupWhere = 'AND employee_group_id = ' . $employeeGroupId;
            }

            $sql = <<<SQL
            SELECT
                e.id AS employee_id, e.`name`, eg.description AS employee_group
            FROM
                employee e
                INNER JOIN employee_group eg on eg.id = e.employee_group_id
            WHERE
                bakery_id = {$bakeryId}

                AND e.status_id = 1
            ORDER BY e.`name`;
SQL;
            $employees = Yii::app()->db->createCommand($sql)->queryAll();
            $row = 2;
            foreach($employees as $employee)
            {
                $activeSheet->setCellValueByColumnAndRow(0, $row, $employee['employee_id']);
                $activeSheet->setCellValueByColumnAndRow(1, $row, $employee['name']);
                $activeSheet->setCellValueByColumnAndRow(2, $row, $employee['employee_group']);

                ++$row;
            }

            $activeSheet = $objPHPExcel->setActiveSheetIndex(2);

            $sql = <<<SQL
            SELECT
            p.id,
            p.sku,
            p.`description`,
            uom.`description` AS uom,
            p.batch_code,
            p.expiry_date,
            pt.`description` AS category,
            ps.`description` AS brand,
            p.price
        FROM
            products p
                INNER JOIN
            units_of_measure uom ON uom.id = p.units_of_measure_id
                INNER JOIN
            product_supplier ps ON ps.id = p.supplier_id
                INNER JOIN
            product_type pt ON pt.id = p.product_type_id
        WHERE
            p.bakery_id = {$bakeryId} AND p.status_id = 1
                AND p.orderable = 1;
SQL;
            $products = Yii::app()->db->createCommand($sql)->queryAll();
            $row = 2;
            foreach($products as $product)
            {
                $activeSheet->setCellValueByColumnAndRow(0, $row, $product['id']);
                $activeSheet->setCellValueByColumnAndRow(1, $row, $product['sku']);
                $activeSheet->setCellValueByColumnAndRow(2, $row, $product['description']);
                $activeSheet->setCellValueByColumnAndRow(3, $row, $product['uom']);
                $activeSheet->setCellValueByColumnAndRow(4, $row, $product['batch_code']);
                $activeSheet->setCellValueByColumnAndRow(5, $row, $product['expiry_date']);
                $activeSheet->setCellValueByColumnAndRow(6, $row, $product['category']);
                $activeSheet->setCellValueByColumnAndRow(7, $row, $product['brand']);
                $activeSheet->setCellValueByColumnAndRow(8, $row, $product['price']);

                ++$row;
            }

            $objPHPExcel->setActiveSheetIndex(0);

            $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
            $objWriter->setPreCalculateFormulas(false);
            setcookie('file_downloaded', 1, time()+20);
            $downloadFilename = 'Bulk Shipments Import File.xlsx';
            header('Content-Disposition: attachment; filename="'.$downloadFilename.'"');
            $objWriter->save('php://output');
        }
    }

    public function actionImportBulkShipmentsFile()
    {
        $this->renderPartial('_bulkShipmentImport');
    }

    public function actionOrderCancelledReasons()
    {
        $reasons = OrdersCancelledReasons::orderCancelledReasons();
        $list = CHtml::listData($reasons, 'id', 'description');
        $dropDown = CHtml::dropDownList('orderCancelledReason', '', $list, array('empty' => '-- Please Select --', 'class' => 'form-control'));
        $html = <<<HTML
        <div class="form-group">
            <label for="orderCancelledReason">Please provide a cancel reason</label>
            {$dropDown}
        </div>
HTML;
        echo $html;
    }

    public function actionResetCancelledOrder($id, $orderId, $tripSheetId, $highRisk = null)
    {
        $orderMainUpdateArray = [
            'order_status_id' => 1,
            'linked_to_van_sale' => 0,
            'collection_order' => 0
        ];
        if($highRisk !== null)
        {
            $orderMainUpdateArray = [
                'order_status_id' => 1,
                'linked_to_van_sale' => 0,
                'collection_order' => 0,
                'high_risk' => 1
            ];
        }
        Order_main::model()->updateByPk($orderId, $orderMainUpdateArray);

        $condition = 'id = ' . $id . ' AND order_id = '. $orderId;
        OrdersCancelled::model()->updateAll([
            'reset' => 1,
            'reset_by' => Yii::app()->user->id,
            'reset_date' => date('Y-m-d H:i:s')
        ], $condition);

        if($tripSheetId !== '')
        {
            $tripSheetCondition = 'order_id = ' . $orderId . ' AND trip_sheet_id = ' . $tripSheetId;
            TripSheetsItems::model()->deleteAll($tripSheetCondition);
        }
        echo <<<HTML
        <div class='alert alert-success'>Order successfully reset, please wait...</div>
HTML;
    }

    public function actionReleaseOrder($orderId)
    {
        if(isset($_POST['order_id'], $_POST['reason']))
        {
            $orderId = $_POST['order_id'];
            $reason = $_POST['reason'];
            $ordersHighRisk = new OrdersHighRisk();
            $ordersHighRisk->order_id = $orderId;
            $ordersHighRisk->reason = $reason;
            $ordersHighRisk->released_by = Yii::app()->user->id;
            $ordersHighRisk->released_date = date('Y-m-d H:i:s');
            $ordersHighRisk->save();

            Order_main::model()->updateByPk($orderId, ['high_risk' => 0]);

            echo 1;
            die();
        }
        $this->renderPartial('_releaseOrder', ['orderId' => $orderId]);
    }

    public function actionResetOrder($id, $orderId, $tripSheetId, $highRisk = null)
    {
        $orderMainUpdateArray = [
            'order_status_id' => 1,
            'linked_to_van_sale' => 0
        ];
        if($highRisk !== null)
        {
            $orderMainUpdateArray = [
                'order_status_id' => 1,
                'linked_to_van_sale' => 0,
                'high_risk' => 1
            ];
        }
        Order_main::model()->updateByPk($orderId, $orderMainUpdateArray);

        $condition = 'id = ' . $id . ' AND order_id = ' . $orderId . ' AND trip_sheet_id = ' . $tripSheetId;
        OrdersMissed::model()->updateAll([
            'actioned' => 1,
            'actioned_by' => Yii::app()->user->id,
            'date' => date('Y-m-d H:i:s')
        ], $condition);

        if($tripSheetId !== '')
        {
            $tripSheetCondition = 'order_id = ' . $orderId . ' AND trip_sheet_id = ' . $tripSheetId;
            TripSheetsItems::model()->deleteAll($tripSheetCondition);
        }
        echo <<<HTML
        <div class='alert alert-success'>Order successfully reset, please wait...</div>
HTML;
    }

    public function actionCompleteCashSettlement($id, $settlementId = 0, $print = '0')
    {
        $tripSheetsSettlement = new TripSheetsSettlement();

        $sql = <<<SQL
        SELECT IFNULL(level_status, 0) AS level_status FROM trip_sheets_cash WHERE trip_sheet_id = :a GROUP BY level_status;
SQL;
        $command = Yii::app()->db->createCommand($sql);
        $command->bindValue(':a', $id, PDO::PARAM_INT);
        $tripSheetStatus = $command->queryScalar();

        if($tripSheetStatus === '1')
        {
            $data = $tripSheetsSettlement->getVarianceTripProducts($id, $settlementId);
            if($print == '1')
            {
                $html = $this->renderPartial('_variance', ['settlementId' => $settlementId, 'data' => $data, 'tripSheetId' => $id, 'cashSplit' => true, 'print' => $print], true);
                
                $pdf = new DistributionPDFFactory('Trip #'.$id.' - Variance Cash Settlement', $id);
                echo $pdf->createCashSettlementVariancePDF($id, $html, false);
            }
            else
            {
                $this->renderPartial('_variance', ['settlementId' => $settlementId, 'data' => $data, 'tripSheetId' => $id, 'cashSplit' => true, 'print' => $print]);
            }
        }
        else
        {
            $data = $tripSheetsSettlement->getBlindCheckTripProducts($id, $settlementId);
            $this->renderPartial('_blindcheck', ['settlementId' => $settlementId, 'data' => $data, 'tripSheetId' => $id, 'cashSplit' => true, 'print' => $print]);
        }
    }

    public function actionCashSettlementBlindCheck()
    {
        $tripSheetsSettlement = new TripSheetsSettlement();
        echo $tripSheetsSettlement->performBlindCheckCash();
    }

    public function actionCashSettlementVariance()
    {
        $tripSheetsSettlement = new TripSheetsSettlement();
        echo $tripSheetsSettlement->performVarianceCash();
    }

    public function actionShowTransferDetail($tripSheetId, $settlementId)
    {
        $con = Yii::app()->db;
        $sql = <<<SQL
        CALL sp_distribution_transfer_combined({$tripSheetId}, {$settlementId});
SQL;

        $asStock = $con->createCommand($sql)->queryAll();
        $locationFrom = $asStock[0]['location_from'];

        $warehouseData = AsStockWarehouses::model()->findAll(['condition' => 'bakery_id = ' . $asStock[0]['bakery_id'] . ' AND status_id = 1', 'order' => 't.description']);
        $warehouseListData = CHtml::listData($warehouseData, 'id', 'description');

        //get the trip sheet's bakery_id
        $tripSheet = TripSheets::model()->findByPk($tripSheetId);
        $settlementEmployees = Employee::model()->getEmployees($tripSheet->bakery_id);
        $fleet = Fleet::model()->getFleet($tripSheet->bakery_id);

        $this->renderPartial('_transferDetail', [
            'settlementEmployees' => $settlementEmployees,
            'fleet' => $fleet,
            'tripSheetId' => $tripSheetId,
            'settlementId' => $settlementId,
            'asStock' => $asStock,
            'locationFrom' => $locationFrom,
            'warehouseListData' => $warehouseListData
        ]);
    }

    public function actionTransferSheetPDF($tripSheetId, $settlementId)
    {
        $con = Yii::app()->db;
        $sql = <<<SQL
        CALL sp_distribution_transfer_combined({$tripSheetId}, {$settlementId});
SQL;

        $asStock = $con->createCommand($sql)->queryAll();
        $locationFrom = $asStock[0]['location_from'];

        $warehouseData = AsStockWarehouses::model()->findAll(['condition' => 'bakery_id = ' . $asStock[0]['bakery_id'] . ' AND status_id = 1', 'order' => 't.description']);
        $warehouseListData = CHtml::listData($warehouseData, 'id', 'description');

        $pdf = new DistributionPDFFactory('Transfer Sheet', $tripSheetId);
        echo $pdf->createTransferSheet($asStock, $locationFrom, $warehouseListData, $tripSheetId, false);
    }

    public function actionStockTransferDetailSubmit()
    {
        $data = [];
        if(isset($_POST['asStockItemDetail']))
        {
            $data = $_POST['asStockItemDetail'];
        }
        $asStock = new AsStock();
        $validateEmpties = $asStock->validateEmpties($data);
        if(!$validateEmpties['valid'])
        {
            echo json_encode($validateEmpties);
            die();
        }

        $validateDuplicates = $asStock->validateDuplicates($data);
        if(!$validateDuplicates['valid'])
        {
            echo json_encode($validateDuplicates);
            die();
        }

        if(!empty($data))
        {
            $validateProductValues = $asStock->validateProductValues($data, $_POST['asStockProductValues']);
            if(!$validateProductValues['valid'])
            {
                echo json_encode($validateProductValues);
                die();
            }
        }

        $status = AsStockMovement::moveStockToWarehouse($_POST, 5);
        echo json_encode($status);
        die();
    }

    public function actionLoadZoneFromWarehouse()
    {
        $warehouseId = $_POST['warehouse_id'];
        if(isset($_POST['row_id']))
        {
            $trSlot = $_POST['tr_slot'];
            $rowId = $_POST['row_id'];
            $productId = $_POST['product_id'];
            $zonesDropDown = AsStockZones::dropDownZoneFromWarehouse($warehouseId, $rowId, $productId, '', $trSlot);
        }
        else // call the global filter
        {
            $zonesDropDown = AsStockZones::dropDownGlobalZoneFromWarehouse($warehouseId);
        }

        echo $zonesDropDown;
    }

    public function actionLoadBayFromZone()
    {
        $zoneId = $_POST['zone_id'];
        if(isset($_POST['row_id']))
        {
            $trSlot = $_POST['tr_slot'];
            $rowId = $_POST['row_id'];
            $productId = $_POST['product_id'];
            $baysDropDown = AsStockBays::dropDownBayFromZone($zoneId, $rowId, $productId, '', $trSlot);
        }
        else // call the global filter
        {
            $baysDropDown = AsStockBays::dropDownGlobalBayFromZone($zoneId);
        }

        echo $baysDropDown;
    }

    public function actionLoadSlotFromBay()
    {
        $bayId = $_POST['bay_id'];
        if(isset($_POST['row_id']))
        {
            $trSlot = $_POST['tr_slot'];
            $rowId = $_POST['row_id'];
            $productId = $_POST['product_id'];
            $slotsDropDown = AsStockSlots::dropDownSlotFromBay($bayId, $rowId, $productId, '', $trSlot);
        }
        else // load the global filter
        {
            $slotsDropDown = AsStockSlots::dropDownGlobalSlotFromBay($bayId);
        }

        echo $slotsDropDown;
    }

    public function actionLoadDistributionTransfer()
    {
        $condition = TripSheetsSettlement::getTransferWhereClause();
        $results = TripSheets::model()->with(
            'tripSheetsSettlement.settlementLocation.warehouse',
            'tripSheetsSettlement.dropOffLocation',
            'fleet',
            'driver',
            'closedBy',
            'tripSheetItems.orderMain'
        )->findAll(['condition' => $condition, 'order' => 't.id']);

        $this->renderPartial('transfer', ['results' => $results]);
    }

    public function actionIndex()
    {
        if(isset($_GET['init']) && $_GET['init'] = 'init')
        {
            if(isset($_SESSION['CS'])) unset($_SESSION['CS']);
        }
        $miscellaneous = new Miscellaneous();
        $order_status = array();
        $active_tab = array();
        $orders = new Order_main();
        $page = '';
        $mapData = null;
        #Setting tabs from orders status
        foreach($miscellaneous->orderStatus(0, 1) as $status)
        {
            if($status['desktop_view'])
            {
                $order_status[$status['description']] = array(
                    'id' => $status["id"],
                    'key' => $status["key"],
                    'description' => $status["description"],
                );
                $active_tab[$status["key"]] = '';
            }
        }
        $active_tab['documents'] = ''; //Appending the invoice tab
        $cashTab = CustomUtils::salesCashCollectSplit();
        if($cashTab)
        {
            $order_status['Cash Settlement'] = [
                'id' => '*',
                'key' => 'cash_settlement',
                'description' => 'Cash Settlement'
            ];
            $active_tab['cash_settlement'] = '';
        }
        #Tabs'default values and settings
        if(!isset($_GET['show']) or $_GET['show'] == 'new')
        {
            $active_tab['new'] = 'selected';
            $order_status_id = 1;
            $shipment_dependant = 0;
            $dashboard_type = 'distribution';
            $page = 'new';
        }
        else if(isset($_GET['show']) and $_GET['show'] == 'documents')
        {
            $active_tab['documents'] = 'selected';
            $order_status_id = 0; #Not set at all
            $shipment_dependant = 0;
            $dashboard_type = 'distribution';
            $page = 'documents';
            $get_invoiced_orders = true;
        }
        else if(isset($_GET['show']))
        {
            $active_tab[$_GET['show']] = 'selected';
            $dashboard_type = 'distribution';
            $page = $_GET['show'];
            foreach($order_status as $val)
            {
                if($val['key'] == $_GET['show']) $order_status_id = $val['id'];
            }
            if($_GET['show'] == 'no_delivery' or $_GET['show'] == 'cancelled')
            {
                $shipment_dependant = 0;
            }
            else
            {
                $shipment_dependant = 1;
            }
        }
        #Do not call get presales if invoices' tab
        if(isset($get_invoiced_orders))
        {
            $result['results'] = array();
        }
        else
        {
            $bakeryWhere = '';
            $ordersMissedBakeryWhere = '';
            $ordersCancelledBakeryWhere = '';
            if(Yii::app()->user->bakery > 0)
            {
                $bakeryWhere = ' AND t.bakery_id = ' . Yii::app()->user->bakery;
                $ordersMissedBakeryWhere = ' AND orders.bakery_id = ' . Yii::app()->user->bakery;
                $ordersCancelledBakeryWhere = $ordersMissedBakeryWhere;
            }

            if($page === 'cash_settlement')
            {
                $result['results'] = $orders->getCashSettlementData();
            }
            elseif($page === 'planned')
            {
                $result['results'] = TripSheets::model()->with('bakery', 'fleet', 'driver','createdBy', 'tripSheetStock.product')->findAll(['condition' => 'trip_sheet_status_id = 1 AND t.merge_trip_sheet_id IS NULL AND t.cancelled = 0 ' . $bakeryWhere, 'order' => 't.id']);
            }
            elseif($page === 'in_dispatch')
            {
                // $condition = '(trip_sheet_status_id = 2 AND t.merge_trip_sheet_id IS NULL) OR primary_trip_sheet = 1 OR t.partially_accept = 1' . $bakeryWhere;
                $condition = '(trip_sheet_status_id IN (2, 9, 10) AND t.merge_trip_sheet_id IS NULL)' . $bakeryWhere;
                $tripSheets = TripSheets::model()->with('openingQtySum', 'plannedQtySum')->findAll(['condition' => $condition, 'order' => 't.id']);
                foreach($tripSheets as $tripSheet)
                {
                    if($tripSheet['openingQtySum'] == 0)
                    {
                        TripSheets::model()->updateByPk($tripSheet->id, [
                            'trip_sheet_status_id' => 11
                        ]);
                    }

                    if($tripSheet['plannedQtySum'] == $tripSheet['openingQtySum']
                    && $tripSheet['trip_sheet_status_id'] != 9
                    && $tripSheet['trip_sheet_status_id'] != 10
                    && $tripSheet['accepted'] != 0)
                    {
                        TripSheets::model()->updateByPk($tripSheet->id, [
                            'trip_sheet_status_id' => 12
                        ]);
                    }
                }

                $condition = '(trip_sheet_status_id IN (2, 4, 9, 10) AND t.merge_trip_sheet_id IS NULL)' . $bakeryWhere . ' AND ((t.partially_accept = 0 AND t.accepted = 0) OR (t.partially_accept = 1 AND t.accepted = 1) OR (t.partially_accept = 1 AND t.accepted = 0))';
                $tripSheets = TripSheets::model()->with('openingQtySum', 'cancelledCount', 'fleet', 'driver', 'dispatchBy', 'dispatchFleet', 'dispatchLocation.dispatchWarehouse', 'asStockMovement.location.warehouse')->findAll(['condition' => $condition, 'order' => 't.id']);
                $i = 0;
                foreach($tripSheets as $tripSheet)
                {
                    if($tripSheet['openingQtySum'] == 0 && $tripSheet['trip_sheet_status_id'] == 4)
                    {
                        unset($tripSheets[$i]);
                    }

                    if($tripSheet['partially_accept'] == 0 && $tripSheet['accepted'] == 1)
                    {
                        unset($tripSheets[$i]);
                    }

                    if($tripSheet['cancelledCount'] > 0)
                    {
                        unset($tripSheets[$i]);
                    }

                    ++$i;
                }
                $tripSheets = array_values($tripSheets);
                $result['results'] = $tripSheets;
            }
            elseif($page === 'loaded')
            {
                $condition = 't.trip_sheet_status_id IN (3, 9, 10, 12) AND (orderMain.order_status_id = 4 OR orderMain.order_status_id IS NULL) AND primary_trip_sheet = 1' . $bakeryWhere;
                $result['results'] = TripSheets::model()->with('fleet', 'driver', 'acceptedBy', 'tripSheetItems.orderMain')->findAll(['condition' => $condition, 'order' => 't.id']);
            }
            elseif($page === 'delivered')
            {
                $condition = 't.trip_sheet_status_id IN (3, 9, 10, 12) AND orderMain.order_status_id in (5,8,13)' . $bakeryWhere;
                $result['results'] = TripSheets::model()->with('fleet', 'driver', 'tripSheetItems.orderMain')->findAll(['condition' => $condition, 'order' => 't.id']);
            }
            elseif($page === 'checkin')
            {
                $data = TripSheets::getSettlementData($bakeryWhere);

                // $data = TripSheets::model()->with('tripSheetsSettlement', 'settlementLocation.warehouse', 'fleet', 'driver','closedBy','tripSheetItems.orderMain')->findAll(['condition' => $condition, 'order' => 't.id']);
                $result['results'] = $data;

            }
            elseif($page === 'transfer')
            {
                $condition = TripSheetsSettlement::getTransferWhereClause();
                $data = TripSheets::model()->with(
                    'tripSheetsSettlement.settlementLocation.warehouse',
                    'tripSheetsSettlement.dropOffLocation',
                    'fleet',
                    'driver',
                    'closedBy',
                    'tripSheetItems.orderMain'
                )->findAll(['condition' => $condition, 'order' => 't.id']);
                $result['results'] = $data;
            }
            elseif($page === 'completed')
            {
                $currentDate = date('Y-m-d');
                $dateFrom = $currentDate;
                $dateTo = $currentDate;
                if(isset($_POST['transferred_date_from'], $_POST['transferred_date_to']))
                {
                    $dateFrom = $_POST['transferred_date_from'];
                    $dateTo = $_POST['transferred_date_to'];
                }
                $result['dates'] = array('start' => $dateFrom, 'end' => $dateTo);

                $condition = <<<SQL
                DATE(tripSheetsSettlement.created_date) >= '{$dateFrom}' AND DATE(tripSheetsSettlement.created_date) <= '{$dateTo}' {$bakeryWhere}
SQL;

                $data = TripSheets::model()->with('settlementCount', 'tripSheetsSettlement.closedBy', 'asStockMovement.transferredBy', 'fleet', 'driver', 'tripSheetItems.orderMain')->findAll(['condition' => $condition, 'order' => 't.id']);
                $result['results'] = $data;
            }
            elseif($page === 'no_delivery')
            {
                $condition = 'orders.order_status_id = 8 AND t.actioned = 0' . $ordersMissedBakeryWhere;
                $result['results'] = OrdersMissed::model()->with('orders.outlet', 'noDeliveryReason')->findAll(['condition' => $condition, 'order' => 't.trip_sheet_id']);
            }
            elseif($page === 'cancelled')
            {
                $currentDate = date('Y-m-d');
                $dateFrom = new DateTime();
                $dateFrom->sub(new DateInterval('P7D'));
                $dateFrom = $dateFrom->format('Y-m-d');
                $dateTo = $currentDate;
                if(isset($_POST['delivery_start_date'], $_POST['delivery_end_date']))
                {
                    $dateFrom = $_POST['delivery_start_date'];
                    $dateTo = $_POST['delivery_end_date'];
                }
                $result['dates'] = array('start' => $dateFrom, 'end' => $dateTo);

                $condition = 'orders.order_status_id = 7' . $ordersCancelledBakeryWhere . ' AND t.reset = 0';
                $condition .= ' AND t.date_created >= \'' . $dateFrom . ' 00:00:00\' AND t.date_created <= \'' . $dateTo . ' 23:59:59\'';
                $result['results'] = OrdersCancelled::model()->with('tripSheetItem', 'orders.outlet', 'orders.cancelledEmployee', 'orderCancelledReason')->findAll(['condition' => $condition, 'order' => 'tripSheetItem.trip_sheet_id']);
            }
            else
            {
                $result = $orders->getPresalesOrders($order_status_id, $shipment_dependant, $dashboard_type, false);
                if($page == 'new')
                {
                    $om = new Order_main();
                    $mapData = $om->getPreSalesNewOrdersMapData();

                }
            }
        }

        if(!isset($result['dates']))
        {
            $fromDate = new DateTime();
            $fromDate->sub(new DateInterval('P7D'));
            $result['dates'] = array('start' => $fromDate->format('Y-m-d'), 'end' => date('Y-m-d'));
        }
        $filterOrderNum = isset($_POST['order_num']) ? $_POST['order_num'] : null;
        isset($result['paging']) ? $paging = $result['paging'] : $paging = null;
        #Render the appropriate view
        $this->render(
            'index',
            array(
                'results' => $result['results'],
                'paging' => $paging,
                'dates' => $result['dates'],
                'filterOrderNum' => $filterOrderNum,
                'active_tab' => $active_tab,
                'page' => $page,
                'order_status_tabs' => $order_status,
                'mapData' => $mapData
            )
        );
    }

    public function actionSetPreSaleOrders()
    {
        if(isset($_POST['ids']))
        {
            $page = $_POST['page'];
            if($_POST['page'] == 'init')
            {
                $page = 0;
            }
            $_SESSION['CS']['PAGE'][$page] = $_POST['ids'];
        }
//        else
//        {
//            if(isset($_SESSION['CS']['PAGE_' . $_POST['page']]))
//            {
//                unset($_SESSION['CS']['PAGE_' . $_POST['page']]);
//            }
//        }
        echo 1;
    }

    public function actionSubmitNoDelivery()
    {
        $connection = Yii::app()->db;
        $orders_tripsheet_array = array();
        // Get trip_sheet_id per order
        $order_sql = "update order_main set order_status_id='8' where id='" . $_POST['order_id'] . "'";
        $order_command = $connection->createCommand($order_sql);
        $order_command->execute();
        $trip_sheets_sql = "update trip_sheets_items set is_active=0
                            where order_id=" . $_POST['order_id'];
        $trip_sheets_order_command = $connection->createCommand($trip_sheets_sql);
        $trip_sheets_order_command->execute();
        $sql = "select trip_sheet_id,order_id from trip_sheets_items where order_id = {$_POST['order_id']}";
        $command = $connection->createCommand($sql);
        $result = $command->queryAll();
        if(count($result) > 0)
        {
            foreach($result as $row)
            {
                $orders_tripsheet_array[$row["trip_sheet_id"]] = $row["order_id"];
            }
        }
        $datetime = date("Y-m-d H:i:s");
        foreach($orders_tripsheet_array as $tripsheet_id => $order_id)
        {
            $order_sql = "insert into orders_missed(id, trip_sheet_id, order_id, comments, actioned, `date`) values(null,$tripsheet_id,$order_id," . $_POST['reason'] . ",0, '" . $datetime . "')";
            $missed_order_command = $connection->createCommand($order_sql);
            $missed_order_command->execute();
        }


    }

    public function actionShowCreditInfo($id)
    {
        $info = OutletCredit::model()->with('outlet')->find('outlet_id = ' . $id);
        $this->renderPartial('_creditInformation', ['info' => $info]);
    }

    public function actionPrintLoadedSheet($tripSheetId)
    {
        $bakeryId = TripSheets::model()->findByPk($tripSheetId)->bakery_id;
        $orderBy = Bakery::getDistributionProductsOrderBy($bakeryId, 'sql_three');
        $tripSheetData = TripSheets::model()->with('tripSheetStock.product.unitOfMeasure', 'tripSheetStock.product.type', 'tripSheetStock.product.supplier', 'fleet', 'driver')->findByPk($tripSheetId, ['order' => $orderBy]);
        $vsStock = TripSheetsStock::checkVanSaleStockDelivered($tripSheetId, $bakeryId);

        $pdf = new DistributionPDFFactory('Loaded Shipment for Trip Sheet #' . $tripSheetId, $tripSheetId);
        echo $pdf->createLoadedShipmentSheet($vsStock, $tripSheetData, $tripSheetId, false);
    }

    public function actionShowSapConfirmationMessage($orderId, $resync = null)
    {
        if(!is_null($resync) && $resync == 'true')
        {
            $order = Order_main::model()->findByPk($orderId);
            $order->sap_status = null;
            $order->save();
            Yii::app()->user->setFlash('success', 'Order #' . $orderId . ' successfully queued for re-sync, please check back in a few minutes');
            $this->redirect(Yii::app()->baseUrl . '/index.php/admin/distribution');
            die();
        }
        //get the BAPI result message
        $sapConfirmation = OrderMainSapConfirmation::model()->find('order_id = :orderId', [':orderId' => $orderId]);
        $errorCount = APIUtils::getBAPIErrorCount($orderId);
        $this->renderPartial('presales/sapMessage', ['orderId' => $orderId, 'sapConfirmation' => $sapConfirmation, 'errorCount' => $errorCount]);
    }

    public function actiongetOrders($id, $shipment_dependant, $dashboard_type)
    {
        $orders = new Order_main();
        $this->renderPartial('view_orders', array('orders' => $orders->getOrders($id, $shipment_dependant, $dashboard_type, $_GET), 'shipment_orders' => $shipment_dependant));
    }

    public function actionhistoricalSales($outletId, $fromDate = null, $toDate = null)
    {
        $orders = new Order_main();
        $fromDate = date('Y-m-d', strtotime('-3 months'));
        $toDate = date('Y-m-d');
        $orders = $orders->getHistorySales($fromDate, $toDate, $outletId);
        $this->renderPartial('historical_sales', array('orders' => $orders));
    }

    public function actiongetOrderProducts($id)
    {
        $orders = new Order_main();
        echo $orders->getOrderProducts($id);
    }

    public function actiongetOrderProductsRecurring($id, $outletId)
    {
        $orders = new Order_main();
        $html = <<<HTML
        <div class="form-group">
            <button id="ordersRecurringDetailsBackBtn" class="btn btn-primary form-control">Back</button>
        </div>
HTML;
        $html .= $orders->getOrderProducts($id, '_recurring');

        $url = Yii::app()->getBaseUrl(true) . '/index.php/admin/outlet/recurringOrders/outletId/' . $outletId;

        $html .= <<<HTML
        <script type="text/javascript">
        $(document).ready(function(){
            $('#ordersRecurringDetailsBackBtn').click(function(){
                $('#diag').load('{$url}');
            });
        });
        </script>
HTML;

        echo $html;
    }

    public function actionShipmentForm($bakeryid)
    {
        $fleet = new Fleet();
        if($bakeryid > 0)
        {
            $append = array('bakery_id' => $bakeryid, 'status_id' => 1);
        }
        else
        {
            $append = array('status_id' => 1);
        }
        $employee_array = Employee::model()->findAllByAttributes($append, array('order' => 'name asc'));
        $this->renderPartial('shipmentform', array('fleet' => $fleet->shipmentformdata($bakeryid),
            'employee_array' => $employee_array));
    }


    public function actionCreateShipment()
    {
        $trip_sheet = new Trip_sheets();
        $trip_sheet->fleet_id = $_GET['vehicle'];
        $trip_sheet->bakery_id = $_GET['bakery_id'];
        $trip_sheet->created_employee_id = Yii::app()->user->id;
        $trip_sheet->created_date = date('Y-m-d H:i:s');
        $trip_sheet->driver_employee_id = $_GET['employee_id'];
        $trip_sheet->expected_delivery_date = $_GET['delivery_date'];
        $trip_sheet->departure = $_GET['delivery_date'];
        $trip_sheet->arrival = $_GET['delivery_date'];
        $trip_sheet->push_status = "";
        $trip_sheet->stock_level_updated = 0;
        $trip_sheet->weight_difference = 0;
        $trip_sheet->trip_completed = 0;
        $trip_sheet->save();
        $order_ids = explode(",", $_GET['order_ids']);
        $current_order_status = 0;
        $order_main = Order_main::model()->findByPk($order_ids[0]);
        $current_order_status = $order_main->order_status_id;
        foreach($order_ids as $order_id)
        {
            $trip_sheets_items = new Trip_sheets_items();
            $trip_sheets_items->trip_sheet_id = $trip_sheet->id;
            $trip_sheets_items->order_id = $order_id;
            $trip_sheets_items->stock_location_id = 0;
            $trip_sheets_items->save();
        }
        $new_order_status = $order_main->getNextStatus($current_order_status);
        $result = $order_main->updateOrderStatus($_GET['order_ids'], $new_order_status);
        if($result > 0)
        {
            echo "1";
        }
        else
        {
            echo "2";
        }
    }

    public function actionPickingSlip($tripSheetId)
    {
        $miscellaneous = new Miscellaneous();
        $picking_sheet_data = $miscellaneous->pickingSheet($tripSheetId);

        $product_grouped_orders = array();
        $order_ids = array();
        $order_products_map = array();
        $order_status = 0;
        foreach($picking_sheet_data as $order)
        {
            $product_grouped_orders[$order["product_group"]][$order["prod_id"]]["prod_name"] = $order["prod_name"];
            $product_grouped_orders[$order["product_group"]][$order["prod_id"]]["prod_id"] = $order["prod_id"];
            $product_grouped_orders[$order["product_group"]][$order["prod_id"]]["unit_of_measure"] = $order['unit_of_measure'];
            $product_grouped_orders[$order["product_group"]][$order["prod_id"]]["batch_code"] = $order['batch_code'];
            $product_grouped_orders[$order["product_group"]][$order["prod_id"]]["expiry_date"] = $order['expiry_date'];
            if(isset($product_grouped_orders[$order["product_group"]][$order["prod_id"]]["ord_ids"]))
            {
                $product_grouped_orders[$order["product_group"]][$order["prod_id"]]["ord_ids"] .= "," . $order["ord_id"];
            }
            else
            {
                $product_grouped_orders[$order["product_group"]][$order["prod_id"]]["ord_ids"] = $order["ord_id"];
            }
            $order_ids[$order["ord_id"]] = $order["ord_id"];
            $order_status = $order["order_status_id"];
            // Map products to their corresponding orders
            $order_products_map[$order["ord_id"]][$order["prod_id"]] = $order["qty_ordered"];
        }
        $pickingSheetBakeryIdSql = <<<SQL
        select ts.bakery_id as bakery from trip_sheets ts inner join bakery b on b.id = ts.bakery_id where ts.id = {$tripSheetId};
SQL;
       print '<pre>';
       print_r($product_grouped_orders);
       die;
        $pickingSheetBakeryId = Yii::app()->db->createCommand($pickingSheetBakeryIdSql)->queryScalar();
        $this->renderPartial('shipment_picking_slip', array('product_grouped_orders' =>
            $product_grouped_orders,
            'order_ids' => $order_ids, 'order_products_map' => $order_products_map,
            'shipment_id' => $id, 'order_status_id' => $order_status, 'pickingSheetBakeryId' => $pickingSheetBakeryId));




//        $miscellaneous = new Miscellaneous();
//        $data = $miscellaneous->shipmentPickingSlip($tripSheetId);
//        $order_products = array();
//        // Group products
//        foreach($data as $product)
//        {
//            $order_products["products"][$product["product_group"]][] = $product;
//        }
//        $order_products["shipment_number"] = $data[0]["trip_sheet_id"];
//        $order_products["fleet"] = $data[0]["veh_desc"] . " (" . $data[0]["veh_reg"] . ")";
//        $order_products["date"] = $data[0]["expected_delivery_date"];
//        $order_products["driver_name"] = $data[0]["driver_name"];
//        $order_products["mlp_name"] = $data[0]["mlp_name"];
//        $order_products["mlp_location"] = $data[0]["mlp_location"];
        $this->renderPartial('shipment_picking_slip', array('data' => $order_products));
    }

    public function actionPickingSheet($id)
    {


        $miscellaneous = new Miscellaneous();
        $picking_sheet_data = $miscellaneous->pickingSheet($id);
        $product_grouped_orders = array();
        $order_ids = array();
        $order_products_map = array();
        $order_status = 0;
        foreach($picking_sheet_data as $order)
        {
            $product_grouped_orders[$order["product_group"]][$order["prod_id"]]["prod_name"] = $order["prod_name"];
            $product_grouped_orders[$order["product_group"]][$order["prod_id"]]["prod_id"] = $order["prod_id"];
            $product_grouped_orders[$order["product_group"]][$order["prod_id"]]["unit_of_measure"] = $order['unit_of_measure'];
            $product_grouped_orders[$order["product_group"]][$order["prod_id"]]["batch_code"] = $order['batch_code'];
            $product_grouped_orders[$order["product_group"]][$order["prod_id"]]["expiry_date"] = $order['expiry_date'];
            if(isset($product_grouped_orders[$order["product_group"]][$order["prod_id"]]["ord_ids"]))
            {
                $product_grouped_orders[$order["product_group"]][$order["prod_id"]]["ord_ids"] .= "," . $order["ord_id"];
            }
            else
            {
                $product_grouped_orders[$order["product_group"]][$order["prod_id"]]["ord_ids"] = $order["ord_id"];
            }
            $order_ids[$order["ord_id"]] = $order["ord_id"];
            $order_status = $order["order_status_id"];
            // Map products to their corresponding orders
            $order_products_map[$order["ord_id"]][$order["prod_id"]] = $order["qty_ordered"];
        }
        $pickingSheetBakeryIdSql = <<<SQL
        select ts.bakery_id as bakery from trip_sheets ts inner join bakery b on b.id = ts.bakery_id where ts.id = {$id};
SQL;
        $pickingSheetBakeryId = Yii::app()->db->createCommand($pickingSheetBakeryIdSql)->queryScalar();
        $this->renderPartial('picking_sheet', array('product_grouped_orders' => $product_grouped_orders,
            'order_ids' => $order_ids, 'order_products_map' => $order_products_map,
            'shipment_id' => $id, 'order_status_id' => $order_status, 'pickingSheetBakeryId' => $pickingSheetBakeryId));
    }

    public function actionDeliveryDetail($orderId, $from = 'loaded')
    {
        $blockedMessage = '';
        $con = Yii::app()->db;

        $bakeryId = Order_main::model()->findByPk($orderId)->bakery_id;
        $orderBy = Bakery::getDistributionProductsOrderBy($bakeryId);
        //'products.sku, products.description, unitOfMeasure.description, productsReturns.sku, productsReturns.description, psUomReturns.description'
        $order = Order_main::model()
            ->with('employee', 'outlet', 'tripSheetsItems', 'bakery', 'orders.products.unitOfMeasure', 'orders.products.type', 'orders.products.supplier', 'ordersReturns', 'ordersReturns.productsReturns.psUomReturns')
            ->findByPk($orderId, ['order' => $orderBy]);

        //check if there is a credit note that needs to be processed first
        $outletId = $order->account;
        $tripSheetId = $order->tripSheetsItems['trip_sheet_id'];
        $sql = <<<SQL
        select
            count(*) as cnt, om.id as credit_note_order_id
        from
            trip_sheets_items tsi
        inner join order_main om on
            om.id = tsi.order_id
            and om.account = :account
        where
            tsi.trip_sheet_id = :tripSheetId
            and tsi.order_id != :orderId
            and om.order_status_id = 19
        group by om.id;
SQL;
        $com = $con->createCommand($sql);
        $com->bindValue(':account', $order->account, PDO::PARAM_INT);
        $com->bindValue(':tripSheetId', $tripSheetId, PDO::PARAM_INT);
        $com->bindValue(':orderId', $orderId, PDO::PARAM_INT);
        $cnt = $com->queryRow();
        if($cnt['cnt'] > 0)
        {
            $blockedMessage = 'You can\'t deliver this order because there is a credit note (' . $cnt['credit_note_order_id'] . ') pending for collection.<br>Please collect/process the credit note first in order to complete this delivery.';
        }

        //check if there is a credit note for this customer that has been processed
        //get the credit note's order_id and also the order_total_amount
        //the above information need to be sent on submit_delivery and the order_total amount need to be subtracted from the deliverie's net amount
        //also this credit amount that was subtracted, needs to show on the invoice
        $sql = <<<SQL
        select
            om.id as credit_note_order_id, om.order_total_amount as credit_note_value
        from
            trip_sheets_items tsi
        inner join order_main om on
            om.id = tsi.order_id
            and om.account = :account
        where
            tsi.trip_sheet_id = :tripSheetId
            and om.order_status_id = 13
        group by om.id;
SQL;
        $com = $con->createCommand($sql);
        $com->bindValue(':account', $order->account, PDO::PARAM_INT);
        $com->bindValue(':tripSheetId', $tripSheetId, PDO::PARAM_INT);
        $creditNoteDetails = $com->queryRow();
        
        if(isset($creditNoteDetails['credit_note_order_id']))
        {
            $cnId = $creditNoteDetails['credit_note_order_id'];

            $cSql = <<<SQL
            select om.id from order_main om where om.credit_note_order_id = :creditNoteOrderId
SQL;
            $sCom = $con->createCommand($cSql);
            $sCom->bindValue(':creditNoteOrderId', $cnId, PDO::PARAM_INT);
            $hasCreditNote = $sCom->queryScalar();
            if(!$hasCreditNote)//continue
            {

            }
            else
            {
                $creditNoteDetails = [];
            }
        }

        $vansale = new Vansales();
        if($from == 'in_dispatch')
        {
            $stock = $vansale->sendClosingStockItems($order['tripSheetsItems']['trip_sheet_id'], false, false, $from);
        }
        else
        {
            $stock = $vansale->sendClosingStockItems($order['tripSheetsItems']['trip_sheet_id']);
        }

        $linkedProducts = CustomUtils::getLinkedProducts($order->bakery_id);

        $this->renderPartial('delivery_detail', array('stock' => $stock, 'linked' => $linkedProducts, 'order' => $order, 'from' => $from, 'blockedMessage' => $blockedMessage, 'creditNoteDetails' => $creditNoteDetails));
    }

    public function actionDeliveryDetailCreditNote($orderId, $from = 'loaded')
    {
        $bakeryId = Order_main::model()->findByPk($orderId)->bakery_id;
        $orderBy = Bakery::getDistributionProductsOrderBy($bakeryId);
        //'products.sku, products.description, unitOfMeasure.description, productsReturns.sku, productsReturns.description, psUomReturns.description'
        $order = Order_main::model()
            ->with('employee', 'outlet', 'tripSheetsItems', 'bakery', 'orders.products.unitOfMeasure', 'orders.products.type', 'orders.products.supplier', 'ordersReturns', 'ordersReturns.productsReturns.psUomReturns')
            ->findByPk($orderId, ['order' => $orderBy]);
        $vansale = new Vansales();
        if($from == 'in_dispatch')
        {
            $stock = $vansale->sendClosingStockItems($order['tripSheetsItems']['trip_sheet_id'], false, false, $from);
        }
        else
        {
            $stock = $vansale->sendClosingStockItems($order['tripSheetsItems']['trip_sheet_id']);
        }

        $linkedProducts = CustomUtils::getLinkedProducts($order->bakery_id);

        $this->renderPartial('delivery_detail_credit_note', array('stock' => $stock, 'linked' => $linkedProducts, 'order' => $order, 'from' => $from));
    }

    public function actionNoDeliveryDetail($orderid, $from = 'loaded')
    {
        $this->renderPartial('no_delivery_detail', array('order_id' => $orderid, 'from' => $from));
    }

    public function actionSubmitPickingSheet()
    {
        $order_main = new Order_main();
        $result = $order_main->updatePickedOrders($_GET);
        echo $result;
        /*if($result){
            echo "Shipment confirmed successfully.";
        }else{
            echo "Coulnt not confirm shipment. Please try again.";
        }*/
    }

    public function actionorderDeliveredCreditNote($preview = false)
    {
        $user = Yii::app()->user->id;
        $connection = Yii::app()->db;

        $outletId = $_POST['outlet_id'];
        $orderId = $_POST['order_id'];

        $qtyValid = true;
        foreach($_POST['returns'] as $returns)
        {
            if($returns['qty'] < 0)
            {
                $qtyValid = false;
            }
        }
        if(!$qtyValid)
        {
            $return = [
                'success' => false,
                'what' => 'error',
                'message' => 'Oh oh! Please enter valid quantities'
            ];

            echo json_encode($return);
            die();
        }
        
        $request = APIUtils::buildSubmitDeliveryCreditNote($orderId, $_POST['returns'], $_POST['email']);

        $return = [
            'success' => false,
            'what' => 'error',
            'message' => 'Whoops! An unexpected error occured'
        ];
        if(!$preview)
        {
            unset($request['Request']['showAdjustmentReasons']);
            if(isset($_POST['from']) && $_POST['from'] == 'in_dispatch')
            {
                $stockMovementId = AsStockMovement::model()->find('trip_sheet_id = :tripSheetId', [':tripSheetId' => $_POST['trip_sheet_id']])->id;
                $status = AsStockMovement::moveStockToEmployeeFromDispatch($stockMovementId, 4);
            }
            $url = Yii::app()->getBaseUrl(true) . '/index.php/api/central/webservice';
            $result = APIUtils::requestPOST($url, $request);
            // echo $result['message'];
            // die();
            if(isset($result['http_code']) && $result['http_code'] === 200)
            {
                $return = [
                    'success' => true,
                    'what' => 'success',
                    'message' => 'Nicely done! Credit note successfully collected'
                ];
            }
        }
        else
        {
            echo json_encode($request);
            die();
        }

        echo json_encode($return);

        die();
    }

    public function actionorderDelivered($preview = false)
    {
        $user = Yii::app()->user->id;
        $connection = Yii::app()->db;

        $outletId = $_POST['outlet_id'];
        $orderId = $_POST['order_id'];
        $orders = [];
        $totalVatValue = 0;
        $grossOrderTotalAmount = 0;
        $orderTotalAmount = 0;
        if(isset($_POST['orders']))
        {
            $pickedQuantities = (int)Orders::model()->findBySql('select sum(qty_picked) as qty_picked from orders where order_id = ' . $orderId)->qty_picked;
            $postedQuantities = 0;

            foreach($_POST['orders'] as $productId => $item)
            {
                $qty = $item['qty'];
                $postedQuantities += $qty;
                $vatPercentage = $item['vat'];
                //get the product price
                $price = CustomUtils::checkPriceFromOrder($productId, $orderId);//CustomUtils::checkPrice($productId, $orderId);
                $vatValue = ($price*($vatPercentage/100))*$qty;
                $totalVatValue += $vatValue;
                $grossOrderTotalAmount += $price*$qty;

                $orders[] = [
                    'product_id' => $productId,
                    'quantity' => $qty,
                    'vat_value' => $vatValue
                ];
            }
        }

        $returns = [];
        if(isset($_POST['returns']))
        {
            foreach($_POST['returns'] as $productId => $item)
            {
                $qty = $item['qty'];
                $vatPercentage = $item['vat'];
                //get the product price
                $price = $item['price'];
                $vatValue = ($price*($vatPercentage/100))*$qty;
                $totalVatValue -= $vatValue;
                $grossOrderTotalAmount -= $price*$qty;

                $returns[] = [
                    'product_id' => $productId,
                    'quantity' => $qty,
                    'vat_value' => $vatValue
                ];
            }
        }

        $discountedTotal = $grossOrderTotalAmount;
        $invoiceDiscountDetails = CustomUtils::checkInvoiceDiscountFromOrder($orderId);
//        $invoiceDiscountDetails = Products::invoiceDiscountDetails($outletId);
        $invoiceDiscount = '%0';
        if(is_array($invoiceDiscountDetails))
        {
            $totalVatValue -= $totalVatValue*($invoiceDiscountDetails['value']/100);
            $invoiceDiscount = $invoiceDiscountDetails['discount_type_description'].$invoiceDiscountDetails['value'];
            $discountedTotal -= $grossOrderTotalAmount*($invoiceDiscountDetails['value']/100);
        }

        $orderTotalAmount = $discountedTotal + $totalVatValue;
        $orderTotalAmountInvoiceOnly = round($orderTotalAmount,2); 
        // var_dump($orderTotalAmount);
        $creditNoteSubtractMessage = '';
        $creditNoteValue = 0;
        $creditNoteOrderId = '';
        if(isset($_POST['credit_note_value']))
        {
            $creditNoteValue = $_POST['credit_note_value'];
            $creditNoteOrderId = $_POST['credit_note_order_id'];
            $orderTotalAmount -= $creditNoteValue;
            $creditNoteSubtractMessage = '. Credit note ' . $creditNoteOrderId . ' value ('.$creditNoteValue.') is more than the delivery amount of ' . $orderTotalAmountInvoiceOnly . '.'; 
        }
        $return = [
            'success' => false,
            'what' => 'error',
            'message' => 'Return values are more than the ordered values, please check your quantities' . $creditNoteSubtractMessage
        ];
        if($orderTotalAmount >= 0)
        {
            $adjustmentId = 0;
            $showAdjustmentReasons = 0;
            if($postedQuantities != $pickedQuantities)
            {
                $showAdjustmentReasons = 1;
            }
            if(isset($_POST['adjustment_id']))
            {
                $adjustmentId = $_POST['adjustment_id'];
            }
            $dateTime = date('Y-m-d H:i:s');
            $documentNumber = APIUtils::generateDocumentNumber($_POST['bakery_id'], Yii::app()->user->id, 'invoice');
            $request = [
                'Request' => [
                    'ignoreDeviceCheck' => 1,
                    'employee_id' => Yii::app()->user->id,
                    'credit_note_value' => $creditNoteValue,
                    'credit_note_order_id' => $creditNoteOrderId,
                    'invoice_only_order_total_amount' => $orderTotalAmountInvoiceOnly,
                    'bakery_id' => $_POST['bakery_id'],
                    'datetime_stamp' => $dateTime,
                    'outlet_id' => $outletId,
                    'order_id' => $orderId,
                    'document_number_prefix' => $documentNumber['prefix'],
                    'document_number' => $documentNumber['number'],
                    'adjustment_id' => $adjustmentId,
                    'contact_email' => $_POST['email'],
                    'cash_collected' => $_POST['cashcollected'],
                    'payment_method_id' => $_POST['paymenttype'],
                    'total_vat_value' => number_format($totalVatValue, 2, '.', ''),
                    'order_products' => $orders,
                    'order_returns' => $returns,
                    'invoice_discount' => $invoiceDiscount,
                    'tripsheetId' => $_POST['trip_sheet_id'],
                    'order_total_amount' => number_format($orderTotalAmount, 2, '.', ''),
                    'gross_order_total_amount' => $grossOrderTotalAmount,
                    'DeviceID' => '',
                    'REG_KEY_VALUE' => '',
                    'RequestRef' => '',
                    'Dt' => $dateTime,
                    'Type' => 'submitDelivery',
                    'showAdjustmentReasons' => $showAdjustmentReasons
                ]
            ];

            if(!$preview)
            {
                $currencyCashCollected = ($_POST['cashcollected'] != '') ? $_POST['cashcollected'] : 0;
                $request['Request']['exchanged_currency_value'] = $currencyCashCollected;
                $request['Request']['cash_collected'] = number_format(($currencyCashCollected/$_POST['selected_currency_exchange_rate']), 2, '.', '');
                $request['Request']['selected_currency_id'] = $_POST['selected_currency_id'];
                $request['Request']['selected_currency'] = $_POST['selected_currency'];
                $request['Request']['selected_currency_exchange_rate'] = $_POST['selected_currency_exchange_rate'];
                unset($request['Request']['showAdjustmentReasons'], $request['Request']['invoice_only_order_total_amount']);
                if(isset($_POST['from']) && $_POST['from'] == 'in_dispatch')
                {
                    $stockMovementId = AsStockMovement::model()->find('trip_sheet_id = :tripSheetId', [':tripSheetId' => $_POST['trip_sheet_id']])->id;
                    $status = AsStockMovement::moveStockToEmployeeFromDispatch($stockMovementId, 4);
                }

                $url = Yii::app()->getBaseUrl(true) . '/index.php/api/central/webservice';
                $result = APIUtils::requestPOST($url, $request);
                if(isset($result['http_code']) && $result['http_code'] === 200)
                {
                    $return = [
                        'success' => true,
                        'what' => 'success',
                        'message' => 'Nicely done! Order successfully delivered'
                    ];
                }
            }
            else
            {
                echo json_encode($request);
                die();
            }
        }

        echo json_encode($return);

        die();
        $sql = <<<SQL
        SELECT
            m.id,
            o.id orderid,
            tsi.trip_sheet_id,
            o.product_id,
            m.account,
            m.order_status_id,
            m.delivered_by,
            o.qty_ordered,
            o.qty_picked,
            o.qty_delivered,
            r.qty_collected,
            m.order_total_amount,
            m.gross_order_total_amount,
            m.cash_collected,
            m.payment_type_id,
            m.picked_gross_order_total_amount,
            m.picked_order_total_amount,
            m.picked_vat_value,
            m.picked_discount_value,
            m.vat_value
        FROM
            order_main m
                LEFT JOIN
            trip_sheets_items tsi ON tsi.order_id = m.id
                LEFT JOIN
            orders o ON o.order_id = m.id
                LEFT JOIN
            orders_returns r ON r.order_id = m.id
        WHERE
            m.id = {$_POST['order_id']};
SQL;

        $dcommand = $connection->createCommand($sql);
        $result = $dcommand->queryAll();
        $cash_collected = '';
        $delivered_gross_order_total_amount = '';
        $delivered_order_total_amount = '';
        $delivered_vat_value = '';
        $outlet_id = '';

        OrdersRolledUp::insertOrder($result);

        foreach($result as $orderDetail)
        {
            $sql = "UPDATE orders SET qty_delivered = '{$orderDetail['qty_picked']}' WHERE id = '{$orderDetail['orderid']}'";
            $delivered_gross_order_total_amount = $orderDetail['picked_gross_order_total_amount'];
            $delivered_order_total_amount = $orderDetail['picked_order_total_amount'];
            $delivered_vat_value = $orderDetail['picked_vat_value'];
            $delivered_discount_value = $orderDetail['picked_discount_value'];
            $outlet_id = $orderDetail['account'];
            $update_command = $connection->createCommand($sql);
            $update_command->execute();
        }
        $cash_collected = $_POST['cashcollected'];
        $payment_type = $_POST['paymenttype'];
        $sql = "UPDATE order_main SET actual_delivery_date = NOW(), "
            . "delivered_by = '{$user}', "
            . "back_end_delivery = 1, "
            . "order_status_id = 5,"
            . "cash_collected = '{$cash_collected}', "
            . "delivered_gross_order_total_amount = '{$delivered_gross_order_total_amount}', "
            . "delivered_order_total_amount = '{$delivered_order_total_amount}', "
            . "delivered_vat_value = '{$delivered_vat_value}',"
            . "delivered_discount_value = '{$delivered_discount_value}', "
            . "payment_type_id = '{$payment_type}' "
            . "WHERE id = '{$_POST['order_id']}'";
        $command = $connection->createCommand($sql);
        $command->execute();
        //returns
        $sql = "SELECT qty, product_id FROM orders_returns WHERE order_id = '{$_POST['order_id']}';";
        $returnCmd = $connection->createCommand($sql);
        $returns = $returnCmd->queryAll();
        foreach($returns as $return)
        {
            $sql = "UPDATE orders_returns SET qty_picked = '{$return['qty']}', qty_collected = '{$return['qty']}' WHERE order_id = '{$_POST['order_id']}' AND product_id = '{$return['product_id']}'";
            $returnUpdate = $connection->createCommand($sql);
            $returnUpdate->execute();
        }

        $outletTransactions = new OutletTransactions();
        $outletdata = array();
        $outletdata['employee_id'] = $user;
        $outletdata['order_id'] = $_POST['order_id'];
        $outletdata['cash_collected'] = $cash_collected;
        $outletdata['payment_method_id'] = $payment_type;
        $outletdata['order_total_amount'] = $delivered_order_total_amount;
        $outletdata['gross_order_total_amount'] = $delivered_gross_order_total_amount;
        $outletdata['bakery_id'] = Yii::app()->user->bakery;
        $outletdata['outlet_id'] = $outlet_id;
        $outletdata = \GuzzleHttp\json_encode($outletdata);
        $outletdata = json_decode($outletdata);
        $outletTransactions->insertTransaction($_POST['order_id'], $outletdata);
        if($_POST['email'])
        {
            $pdfData = MailFactory::getPurchaseOrderDetails($_POST['order_id']);
            $pdfFactory = new PDFFactory($pdfData, 'Purchase Order');
            $pdf = $pdfFactory->createPurchaseOrder($_POST['order_id']);
            $filePath = Yii::app()->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'files';
            $attachment = str_replace($filePath, 'files', $pdf);
            $isEmail = $_POST['email'];
            $logId = CommunicationLog::log($isEmail, 'CREATED', null, $attachment);
            //try to send an email
            try
            {
                $email = new MailFactory('purchaseOrder', $_POST['order_id'], $isEmail);
                $details = $email->sendPurchaseOrderEmail($pdf);
                CommunicationLog::updateLog('SENT', null, $logId);
            }
            catch(Exception $e)
            {
                CommunicationLog::updateLog('FAILED', $e->getMessage(), $logId);
            }
        }
    }

    public function actionCombinePDF($tripSheetId, $from = 'in_dispatch', $tripSheetSettlementId = '0')
    {
        set_time_limit(0);
        $items = [];
        $condition = 't.trip_sheet_id = ' . $tripSheetId . ' AND orderMain.order_status_id != 19';
        if($from == 'dispatch')
        {
            $items = TripSheetsItems::model()->with('orderMain')->findAll(['condition' => $condition, 'order' => 't.id']);
        }
        if($from == 'checkin')
        {
            $condition .= ' AND orderMain.order_status_id = 5 AND orderMain.actual_delivery_date != \'0000-00-00 00:00:00\' AND settlement.id = ' . $tripSheetSettlementId . ' and orderMain.actual_delivery_date <= settlement.created_date';
            $items = TripSheetsItems::model()->with('orderMain', 'settlement')->findAll(['condition' => $condition, 'order' => 't.id']);
        }
        
        if(empty($items))
        {
            Yii::app()->user->setFlash('warning', 'Shipment #' . $tripSheetId . ' doesn\'t have any orders to download in PDF form');
            $this->redirect(Yii::app()->baseUrl . '/index.php/admin/distribution?show=' . $from);
            die();
        }
        else
        {

            $pdfFactory = new PDFCombineFactory('Invoice', 'Invoice PDFs for Shipment # ' . $tripSheetId, $tripSheetId);
            $pdfFactory->setTemplateDetails();
            $i = 0;
            foreach($items as $item)
            {
                $pdfData = MailFactory::getInvoiceDetails($item['order_id']);

                $pageBreak = false;
                if($i < (count($items) - 1))
                {
                    $pageBreak = true;
                }
                $pdfFactory->createInvoice($item['order_id'], $pdfData, $pageBreak);

                ++$i;
            }
            echo $pdfFactory->show(false);
        }
    }

    public function actionShowPDF($orderStatusId, $orderId, $hash = null)
    {
        set_time_limit(0);

        if(!is_null($hash))
        {
            $hashValid = FieldFormatter::format(FIELD_FORMATTER_VALIDATE_HASH_LINK, $orderId . '=' . $hash);
            if(!$hashValid)
            {
                $this->redirect(array('default/login'));
            }
        }
        else
        {
            $this->redirect(array('default/login'));
        }

       // echo "<br>ORDER STATUS ID:".$orderStatusId;
       // die;
        switch($orderStatusId)
        {
            case 0:
                $pdfData = MailFactory::getGoodsReceivedOrderDetails($orderId);
                $pdfFactory = new PDFFactory($pdfData, 'Plant Order Purchase Order');
                $pdf = $pdfFactory->createGoodsReceivedOrder($orderId, false, true);
            break;
            //-----|UM-2230|---------
            case 1:

                $pdfData = MailFactory::getFormDetails($orderId);
                $pdfFactory = new PDFFactory($pdfData, 'Form', false, true);
                $pdfFactory->createForm($orderId, false, true);

            break;

            case 2:

                $pdfData = MailFactory::getFormDetails($orderId);
                $pdfFactory = new PDFFactory($pdfData, 'Form', false, true);
                $pdfFactory->createForm($orderId, false, true);

            break;
            //-----END-UM-2230|-------
            case 4:

                $pdfData = MailFactory::getPurchaseOrderDetails($orderId);
                $pdfFactory = new PDFFactory($pdfData, 'Purchase Order', false, true);
                $pdfFactory->createPurchaseOrder($orderId, false, true);

            break;

            case 5:

                $pdfData = MailFactory::getInvoiceDetails($orderId);
                $pdfFactory = new PDFFactory($pdfData, 'Invoice', false, true);
                $pdf = $pdfFactory->createInvoice($orderId, false, true);

            break;

            case 8:

                $pdfData = MailFactory::getPurchaseOrderDetails($orderId);
                $pdfFactory = new PDFFactory($pdfData, 'Purchase Order', false, true);
                $pdfFactory->createPurchaseOrder($orderId, false, true);

            break;

            case 13:

                $pdfData = MailFactory::getCreditNoteDetails($orderId);
                $pdfFactory = new PDFFactory($pdfData, 'Credit Note', false, true);
                $pdfFactory->createCreditNote($orderId, false, true);

            break;

            case 15:

                $pdfData = MailFactory::getPurchaseOrderDetails($orderId);
                $pdfFactory = new PDFFactory($pdfData, 'Purchase Order', false, true);
                $pdfFactory->createPurchaseOrder($orderId, false, true);

            break;

            case 18:

                $pdfData = MailFactory::getCreditNoteDetails($orderId);
                $pdfFactory = new PDFFactory($pdfData, 'Credit Note', false, true);
                $pdfFactory->createCreditNote($orderId, false, true);

            break;

            case 19:

                $pdfData = MailFactory::getCreditNoteDetails($orderId);
                $pdfFactory = new PDFFactory($pdfData, 'Credit Note', false, true);
                $pdfFactory->createCreditNote($orderId, false, true);

            break;
  
            default:

                $pdfData = MailFactory::getPurchaseOrderDetails($orderId);
                $pdfFactory = new PDFFactory($pdfData, 'Purchase Order', false, true);
                $pdfFactory->createPurchaseOrder($orderId, false, true);
        }
    }

    public function actionGetOrdersDetailed($id, $from = '')
    {
        $orders = new Order_main();
        $orderStatuses = '2,4,5,6,8,9,13,15,18,19';
        // Since most of the code is generic, the shipment number won't always be numeric
        if(!is_numeric($id))
        {
            $temp_shipment_number = explode('_', $id);
            $tripSheetId = $temp_shipment_number[1];
        }
        else
        {
            $tripSheetId = $id;
        }
        $orders = $orders->getOrdersDetailed($orderStatuses, $tripSheetId, 0, 'distribution', false, true);

        if(!empty($orders['results']))
        {
            $baseUrl = Yii::app()->request->baseUrl;
            $lat = Yii::app()->params['mapCenterLat'];
            $lng = Yii::app()->params['mapCenterLng'];
            $defaultZoom = Yii::app()->params['mapDefaultZoom'];
            $imagePath = Yii::app()->baseUrl . '/images/';
            $mapId = 'gMap';
            $mapData = json_encode($orders['results']);
            $key = Yii::app()->params['googleAPIKey'];
            $initMap = <<<HTML
        <script async defer src="https://maps.googleapis.com/maps/api/js?key={$key}&callback=initiateMap"></script>
HTML;
            $map = <<<HTML
            <div class="center" style="margin-bottom: 10px;">
                <button id="toggleGMap{$mapId}" class="btn btn-default btn-sm">Toggle Map</button>
            </div>
            <div id="{$mapId}" style="display: none; margin-bottom: 10px;">
            </div>
            {$initMap}
            <script type="text/javascript">
            $(document).ready(function(){
                $('#toggleGMap{$mapId}').click(function(e){
                    e.preventDefault();
                    $('#{$mapId}').toggle();
                });
            });
            function initiateMap()
            {
                var mapCenter = {lat: {$lat}, lng: {$lng}};
                map = new google.maps.Map(document.getElementById('{$mapId}'), {
                  zoom: {$defaultZoom},
                  center: mapCenter,
                  scrollwheel: false
                });

                bounds = new google.maps.LatLngBounds();
                sequence = 1;

                var data = {$mapData};

                markersArr = [];
                for(i = 0; i < data.length; i++)
                {
                    createMarker(data[i]);
                }

                map.fitBounds(bounds);
            }

            $(document).ready(function(){

                $('#{$mapId}').css({
                    width: '100%',
                    height: '350px'
                });
            });

            var map = null;
            var markersArr = null;
            var bounds = null;
            var sequence = null;


            function createMarker(markerData)
            {
                var latLng = new google.maps.LatLng(markerData.gps_lat, markerData.gps_lon);
                var finalLatLng = latLng;

                for (var orderNumber in markersArr)
                {
                    var existingMarker = markersArr[orderNumber];
                    var pos = existingMarker.getPosition();

                    //if a marker already exists in the same position as this marker
                    if (latLng.equals(pos)) {
                        //update the position of the coincident marker by applying a small multiplier to its coordinates
                        var newLat = latLng.lat() + (Math.random() -.5) / 1500;// * (Math.random() * (max - min) + min);
                        var newLng = latLng.lng() + (Math.random() -.5) / 1500;// * (Math.random() * (max - min) + min);
                        finalLatLng = new google.maps.LatLng(newLat,newLng);
                    }
                }

                var orderStatusId = markerData.order_status_id;
                var markerIcon = getMarkerIcon(orderStatusId);
                var markerOptions = {
                    map: map,
                    icon: {
                        labelOrigin: new google.maps.Point(10,12),
                        url: markerIcon
                    },
                    position: finalLatLng,
                    title: markerData.id
                };
                var marker = new google.maps.Marker(markerOptions);
                markersArr[markerData.id] = marker;

                var whatDate = 'Expected Delivery Date: ' + markerData.expected_delivery_date;
                if(orderStatusId == 5 || orderStatusId == 9)
                {
                    var text = '' + sequence + '';
                    sequence++;
                    var markerLabel = {
                        color: '#FFFFFF',
                        fontWeight: 'bold',
                        fontSize: '16px',
                        text: text
                    };

                    marker.setLabel(markerLabel);

                    whatDate = 'Actual Delivery Date: ' + markerData.actual_delivery_date;
                }

                var infoWindowContent = '<div style="width: 300px;">Customer: ' + markerData.outlet_name + '<br>Order #: ' + markerData.id + '<br>' + whatDate + '</div>';
                var infoWindowOptions = {
                    content: infoWindowContent
                };
                var infoWindow = new google.maps.InfoWindow(infoWindowOptions);

                google.maps.event.addListener(marker, 'click', function(){
                    infoWindow.open(map, marker);
                });

                bounds.extend(marker.getPosition());
            }

            function getMarkerIcon(orderStatusId)
            {
                var image = 'marker_red.png';

                if(orderStatusId == 5)
                {
                    image = 'marker_green.png'
                }
                if(orderStatusId == 8)
                {
                    image = 'marker_orange.png'
                }
                if(orderStatusId == 9)
                {
                    image = 'marker_blue.png'
                }
                if(orderStatusId == 13)
                {
                    image = 'marker_blue.png'
                }

                return '{$imagePath}markers/' + image;
            }
            </script>
HTML;
        }
        else
        {
            $map = '';
            $orders['html'] = <<<HTML
            <div class="center text-muted">No deliveries made</div>
HTML;
        }
        //check if there are van sale opening stock for this trip sheet
        $tripSheet = TripSheets::model()->findByPk($tripSheetId);
        $bakeryId = $tripSheet->bakery_id;
        $vsStock = TripSheetsStock::checkVanSaleStockDelivered($tripSheetId, $bakeryId);

        $vsStockHtml = '<h4 style="margin-top: 0;" class="center text-primary">Van Sale Stock</h4>';
        if(!empty($vsStock))
        {
            $vsStockHtml .= <<<HTML
            <table class="customTableNoHover">
                <tr>
                    <td class="customTableSubHeading">Sku</td>
                    <td class="customTableSubHeading">Product</td>
                    <td class="customTableSubHeading">Expiry Date</td>
                    <td class="customTableSubHeading">Loaded Qty</td>
                    <td class="customTableSubHeading">Delivered Qty</td>
                    <td class="customTableSubHeading">Available Qty</td>
                </tr>

HTML;
            foreach($vsStock as $stock)
            {
                if($stock['opening_qty'] > 0)
                {
                    $vsStockHtml .= <<<HTML
                    <tr>
                        <td class="customTableData">{$stock['sku']}</td>
                        <td class="customTableData"><span class="bold">{$stock['unit_of_measure']}</span> - {$stock['description']} ({$stock['batch_code']})</td>
                        <td class="customTableData">{$stock['expiry_date']}</td>
                        <td class="customTableData">{$stock['opening_qty']}</td>
                        <td class="customTableData">{$stock['qty_delivered']}</td>
                        <td class="customTableData">{$stock['qty']}</td>
                    </tr>
HTML;
                }
            }
            $vsStockHtml .= <<<HTML
            </table>
HTML;
        }
        else
        {
            $vsStockHtml .= <<<HTML
            <div class="center text-muted">No van sale stock found</div>
HTML;
        }

        $tripSheet = TripSheets::model()->findByPk($tripSheetId);
        $requestData = [
            'Request' => [
                'ignoreDeviceCheck' => 1,
                'just_view' => 1,
                'employee_id' => $tripSheet->driver_employee_id,
                'bakery_id' => $tripSheet->bakery_id,
                'Dt' => date('Y-m-d H:i:s'),
                'trip_sheet_id' => $tripSheetId,
                'DeviceID' => '',
                'REG_KEY_VALUE' => '',
                'RequestRef' => 'closingstock_'.$tripSheet->driver_employee_id.'_'.date('YmdHis'),
                'Type' => 'checkclosingstockbyadmin',
                'from' => $from
            ]
        ];
        $tss = APIUtils::requestPOST(Yii::app()->getBaseUrl(true).'/index.php/api/central/webservice' ,$requestData);
        // echo $tss['message'];
        // die();
        $tssData = json_decode($tss['message']);
        $vsStockHtmlWs = '';
        if($from != 'completed')
        {
            $vsStockHtmlWs = '<h4 style="margin-top: 0;" class="center text-primary">Shipment Stock</h4>';
            if(isset($tssData->Data->closed_trip_details->products))
            {
                $vsStockHtmlWs .= <<<HTML
                <table class="customTableNoHover">
                    <tr>
                        <td class="customTableSubHeading fit">Sku</td>
                        <td class="customTableSubHeading">Product</td>
                        <td class="customTableSubHeading fit">Batch Code</td>
                        <td class="customTableSubHeading fit">Expiry Date</td>
                        <td class="customTableSubHeading fit">Planned Qty</td>
                        <td class="customTableSubHeading fit">Accepted Qty</td>
                        <td class="customTableSubHeading fit">Delivered Qty</td>
                        <td class="customTableSubHeading fit">Available Qty</td>
                    </tr>
    
HTML;
                $stock = $tssData->Data->closed_trip_details->products;
                foreach($stock as $item)
                {
                    // we have to remove the below if statement, because available stock
                    // if($item->opening_qty > 0)
                    // {
                        $highlightLinked = '';
                        if($item->linked_product > 0)
                        {
                            $highlightLinked = 'highlightLinked';
                        }
                        $highlightNonOriginal = '';
                        if($item->original_product === 0)
                        {
                            $highlightNonOriginal = 'highlightNonOriginal';
                        }
    
    
                        $qtyDelivered = $item->qty_delivered;
                        $qtyAccepted = $item->opening_qty_original;
                        if($from == 'completed')
                        {
                            $qtyDelivered = $item->qty_delivered_completed;
                        }
    
                        $vsStockHtmlWs .= <<<HTML
                        <tr class="{$highlightLinked} {$highlightNonOriginal}">
                            <td class="customTableData fit">{$item->sku}</td>
                            <td class="customTableData"><span class="bold">{$item->unit_of_measure}</span> - {$item->product}</td>
                            <td class="customTableData fit">{$item->batch_code}</td>
                            <td class="customTableData fit">{$item->expiry_date}</td>
                            <td class="customTableData fit">{$item->planned_qty}</td>
                            <td class="customTableData fit">{$qtyAccepted}</td>
                            <td class="customTableData fit">{$qtyDelivered}</td>
                            <td class="customTableData fit">{$item->qty_rolled_up} of {$item->qty_rolled_up_out_of}</td>
                        </tr>
HTML;
                    // }
                }
                $vsStockHtmlWs .= '</table>';
            }
        }

//        {$vsStockHtml}
        //get the top-ups/trip sheets for this primary trip sheet
        $acceptedStockHtml = '';
        if($tripSheet->primary_trip_sheet == 1)
        {
            $acceptedStock = TripSheetsAccepted::getAcceptedStock($tripSheetId);
            $acceptedStockHtml = $this->renderPartial('_acceptedStock', ['acceptedStock' => $acceptedStock], true);
        }

        $stockUplifShipmentAcceptedStockHtml = '';
        if($tripSheet->stock_uplift_shipment == '1' && $from != 'completed')
        {
            $comOneResult = AsStockMovement::stockUpliftShipmentAccepted($tripSheetId);
            $comTwoResult = [];
            $stockUplifShipmentAcceptedStockHtml = $this->renderPartial('_stockUpliftShipmentDetails', ['resultOne' => $comOneResult, 'resultTwo' => $comTwoResult, 'tripSheetId' => $tripSheetId, 'tripSheetSettlementId' => 0], true);
        }

        if(!empty($orders['results']))
        {
            $deliveriesMade = <<<HTML
            <h4 class="center text-primary">Deliveries Made</h4>
            {$map}
            {$orders['html']}
HTML;
        }
        else
        {
            $deliveriesMade = '';
        }

        $final = <<<HTML
        {$acceptedStockHtml}
        {$stockUplifShipmentAcceptedStockHtml}
        {$vsStockHtmlWs}
        {$deliveriesMade}
HTML;
        echo $final;
    }

    public function actioncancelOrderProduct($id_status_is)
    {
        $do = explode("_", $id_status_is);
        $id = $do[1];
        $current_status = $do[2];
        if($current_status == 5 || $current_status == 6 || $current_status == "Deleivered")
            $new_status = 9;
        else
            $new_status = 7;
        $order = new Order_main();
        $order->updateOrderStatus($id, $new_status);
        echo <<<HTML
        <div class='alert alert-success'>Order cancelled successfully.</div>
HTML;
    }

    public function actioncompleteDeliveries()
    {
        $deliveries = new Miscellaneous();
        var_dump($deliveries->completeDeliveries());
    }

    public function actionshipmentcomplete($id)
    {
        $miscellaneous = new Miscellaneous();
        #var_dump($order);die;
        $data = array();
        $data['id'] = $id;
        $tripSheetStatus = $miscellaneous->getStatusLevel($data['id']);
        if($tripSheetStatus[0]['level_status'] === '1')
        {
            $returns["cash_settled"] = $miscellaneous->getCashSettled($id);
            $data['captured_amount'] = (isset($returns["cash_settled"][0]['settled'])) ? $returns["cash_settled"][0]['settled'] : 0;
            //get products captured
            $returns["products"] = $miscellaneous->getCapturedProducts($id);
            foreach($returns["products"] as $captured)
            {
                $data['products'][$captured['product_id']] = $captured['qty'];
            }
            //get products expected
            $returns["expected_products"] = $miscellaneous->getExpectedProducts($id);
            foreach($returns["expected_products"] as $expected)
            {
                if($expected['qty_picked'] != $expected['qty_delivered'])
                {
                    $data['expected_products'][$expected['prod_id']] = $expected['qty_picked'] - $expected['qty_delivered'];
                }
            }
            //get returns captured
            $returns["returns"] = $miscellaneous->getProductsReturnedCapture($id);
            foreach($returns["returns"] as $captured_returns)
            {
                $data['returns'][$captured_returns['product_id']] = $captured_returns['qty'];
            }
            //get returns expected
            $returns["expected_returns"] = $miscellaneous->getProductsReturnsExpected($id);
            foreach($returns["expected_returns"] as $expected_returns)
            {
                $data['expected_returns'][$expected_returns['id']] = $expected_returns['qty_expected'];
            }
            $misc = new Miscellaneous();
            echo $misc->updateOrderReturns($data);
        }
        else
        {
            $tripSheet = Trip_sheets::model()->findBySql('SELECT IFNULL(cash_settlement_indicator, 0) as cash_settlement_indicator FROM trip_sheets WHERE id = ' . $id);
            $cashSettlementIndicator = $tripSheet->cash_settlement_indicator;
            if(intval($cashSettlementIndicator) == 0)
            {
                $updateSql = <<<SQL
                update trip_sheets set cash_settlement_indicator = 1 where id = :a
SQL;
                $con = Yii::app()->db;
                $com = $con->createCommand($updateSql);
                $com->bindValue(':a', $id, PDO::PARAM_INT);
                $com->execute();
            }
            $shipment_complete = new Miscellaneous();
            echo $shipment_complete->shipmentComplete($id);
        }
    }

    public function actionPerformBlindCheckCash()
    {
        $tripSheetId = $_POST['tripSheetId'];
        $con = Yii::app()->db;
        $rowsUpdated = 0;
        $cashTab = CustomUtils::salesCashCollectSplit();
        if($cashTab)
        {
            //check if there is an entry in trip_sheets_cash
            $entryCom = $con->createCommand('select id from trip_sheets_cash where trip_sheet_id = :a');
            $entryCom->bindValue(':a', $_POST['tripSheetId'], PDO::PARAM_INT);
            $hasEntry = $entryCom->queryScalar();
            if(!$hasEntry)//insert
            {
                //get the sum of cash collected for this trip sheet
                $expectedCashSql = <<<SQL
                SELECT
                    IFNULL(SUM(om.cash_collected), 0)
                FROM
                    order_main om
                        INNER JOIN
                    trip_sheets_items tsi ON tsi.order_id = om.id
                WHERE
                    tsi.trip_sheet_id = :a AND order_status_id = 5;
SQL;
                $expectedCashCom = $con->createCommand($expectedCashSql);
                $expectedCashCom->bindValue(':a', $tripSheetId, PDO::PARAM_INT);
                $expectedCash = $expectedCashCom->queryScalar();
                //follow up cash expected
                $expectedCashFollowUpSql = <<<SQL
                SELECT
                    IFNULL(SUM(amount_collected), 0)
                FROM
                    cash_collected
                WHERE
                    cash_collected_date >= (SELECT
                            MIN(actual_delivery_date)
                        FROM
                            order_main
                        WHERE
                            id IN (SELECT
                                    order_id
                                FROM
                                    trip_sheets_items
                                WHERE
                                    trip_sheet_id = :a)
                                AND order_status_id = 5)
                        AND cash_collected_date <= NOW()
                        AND status = 0;
SQL;
                $expectedCashFollowUpCom = $con->createCommand($expectedCashFollowUpSql);
                $expectedCashFollowUpCom->bindValue(':a', $tripSheetId, PDO::PARAM_INT);
                $expectedCashFollowUp = $expectedCashFollowUpCom->queryScalar();
                $insertSql = <<<SQL
                insert into trip_sheets_cash (trip_sheet_id, expected, settled, actual, expected_follow_up, settled_follow_up, actual_follow_up, trip_sheets_cash_status_id, settled_by_employee_id, settled_date)
                values (:a, :b, :b, 0.00, :c, :c, 0.00, 0, :d, NOW());
SQL;
                $insertCom = $con->createCommand($insertSql);
                $insertCom->bindValue(':a', $tripSheetId, PDO::PARAM_INT);
                $insertCom->bindValue(':b', $expectedCash, PDO::PARAM_STR);
                $insertCom->bindValue(':c', $expectedCashFollowUp, PDO::PARAM_STR);
                $insertCom->bindValue(':d', Yii::app()->user->id, PDO::PARAM_INT);
                $insertCom->execute();
            }
            $tscUpdateSql = <<<SQL
                update trip_sheets_cash set settled = :a, settled_follow_up = :b, settled_by_employee_id = :d, settled_date = NOW(), level_status = 1 where trip_sheet_id = :c
SQL;
            $tscUpdateCom = $con->createCommand($tscUpdateSql);
            $tscUpdateCom->bindValue(':a', $_POST['captured_amount'], PDO::PARAM_STR);
            $tscUpdateCom->bindValue(':b', $_POST['captured_amount_follow_up'], PDO::PARAM_STR);
            $tscUpdateCom->bindValue(':c', $tripSheetId, PDO::PARAM_INT);
            $tscUpdateCom->bindValue(':d', Yii::app()->user->id, PDO::PARAM_INT);
            $tscUpdate = $tscUpdateCom->execute();
            $rowsUpdated += $tscUpdate;
            echo 1;
            return;
        }
    }

    public function actionPerformVarianceCash()
    {
        $difference = $_POST['varianceCashCollectedExpected'] - $_POST['varianceCashCollectedActual'];
        $differenceFollowUp = $_POST['varianceCashCollectedFollowUpExpected'] - $_POST['varianceCashCollectedFollowUpActual'];
        $tripSheetId = $_POST['tripSheetId'];
        $con = Yii::app()->db;
        $rowsUpdated = 0;
        $tscUpdateSql = <<<SQL
                update trip_sheets_cash set actual = :a, actual_follow_up = :b, difference = :e, difference_follow_up = :f, settled_by_employee_id = :d, settled_date = NOW(), level_status = 2 where trip_sheet_id = :c
SQL;
        $tscUpdateCom = $con->createCommand($tscUpdateSql);
        $tscUpdateCom->bindValue(':a', $_POST['varianceCashCollectedActual'], PDO::PARAM_STR);
        $tscUpdateCom->bindValue(':b', $_POST['varianceCashCollectedFollowUpActual'], PDO::PARAM_STR);
        $tscUpdateCom->bindValue(':c', $tripSheetId, PDO::PARAM_INT);
        $tscUpdateCom->bindValue(':d', Yii::app()->user->id, PDO::PARAM_INT);
        $tscUpdateCom->bindValue(':e', $difference, PDO::PARAM_STR);
        $tscUpdateCom->bindValue(':f', $differenceFollowUp, PDO::PARAM_STR);
        $tscUpdate = $tscUpdateCom->execute();
        $rowsUpdated += $tscUpdate;
        //first insert into the cash_collect_settlement table to get the last inserted id
        $iSql = <<<SQL
            insert into cash_collected_settlement (total_cash_collect_expected, total_cash_collect_settled, trip_sheets_id, settled_date) values (:a, :b, :c, NOW());
SQL;
        $iCom = $con->createCommand($iSql);
        $iCom->bindValue(':a', $_POST['varianceCashCollectedFollowUpExpected'], PDO::PARAM_STR);
        $iCom->bindValue(':b', $_POST['varianceCashCollectedFollowUpActual'], PDO::PARAM_STR);
        $iCom->bindValue(':c', $tripSheetId, PDO::PARAM_INT);
        $iCom->execute();
        $cashCollectSettlementId = Yii::app()->db->getLastInsertId();
        //update the cash_collect table
        $cashCollectedIdSql = <<<SQL
        SELECT
            cc.id,
            o.bakery_id,
            cc.outlet_id,
            0 AS order_id,
            CONCAT('Ref #', ' ', cc.reference_nr) AS description,
            3 AS payment_type_id,
            0 AS debit_amount,
            cc.amount_collected AS credit_amount,
            0 AS payment_allocation_status_id
        FROM
            cash_collected cc
                INNER JOIN
            outlet o ON o.id = cc.outlet_id
        WHERE
            cc.status = 0
                AND cc.cash_collected_date >= (SELECT
                    MIN(actual_delivery_date)
                FROM
                    order_main
                WHERE
                    id IN (SELECT
                            order_id
                        FROM
                            trip_sheets_items
                        WHERE
                            trip_sheet_id = :a)
                        AND order_status_id = 5)
                AND cash_collected_date <= NOW();
SQL;
        $cashCollectedIdCom = $con->createCommand($cashCollectedIdSql);
        $cashCollectedIdCom->bindValue(':a', $tripSheetId, PDO::PARAM_STR);
        $cashCollectedIds = $cashCollectedIdCom->queryAll();
        $approvedDate = date('Y-m-d H:i:s');
        for($i = 0; $i < count($cashCollectedIds); $i++)
        {
            $cc = CashCollected::model()->findByPk($cashCollectedIds[$i]['id']);
            $cc->approved_by = Yii::app()->user->id;
            $cc->approved_date = $approvedDate;
            $cc->cash_collected_settlement_id = $cashCollectSettlementId;
            $cc->status = 1;
            $cc->save();
            $outletId = $cashCollectedIds[$i]['outlet_id'];
            //insert into outlet_transactions
            $ot = new OutletTransactions();
            $ot->bakery_id = $cashCollectedIds[$i]['bakery_id'];
            $ot->outlet_id = $outletId;
            $ot->order_id = $cashCollectedIds[$i]['order_id'];
            $ot->description = $cashCollectedIds[$i]['description'];
            $ot->payment_type_id = $cashCollectedIds[$i]['payment_type_id'];
            $ot->debit_amount = $cashCollectedIds[$i]['debit_amount'];
            $ot->credit_amount = $cashCollectedIds[$i]['credit_amount'];
            $ot->created = date('Y-m-d H:i:s');
            $ot->create_employee_id = Yii::app()->user->id;
            $ot->payment_allocation_status_id = $cashCollectedIds[$i]['payment_allocation_status_id'];
            $ot->save();
            // Update outlet available credit
            $outletCredit = new OutletCredit();
            $outletCredit->updateAvailableCredit($outletId);
        }
        echo 1;
        return;
    }

    public function actionupdateReturns()
    {
        $misc = new Miscellaneous();
        echo $misc->updateOrderReturns($_GET);
    }

    public function actionPreSalesImport()
    {
        $orders = [];
        $this->renderPartial('presales/_ps_import', ['orders' => $orders]);
    }

    public function actionImportPsFileValidate()
    {
        if(isset($_FILES['psImportFile']))
        {
            $orders = [];
            $alertType = 'danger';
            $alertMsg = 'Whoops! This is not an excel file with an xlsx extension';
            //check the file extension for xlsx
            $details = $_FILES['psImportFile'];
            if(strtolower(explode('.', $details['name'])[1]) != 'xlsx')//not an xlsx file
            {

            }
            else
            {
                $phpExcelPath = Yii::getPathOfAlias('ext.phpexcel.Classes');
                include($phpExcelPath . DIRECTORY_SEPARATOR . 'PHPExcel.php');
                $tmpFile = $details['tmp_name'];
                $objReader = new PHPExcel_Reader_Excel2007();
                $objPHPExcel = $objReader->load($tmpFile);
                $objPHPExcel->setActiveSheetIndex(0);
                $activeSheet = $objPHPExcel->getActiveSheet();
                $headings = [];
                $highestRow = $activeSheet->getHighestRow();
                $highestColumn = $activeSheet->getHighestColumn();
                $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
                for($row = 1; $row <= 1; $row++)
                {
                    for($col = 0; $col <= $highestColumnIndex; $col++)
                    {
                        $heading = $activeSheet->getCellByColumnAndRow($col, $row)->getValue();
                        $headings[$col] = $heading;
                    }
                }
                if(!isset($headings[0]))//file is empty
                {
                    $alertMsg = 'Whoops! This file seems to be empty';
                }
                else
                {
                    $con = Yii::app()->db;
                    $bakeryId = Yii::app()->user->bakery;
                    $missingFieldsArr = [];
                    //get the mappings
                    $fieldMappings = $con->createCommand('select * from field_mapping_ps_import')->queryAll();
                    $createCols = '`id` INT NOT NULL AUTO_INCREMENT, ';
                    $useColumns = [];
                    $tmpInsertSql = 'insert into import_ps_wizard_tmp (';
                    $i = 0;
                    $cellValidate = [];
                    foreach($fieldMappings as $mappings)
                    {
                        ($i == 0) ? $createCols .= '`' . $mappings['mac_field'] . '` VARCHAR(255) NULL' : $createCols .= ', `' . $mappings['mac_field'] . '` VARCHAR(255) NULL';
                        ($i == 0) ? $tmpInsertSql .= '`' . $mappings['mac_field'] . '`' : $tmpInsertSql .= ', `' . $mappings['mac_field'] . '`';
                        if(in_array($mappings['file_field'], $headings))
                        {
                            $useColumns[array_search($mappings['file_field'], $headings)] = true;
                            $cellValidate[array_search($mappings['file_field'], $headings)] = $mappings['field_type'];
                        }
                        if(!in_array($mappings['file_field'], $headings))
                        {
                            $missingFieldsArr[] = $mappings['file_field'];
                        }
                        ++$i;
                    }
                    $tmpInsertSql .= ') VALUES ';
                    if(!empty($missingFieldsArr))
                    {
                        $alertMsg = 'Whoops! The following column(s) are missing from the file<ul>';
                        for($i = 0; $i < count($missingFieldsArr); $i++)
                        {
                            $alertMsg .= '<li>' . $missingFieldsArr[$i] . '</li>';
                        }
                        $alertMsg .= '</ul>';
                    }
                    else
                    {
                        $cellValidationErrors = [];
                        //check the types of the cells in the upload
                        for($row = 2; $row <= $highestRow; $row++)//start from row 2, don't read headings
                        {
                            foreach($useColumns as $col => $use)
                            {
                                $cell = $activeSheet->getCellByColumnAndRow($col, $row);
                                if(array_key_exists($col, $cellValidate))
                                {
                                    $validateType = $cellValidate[$col];
                                    $uploadValue = $cell->getValue();
                                    $cellCoordinate = $cell->getCoordinate();
                                    switch($validateType)
                                    {
                                        case 'date':
                                            if(PHPExcel_Shared_Date::isDateTime($cell))//formatted in Excel as a date
                                            {
                                                if(!strtotime($cell->getFormattedValue()))
                                                {
                                                    $cellValidationErrors[] = [
                                                        'cell' => $cellCoordinate,
                                                        'type' => $validateType
                                                    ];
                                                }
                                            }
                                            else//should just be a date value
                                            {
                                                if(!strtotime($cell->getFormattedValue()))
                                                {
                                                    $cellValidationErrors[] = [
                                                        'cell' => $cellCoordinate,
                                                        'type' => $validateType
                                                    ];
                                                }
                                            }
                                            break;
                                        case 'float':
                                            if(!is_float($uploadValue))
                                            {
                                                $cellValidationErrors[] = [
                                                    'cell' => $cellCoordinate,
                                                    'type' => 'number'
                                                ];
                                            }
                                            break;
                                        default://string

                                    }
                                }
                            }
                        }
                        if(!empty($cellValidationErrors))
                        {
                            $alertMsg = 'Eish! The following fields are not of the correct type<ul>';
                            for($i = 0; $i < count($cellValidationErrors); $i++)
                            {
                                $extraMsg = '';
                                if($cellValidationErrors[$i]['type'] === 'date')
                                {
                                    $extraMsg = ' or is not in the format yyyy-mm-dd';
                                }
                                $alertMsg .= '<li>Cell: ' . $cellValidationErrors[$i]['cell'] . ' is not a ' . $cellValidationErrors[$i]['type'] . $extraMsg . '</li>';
                            }
                            $alertMsg .= '</ul>';
                        }
                        else
                        {
                            $con->createCommand('drop table if exists import_ps_wizard_tmp;')->execute();
                            $con->createCommand('create table import_ps_wizard_tmp ( ' . $createCols . ', PRIMARY KEY (`id`)) CHARSET=utf8;')->execute();
                            $values = [];
                            for($row = 2; $row <= $highestRow; $row++)//start from row 2, don't read headings
                            {
                                $rowValues = [];
                                foreach($useColumns as $col => $use)
                                {
                                    $cell = $activeSheet->getCellByColumnAndRow($col, $row);
                                    if(PHPExcel_Shared_Date::isDateTime($cell))//for date columns
                                    {
                                        $excelDateValue = $cell->getFormattedValue();
                                        $date = DateTime::createFromFormat('Y-m-d', $excelDateValue);
                                        if(!$date)
                                        {
                                            $date = DateTime::createFromFormat('Y/m/d', $excelDateValue);
                                        }
                                        $dateValue = $date->format('Y-m-d');
                                        $rowValues[] = '"' . $dateValue . '"';
                                    }
                                    else
                                    {
                                        $rowValues[] = '"' . addslashes($cell->getValue()) . '"';
                                    }
                                }
                                $values[] = '(' . implode(',', $rowValues) . ')';
                            }
                            $extendedValues = array_chunk($values, 500);
                            foreach($extendedValues as $chunk)
                            {
                                $extendedVals = '';
                                for($i = 0; $i < count($chunk); $i++)
                                {
                                    ($i == 0) ? $extendedVals .= $chunk[$i] : $extendedVals .= ', ' . $chunk[$i];
                                }
                                $tmpInsertSql .= $extendedVals . ';';
                                $con->createCommand($tmpInsertSql)->execute();
                            }
                            $om = new Order_main();
                            $orders = $om->getPSImportedWizardOrders();
                            $uomSql = <<<SQL
                            SELECT
                                w.device_order_id,
                                w.product_id AS sku,
                                p.description AS product,
                                w.unit_of_measure AS file_unit_of_measure,
                                uom.description AS db_unit_of_measure,
                                CASE
                                    WHEN w.unit_of_measure != uom.description THEN 0
                                    ELSE 1
                                END AS valid
                            FROM
                                import_ps_wizard_tmp w
                                    LEFT JOIN
                                products p ON p.sku = w.product_id
                                    LEFT JOIN
                                units_of_measure uom ON uom.id = p.units_of_measure_id
                            WHERE
                                p.bakery_id = {$bakeryId};
SQL;
                            $uoms = $con->createCommand($uomSql)->queryAll();
                            $invalidUom = [];
                            foreach($uoms as $uom)
                            {
                                if($uom['valid'] === '0')
                                {
                                    $invalidUomTmp = [
                                        'device_order_id' => $uom['device_order_id'],
                                        'sku' => $uom['sku'],
                                        'product' => $uom['product'],
                                        'file_uom' => $uom['file_unit_of_measure'],
                                        'db_uom' => $uom['db_unit_of_measure']
                                    ];
                                    $invalidUom[] = $invalidUomTmp;
                                }
                            }
                            $alertMsg = '';
                            if(!empty($invalidUom))
                            {
                                $alertMsg .= '<div class="alert alert-warning">Just a note that the following product(s) will be IGNORED because of a UNIT OF MEASURE mismatch<ul>';
                                for($i = 0; $i < count($invalidUom); $i++)
                                {
                                    $alertMsg .= '<li>Document #: ' . $invalidUom[$i]['device_order_id'] . ' - ' . $invalidUom[$i]['sku'] . ' - ' . $invalidUom[$i]['product'] . ': File UOM : <span class="bold">' . $invalidUom[$i]['file_uom'] . '</span> Product Management UOM: <span class="bold">' . $invalidUom[$i]['db_uom'] . '</span></li>';
                                }
                                $alertMsg .= '</ul></div>';
                            }
                            $alertType = 'success';
                            $alertMsg .= '<div>Please deselect the orders you would like to ignore</div>';
                        }
                    }
                }
            }
            $this->renderPartial('presales/_ps_import_result', ['alertType' => $alertType, 'alertMsg' => $alertMsg, 'orders' => $orders]);
        }
    }

    public function actionCreatePreSalesOrdersImport()
    {
        $where = ' AND device_order_id in (';
        $i = 0;
        foreach($_POST['psImportOrders'] as $deviceOrderId => $on)
        {
            $where .= ($i === 0) ? '\'' . addslashes($deviceOrderId) . '\'' : ', \'' . addslashes($deviceOrderId) . '\'';
            ++$i;
        }
        $where .= ')';
        $om = new Order_main();
        $orders = $om->getPSImportedWizardOrders($where);
        $con = Yii::app()->db;
        $success = true;
        $currentTime = date('H:i:s');
        foreach($orders as $order)
        {
            $orderMain = new Order_main();
            $orderMain->id = $order['order_id'];
            $orderMain->device_order_id = $order['device_order_id'];
            $orderMain->account = $order['account'];
            $orderMain->bakery_id = $order['bakery_id'];
            $orderMain->purchase_order_nr = $order['purchase_order_nr'];
            $orderMain->order_taker_employee_nr = $order['order_taker_employee_nr'];
            $orderMain->order_date = $order['order_date'] . ' ' . $currentTime;
            $orderMain->expected_delivery_date = $order['expected_delivery_date'];
            $orderMain->order_status_id = $order['order_status_id'];
            $orderMain->order_type = $order['order_type'];
            $orderMain->payment_type_id = $order['payment_type_id'];
            $orderMain->linked_to_van_sale = $order['linked_to_van_sale'];
            $orderMain->combined_load_pre_sale_import = $order['combined_load_pre_sale_import'];
            $orderMain->order_total_amount = $order['order_total_amount'];
            $orderMain->gross_order_total_amount = $order['gross_order_total_amount'];
            $orderMain->vat_value = $order['vat_value'];
            $orderMain->discount = $order['discount'];
            $orderMain->discount_value = $order['discount_value'];
            $orderMain->cash_collected = 0.00;
            if(!$orderMain->save())
            {
                $success = false;
            }
            else
            {
                //if create, get the orders for this order_id from the wizard tmp table
                $ordersSql = <<<SQL
                    SELECT
                        p.id AS product_id,
                        w.qty_ordered,
                        0 AS discount,
                        ROUND(p.price, 2) AS price,
                        ROUND(p.price, 2) AS gross_price,
                        p.vat AS vat_percentage,
                        ROUND(((w.qty_ordered * p.price) * (p.vat / 100)),
                                2) AS vat_value,
                        uom.description
                    FROM
                        import_ps_wizard_tmp w
                            INNER JOIN
                        units_of_measure uom ON uom.description = w.unit_of_measure
                            INNER JOIN
                        products p ON p.sku = w.product_id
                            AND p.units_of_measure_id = uom.id
                            AND IFNULL(p.batch_code, '') = w.batch_code
                    WHERE
                        w.device_order_id = :deviceOrderId
SQL;
                $ordersCommand = $con->createCommand($ordersSql);
                $ordersCommand->bindValue(':deviceOrderId', $order['device_order_id'], PDO::PARAM_STR);
                $ordersResult = $ordersCommand->queryAll();
                foreach($ordersResult as $sku)
                {
                    $skuOrder = new Orders();
                    $skuOrder->order_id = $order['order_id'];
                    $skuOrder->product_id = $sku['product_id'];
                    $skuOrder->qty_ordered = $sku['qty_ordered'];
                    $skuOrder->qty_picked = 0;
                    $skuOrder->discount = $sku['discount'];
                    $skuOrder->price = $sku['price'];
                    $skuOrder->gross_price = $sku['gross_price'];
                    $skuOrder->vat_percentage = $sku['vat_percentage'];
                    $skuOrder->vat_value = $sku['vat_value'];
                    $orderSave = $skuOrder->save();
                    if(!$orderSave)
                    {
                        $success = false;
                    }
                }
            }
        }
        if($success)
        {
            $what = 'success';
            $msg = 'Orders successfully imported, please wait...';
        }
        else
        {
            $what = 'danger';
            $msg = 'Whoops! Orders failed to import';
        }
        $return = '<div class="alert alert-' . $what . '">' . $msg . '</div>';
        echo $return;
    }

    public function actionReturnsSheet($id, $type = 2)
    {
        $order = 'type.description, product.description, unitOfMeasure.description';
        $bakeryId = TripSheets::model()->findByPk($id)->bakery_id;
        $bakery = Bakery::model()->findByPk($bakeryId);
        $orderBy = Bakery::getDistributionProductsOrderBy($bakeryId, 'sql_three');
        $data = TripSheets::model()->with('bakery', 'fleet', 'driver', 'tripSheetStock.product.type', 'tripSheetStock.product.supplier', 'tripSheetStock.product.unitOfMeasure')->findByPk($id, ['condition' => 'tripSheetStock.product_indicator = 1', 'order' => $orderBy]);

        $returns = TripSheets::model()->with('bakery', 'fleet', 'driver', 'tripSheetStock.product.type', 'tripSheetStock.product.supplier', 'tripSheetStock.product.unitOfMeasure')->findByPk($id, ['condition' => 'tripSheetStock.product_indicator = 2', 'order' => $orderBy]);

        $pdf = new DistributionPDFFactory('Stock Settlement Sheet', $id);
        echo $pdf->createReturnsSheet($data, $returns, $id, $bakery, false);
    }

    public function actionStockUpliftShipmentDetails($tripSheetId, $tripSheetSettlementId)
    {
        $comOneResult = AsStockMovement::stockUpliftShipmentAccepted($tripSheetId);

        $comTwoResult = AsStockMovement::stockUpliftShipmentClosed($tripSheetId, $tripSheetSettlementId);

        $this->renderPartial('_stockUpliftShipmentDetails', ['resultOne' => $comOneResult, 'resultTwo' => $comTwoResult, 'tripSheetId' => $tripSheetId, 'tripSheetSettlementId' => $tripSheetSettlementId]);
    }

    public function actionShipmentDetails($tripSheetId, $tripSheetSettlementId)
    {
        $tripSheetData = TripSheets::model()->findByPk($tripSheetId);
        $bakeryId = $tripSheetData->bakery_id;
        $orderBy = Bakery::getDistributionProductsOrderBy($bakeryId, 'sql_three');
        $orderBy = str_replace('unitOfMeasure', 'psUomOrders', $orderBy);
        $orderBy = str_replace('product', 'products', $orderBy);
        $tripSheet = TripSheets::model()->with([
            'tripSheetItems.orderMain.order_status',
            'tripSheetItems.orderMain.orders.products.unitOfMeasure' => ['alias' => 'psUomOrders'],
            'tripSheetItems.orderMain.orders.products.type',
            'tripSheetItems.orderMain.orders.products.supplier',
            'tripSheetItems.orderMain.ordersReturns.productsReturns.unitOfMeasure' => ['alias' => 'psUomReturns'],
            'asStockMovement.stockMovementItems.product.unitOfMeasure'
        ])->findByPk($tripSheetId, ['order' => $orderBy]);
        //get the opening and closing signatures and photos
        $comOneResult = AsStockMovement::stockUpliftShipmentAccepted($tripSheetId);
        $comTwoResult = AsStockMovement::stockUpliftShipmentClosed($tripSheetId, $tripSheetSettlementId);
        $this->renderPartial('_shipmentDetail', ['tripSheet' => $tripSheet, 'resultOne' => $comOneResult, 'resultTwo' => $comTwoResult, 'settlementId' => $tripSheetSettlementId]);
    }

    public function actionPrintInvoice($id)
    {
        $id = trim($id);
        if(isset($id) and trim($id) > 0)
        {
            $documents = new Documents();
            #Confirm that id esist and is completed
            $order = new Order_main();
            $oDetails = $order->getConformOrderStatusById($id);
            $links = $documents->getInvoiceLinkByOrderId($id);
            if(!isset($links['invoice'])) $links['invoice'] = '';
            if(!isset($links['purchase'])) $links['purchase'] = '';
            #If invoice link do not exist then generate an invoice
            #Creating an proformainvoice
            if($links['invoice'] == '' && (isset($oDetails['order_status_id']) && $oDetails['order_status_id'] > 4))
            {
                $pdfData = MailFactory::getInvoiceDetails($id);
                $pdfFactory = new PDFFactory($pdfData, 'Proforma Invoice');
                $pdf = $pdfFactory->createInvoice($id);
                $filePath = Yii::app()->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'files';
                $invoice_link = str_replace($filePath, 'files', $pdf);
                $links['invoice'] = $documents->logDocument(2, $oDetails['employee'], 'PROFORMA INVOICE', $invoice_link, $oDetails['account'], $oDetails['bakery_id'], $id);
            }

            #If purchase order link do not exist then generate a purchase order
            #Creating an purchase order
            else if($links['purchase'] == '' && (isset($oDetails['order_status_id']) && $oDetails['order_status_id'] < 5))
            {
                $pdfData = MailFactory::getPurchaseOrderDetails($id);
                $pdfFactory = new PDFFactory($pdfData, 'Purchase Order');
                $pdf = $pdfFactory->createPurchaseOrder($id);
                $filePath = Yii::app()->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'files';
                $purchase_link = str_replace($filePath, 'files', $pdf);
                $links['purchase'] = $documents->logDocument(1, $oDetails['employee'], 'PURCHASE ORDER', $purchase_link, $oDetails['account'], $oDetails['bakery_id'], $id);
            }
            #Elseway it should contain doc path from DB
            else
            {
                //Nothing to be done til...
            }
            foreach($links as $key => $link)
            {
                if($links[$key] != '')
                {
                    $links[$key] = Yii::app()->baseUrl . DIRECTORY_SEPARATOR . 'files' . DIRECTORY_SEPARATOR . $link;
                }
            }
            #Return results
            echo json_encode($links);

        }
    }

    public function actionMoveShipmentForm($id, $orders = null)
    {
        //get the info from trip_sheets
        $tripSheet = TripSheets::model()->findByPk($id);
        //get the fleet
        $fleet = Fleet::model()->getFleet($tripSheet->bakery_id);
        //get the employees
        $employees = Employee::model()->getEmployees($tripSheet->bakery_id);
        $psOrders = [];
        if($orders === '1')
        {
            $psOrders = TripSheetsItems::model()
                ->with('orderMain.outlet', 'orderMain.employee')
                ->findAll([
                'condition' => 't.trip_sheet_id = ' . $id
            ]);
        }

        $bakery = Bakery::model()->findByPk($tripSheet->bakery_id);

        $this->renderPartial('_moveShipmentForm',
            array(
                'tripSheet' => $tripSheet,
                'fleet' => $fleet,
                'employees' => $employees,
                'tripSheetId' => $id,
                'psOrders' => $psOrders,
                'bakery' => $bakery
            )
        );
    }

    public function actionMoveShipment($id)
    {
        $driver = $_POST['driver_employee_id'];
        if($_POST['driver_employee_id'] === '')
        {
            $driver = 0;
        }
        $fleet = $_POST['fleet_id'];
        if($_POST['fleet_id'] === '')
        {
            $fleet = 0;
        }

        $valid = TripSheets::checkDriverAndFleet($driver, $fleet);
        if(!$valid['valid'])
        {
            $result = [
                'code' => 2,
                'msg' => $valid['msg']
            ];
            echo json_encode($result);
            die();
        }

        if($driver !== 0)
        {
            $bakeryId = TripSheets::model()->findByPk($id)->bakery_id;
            $bakery = Bakery::model()->findByPk($bakeryId);
            if($bakery->multiple_shipment_create == 0)
            {
                $tsCount = TripSheets::checkDriverLoadedShipment($driver, $_POST['expected_delivery_date']);
                if($tsCount > 0)
                {
                    $result = [
                        'code' => 2,
                        'msg' => 'Whoops! This user already has a planned load for the shipment date selected'
                    ];
                    echo json_encode($result);
                    die();
                }
            }
        }

        $result = 0;
        try
        {
            $notes = (isset($_POST['notes']) && $_POST['notes'] !== '') ? $_POST['notes'] : null;
            $comments = (isset($_POST['comments']) && $_POST['comments'] !== '') ? $_POST['comments'] : null;
            $tripSheet = TripSheets::model()->findByPk($id);
            $tripSheet->fleet_id = $fleet;
            $tripSheet->driver_employee_id = $driver;
            $tripSheet->expected_delivery_date = $_POST['expected_delivery_date'];
            $tripSheet->partially_accept = $_POST['partialLoad'];
            $tripSheet->notes = $notes;
            $tripSheet->comments = $comments;
            $tripSheet->updated_by = Yii::app()->user->id;
            $tripSheet->updated_date = date('Y-m-d H:i:s');

            if($tripSheet->collection_trip_sheet == 1)
            {
                if($driver > 0)
                {
                    $tripSheet->collection_trip_sheet = 0;

                    $orderMain = Order_main::model()->with('tripSheetsItems')->find('tripSheetsItems.trip_sheet_id = ' . $id);
                    $orderMain->collection_order = 0;
                    $orderMain->order_type = 1;
                    $orderMain->save();
                }
            }

            if($tripSheet->update())
            {
                $result = 1;
            }
        }
        catch(CDbException $e)
        {
        }

        try{
            if(isset($_POST['psOrders']))
            {
                foreach($_POST['psOrders'] as $orderId => $val)
                {
                    //remove stock from trip_sheets_stock
                    $items = Orders::model()->findAll('order_id = ' . $orderId . ' AND qty_picked > 0');
                    foreach($items as $item)
                    {
                        $stockItem = TripSheetsStock::model()->find('trip_sheet_id = ' . $id . ' AND product_id = ' . $item['product_id'] . ' AND product_indicator = 1');
                        if($stockItem !== null)
                        {
                            $stockItem->opening_qty -= $item['qty_picked'];
                            $stockItem->save();
                        }
//                        $sql = <<<SQL
//                        CALL sp_subtract_tripsheet_stock({$item['product_id']}, {$item['qty_picked']}, {$id}, 1);
//SQL;
//                        Yii::app()->db->createCommand($sql)->execute();

                    }

                    //update order status to 1 and linked_to_van_sales = 0
                    Order_main::model()->updateByPk($orderId,['order_status_id' => 1, 'linked_to_van_sale' => 0]);
                    //remove from trip_sheets_items table
                    TripSheetsItems::model()->deleteAllByAttributes(['trip_sheet_id' => $id, 'order_id' => $orderId]);
                }
                // $result = [
                //     'code' => 3,
                //     'msg' => 'Nice! Order(s) successfully removed from this shipment.'
                // ];
                // echo json_encode($result);
                // die();
            }
        }catch(CDbException $e){

        }

        echo $result;
    }

    public function actionDeleteTradeReplacementCreditNote()
    {
        $this->renderPartial('_deleteTradeReplacementCreditNote', ['orderId' => $_POST['orderId']]);
    }

    public function actionDeleteTradeReplacementCreditNoteProcess()
    {
        $orderId = $_POST['orderId'];
        $tradeReturn = TradeReturnsMain::model()->find('order_id = :orderId', [':orderId' => $orderId]);
        if(isset($tradeReturn))
        {
            $tradeReturn->approved_by = null;
            $tradeReturn->approved_date = null;
            $tradeReturn->status = 0;
            $tradeReturn->order_id = null;
            $tradeReturn->order_created_by = null;
            $tradeReturn->order_created_date = null;

            $tradeReturn->save();
        }

        OrdersReturns::model()->deleteAll('order_id = :orderId', [':orderId' => $orderId]);
        Order_main::model()->deleteByPk($orderId);

        echo 1;
    }

    public function actionPrepWizardCreate()
    {
        $_SESSION['CS']['VS']['WIZARD']['ACTION'] = 'create';
        if(isset($_SESSION['CS']['VS']['WIZARD']['TRIP_SHEET_ID'])) unset($_SESSION['CS']['VS']['WIZARD']['TRIP_SHEET_ID']);
        echo 1;
    }

    public function actionWizardCreateShipment($step = 1, $returnPost = 0)
    {
        $blockedMessage = '';
        $mergeCreditNotesBtn = false;
        // check here if a credit note has at least an order associated with it for the same customer
        if(isset($_POST['psOrders']) && $step == 1)
        {
            $customers = [];
            foreach($_POST['psOrders'] as $orderId)
            {
                $om = Order_main::model()->with('outlet')->findByPk($orderId);
                if(!isset($customers[$om->account]))
                {
                    $customers[$om->account] = [
                        'credit_note' => 0,
                        'credit_note_total' => 0,
                        'pre_sale' => 0,
                        'pre_sale_total' => 0,
                        'customer_name' => $om->outlet['description']
                    ];
                }
                
                if($om->order_status_id == 18)
                {
                    ++$customers[$om->account]['credit_note'];
                    $customers[$om->account]['credit_note_total'] += $om->order_total_amount;
                }
                if($om->order_status_id == 1)
                {
                    ++$customers[$om->account]['pre_sale']; 
                    $customers[$om->account]['pre_sale_total'] += $om->order_total_amount;
                }
            }
            $valid = true;
            foreach($customers as $cId => $info)
            {
                // we block the user if there isn't at least one credit note against an invoice
                if($info['credit_note'] > 0 && $info['pre_sale'] == 0)
                {
                    $valid = false;
                    $blockedMessage .= 'Customer ' . $info['customer_name'] . ' does not have an associated pre-sale order planned against a credit note<br>';
                }
            }
            if($valid)// if still valid we check whether there are more than one credit note per invoice
            {
                foreach($customers as $cId => $info)
                {
                    // we block the user if there is more than one credit note per invoice
                    if($info['credit_note'] > 0 && $info['credit_note'] != 1)
                    {
                        $creditNoteCount = $info['credit_note'];
                        $valid = false;
                        $blockedMessage .= $creditNoteCount . ' credit notes found for Customer ' . $info['customer_name'] . '. Please merge the credit notes if you want to continue. Only 1 credit note allowed per customer<br>';
                        $mergeCreditNotesBtn = true;
                    }
                }
            }
            if($valid)// if still valid we check whether the credit note amounts exceed the pre sale amount
            {
                foreach($customers as $cId => $info)
                {
                    if($info['credit_note_total'] > $info['pre_sale_total'])
                    {
                        $creditNoteFormatted = number_format($info['credit_note_total'], 2, '.', ' ');
                        $presaleFormatted = number_format($info['pre_sale_total'], 2, '.', ' ');
                        $valid = false;
                        $blockedMessage .= 'The credit note amount ('.$creditNoteFormatted.') exceeds the pre-sale amount ('.$presaleFormatted.') for Customer ' . $info['customer_name'] . '<br>';
                    }
                }
            }
        }

        set_time_limit(0);
        $bakeryId = Yii::app()->user->bakery;
        $finalStep = false;
        $con = Yii::app()->db;
        switch($step)
        {
            case 2:
                if(isset($_POST['driver']))
                {
                    if($_POST['driver'] === '')
                    {
                        $_POST['driver'] = 0;
                    }
                    $_SESSION['CS']['VS']['STEP_1']['driver'] = $_POST['driver'];
                }
                if(isset($_POST['truck']))
                {
                    if($_POST['truck'] === '')
                    {
                        $_POST['truck'] = 0;
                    }
                    if(isset($_SESSION['CS']['VS']['STEP_1']) && isset($_SESSION['CS']['VS']['STEP_2']) && $_POST['truck'] != $_SESSION['CS']['VS']['STEP_1']['truck'])
                    {
                        unset($_SESSION['CS']['VS']['STEP_2']);
                    }
                    $_SESSION['CS']['VS']['STEP_1']['truck'] = $_POST['truck'];
                }

                if(isset($_POST['shipmentDate']))
                {
                    $_SESSION['CS']['VS']['STEP_1']['shipmentDate'] = $_POST['shipmentDate'];
                }

                $valid = TripSheets::checkDriverAndFleet($_SESSION['CS']['VS']['STEP_1']['driver'], $_SESSION['CS']['VS']['STEP_1']['truck']);
                if(!$valid['valid'])
                {
                    echo 2;
                    return;
                }

                if(isset($_POST['notes']))
                {
                    $_SESSION['CS']['VS']['STEP_1']['notes'] = $_POST['notes'];
                }
                if(isset($_POST['comments']))
                {
                    $_SESSION['CS']['VS']['STEP_1']['comments'] = $_POST['comments'];
                }

                //only check for planned shipments when driver and truck is selected
                if($_SESSION['CS']['VS']['STEP_1']['driver'] != 0)
                {
                    $bakery = Bakery::model()->findByPk($bakeryId);
                    if($bakery->multiple_shipment_create == 0)
                    {
                        $tsCount = TripSheets::checkDriverLoadedShipment($_SESSION['CS']['VS']['STEP_1']['driver'], $_SESSION['CS']['VS']['STEP_1']['shipmentDate']);
                        //check if the posted driver already has a shipment planned for him/her
                        if(isset($_SESSION['CS']['VS']['WIZARD']['ACTION']) && $_SESSION['CS']['VS']['WIZARD']['ACTION'] == 'update')
                        {
                            $trip = TripSheets::model()->find('id = ' . $_SESSION['CS']['VS']['WIZARD']['TRIP_SHEET_ID'] . ' AND merge_trip_sheet_id IS NULL');
                            if($tsCount > 0 && $trip->expected_delivery_date != $_SESSION['CS']['VS']['STEP_1']['shipmentDate'])
                            {
                                echo 0;
                                return;
                            }
                        }
                        else
                        {
                            if($tsCount > 0)
                            {
                                echo 0;
                                return;
                            }
                        }
                    }
                }

                if(isset($_POST['productId']))//adds a product
                {
                    if(!isset($_SESSION['CS']['VS']['STEP_2']['ADDED_STOCK']))
                    {
                        $_SESSION['CS']['VS']['STEP_2']['ADDED_STOCK'] = [];
                    }
                    $prodId = $_POST['productId'];
                    $sql = <<<SQL
                SELECT
                    p.id,
                    pp.parent_product_id,
                    fn_parentUnits(p.id) as parent_product_units,
                    uom.description AS unit_of_measure,
                    p.units_of_measure_id,
                    p.sku,
                    p.batch_code,
                    p.expiry_date,
                    p.description AS product,
                    0 AS opening_qty
                FROM
                    products p
                        LEFT JOIN
                    products_parent pp ON pp.product_id = p.id
                        LEFT JOIN
                    units_of_measure uom ON uom.id = p.units_of_measure_id
                WHERE
                    p.id = {$prodId}
                        AND uom.description IS NOT NULL
                        AND p.status_id = 1 AND p.exclude_van_sale = 0
                UNION SELECT
                    p.id,
                    pp.parent_product_id,
                    fn_parentUnits(p.id) as parent_product_units,
                    uom.description AS unit_of_measure,
                    p.units_of_measure_id,
                    p.sku,
                    p.batch_code,
                    p.expiry_date,
                    p.description AS product,
                    0 AS opening_qty
                FROM
                    products_parent pp
                        LEFT JOIN
                    products p ON p.id = pp.product_id
                        LEFT JOIN
                    units_of_measure uom ON uom.id = p.units_of_measure_id
                WHERE
                    pp.parent_product_id = {$prodId}
                    AND pp.status_id = 1
                    AND p.status_id = 1
                        AND uom.description IS NOT NULL AND p.exclude_van_sale = 0
                UNION SELECT
                    p2.id AS id,
                    NULL AS parent_product_id,
                    NULL AS parent_product_units,
                    uom.description AS unit_of_measure,
                    p2.units_of_measure_id,
                    p.sku,
                    p.batch_code,
                    p.expiry_date,
                    p2.description AS product,
                    0 AS opening_qty
                FROM
                    products p
                        LEFT JOIN
                    products_parent pp ON pp.product_id = p.id
                        LEFT JOIN
                    products p2 ON p2.id = pp.parent_product_id
                        LEFT JOIN
                    units_of_measure uom ON uom.id = p2.units_of_measure_id
                WHERE
                    p.id = {$prodId}
                    AND p.status_id = 1
                    AND p2.status_id = 1
                    AND pp.status_id = 1
                        AND uom.description IS NOT NULL  AND p.exclude_van_sale = 0;
SQL;
                    $results = $con->createCommand($sql)->queryAll();
                    foreach($results as $res)
                    {
                        $addedStock = [
                            'id' => $res['id'],
                            'parent_product_id' => $res['parent_product_id'],
                            'parent_product_units' => $res['parent_product_units'],
                            'unit_of_measure' => $res['unit_of_measure'],
                            'units_of_measure_id' => $res['units_of_measure_id'],
                            'sku' => $res['sku'],
                            'batch_code' => $res['batch_code'],
                            'expiry_date' => $res['expiry_date'],
                            'product' => $res['product'],
                            'opening_qty' => $res['opening_qty']
                        ];
                        array_push($_SESSION['CS']['VS']['STEP_2']['ADDED_STOCK'], $addedStock);
                        $_SESSION['CS']['VS']['STEP_2']['QTY'][$res['id']] = 0;
                        if(isset($_SESSION['CS']['VS']['STEP_2']['REMOVED']))
                        {
                            for($i = 0; $i < count($_SESSION['CS']['VS']['STEP_2']['REMOVED']); $i++)
                            {
                                if($_SESSION['CS']['VS']['STEP_2']['REMOVED'][$i] == $res['id'])
                                {
                                    unset($_SESSION['CS']['VS']['STEP_2']['REMOVED'][$i]);
                                    $reIndexed = array_values($_SESSION['CS']['VS']['STEP_2']['REMOVED']);
                                    $_SESSION['CS']['VS']['STEP_2']['REMOVED'] = $reIndexed;
                                }
                            }
                        }
                    }
                }
                if(isset($_POST['productIds']))//removes an item from the array
                {
//                    var_dump('here');
                    $ids = explode('|', $_POST['productIds']);
                    if(!isset($_SESSION['CS']['VS']['STEP_2']['REMOVED']))
                    {
                        $_SESSION['CS']['VS']['STEP_2']['REMOVED'] = [];
                    }
                    foreach($ids as $id)
                    {
                        for($i = 0; $i < count($_SESSION['CS']['VS']['STEP_2']['OPENING_STOCK']); $i++)
                        {
                            if($_SESSION['CS']['VS']['STEP_2']['OPENING_STOCK'][$i]['id'] == $id)
                            {
                                unset($_SESSION['CS']['VS']['STEP_2']['OPENING_STOCK'][$i]);
                                $reIndexed = array_values($_SESSION['CS']['VS']['STEP_2']['OPENING_STOCK']);
                                $_SESSION['CS']['VS']['STEP_2']['OPENING_STOCK'] = $reIndexed;
                            }
                        }
                        if(!in_array($id, $_SESSION['CS']['VS']['STEP_2']['REMOVED']))
                        {
                            $_SESSION['CS']['VS']['STEP_2']['REMOVED'][] = $id;
                        }
                    }
                    //remove the added added stock as well
                    foreach($_SESSION['CS']['VS']['STEP_2']['REMOVED'] as $id)
                    {
                        if(isset($_SESSION['CS']['VS']['STEP_2']['ADDED_STOCK']))
                        {
                            for($i = 0; $i < count($_SESSION['CS']['VS']['STEP_2']['ADDED_STOCK']); $i++)
                            {
                                if($_SESSION['CS']['VS']['STEP_2']['ADDED_STOCK'][$i]['id'] == $id)
                                {
                                    unset($_SESSION['CS']['VS']['STEP_2']['ADDED_STOCK'][$i]);
                                    $reIndexed = array_values($_SESSION['CS']['VS']['STEP_2']['ADDED_STOCK']);
                                    $_SESSION['CS']['VS']['STEP_2']['ADDED_STOCK'] = $reIndexed;
                                }
                            }
                        }
                    }
//                    var_dump($_SESSION['CS']['VS']['STEP_2']['REMOVED']);
//                    var_dump($_SESSION['CS']['VS']['STEP_2']['QTY']);
                    foreach($_SESSION['CS']['VS']['STEP_2']['REMOVED'] as $pId)
                    {
                        if(isset($_SESSION['CS']['VS']['STEP_2']['QTY'][$pId]))
                        {
                            unset($_SESSION['CS']['VS']['STEP_2']['QTY'][$pId]);
                        }
                        if(isset($_SESSION['CS']['VS']['STEP_2']['WITH_LINKED'][$pId]))
                        {
                            unset($_SESSION['CS']['VS']['STEP_2']['WITH_LINKED'][$pId]);
                        }
                    }

//                    var_dump($_SESSION['CS']['VS']['STEP_2']['QTY']);
                }
                //get the suggested open load
                $notIds = '';
                if(isset($_SESSION['CS']['VS']['STEP_2']['REMOVED']) && count($_SESSION['CS']['VS']['STEP_2']['REMOVED']) > 0)
                {
                    $notIds = ' AND p.id NOT IN (' . implode(', ', $_SESSION['CS']['VS']['STEP_2']['REMOVED']) . ')';
                }
                if(isset($_SESSION['CS']['VS']['WIZARD']['ACTION']) && $_SESSION['CS']['VS']['WIZARD']['ACTION'] == 'update')
                {
                    $sql = <<<SQL
                    SELECT
                        ts.id AS trip_sheet_id,
                        p.id,
                        pp.parent_product_id,
                        fn_parentUnits(p.id) as parent_product_units,
                        uom.description AS unit_of_measure,
                        p.units_of_measure_id,
                        p.description AS product,
                        tss.opening_qty,
                        p.sku,
                        p.batch_code,
                        p.expiry_date
                    FROM
                        trip_sheets ts
                            INNER JOIN
                        trip_sheets_stock tss ON tss.trip_sheet_id = ts.id
                            INNER JOIN
                        products p ON p.id = tss.product_id
                            INNER JOIN
                        units_of_measure uom ON uom.id = p.units_of_measure_id
                            LEFT JOIN
                        products_parent pp ON pp.product_id = p.id
                    WHERE
                        ts.id = {$_SESSION['CS']['VS']['WIZARD']['TRIP_SHEET_ID']} AND tss.product_indicator = 1
                        AND p.status_id = 1
                        AND tss.opening_qty > 0
                        AND p.exclude_van_sale = 0
                            AND p.id NOT IN (SELECT
                                pl.linked_product_id
                            FROM
                                products_linked pl
                                    LEFT JOIN
                                trip_sheets_stock tss ON tss.product_id = pl.linked_product_id
                            WHERE
                                tss.trip_sheet_id = ts.id
                            GROUP BY pl.linked_product_id)
                            {$notIds}
                    ORDER BY p.description;
SQL;

                    $openingStock = $con->createCommand($sql)->queryAll();
                }
                else
                {
                    //check the previously created shipment for the fleet selected van stock
                    $lastTripSheetIdSql = <<<SQL
                    select
                        t.id
                    from
                        trip_sheets t
                    inner join
                    (
                        select
                            ts.fleet_id,
                            ts.driver_employee_id,
                            max(ts.created_date) as last_trip_sheet_created_date
                        from
                            trip_sheets ts
                        group by
                            ts.fleet_id, ts.driver_employee_id
                    ) v on
                        v.fleet_id = t.fleet_id
                        and v.last_trip_sheet_created_date = t.created_date
                    where
                        t.fleet_id = {$_SESSION['CS']['VS']['STEP_1']['truck']}
                        AND t.driver_employee_id = {$_SESSION['CS']['VS']['STEP_1']['driver']};
SQL;
                    $lastTripSheetId = $con->createCommand($lastTripSheetIdSql)->queryScalar();
                    $openingStock = [];
                    if(!$lastTripSheetId)//means no previous van stock was found for the fleet selected
                    {

                    }
                    else
                    {
                        $vanStock = TripSheetsStock::checkVanSaleStock($lastTripSheetId, '', false, $notIds);
    
                        if(!empty($vanStock))
                        {
                            foreach($vanStock as $vs)
                            {
                                $openingQty = $vs['qty']-$vs['ps_qty_ordered'];
                                $openingStock[] = [
                                    'trip_sheet_id' => $vs['trip_sheet_id'],
                                    'id' => $vs['product_id'],
                                    'parent_product_id' => $vs['parent_product_id'],//the products parent id
                                    'parent_product_units' => $vs['parent_product_id'],
                                    'unit_of_measure' => $vs['unit_of_measure'],
                                    'units_of_measure_id' => $vs['units_of_measure_id'],
                                    'product' => $vs['description'],//the description of the product
                                    'opening_qty' => $openingQty,
                                    'sku' => $vs['sku'],
                                    'batch_code' => $vs['batch_code'],
                                    'expiry_date' => $vs['expiry_date']
                                ];
                            }
                        }
                    }
//                    AND ts.closing_location_id = {$_SESSION['CS']['VS']['STEP_1']['stockLocation']}
//                     $sql = <<<SQL
//                     SELECT
//                         ts.id AS trip_sheet_id,
//                         p.id,
//                         pp.parent_product_id,
//                         fn_parentUnits(p.id) as parent_product_units,
//                         uom.description AS unit_of_measure,
//                         p.units_of_measure_id,
//                         p.description AS product,
//                         tss.opening_qty,
//                         p.sku,
//                         p.batch_code,
//                         p.expiry_date
//                     FROM
//                         trip_sheets ts
//                             INNER JOIN
//                         (SELECT
//                             fleet_id,
//                                 bakery_id,
//                                 closing_location_id,
//                                 MAX(backend_closed_load_datetime) AS prev_date
//                         FROM
//                             trip_sheets
//                         WHERE
//                             trip_completed = 1 AND level_status = 1
//                                 AND backend_closed_load_datetime IS NOT NULL
//                         GROUP BY fleet_id , bakery_id , closing_location_id
//                         ORDER BY MAX(backend_closed_load_datetime) DESC LIMIT 1) tmp ON tmp.fleet_id = ts.fleet_id
//                             AND tmp.bakery_id = ts.bakery_id
//                             -- AND tmp.closing_location_id = ts.closing_location_id
//                             AND tmp.prev_date = ts.backend_closed_load_datetime
//                             INNER JOIN
//                         trip_sheets_stock tss ON tss.trip_sheet_id = ts.id
//                             INNER JOIN
//                         products p ON p.id = tss.product_id
//                             INNER JOIN
//                         units_of_measure uom ON uom.id = p.units_of_measure_id
//                             LEFT JOIN
//                         products_parent pp ON pp.product_id = p.id
//                     WHERE
//                         ts.trip_completed = 1
//                             AND ts.fleet_id = {$_SESSION['CS']['VS']['STEP_1']['truck']}
//                             AND tss.product_indicator = 1
//                             AND p.status_id = 1
//                             AND p.exclude_van_sale = 0
//                             AND tss.opening_qty > 0
//                             AND p.id NOT IN (SELECT
//                                 pl.linked_product_id
//                             FROM
//                                 products_linked pl
//                                     LEFT JOIN
//                                 trip_sheets_stock tss ON tss.product_id = pl.linked_product_id
//                             WHERE
//                                 tss.trip_sheet_id = ts.id
//                             GROUP BY pl.linked_product_id)
//                             {$notIds}
//                     ORDER BY p.description;
// SQL;
                }
                // $openingStock = $con->createCommand($sql)->queryAll();
//                echo $sql;
//                die();
                //get the top up stock and subtract from the opening stock
                if(!empty($openingStock))
                {
                    $tsId = isset($openingStock[0]['trip_sheet_id']) ? $openingStock[0]['trip_sheet_id'] : 0;
                    if(isset($_SESSION['CS']['VS']['WIZARD']['TRIP_SHEET_ID']))
                    {
                        $tsId = $_SESSION['CS']['VS']['WIZARD']['TRIP_SHEET_ID'];
                    }
                    //subtract pre sales quantities from van sale suggested opening stock
                    $preSaleQtyPickedSql = <<<SQL
                SELECT
                    o.product_id, SUM(qty_picked) AS qty
                FROM
                    trip_sheets_items tsi
                        INNER JOIN
                    order_main om ON om.id = tsi.order_id
                        INNER JOIN
                    orders o ON o.order_id = om.id
                WHERE
                    tsi.trip_sheet_id = {$tsId}
                GROUP BY o.product_id;
SQL;
                    $preSaleQtyPicked = $con->createCommand($preSaleQtyPickedSql)->queryAll();
                    $misc = new Miscellaneous();
                    $psqp = [];
                    foreach($preSaleQtyPicked as $picked)
                    {
                        $psqp[$picked['product_id']] = $picked['qty'];
                    }
                    $r = [];//$misc->rollUpStock($psqp);
                    $c = 0;
                    foreach($openingStock as $stock)
                    {
                        if(isset($r[$stock['id']]))
                        {
                            $openingStock[$c]['opening_qty'] -= $r[$stock['id']];
                        }
                        ++$c;
                    }
                    $openingStockFinal = [];
                    foreach($openingStock as $stock)
                    {
                        if($stock['opening_qty'] > 0)
                        {
                            array_push($openingStockFinal, $stock);
                        }
                    }
                    $openingStock = $openingStockFinal;
//                    $tsId = isset($openingStock[0]['trip_sheet_id']) ? $openingStock[0]['trip_sheet_id'] : 0;
                    $topUpStockSql = <<<SQL
                    SELECT
                        product_id, SUM(qty) AS qty
                    FROM
                        trip_sheets_stock_topup
                    WHERE
                        trip_sheet_id = {$tsId}
                            AND product_indicator = 1
                    GROUP BY product_id;
SQL;
                    $topUpStock = $con->createCommand($topUpStockSql)->queryAll();
                    if(!empty($topUpStock))
                    {
                        $topUpStockRollUp = [];
                        foreach($topUpStock as $stock)
                        {
                            $topUpStockRollUp[$stock['product_id']] = $stock['qty'];
                        }
                        $misc = new Miscellaneous();
                        $topUpStockRollUpFinal = [];//$misc->rollUpStock($topUpStockRollUp);
                        $i = 0;
                        foreach($openingStock as $opening)
                        {
                            if(isset($topUpStockRollUpFinal[$opening['id']]))
                            {
                                $openingStock[$i]['opening_qty'] = $openingStock[$i]['opening_qty'] - $topUpStockRollUpFinal[$opening['id']];
                            }
                            ++$i;
                        }
                    }
                }
                $_SESSION['CS']['VS']['STEP_2']['OPENING_STOCK'] = $openingStock;
                if(isset($_SESSION['CS']['VS']['STEP_2']['ADDED_STOCK']))
                {
                    foreach($_SESSION['CS']['VS']['STEP_2']['ADDED_STOCK'] as $addedStock)
                    {
                        array_unshift($_SESSION['CS']['VS']['STEP_2']['OPENING_STOCK'], $addedStock);
                    }
                    $_SESSION['CS']['VS']['STEP_2']['OPENING_STOCK'] = array_merge($_SESSION['CS']['VS']['STEP_2']['OPENING_STOCK'], $_SESSION['CS']['VS']['STEP_2']['ADDED_STOCK']);
                }
//                var_dump($_SESSION['CS']['VS']['STEP_2']['OPENING_STOCK']);
                if(empty($_SESSION['CS']['VS']['STEP_2']['OPENING_STOCK']))
                {
                    $_SESSION['CS']['VS']['STEP_2']['FINAL'] = [];
                }
                else
                {
                    $products = [];
                    foreach($_SESSION['CS']['VS']['STEP_2']['OPENING_STOCK'] as $stock)
                    {
//                        if(!isset($_SESSION['CS']['VS']['STEP_2']['QTY']))
//                        {
                        if(!in_array($stock['id'] . ' ' . $stock['product'], $products))
                        {
                            $products[] = $stock['id'] . ' ' . $stock['product'];
                        }
//                        }
//                        else
//                        {
//                            if(isset($_SESSION['CS']['VS']['STEP_2']['QTY'][$stock['id']]) && !in_array($stock['product'], $products))
//                            {
//                                $products[] = $stock['product'];
//                            }
//                        }
                    }
                    $final = [];
                    foreach($products as $product)
                    {
                        $final[] = [
                            'product' => $product,
                            'CS' => [],
                            'EA' => []
                        ];
                    }
                    $i = 0;
                    foreach($final as $empty)
                    {
                        foreach($_SESSION['CS']['VS']['STEP_2']['OPENING_STOCK'] as $stock)
                        {
                            if($stock['units_of_measure_id'] == '1' && $stock['id'] . ' ' . $stock['product'] == $final[$i]['product'])
                            {
                                $final[$i]['CS'] = [
                                    'unit_of_measure' => $stock['unit_of_measure'],
                                    'product_id' => $stock['id'],
                                    'qty' => $stock['opening_qty'],
                                    'sku' => $stock['sku'],
                                    'batch_code' => $stock['batch_code'],
                                    'expiry_date' => $stock['expiry_date'],
                                    'parent_product_units' => $stock['parent_product_units']
                                ];
                            }
                            if($stock['units_of_measure_id'] > '1' && $stock['id'] . ' ' . $stock['product'] == $final[$i]['product'])
                            {
                                $final[$i]['EA'] = [
                                    'unit_of_measure' => $stock['unit_of_measure'],
                                    'product_id' => $stock['id'],
                                    'qty' => $stock['opening_qty'],
                                    'sku' => $stock['sku'],
                                    'batch_code' => $stock['batch_code'],
                                    'expiry_date' => $stock['expiry_date'],
                                    'parent_product_units' => $stock['parent_product_units']
                                ];
                            }
                        }
                        ++$i;
                    }
                    if(isset($_SESSION['CS']['VS']['STEP_2']['QTY']))//update the values with the values in the session
                    {
                        for($i = 0; $i < count($final); $i++)
                        {
                            $case = $final[$i]['CS'];
                            if(!empty($case))
                            {
                                foreach($_SESSION['CS']['VS']['STEP_2']['QTY'] as $pId => $qty)
                                {
                                    if($case['product_id'] == $pId)
                                    {
                                        $final[$i]['CS']['qty'] = $qty;
                                    }
                                }
                            }
                            $each = $final[$i]['EA'];
                            if(!empty($each))
                            {
                                foreach($_SESSION['CS']['VS']['STEP_2']['QTY'] as $pId => $qty)
                                {
                                    if($each['product_id'] == $pId)
                                    {
                                        $final[$i]['EA']['qty'] = $qty;
                                    }
                                }
                            }
                        }
                    }
                    $_SESSION['CS']['VS']['STEP_2']['FINAL'] = $final;
                }
                if(isset($_SESSION['CS']['VS']['STEP_2']['EXCLUDE'])) unset($_SESSION['CS']['VS']['STEP_2']['EXCLUDE']);
                for($i = 0; $i < count($_SESSION['CS']['VS']['STEP_2']['FINAL']); $i++)
                {
                    $product = $_SESSION['CS']['VS']['STEP_2']['FINAL'][$i];
                    foreach($product as $p)
                    {
                        if(is_array($p) && !empty($p))
                        {
                            $_SESSION['CS']['VS']['STEP_2']['EXCLUDE'][$p['product_id']] = $p['qty'];
                        }
                    }
                }

                $preSaleOrdersOnly = false;
                $preSaleOrdersOnlyIndicator = Bakery::model()->find([
                    'select' => 'create_shipment_pre_sale_orders',
                    'condition' => 'id = ' . $bakeryId
                ])->create_shipment_pre_sale_orders;
                if($preSaleOrdersOnlyIndicator === '1')
                {
                    $preSaleOrdersOnly = true;
                    if(isset($_SESSION['CS']['VS']['STEP_2']['FINAL']))
                    {
                        $_SESSION['CS']['VS']['STEP_2']['FINAL'] = [];
                    }
                }

//                var_dump($_SESSION['CS']['VS']['STEP_2']['FINAL']);
                $content = $this->renderPartial('wizard/_step2', ['preSaleOrdersOnly' => $preSaleOrdersOnly, 'openingStock' => $_SESSION['CS']['VS']['STEP_2']['FINAL']], true);
                $activeStep = 2;
                break;
            case 3:
                if(isset($_POST['openingStock']))
                {
//                    var_dump($_POST['openingStock']);
                    if(isset($_SESSION['CS']['VS']['STEP_2']['QTY'])) unset($_SESSION['CS']['VS']['STEP_2']['QTY']);
                    if(isset($_SESSION['CS']['VS']['STEP_2']['WITH_LINKED'])) unset($_SESSION['CS']['VS']['STEP_2']['WITH_LINKED']);
                    $stock = $_POST['openingStock'];
                    $linked = [];
                    foreach($stock as $pId => $qty)
                    {
                        $_SESSION['CS']['VS']['STEP_2']['QTY'][$pId] = $qty;
//                        if(isset($_SESSION['CS']['VS']['STEP_2']['QTY_PUNCHED'][$pId]))
//                        {
//                            unset($_SESSION['CS']['VS']['STEP_2']['QTY_PUNCHED'][$pId]);
//                        }

                        $linked[$pId] = $qty;
                    }
                    $misc = new Miscellaneous();
                    $linkedArr = $misc->addLinkedProducts($linked);
                    $_SESSION['CS']['VS']['STEP_2']['WITH_LINKED'] = $linkedArr;
                }
                if($returnPost == 1)
                {
                    if(isset($_SESSION['CS']['VS']['STEP_2']['QTY']))
                    {
                        //this set the value that was punched in
                        $_SESSION['CS']['VS']['STEP_2']['QTY_PUNCHED'] = $_SESSION['CS']['VS']['STEP_2']['QTY'];
                        unset($_SESSION['CS']['VS']['STEP_2']['QTY']);
                    }
                    if(isset($_SESSION['CS']['VS']['STEP_2']['WITH_LINKED'])) unset($_SESSION['CS']['VS']['STEP_2']['WITH_LINKED']);
                    echo $returnPost;
                    return;
                }
                //selected pre sale new orders
                $selectedPSOrdersWhere = 'AND 1 = 0';
                if(isset($_SESSION['CS']['VS']['STEP_1']['ORDERS']))
                {
                    $orderIdKeys = array_keys($_SESSION['CS']['VS']['STEP_1']['ORDERS']);
                    $selectedPSOrdersWhere = ' AND om.id IN (' . implode(',', $orderIdKeys) . ')';
                }
                //get the pre sales orders
                $sql = <<<SQL
                SELECT
                    om.id,
                    o.description AS outlet,
                    o.address,
                    om.order_date,
                    om.order_total_amount,
                    om.expected_delivery_date,
                    e.name
                FROM
                    order_main om
                        INNER JOIN
                    employee e ON e.id = om.order_taker_employee_nr
                        INNER JOIN
                    outlet o on o.id = om.account
                WHERE
                    om.bakery_id = {$bakeryId}
                        AND om.order_status_id IN (1,18)
                        AND om.linked_to_van_sale = 0
                        AND om.combined_load_pre_sale_import = 0
                        {$selectedPSOrdersWhere}
                ORDER BY order_date;
SQL;
                $psOrders = $con->createCommand($sql)->queryAll();
                if(isset($_SESSION['CS']['VS']['WIZARD']['ACTION']) && $_SESSION['CS']['VS']['WIZARD']['ACTION'] == 'update')
                {
                    $sql = <<<SQL
                    SELECT
                        om.id,
                        o.description AS outlet,
                        o.address,
                        om.order_date,
                        om.order_total_amount,
                        om.expected_delivery_date,
                        e.name
                    FROM
                        trip_sheets_items tsi
                            INNER JOIN
                        order_main om ON om.id = tsi.order_id
                            INNER JOIN
                        employee e ON e.id = om.order_taker_employee_nr
                            INNER JOIN
                        outlet o ON o.id = om.account
                    WHERE
                        tsi.trip_sheet_id = {$_SESSION['CS']['VS']['WIZARD']['TRIP_SHEET_ID']}
                        AND om.combined_load_pre_sale_import = 0
SQL;
                    $tripSheetPreSalesOrders = $con->createCommand($sql)->queryAll();
                    foreach($tripSheetPreSalesOrders as $tripOrder)
                    {
                        array_unshift($psOrders, $tripOrder);
                        $_SESSION['CS']['VS']['STEP_3']['ORDERS'][$tripOrder['id']] = 1;
                        $products = Orders::model()->findAll('order_id = ' . $tripOrder['id']);
                        foreach($products as $product)
                        {
                            if(!isset($_SESSION['CS']['VS']['STEP_3']['QTY_PICKED'][$tripOrder['id']][$product['product_id']]))
                            {
                                $_SESSION['CS']['VS']['STEP_3']['QTY_PICKED'][$tripOrder['id']][$product['product_id']] = $product['qty_picked'];
                            }
                        }
                    }
                }
//                var_dump($_SESSION['CS']['VS']['STEP_2']['FINAL']);
                $content = $this->renderPartial('wizard/_step3', ['psOrders' => $psOrders], true);
                $activeStep = 3;
                break;
            case 4:
                if(isset($_POST['psOrders']))//if a post was done from step 3
                {
                    if(isset($_SESSION['CS']['VS']['STEP_3']['ORDERS'])) unset($_SESSION['CS']['VS']['STEP_3']['ORDERS']);
                    foreach($_POST['psOrders'] as $oId => $val)
                    {
                        $_SESSION['CS']['VS']['STEP_3']['ORDERS'][$oId] = 1;
                    }
                }
                if(isset($_POST['qty_picked']))
                {
                    if(isset($_SESSION['CS']['VS']['STEP_3']['QTY_PICKED'])) unset($_SESSION['CS']['VS']['STEP_3']['QTY_PICKED']);
                    foreach($_POST['qty_picked'] as $orderId => $products)
                    {
                        foreach($products as $pId => $qty)
                        {
                            $_SESSION['CS']['VS']['STEP_3']['QTY_PICKED'][$orderId][$pId] = $qty;
                        }
                    }
                }
                $content = $this->renderPartial('wizard/_vs_import', null, true);
                //$content = $this->renderPartial('wizard/_step4', ['step1' => $step1, 'step2' => $step2, 'step3' => $step3], true);
                $activeStep = 4;
                break;
            case 5:
                $orders = [];
                $vs = new Vansales();
                if(isset($_SESSION['CS']['VS']['WIZARD']['ACTION']) && $_SESSION['CS']['VS']['WIZARD']['ACTION'] == 'update' && !isset($_SESSION['CS']['VS']['STEP_5']['ORDERS']))
                {
                    $where = 'tsi.trip_sheet_id = ' . $_SESSION['CS']['VS']['WIZARD']['TRIP_SHEET_ID'];
                    $orders = $vs->getPSImportedOrderMainOrders($where);
                }
                elseif(isset($_SESSION['CS']['VS']['STEP_5']['ORDERS']))
                {
                    $deviceOrderIds = array_keys($_SESSION['CS']['VS']['STEP_5']['ORDERS']);
                    $i = 0;
                    foreach($deviceOrderIds as $ids)
                    {
                        $deviceOrderIds[$i] = '"' . $ids . '"';
                        ++$i;
                    }
                    $dOIds = implode(',', $deviceOrderIds);
                    //check whether the orders already exists in order_main, if they do then query this table
                    $inOrderMain = Order_main::model()->findAll('device_order_id IN (' . $dOIds . ')');
                    if(!empty($inOrderMain) && !$_SESSION['CS']['VS']['STEP_5']['USE_FILE'])
                    {
                        $where = 'tsi.trip_sheet_id = ' . $_SESSION['CS']['VS']['WIZARD']['TRIP_SHEET_ID'];
                        $orders = $vs->getPSImportedOrderMainOrders($where);
                    }
                    else
                    {
                        $orders = $vs->getPSImportedWizardOrders();
                    }
                }
                $content = $this->renderPartial('wizard/_ps_import', ['orders' => $orders], true);
                $activeStep = 5;
                break;
            case 6:
                if(isset($_POST['psOrders']))//if a post was done from step 3
                {
                    if(isset($_SESSION['CS']['VS']['STEP_3']['ORDERS'])) unset($_SESSION['CS']['VS']['STEP_3']['ORDERS']);
                    foreach($_POST['psOrders'] as $oId => $val)
                    {
                        $_SESSION['CS']['VS']['STEP_3']['ORDERS'][$oId] = 1;
                    }
                }
                if(isset($_POST['qty_picked']))
                {
                    if(isset($_SESSION['CS']['VS']['STEP_3']['QTY_PICKED'])) unset($_SESSION['CS']['VS']['STEP_3']['QTY_PICKED']);
                    foreach($_POST['qty_picked'] as $orderId => $products)
                    {
                        foreach($products as $pId => $qty)
                        {
                            $_SESSION['CS']['VS']['STEP_3']['QTY_PICKED'][$orderId][$pId] = $qty;
                        }
                    }
                }

                $useFile = false;
                if(isset($_SESSION['CS']['VS']['STEP_5']))
                {
                    $useFile = $_SESSION['CS']['VS']['STEP_5']['USE_FILE'];
                    unset($_SESSION['CS']['VS']['STEP_5']);
                }
                if(isset($_POST['psImportOrders']))
                {
                    foreach($_POST['psImportOrders'] as $psImportId => $val)
                    {
                        $_SESSION['CS']['VS']['STEP_5']['ORDERS'][$psImportId] = 1;
                    }
                }
                $_SESSION['CS']['VS']['STEP_5']['USE_FILE'] = $useFile;
                $driverDetails = Employee::model()->findByPk($_SESSION['CS']['VS']['STEP_1']['driver']);
                $driver = isset($driverDetails) ? $driverDetails->name : 'N/A';
                $truck = Fleet::model()->findByPk($_SESSION['CS']['VS']['STEP_1']['truck']);
//                $location = LocStockLocations::model()->findByPk($_SESSION['CS']['VS']['STEP_1']['stockLocation'])->description;
                $step1 = [
                    'truck_weight' => isset($truck->weight) ? $truck->weight : 'N/A',
                    'truck_id' => isset($truck->id) ? $truck->id : 0,
                    'driver' => $driver,
                    'truck' => isset($truck->description) ? $truck->description : 'N/A',
//                    'location' => $location,
                    'shipmentDate' => $_SESSION['CS']['VS']['STEP_1']['shipmentDate'],
                    'notes' => (isset($_SESSION['CS']['VS']['STEP_1']['notes']) && $_SESSION['CS']['VS']['STEP_1']['notes'] !== '') ? $_SESSION['CS']['VS']['STEP_1']['notes'] : '-',
                    'comments' => (isset($_SESSION['CS']['VS']['STEP_1']['comments']) && $_SESSION['CS']['VS']['STEP_1']['comments'] !== '') ? $_SESSION['CS']['VS']['STEP_1']['comments'] : '-',
                ];
                $step2 = $_SESSION['CS']['VS']['STEP_2']['FINAL'];
                if(isset($_SESSION['CS']['VS']['STEP_2']['QTY']))//update the values with the values in the session
                {
//                    var_dump($_SESSION['CS']['VS']['STEP_2']['QTY']);
//                    var_dump($_SESSION['CS']['VS']['STEP_2']['QTY']);
                    for($i = 0; $i < count($step2); $i++)
                    {
                        $case = $step2[$i]['CS'];
                        if(!empty($case))
                        {
                            foreach($_SESSION['CS']['VS']['STEP_2']['QTY'] as $pId => $qty)
                            {
                                if($case['product_id'] == $pId)
                                {
                                    $step2[$i]['CS']['qty'] = $qty;
                                }
                            }
                        }
                        $each = $step2[$i]['EA'];
                        if(!empty($each))
                        {
                            foreach($_SESSION['CS']['VS']['STEP_2']['QTY'] as $pId => $qty)
                            {
                                if($each['product_id'] == $pId)
                                {
                                    $step2[$i]['EA']['qty'] = $qty;
                                }
                            }
                        }
                    }
                }
                $preSaleProductIds = [];
                $preSaleProductsFinal = [];
                if(isset($_SESSION['CS']['VS']['STEP_3']['QTY_PICKED']))
                {
                    foreach($_SESSION['CS']['VS']['STEP_3']['QTY_PICKED'] as $orderId => $products)
                    {
                        foreach($products as $pId => $qty)
                        {
                            if(isset($_SESSION['CS']['VS']['STEP_2']['QTY']))
                            {
                                if(!in_array($pId, $preSaleProductIds) && !in_array($pId, array_keys($_SESSION['CS']['VS']['STEP_2']['QTY']))) $preSaleProductIds[] = $pId;
                            }
                        }
                    }
                    if(empty($preSaleProductIds) && !isset($_SESSION['CS']['VS']['STEP_2']['QTY']))
                    {
                        foreach($_SESSION['CS']['VS']['STEP_3']['QTY_PICKED'] as $orderId => $products)
                        {
                            foreach($products as $pId => $qty)
                            {
                                if(!in_array($pId, $preSaleProductIds)) $preSaleProductIds[] = $pId;
                            }
                        }
                    }
                    if(!empty($preSaleProductIds))
                    {
                        $presSaleProductsQuantities = [];
                        foreach($preSaleProductIds as $pId)
                        {
                            foreach($_SESSION['CS']['VS']['STEP_3']['QTY_PICKED'] as $orderId => $products)
                            {
                                if(isset($_SESSION['CS']['VS']['STEP_3']['QTY_PICKED'][$orderId][$pId]))
                                {
                                    $presSaleProductsQuantities[$pId] = 0;
                                }
                            }
                            foreach($_SESSION['CS']['VS']['STEP_3']['QTY_PICKED'] as $orderId => $products)
                            {
                                if(isset($_SESSION['CS']['VS']['STEP_3']['QTY_PICKED'][$orderId][$pId]))
                                {
                                    $presSaleProductsQuantities[$pId] += $_SESSION['CS']['VS']['STEP_3']['QTY_PICKED'][$orderId][$pId];
                                }
                            }
                        }
                        foreach($preSaleProductIds as $pId)
                        {
                            $product = Products::model()->with('unitOfMeasure')->findByPk($pId);
                            $caseArr = [];
                            $eachArr = [];
                            if($product->units_of_measure_id == 1)
                            {
                                $caseArr = [
                                    'unit_of_measure' => $product['unitOfMeasure']->description,
                                    'product_id' => $pId,
                                    'qty' => isset($presSaleProductsQuantities[$pId]) ? $presSaleProductsQuantities[$pId] : 0,
                                    'sku' => $product->sku,
                                    'batch_code' => $product->batch_code,
                                    'expiry_date' => $product->expiry_date
                                ];
                            }
                            if($product->units_of_measure_id > 1)
                            {
                                $eachArr = [
                                    'unit_of_measure' => $product['unitOfMeasure']->description,
                                    'product_id' => $pId,
                                    'qty' => isset($presSaleProductsQuantities[$pId]) ? $presSaleProductsQuantities[$pId] : 0,
                                    'sku' => $product->sku,
                                    'batch_code' => $product->batch_code,
                                    'expiry_date' => $product->expiry_date
                                ];
                            }
                            $preSaleProductsFinal[] = [
                                'product' => $product->id . ' ' . $product->description,
                                'CS' => $caseArr,
                                'EA' => $eachArr
                            ];
                        }
                    }
                }
                $step2 = array_merge($step2, $preSaleProductsFinal);
                if(isset($_SESSION['CS']['VS']['STEP_3']['QTY_PICKED']))//update the values with the values in the session from step 3 the pre sales
                {
                    foreach($_SESSION['CS']['VS']['STEP_3']['QTY_PICKED'] as $orderId => $products)
                    {
                        for($i = 0; $i < count($step2); $i++)
                        {
                            $case = $step2[$i]['CS'];
                            if(!empty($case))
                            {
                                if(isset($_SESSION['CS']['VS']['STEP_2']['QTY']))
                                {
                                    foreach($_SESSION['CS']['VS']['STEP_2']['QTY'] as $pId => $qty)
                                    {
                                        if($case['product_id'] == $pId && isset($products[$pId]))
                                        {
                                            $step2[$i]['CS']['qty'] += $_SESSION['CS']['VS']['STEP_3']['QTY_PICKED'][$orderId][$pId];
                                        }
                                    }
                                }
                            }
                            $each = $step2[$i]['EA'];
                            if(!empty($each))
                            {
                                if(isset($_SESSION['CS']['VS']['STEP_2']['QTY']))
                                {
                                    foreach($_SESSION['CS']['VS']['STEP_2']['QTY'] as $pId => $qty)
                                    {
                                        if($each['product_id'] == $pId && isset($products[$pId]))
                                        {
                                            $step2[$i]['EA']['qty'] += $_SESSION['CS']['VS']['STEP_3']['QTY_PICKED'][$orderId][$pId];
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                $step3 = [
                    'pre_sales' => [],
                    'credit_notes' => []
                ];
                if(isset($_SESSION['CS']['VS']['STEP_3']['ORDERS']) && !empty($_SESSION['CS']['VS']['STEP_3']['ORDERS']))
                {
                    $orderIds = implode(',', array_keys($_SESSION['CS']['VS']['STEP_3']['ORDERS']));
                    $step3Sql = <<<SQL
                    SELECT
                        om.id, om.expected_delivery_date, e.name, o.description as outlet
                    FROM
                        order_main om
                            INNER JOIN
                        employee e ON e.id = om.order_taker_employee_nr
                            INNER JOIN
                        outlet o on o.id = om.account
                    WHERE
                        om.id in ($orderIds)
                            AND om.order_status_id = 1
                    ORDER BY order_date;
SQL;
                    $step3['pre_sales'] = $con->createCommand($step3Sql)->queryAll();

                    $step3CnSql = <<<SQL
                    SELECT
                        om.id, om.expected_delivery_date, e.name, o.description as outlet
                    FROM
                        order_main om
                            INNER JOIN
                        employee e ON e.id = om.order_taker_employee_nr
                            INNER JOIN
                        outlet o on o.id = om.account
                    WHERE
                        om.id in ($orderIds)
                            AND om.order_status_id = 18
                    ORDER BY order_date;
SQL;
                    $step3['credit_notes'] = $con->createCommand($step3CnSql)->queryAll();
                }
                $step5 = [];
                if(isset($_SESSION['CS']['VS']['STEP_5']['ORDERS']))
                {
                    $deviceOrderIds = array_keys($_SESSION['CS']['VS']['STEP_5']['ORDERS']);
                    $i = 0;
                    foreach($deviceOrderIds as $ids)
                    {
                        $deviceOrderIds[$i] = '"' . $ids . '"';
                        ++$i;
                    }
                    $dOIds = implode(',', $deviceOrderIds);
                    $whereSql = <<<SQL
                    AND w.device_order_id in ({$dOIds})
SQL;
                    $vs = new Vansales();
                    if($_SESSION['CS']['VS']['STEP_5']['USE_FILE'])//get data from the tmp wizard table
                    {
                        $step5 = $vs->getPSImportedWizardOrders($whereSql);
                    }
                    else//get the data from order_main
                    {
                        $step5 = $vs->getPSImportedOrderMainOrders('tsi.trip_sheet_id = ' . $_SESSION['CS']['VS']['WIZARD']['TRIP_SHEET_ID'] . ' AND om.device_order_id IN (' . $dOIds . ')');
                    }
                }

                $totalCustomers = count($step3);
                $totalWeight = 0;
                $totalUnits = 0;
                foreach($step2 as $stuff)
                {
                    $cs = $stuff['CS'];
                    if(count($cs) > 0)
                    {
                        $csPId = $cs['product_id'];
                        $product = Products::model()->findByPk($csPId);
                        $totalWeight += $product->weight*$cs['qty'];
                        $totalUnits += $product->units*$cs['qty'];
                    }
                    $ea = $stuff['EA'];
                    if(count($ea) > 0)
                    {
                        $eaPId = $ea['product_id'];
                        $product = Products::model()->findByPk($eaPId);
                        $totalWeight += $product->weight*$ea['qty'];
                        $totalUnits += $product->units*$ea['qty'];
                    }
                }

                $totals = [
                    'vehicleWeight' => $step1['truck_weight'],
                    'totalCustomers' => $totalCustomers,
                    'totalUnits' => $totalUnits,
                    'totalWeight' => $totalWeight
                ];

//                var_dump($_SESSION['CS']['VS']['STEP_2']['QTY']);
                $content = $this->renderPartial('wizard/_step4', ['step1' => $step1, 'step2' => $step2, 'step3' => $step3, 'step5' => $step5, 'totals' => $totals], true);
                $activeStep = 4;
                break;
            case 7:
                //var_dump($_SESSION['CS']['VS']['STEP_2']['WITH_LINKED']);
                // die();
                if(!isset($_SESSION['CS']['VS']['STEP_2']['QTY'])
                    && !isset($_SESSION['CS']['VS']['STEP_3']['ORDERS'])
                    && !isset($_SESSION['CS']['VS']['STEP_5']['ORDERS'])
                )
                {
                    $driver = Employee::model()->findByPk($_SESSION['CS']['VS']['STEP_1']['driver'])->name;
                    $truck = Fleet::model()->findByPk($_SESSION['CS']['VS']['STEP_1']['truck'])->description;
//                    $location = LocStockLocations::model()->findByPk($_SESSION['CS']['VS']['STEP_1']['stockLocation'])->description;
                    $step1 = [
                        'driver' => $driver,
                        'truck' => $truck,
//                        'location' => $location,
                        'shipmentDate' => $_SESSION['CS']['VS']['STEP_1']['shipmentDate']
                    ];
                    $this->renderPartial('wizard/_step4', ['step1' => $step1, 'step2' => [], 'step3' => [], 'step5' => []]);
                    die();
                }
                $success = true;
                //if this is an update then delete and recreate
                if(isset($_SESSION['CS']['VS']['WIZARD']['ACTION']) && $_SESSION['CS']['VS']['WIZARD']['ACTION'] == 'update')
                {
                    $vanSale = new Vansales();
                    $deleted = $vanSale->deletePlannedLoad($_SESSION['CS']['VS']['WIZARD']['TRIP_SHEET_ID']);
                    if($deleted != 1)
                    {
                        $success = false;
                    }
                }
                $notes = (isset($_SESSION['CS']['VS']['STEP_1']['notes']) && $_SESSION['CS']['VS']['STEP_1']['notes'] !== '') ? $_SESSION['CS']['VS']['STEP_1']['notes'] : null;
                $comments = (isset($_SESSION['CS']['VS']['STEP_1']['comments']) && $_SESSION['CS']['VS']['STEP_1']['comments'] !== '') ? $_SESSION['CS']['VS']['STEP_1']['comments'] : null;
                //create the trip sheet
                $tripSheet = new TripSheets();
                $tripSheet->fleet_id = $_SESSION['CS']['VS']['STEP_1']['truck'];
                $tripSheet->bakery_id = $bakeryId;
                $tripSheet->opening_branch_id = $bakeryId;
                $tripSheet->opening_location_id = 0;//$_SESSION['CS']['VS']['STEP_1']['stockLocation'];
                $tripSheet->created_employee_id = Yii::app()->user->id;
                $tripSheet->created_date = date('Y-m-d H:i:s');
                $tripSheet->driver_employee_id = $_SESSION['CS']['VS']['STEP_1']['driver'];
                $tripSheet->expected_delivery_date = $_SESSION['CS']['VS']['STEP_1']['shipmentDate'];
                $tripSheet->combined_load = 1;
                $tripSheet->trip_sheet_status_id = 1;
                $tripSheet->opening_mileage = 0;
                $tripSheet->trip_type = 1;
                $tripSheet->notes = $notes;
                $tripSheet->comments = $comments;
                if($tripSheet->save())
                {
                    //create the trip sheet stock
                    $tripSheetId = $tripSheet->id;
                    $misc = new Miscellaneous();
                    if(isset($_SESSION['CS']['VS']['STEP_2']['QTY']))
                    {
                        foreach($_SESSION['CS']['VS']['STEP_2']['QTY'] as $pId => $qty)
                        {
                            if(isset($_SESSION['CS']['VS']['STEP_2']['WITH_LINKED'][$pId]))
                            {
                                unset($_SESSION['CS']['VS']['STEP_2']['WITH_LINKED'][$pId]);
                            }
                        }
                    }
                    if(isset($_SESSION['CS']['VS']['STEP_3']['QTY_PICKED']))
                    {
                        foreach($_SESSION['CS']['VS']['STEP_3']['QTY_PICKED'] as $orderId => $products)
                        {
                            foreach($products as $pId => $qty)
                            {
                                if(isset($_SESSION['CS']['VS']['STEP_2']['QTY'][$pId]))
                                {
                                    $_SESSION['CS']['VS']['STEP_2']['QTY'][$pId] += $qty;
                                }
                                else
                                {
                                    $_SESSION['CS']['VS']['STEP_2']['QTY'][$pId] = $qty;
                                }
                            }
                        }
                    }
                    if(isset($_SESSION['CS']['VS']['STEP_2']['WITH_LINKED']))
                    {
                        foreach($_SESSION['CS']['VS']['STEP_2']['WITH_LINKED'] as $pId => $qty)
                        {
                            if(isset($_SESSION['CS']['VS']['STEP_2']['QTY'][$pId]))
                            {
                                $_SESSION['CS']['VS']['STEP_2']['QTY'][$pId] += $_SESSION['CS']['VS']['STEP_2']['WITH_LINKED'][$pId];
                            }
                            else
                            {
                                $_SESSION['CS']['VS']['STEP_2']['QTY'][$pId] = $_SESSION['CS']['VS']['STEP_2']['WITH_LINKED'][$pId];
                            }
                        }
                    }
                    if(isset($_SESSION['CS']['VS']['STEP_2']['QTY']))
                    {
                        $stock = $_SESSION['CS']['VS']['STEP_2']['QTY']; //$misc->rollUpStock($_SESSION['CS']['VS']['STEP_2']['QTY']);
                        $stockIInsertedCount = 0;
                        foreach($stock as $pId => $qty)
                        {
                            $sql = <<<SQL
                        insert into trip_sheets_stock (trip_sheet_id, product_id, planned_qty, opening_qty, variance_id, product_indicator)
                         values ({$tripSheetId}, {$pId}, {$qty}, {$qty}, 0, 1);
SQL;
                            $stockIInsertedCount += $con->createCommand($sql)->execute();
                        }
                        if($stockIInsertedCount == 0)
                        {
                            $success = false;
                        }
                    }
                    //create the trip sheet items
                    if(isset($_SESSION['CS']['VS']['STEP_3']['ORDERS']) && !empty($_SESSION['CS']['VS']['STEP_3']['ORDERS']))
                    {
                        $orderIds = array_keys($_SESSION['CS']['VS']['STEP_3']['ORDERS']);
                        for($i = 0; $i < count($orderIds); $i++)
                        {
                            $tripSheetItem = new TripSheetsItems();
                            $tripSheetItem->trip_sheet_id = $tripSheetId;
                            $tripSheetItem->order_id = $orderIds[$i];
                            $tripSheetItem->stock_location_id = 0;
                            $tripSheetItem->is_active = 0;
                            if(!$tripSheetItem->save())
                            {
                                $success = false;
                            }
                            //update order_main set linked_to_van_sale = 1
                            $orderMain = Order_main::model()->findByPk($orderIds[$i]);
                            $orderMain->linked_to_van_sale = 1;
                            $orderMain->order_status_id = ($orderMain->order_status_id == 1) ? 2 : 19;
                            if(!$orderMain->save())
                            {
                                $success = false;
                            }
                            $orders = Orders::model()->findAll('order_id = ' . $orderIds[$i]);
                            foreach($orders as $order)
                            {
                                $order->qty_picked = isset($_SESSION['CS']['VS']['STEP_3']['QTY_PICKED'][$orderIds[$i]][$order->product_id]) ? $_SESSION['CS']['VS']['STEP_3']['QTY_PICKED'][$orderIds[$i]][$order->product_id] : $order->qty_ordered;
                                $order->save();
                            }
                        }
                    }
                    if(isset($_SESSION['CS']['VS']['STEP_5']['ORDERS']))
                    {
                        $deviceOrderIds = array_keys($_SESSION['CS']['VS']['STEP_5']['ORDERS']);
                        $i = 0;
                        foreach($deviceOrderIds as $ids)
                        {
                            $deviceOrderIds[$i] = '"' . $ids . '"';
                            ++$i;
                        }
                        $dOIds = implode(',', $deviceOrderIds);
                        $inOrderMain = Order_main::model()->findAll('device_order_id IN (' . $dOIds . ')');
                        if(!empty($inOrderMain) && !$_SESSION['CS']['VS']['STEP_5']['USE_FILE'])
                        {
                            //only create the trip_sheets_item
                            for($i = 0; $i < count($inOrderMain); $i++)
                            {
                                $tripSheetItem = new Trip_sheets_items();
                                $tripSheetItem->trip_sheet_id = $tripSheetId;
                                $tripSheetItem->order_id = $inOrderMain[$i]->id;
                                $tripSheetItem->stock_location_id = 0;
                                $tripSheetItem->is_active = 0;
                                if(!$tripSheetItem->save())
                                {
                                    $success = false;
                                }
                            }
                        }
                        else
                        {
                            $whereSql = <<<SQL
                            AND w.device_order_id in ({$dOIds})
SQL;
                            $vs = new Vansales();
                            $orders = $vs->getPSImportedWizardOrders($whereSql);
                            $orderIds = [];
                            $lookupMap = [];
                            foreach($orders as $order)
                            {
                                $orderIds[] = $order['order_id'];
                                $lookupMap[$order['device_order_id']] = $order['order_id'];
                            }
                            foreach($lookupMap as $deviceOrderId => $orderId)
                            {
                                $tripSheetItem = new Trip_sheets_items();
                                $tripSheetItem->trip_sheet_id = $tripSheetId;
                                $tripSheetItem->order_id = $orderId;
                                $tripSheetItem->stock_location_id = 0;
                                $tripSheetItem->is_active = 0;
                                if(!$tripSheetItem->save())
                                {
                                    $success = false;
                                }
                            }
                            //create order_main and orders here...
                            foreach($orders as $order)
                            {
                                if(isset($lookupMap[$order['device_order_id']]))
                                {
                                    $hasOrderMainEntry = Order_main::model()->find('device_order_id = :deviceOrderId', [':deviceOrderId' => $order['device_order_id']]);
                                    if(isset($hasOrderMainEntry))//update the order_id in order_main as the entry already exists
                                    {
                                        $oldOrderId = $hasOrderMainEntry->id;
                                        $hasOrderMainEntry->id = $lookupMap[$order['device_order_id']];
                                        $hasOrderMainEntry->save();
                                        //update the orders as well
                                        Orders::model()->updateAll(['order_id' => $lookupMap[$order['device_order_id']]], 'order_id = ' . $oldOrderId);
                                    }
                                    else
                                    {
                                        $orderMain = new Order_main();
                                        $orderMain->id = $order['order_id'];
                                        $orderMain->device_order_id = $order['device_order_id'];
                                        $orderMain->account = $order['account'];
                                        $orderMain->bakery_id = $order['bakery_id'];
                                        $orderMain->purchase_order_nr = $order['purchase_order_nr'];
                                        $orderMain->order_taker_employee_nr = $order['order_taker_employee_nr'];
                                        $orderMain->order_date = $order['order_date'];
                                        $orderMain->expected_delivery_date = $order['expected_delivery_date'];
                                        $orderMain->order_status_id = $order['order_status_id'];
                                        $orderMain->order_type = $order['order_type'];
                                        $orderMain->payment_type_id = $order['payment_type_id'];
                                        $orderMain->linked_to_van_sale = $order['linked_to_van_sale'];
                                        $orderMain->combined_load_pre_sale_import = $order['combined_load_pre_sale_import'];
                                        $orderMain->order_total_amount = $order['order_total_amount'];
                                        $orderMain->gross_order_total_amount = $order['gross_order_total_amount'];
                                        $orderMain->vat_value = $order['vat_value'];
                                        $orderMain->discount = $order['discount'];
                                        $orderMain->discount_value = $order['discount_value'];
                                        $orderMain->cash_collected = 0.00;
                                        if(!$orderMain->save())
                                        {
                                            $success = false;
                                        }
                                        else
                                        {
                                            //if create, get the orders for this order_id from the wizard tmp table
                                            $ordersSql = <<<SQL
                                            SELECT
                                                p.id AS product_id,
                                                w.qty_ordered,
                                                w.qty_picked,
                                                ROUND(((w.gross_price * w.discount) / 100), 2) AS discount,
                                                ROUND(w.gross_price - ((w.gross_price * w.discount) / 100),
                                                        2) AS price,
                                                ROUND(w.gross_price, 2) AS gross_price,
                                                w.vat_percentage,
                                                ROUND(((w.order_total_amount) * (w.vat_percentage / 100)),
                                                        2) AS vat_value,
                                                uom.description
                                            FROM
                                                import_ps_wizard_tmp w
                                                    INNER JOIN
                                                units_of_measure uom ON uom.description = w.unit_of_measure
                                                    INNER JOIN
                                                products p ON p.sku = w.product_id
                                                    AND p.units_of_measure_id = uom.id
                                            WHERE
                                                w.device_order_id = :deviceOrderId
SQL;
                                            $ordersCommand = $con->createCommand($ordersSql);
                                            $ordersCommand->bindValue(':deviceOrderId', $order['device_order_id'], PDO::PARAM_STR);
                                            $ordersResult = $ordersCommand->queryAll();
                                            foreach($ordersResult as $sku)
                                            {
                                                $skuOrder = new Orders();
                                                $skuOrder->order_id = $order['order_id'];
                                                $skuOrder->product_id = $sku['product_id'];
                                                $skuOrder->qty_ordered = $sku['qty_ordered'];
                                                $skuOrder->qty_picked = $sku['qty_picked'];
                                                $skuOrder->discount = $sku['discount'];
                                                $skuOrder->price = $sku['price'];
                                                $skuOrder->gross_price = $sku['gross_price'];
                                                $skuOrder->vat_percentage = $sku['vat_percentage'];
                                                $skuOrder->vat_value = $sku['vat_value'];
                                                $orderSave = $skuOrder->save();
                                                if(!$orderSave)
                                                {
                                                    $success = false;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        $tripStockCommand = $con->createCommand('select product_id, opening_qty from trip_sheets_stock where trip_sheet_id = :tripSheetId');
                        $tripStockCommand->bindValue(':tripSheetId', $tripSheetId, PDO::PARAM_INT);
                        $tripStock = $tripStockCommand->queryAll();
                        $tStock = $misc->prepRollUpArray($tripStock);
                        foreach($tripStock as $stock)
                        {
                            $tStock[$stock['product_id']] = $stock['opening_qty'];
                        }
                        $importProductsSql = <<<SQL
                        SELECT
                            o.product_id, SUM(o.qty_picked) AS opening_qty
                        FROM
                            orders o
                                INNER JOIN
                            order_main om ON om.id = o.order_id
                        WHERE
                            om.device_order_id IN ($dOIds)
                                AND om.combined_load_pre_sale_import = 1
                        GROUP BY o.product_id;
SQL;
                        $products = $con->createCommand($importProductsSql)->queryAll();
                        $prepared = [
                            'insert' => [],
                            'update' => [],
                        ];
                        foreach($products as $product)
                        {
                            if(array_key_exists($product['product_id'], $tStock))
                            {
                                $prepared['update'][$product['product_id']] = $product['opening_qty'] + $tStock[$product['product_id']];
                            }
                            else
                            {
                                $prepared['insert'][$product['product_id']] = $product['opening_qty'];
                            }
                        }
//                        $prepared['insert'] = $misc->rollUpStock($prepared['insert']);
//                        $prepared['update'] = $misc->rollUpStock($prepared['update']);
                        foreach($prepared['insert'] as $pId => $val)
                        {
                            if(array_key_exists($pId, $tStock))
                            {
                                $prepared['update'][$pId] = $val + $tStock[$pId];
                                unset($prepared['insert'][$pId]);
                            }
                        }
                        foreach($prepared as $what => $data)
                        {
                            foreach($data as $pId => $qty)
                            {
                                if($what === 'insert')
                                {
                                    $insertSql = <<<SQL
                                    insert into trip_sheets_stock (trip_sheet_id, product_id, opening_qty, closing_qty, variance_id, product_indicator)
                                    values ({$tripSheetId}, {$pId}, {$qty}, 0, 0, 1);
SQL;
                                    $con->createCommand($insertSql)->execute();
                                }
                                else
                                {
                                    $updateSqlSql = <<<SQL
                                    update trip_sheets_stock set opening_qty = {$qty} where product_id = {$pId} and trip_sheet_id = {$tripSheetId};
SQL;
                                    $con->createCommand($updateSqlSql)->execute();
                                }
                            }
                        }
                        $postSelectSql = <<<SQL
                        select product_id, opening_qty from trip_sheets_stock where trip_sheet_id = {$tripSheetId}
SQL;
                        $postSelectStock = $con->createCommand($postSelectSql)->queryAll();
                        $postSelectStockPre = $misc->prepRollUpArray($postSelectStock);
                        $postSelectStockRollUp = [];//$misc->rollUpStock($postSelectStockPre);
                        foreach($postSelectStockRollUp as $pId => $qty)
                        {
                            $updateSqlSql = <<<SQL
                            update trip_sheets_stock set opening_qty = {$qty} where product_id = {$pId} and trip_sheet_id = {$tripSheetId};
SQL;
                            $con->createCommand($updateSqlSql)->execute();
                        }
                    }
                    if(isset($_SESSION['CS']['VS']['WIZARD']['ACTION']) && $_SESSION['CS']['VS']['WIZARD']['ACTION'] == 'update')
                    {
                        $data = [];
                        $data['trip_sheet_id'] = $_SESSION['CS']['VS']['WIZARD']['TRIP_SHEET_ID'];
                        $data['from_trip_sheet_id'] = $tripSheetId;
                        TripSheetsPlannedLoadLog::log(TripSheetsPlannedLoadLog::$LOG_UPDATED, $data);
                    }
                    $data = [];
                    $data['trip_sheet_id'] = $tripSheetId;
                    TripSheetsPlannedLoadLog::log(TripSheetsPlannedLoadLog::$LOG_CREATED, $data);
                    if(isset($_SESSION['CS'])) unset($_SESSION['CS']);
                }
                else
                {
                    $success = false;
                }


                // add parent and child products to the trip sheet stock table if it isn't already in the table
                // this is needed for available stock to work correctly when there was only opened with eaches
                CustomUtils::addParentProduct($tripSheetId, $bakeryId);
                CustomUtils::addChildProduct($tripSheetId, $bakeryId);

                //do a post update on trip_sheets_stock and trip_sheets for the opening values
                CustomUtils::updateTripSheetValues($tripSheetId, 'opening_qty_product_price', 'opening_qty_value');

                $content = $this->renderPartial('wizard/_step5', ['success' => $success], true);
                $activeStep = 6;
                $finalStep = true;
                break;
            default:
                if(isset($_POST['psOrders']))
                {
                    $ordersValid = true;
                    $orderInvalidData = [];
                    $validOrderstatuses = [1, 18];
                    foreach($_POST['psOrders'] as $orderId)
                    {

                        // array_push($orderInvalidData, $orderId);
                        $sql = <<<SQL
                        SELECT order_status_id FROM order_main WHERE id = :orderId;
SQL;
                        $com = $con->createCommand($sql);
                        $com->bindValue(':orderId', $orderId, PDO::PARAM_INT);
                        $orderStatus = $com->queryScalar();
                        if(!in_array($orderStatus, $validOrderstatuses))
                        {
                            $ordersValid = false;
                            array_push($orderInvalidData, $orderId);
                        }
                    }

                    if($ordersValid)
                    {
                        if(isset($_SESSION['CS']['VS']['STEP_1']['ORDERS'])) unset($_SESSION['CS']['VS']['STEP_1']['ORDERS']);
                        foreach($_POST['psOrders'] as $orderId)
                        {
                            $_SESSION['CS']['VS']['STEP_1']['ORDERS'][$orderId] = 1;
                        }
                    }
                    else//the order status of some of the orders have changed
                    {
                        $this->renderPartial('wizard/_ordersStatusChange', ['data' => $orderInvalidData]);
                        return;
                    }
                }

//                if(isset($_SESSION['CS']['PAGE']))
//                {
//                    $orders = $_SESSION['CS']['PAGE'];
//                    foreach($orders as $order)
//                    {
//                        foreach($order as $orderId)
//                        {
//                            $_SESSION['CS']['VS']['STEP_1']['ORDERS'][$orderId] = 1;
//                        }
//                    }
//                }
                $employees = Employee::model()->getEmployees($bakeryId);
                $fleets = Fleet::model()->getFleet($bakeryId);
                $stockLocations = LocStockLocations::model()->getStockLocation($bakeryId);
                $inputData = [
                    'driver' => null,
                    'truck' => null,
                    'stockLocation' => null,
                    'shipmentDate' => null,
                    'notes' => null,
                    'comments' => null
                ];
                if(isset($_SESSION['CS']['VS']['STEP_1']['driver'])) $inputData['driver'] = $_SESSION['CS']['VS']['STEP_1']['driver'];
                if(isset($_SESSION['CS']['VS']['STEP_1']['truck'])) $inputData['truck'] = $_SESSION['CS']['VS']['STEP_1']['truck'];
//                if(isset($_SESSION['CS']['VS']['STEP_1']['stockLocation'])) $inputData['stockLocation'] = $_SESSION['CS']['VS']['STEP_1']['stockLocation'];
                if(isset($_SESSION['CS']['VS']['STEP_1']['shipmentDate'])) $inputData['shipmentDate'] = $_SESSION['CS']['VS']['STEP_1']['shipmentDate'];
                if(isset($_SESSION['CS']['VS']['STEP_1']['notes'])) $inputData['notes'] = $_SESSION['CS']['VS']['STEP_1']['notes'];
                if(isset($_SESSION['CS']['VS']['STEP_1']['comments'])) $inputData['comments'] = $_SESSION['CS']['VS']['STEP_1']['comments'];
                $content = $this->renderPartial('wizard/_step1', ['drivers' => $employees, 'trucks' => $fleets, 'stockLocations' => $stockLocations, 'inputData' => $inputData, 'blockedMessage' => $blockedMessage, 'mergeCreditNotesBtn' => $mergeCreditNotesBtn], true);
                $activeStep = 1;
        }
        $this->renderPartial('wizard/main', ['content' => $content, 'activeStep' => $activeStep, 'finalStep' => $finalStep]);
    }

    public function actionWizardProductSearch()
    {
        $products = Products::model()->searchWizardProducts();
        echo $products;
    }

    public function actionWizardLoadPreSalesProducts($orderId)
    {
        $orders = Orders::model()->with('products', 'products.unitOfMeasure')->findAll('order_id = ' . $orderId);
        $this->renderPartial('wizard/_presaleProducts', ['orders' => $orders]);
    }

    public function actionMergeShipments($from)
    {
        if(isset($_POST['mergeShipmentFormBtn']))
        {
            $con = Yii::app()->db;
            $shipmentDate = $_POST['shipmentDate'];
            $tripSheets = $_POST['tripSheetIds'];
            $tripSheetsIn = implode(',', $tripSheets);
            $bakeryId = $_POST['bakery_id'];
            $partiallyAccept = (isset($_POST['partialLoad'])) ? $_POST['partialLoad'] : 0;
            $driver = 0;
            if($_POST['driver'] !== '')
            {
                $driver = $_POST['driver'];
            }
            $truck = 0;
            if($_POST['truck'] !== '')
            {
                $truck = $_POST['truck'];
            }

            $to = $from;
            if($from === 'dispatch')
            {
                $to = 'in_'.$from;
            }

            $valid = TripSheets::checkDriverAndFleet($driver, $truck);
            if(!$valid['valid'])
            {
                Yii::app()->user->setFlash('error', $valid['msg']);
                $this->redirect(Yii::app()->getBaseUrl(true) . '/index.php/admin/' . $this->id . '?show=' . $to);
                die();
            }

            $validateDriverLoadedShipment = true;
            foreach($_POST['tripSheetIds'] as $id)
            {
                $ts = TripSheets::model()->findByPk($id);
                if($ts->driver['id'] == $driver)
                {
                    $validateDriverLoadedShipment = false;
                    break;
                }
            }

            if($validateDriverLoadedShipment)
            {
                $bakery = Bakery::model()->findByPk($bakeryId);
                if($bakery->multiple_shipment_create == 0)
                {
                    $tsCount = TripSheets::checkDriverLoadedShipment($driver, $shipmentDate);
                    if($tsCount > 0)
                    {
                        Yii::app()->user->setFlash('error', 'Whoops! This user already has a planned load for the shipment date selected.');
                        $this->redirect(Yii::app()->getBaseUrl(true) . '/index.php/admin/' . $this->id . '?show=' . $to);
                        die();
                    }
                }
            }

            //1. create the new trip_sheet
            $branchId = $_POST['bakery_id'];
            $tripSheet = new TripSheets();
            $tripSheet->fleet_id = $truck;
            $tripSheet->bakery_id = $branchId;
            $tripSheet->trip_sheet_status_id = $_POST['trip_sheet_status_id'];
            $tripSheet->opening_branch_id = $branchId;
            $tripSheet->opening_location_id = 0;
            $tripSheet->closing_branch_id = 0;
            $tripSheet->closing_location_id = 0;
            $tripSheet->created_employee_id = Yii::app()->user->id;
            $tripSheet->created_date = date('Y-m-d H:i:s');
            $tripSheet->closed_by_employee_id = 0;
            $tripSheet->driver_employee_id = $driver;
            $tripSheet->expected_delivery_date = $shipmentDate;
            $tripSheet->opening_mileage = 0;
            $tripSheet->trip_type = 1;
            $tripSheet->combined_load = 1;
            $tripSheet->partially_accept = $partiallyAccept;
            if($from === 'dispatch')
            {
                $tripSheet->dispatch_employee_id = isset($_POST['dispatch_employee_id']) ? $_POST['dispatch_employee_id'] : 0;
                $tripSheet->dispatch_fleet_id = isset($_POST['dispatch_fleet_id']) ? $_POST['dispatch_fleet_id'] : 0;
            }
            $tripSheet->save();
            $newTripSheetId = $tripSheet->id;

            //2. create the new trip_sheet_stock
            $sql = <<<SQL
            INSERT INTO trip_sheets_stock (trip_sheet_id, product_id, product_indicator, planned_qty, opening_qty)
            SELECT
                {$newTripSheetId} AS trip_sheet_id,
                tss.product_id,
                tss.product_indicator,
                SUM(tss.planned_qty) AS planned_qty,
                SUM(tss.opening_qty) AS opening_qty
            FROM
                trip_sheets_stock tss
            WHERE
                tss.trip_sheet_id IN ({$tripSheetsIn})
            GROUP BY tss.product_id, tss.product_indicator;
SQL;

            $con->createCommand($sql)->execute();

            //update product pricing
            CustomUtils::updateTripSheetValues($newTripSheetId, 'opening_qty_product_price', 'opening_qty_value');

            //3. update trip_sheets_items with the new trip_sheet_id and update trip_sheet with the merge_trip_sheet_id
            foreach($tripSheets as $tripSheetId)
            {
                TripSheets::model()->updateByPk($tripSheetId, ['merge_trip_sheet_id' => $newTripSheetId, 'partially_accept' => 0]);

                TripSheetsItems::model()->updateAll(['trip_sheet_id' => $newTripSheetId], 'trip_sheet_id = ' . $tripSheetId);

                TripSheetsPlannedLoadLog::log(TripSheetsPlannedLoadLog::$LOG_MERGED, ['trip_sheet_id' => $tripSheetId, 'from_trip_sheet_id' => $newTripSheetId]);
            }

            if($from === 'dispatch')
            {
                //4. create a new as_stock_movement item
                //5. create the new as_stock_movement_items for the new as_stock_movement item and new trip_sheet
                //6. update the stock
                AsStockMovement::mergeShipmentStock($newTripSheetId, $tripSheetsIn);
                //update product pricing
                CustomUtils::updateTripSheetValues($newTripSheetId, 'picked_qty_product_price', 'picked_qty_value');
            }

            $data['trip_sheet_id'] = $newTripSheetId;
            TripSheetsPlannedLoadLog::log(TripSheetsPlannedLoadLog::$LOG_CREATED, $data);

            Yii::app()->user->setFlash('success', 'Whoop whoop! Shipments successfully merged. New shipment # ' . $newTripSheetId);
            $this->redirect(Yii::app()->getBaseUrl(true) . '/index.php/admin/' . $this->id . '?show=' . $to);
            die();
        }
        $ids = implode(',', $_POST['id']);
        $tripSheets = TripSheets::model()
            ->with([
                'tripSheetItems.orderMain' => [
                    'select' => 'tripSheetItems.id, orderMain.id, orderMain.expected_delivery_date'
                ],
                'tripSheetItems.orderMain.outlet' => [
                    'select' => 'outlet.description'
                ],
                'tripSheetItems.orderMain.employee' => [
                    'select' => 'employee.name'
                ],
                'createdBy' => [
                    'select' => 'name'
                ],
                'tripSheetStock' => [
                    'select' => 'tripSheetStock.opening_qty'
                ],
                'tripSheetStock.product' => [
                    'select' => 'product.weight'
                ],
                'fleet' => [
                    'select' => 'description, registration, weight'
                ],
                'driver' => [
                    'select' => 'name'
                ]
            ])
//            ->with('tripSheetItems.orderMain.orders.products', 'tripSheetItems.orderMain.outlet', 'tripSheetItems.orderMain.employee', 'tripSheetStock', 'createdBy', 'fleet', 'driver')
            ->findAll(['select' => 't.id, t.accepted, t.trip_sheet_status_id, t.bakery_id, t.expected_delivery_date, t.created_date', 'condition' => 't.id in ('.$ids.')']);

        $i = 0;
        $previousIndex = $i - 1;
        $sameBranches = true;
        $bakeryId = 0;
        $tripSheetStatus = 0;
        foreach($tripSheets as $tripSheet)
        {
            if(isset($tripSheets[$previousIndex]->bakery_id) && $tripSheets[$previousIndex]->bakery_id !== $tripSheet->bakery_id)
            {
                $sameBranches = false;
                break;
            }

            $bakeryId = $tripSheet->bakery_id;
            $tripSheetStatus = $tripSheet->trip_sheet_status_id;

            ++$i;
            ++$previousIndex;
        }
        if(!$sameBranches)
        {
            echo json_encode(['code' => 0, 'msg' => 'Can\'t merge shipments from different branches']);
            die();
        }

        $accepted = false;
        $acceptedTripSheetIds = '';
        $c = 0;
        foreach($tripSheets as $tripSheet)
        {
            if($tripSheet->accepted > 0)
            {
                $accepted = true;
                $acceptedTripSheetIds .= ($c === 0) ? $tripSheet->id : ', ' . $tripSheet->id;
            }

            ++$c;
        }

        if($accepted)
        {
            echo json_encode(['code' => 0, 'msg' => 'Can\'t merge shipments that has already been accepted by a driver. Shipments '.$acceptedTripSheetIds.'. Please refresh this page']);
            die();
        }


        $shipmentStockSql = <<<SQL
      SELECT
         tss.product_id,
         uom.description AS unit_of_measure, CONCAT('[',p.sku,'] - ', p.id, p.description, ' : ', p.batch_code, '(', p.expiry_date, ')') AS product, CASE WHEN tss.product_indicator = 1 THEN 'Ordered' ELSE 'Return' END AS product_indicator, SUM(tss.opening_qty) AS qty,
         (SUM(p.units)*SUM(tss.opening_qty)) AS units,
         (p.weight*SUM(tss.opening_qty)) AS weight
        FROM
         trip_sheets_stock tss
        INNER JOIN
        products p ON p.id = tss.product_id
        INNER JOIN
        units_of_measure uom ON uom.id = p.units_of_measure_id
        WHERE
         tss.trip_sheet_id IN ({$ids})
        GROUP BY tss.product_id, tss.product_indicator
        HAVING SUM(tss.opening_qty) > 0;
SQL;

        $shipmentStock = Yii::app()->db->createCommand($shipmentStockSql)->queryAll();

        $preSaleOrderCount = 0;
        $numberOfCustomers = [];
        $totalUnits = 0;
        $totalWeight = 0;
        foreach($tripSheets as $tripSheet)
        {
            if(!empty($tripSheet['tripSheetItems']))
            {
                ++$preSaleOrderCount;
                foreach($tripSheet['tripSheetItems'] as $item)
                {
                    if(!in_array($item['orderMain']['outlet']['id'], $numberOfCustomers))
                    {
                        array_push($numberOfCustomers, $item['orderMain']['outlet']['id']);
                    }
                }
            }
        }
        foreach($shipmentStock as $stock)
        {
            $totalUnits += $stock['units'];
            $totalWeight += $stock['weight'];
        }

        //get the dispatch area for the warehouse
        $bakeryLocationWhere = CustomUtils::warehouseLocationWhereClause();
        $stockLocations = AsStockLocations::model()->with('warehouse')->findAll(['condition' => 't.location_type_id = 2 and t.status_id = 1' . $bakeryLocationWhere, 'order' => 'warehouse.description, t.description']);
        $dispatchList = [];
        foreach($stockLocations as $loc)
        {
            $dispatchList[$loc['warehouse']['description']][$loc['id']] = $loc['description'];
        }

        $bakery = Bakery::model()->findByPk($bakeryId);

        $employees = Employee::model()->getEmployees($bakeryId);
        $fleets = Fleet::model()->getFleet($bakeryId);
        $html = $this->renderPartial('_mergeShipments', [
            'from' => $from,
            'dispatchList' => $dispatchList,
            'numberOfCustomers' => count($numberOfCustomers),
            'totalUnits' => $totalUnits,
            'totalWeight' => $totalWeight,
            'preSaleOrderCount' => $preSaleOrderCount,
            'shipmentStock' => $shipmentStock,
            'tripSheetStatus' => $tripSheetStatus,
            'bakeryId' => $bakeryId,
            'tripSheets' => $tripSheets,
            'drivers' => $employees,
            'trucks' => $fleets,
            'bakery' => $bakery
        ], true);

        echo $html;
        die();
    }

    public function actionEditOrder($orderId, $from = 'new')
    {
        $data = MailFactory::getPurchaseOrderDetails($orderId, false, true);

        $returnTypes = ReturnTypes::returnTypesList();
        $paymentTypes = PaymentType::paymentTypesList();

        $this->renderPartial('_editOrder', ['from' => $from, 'data' => $data, 'returnTypes' => $returnTypes, 'paymentTypes' => $paymentTypes, 'recurringOrder' => 'false']);
    }

    public function actionEditOrderRecurring($orderId, $from = 'new')
    {
        $data = MailFactory::getPurchaseOrderDetails($orderId, false, true, true);

        $outletId = (isset($data['ordered'][0]['outlet_id'])) ? $data['ordered'][0]['outlet_id'] : 0;

        $returnTypes = ReturnTypes::returnTypesList();
        $paymentTypes = PaymentType::paymentTypesList();

        $html = <<<HTML
        <div class="form-group">
            <button id="ordersRecurringDetailsBackBtn" class="btn btn-primary form-control">Back</button>
        </div>
HTML;

        $url = Yii::app()->getBaseUrl(true) . '/index.php/admin/outlet/recurringOrders/outletId/' . $outletId;

        $html .= <<<HTML
        <script type="text/javascript">
        $(document).ready(function(){
            $('#ordersRecurringDetailsBackBtn').click(function(){
                $('#diag').load('{$url}');
            });
        });
        </script>
HTML;

        echo $html;
        $this->renderPartial('_editOrder', ['from' => $from, 'data' => $data, 'returnTypes' => $returnTypes, 'paymentTypes' => $paymentTypes, 'recurringOrder' => 'true']);
    }

    public function actionSearchProducts($outletId, $type = 'order', $manufacturerId = '0')
    {
        $query = $_POST['q'];
        $productsOrders = (isset($_POST['o'])) ? $_POST['o'] : '';
        $productsReturns = (isset($_POST['r'])) ? $_POST['r'] : '';
        $model = Products::model()->getActiveProductsDropdown($query, $type, $outletId, null, $productsOrders, $productsReturns, $manufacturerId);

        $productList = [];
        foreach($model as $m){
            $productList[] = [
                'id' => $m['id'],
                'value' => $m['description'],
                'label'=> $m['description'] . ' (' . $m['batch_code'] . ')',
                'batch_code' => $m['batch_code'],
                'expiry_date' => $m['expiry_date'],
                'price' => $m['price'],
                'parent_product_units' => $m['parent_product_units']
            ];
        }

        $json = json_encode($productList);

        echo $json;
    }

    public function actionReturnOrderedRow($productId = null, $outletId = null, $return = false)
    {
        $productId = isset($_POST['productId']) ? $_POST['productId'] : $productId;
        $outletId = isset($_POST['outletId']) ? $_POST['outletId'] : $outletId;
        $sql = <<<SQL
        SELECT
        p.id AS product_id,
        CONCAT('[', p.sku , '] ', uom.description, ' - ', p.description, ' @ ') AS ordered_product,
        fn_checkPricing(p.price, o.id, o.outlet_price_group_id, p.id) AS discounted_price,
        fn_checkBasePrice(p.price, o.outlet_price_group_id, o.id, p.id) AS original_price,
        (fn_checkBasePrice(p.price, o.outlet_price_group_id, o.id, p.id) - fn_checkPricing(p.price, o.id, o.outlet_price_group_id, p.id)) as discount,
        p.vat as ordered_item_vat_percentage
        FROM
        outlet o
        INNER JOIN
        products p ON p.bakery_id = o.bakery_id
        INNER JOIN
        units_of_measure uom ON uom.id = p.units_of_measure_id
        WHERE
        o.id = {$outletId}
        AND p.id = {$productId};
SQL;
        $orders = Yii::app()->db->createCommand($sql)->queryRow();
        if($return)
        {
            return $orders;
        }

        echo $this->renderPartial('_loadOrderedRow', ['orders' => $orders], true);
    }

    public function actionReturnReturnedRow()
    {
        $productId = $_POST['productId'];
        $outletId = $_POST['outletId'];
        $sql = <<<SQL
        SELECT
        p.id AS product_id,
        ROUND(fn_checkPricing(p.price, o.id, o.outlet_price_group_id, p.id), 2) AS base_price,
        CONCAT('[', p.sku , '] ', uom.description, ' - ', p.description, ' @ ', ROUND(fn_checkPricing(p.price, o.id, o.outlet_price_group_id, p.id), 2)) AS returned_product,
        ROUND(fn_checkPricing(p.price, o.id, o.outlet_price_group_id, p.id), 2) AS price,
        p.vat
        FROM
        outlet o
        INNER JOIN
        products p ON p.bakery_id = o.bakery_id
        INNER JOIN
        units_of_measure uom ON uom.id = p.units_of_measure_id
        WHERE
        o.id = {$outletId}
        AND p.id = {$productId};
SQL;
        $returns = Yii::app()->db->createCommand($sql)->queryRow();

        $returnTypes = ReturnTypes::returnTypesList();

        echo $this->renderPartial('_loadReturnedRow', ['returns' => $returns, 'returnTypes' => $returnTypes], true);
    }

    public function actionEditOrderPreview($from)
    {
        set_time_limit(0);
        $orderId = $_POST['order_id'];
        $recurringOrder = (isset($_POST['recurring_order'])) ? $_POST['recurring_order'] : 'false';
        $orderMain = 'Order_main';
        if($recurringOrder == 'true')
        {
            $orderMain = 'OrderMainRecurring';
        }
        //get the original order from order_main
        $originalOrder = $orderMain::model()->findByPk($orderId);
        $deviceOrderId = $originalOrder->device_order_id;
        $poDocumentNumberPrefix = $originalOrder->po_document_prefix;
        $poDocumentNumber = $originalOrder->po_document_number;
        $outletId = $_POST['outlet_id'];
        $bakeryId = $_POST['bakery_id'];
        $expectedDeliveryDate = $_POST['expected_delivery_date'];
        $paymentTypeId = $_POST['payment_type_id'];
        $purchaseOrderNr = $_POST['purchase_order_nr'];
        $comments = $_POST['comments'];

        $grossOrderTotalAmount = 0.00;
        $vatValue = 0.00;
        $updatedDate = date('Y-m-d H:i:s');
        $updatedBy = Yii::app()->user->id;
        $manufacturerLimit = CustomUtils::getManufacturerLimitWhere($originalOrder->manufacturer_id);
        if(isset($_POST['ordered']))
        {
            $ordered = $_POST['ordered'];

            $linked = [];
            foreach($ordered as $productId => $detail)
            {
                $sql = <<<SQL
                SELECT
                    l.linked_product_id
                FROM
                    products_linked l
                        INNER JOIN
                    products p ON p.id = l.linked_product_id
                WHERE
                    l.product_id = {$productId} AND l.status_id = 1
                        AND p.status_id = 1
                        {$manufacturerLimit};
SQL;
                $linkedProducts = Yii::app()->db->createCommand($sql)->queryColumn();
                foreach($linkedProducts as $linkedProduct)
                {
                    if(!isset($linked[$linkedProduct]))
                    {
                        $linked[$linkedProduct] = (int)$detail['qty'];
                    }
                    else
                    {
                        $linked[$linkedProduct] += (int)$detail['qty'];
                    }
                }
            }

            foreach($ordered as $productId => $detail)
            {
                if(isset($linked[$productId]))
                {
                    $linkedQty = $linked[$productId];
                    if($detail['qty'] < $linkedQty)
                    {
                        $ordered[$productId]['qty'] = $linkedQty;
                    }
                }
            }

            foreach($linked as $pId => $qty)
            {
                if(!isset($ordered[$pId]))
                {
                    $linkedOrder = $this->actionReturnOrderedRow($pId, $outletId, true);
                    $ordered[$pId] = [
                        'qty' => $qty,
                        'product_id' => $linkedOrder['product_id'],
                        'price' => $linkedOrder['discounted_price'],
                        'gross_price' => $linkedOrder['original_price'],
                        'discount' => $linkedOrder['discount'],
                        'vat_percentage' => $linkedOrder['ordered_item_vat_percentage']
                    ];
                }
            }

            $dbFactoryOrder = new DBFactory();

            foreach($ordered as $key => $order)
            {
                if(is_int($key))
                {
                    $discountDetails = Products::productDiscountDetails($outletId, $order['product_id']);
                    $pricingEngineId = 0;
                    if(is_array($discountDetails))
                    {
                        $pricingEngineId = $discountDetails['id'];
                    }
                    $qty = $order['qty'];
                    $price = $order['price'];
                    $vatPercentage = $order['vat_percentage'];
                    $data = [
                        'order_id' => $orderId,
                        'product_id' => $order['product_id'],
                        'qty_ordered' => $qty,
                        'qty_picked' => $qty,
                        'price' => $price,
                        'gross_price' => $order['gross_price'],
                        'discount' => $order['discount'],
                        'pricing_engine_id' => $pricingEngineId,
                        'vat_percentage' => $vatPercentage,
                        'vat_value' => round(($qty*$price)*($vatPercentage/100), 2),
                        'stock_location_id' => 0,
                        'container_product_id' => 0,
                        'container_price' => 0.00,
                        'qty_picked_fcpo' => 0,
                        'qty_delivered_fcpo' => 0,
                        'updated_date' => $updatedDate,
                        'updated_by' => $updatedBy
                    ];
                    $dbFactoryOrder->createExtendedInsertValues($data);

                    $grossOrderTotalAmount += $qty*$price;
                    $vatValue += round(($qty*$price)*($vatPercentage/100), 2);
                }
            }

            $dbFactoryOrder->createExtendedInsertStatement('orders_preview');
        }

        if(isset($_POST['returned']))
        {
            $returned = $_POST['returned'];

            $dbFactoryReturn = new DBFactory();

            foreach($returned as $key => $return)
            {
                if(is_int($key))
                {
                    $qty = $return['qty'];
                    $price = $return['price'];
                    $vatPercentage = $return['vat'];
                    $data = [
                        'order_id' => $orderId,
                        'return_types_id' => $return['reason'],
                        'product_id' => $return['product_id'],
                        'qty' => $qty,
                        'price' => $price,
                        'vat_percentage' => $vatPercentage,
                        'vat_value' => round(($qty*$price)*($vatPercentage/100), 2),
                        'updated_date' => $updatedDate,
                        'updated_by' => $updatedBy
                    ];
                    $dbFactoryReturn->createExtendedInsertValues($data);

                    $grossOrderTotalAmount -= $qty*$price;
                    $vatValue -= round(($qty*$price)*($vatPercentage/100), 2);
                }
            }

            $dbFactoryReturn->createExtendedInsertStatement('orders_returns_preview');
        }
        else
        {
            $dbFactoryReturn = new DBFactory();
            $dbFactoryReturn->createExtendedInsertStatement('orders_returns_preview', 500, true);
        }

        $invoiceDetails = Products::invoiceDiscountDetails($outletId);
        $invoiceDiscountValue = 0;
        $invoiceDiscount = '%0';
        $pricingEngineDiscountId = 0;
        $discountedTotal = $grossOrderTotalAmount;
        if(is_array($invoiceDetails))
        {
            $pricingEngineDiscountId = $invoiceDetails['id'];
            $invoiceDiscount = $invoiceDetails['discount_type_description'] . $invoiceDetails['value'];
            $invoiceDiscountValue = $grossOrderTotalAmount*($invoiceDetails['value']/100);
            $discountedTotal = $grossOrderTotalAmount - $invoiceDiscountValue;

            $vatValue -= $vatValue*($invoiceDetails['value']/100);
        }
        $orderTotalAmount = round($discountedTotal + $vatValue, 2);
        $dbFactoryMain = new DBFactory();
        $data = [
            'id' => $orderId,
            'account' => $outletId,
            'bakery_id' => $bakeryId,
            'payment_ref' => $originalOrder->payment_ref,
            'branch_company_id' => $originalOrder->branch_company_id,
            'purchase_order_nr' => $purchaseOrderNr,
            'external_order_id' => $originalOrder->external_order_id,
            'device_order_id' => $deviceOrderId,
            'po_document_prefix' => $poDocumentNumberPrefix,
            'po_document_number' => $poDocumentNumber,
            'comments' => $comments,
            'contact_email' => $originalOrder->contact_email,
            'order_taker_employee_nr' => $originalOrder->order_taker_employee_nr,
            'order_date' => $originalOrder->order_date,
            'expected_delivery_date' => $expectedDeliveryDate,
            'order_total_amount' => $orderTotalAmount,
            'gross_order_total_amount' => $grossOrderTotalAmount,
            'vat_value' => $vatValue,
            'order_status_id' => 1,
            'discount' => $invoiceDiscount,
            'discount_value' => $invoiceDiscountValue,
            'payment_type_id' => $paymentTypeId,
            'order_type' => 1,
            'pricing_engine_discount_id' => $pricingEngineDiscountId,
            'linked_to_van_sale' => 0,
            'updated_date' => $updatedDate,
            'updated_by' => $updatedBy,
            'high_risk' => $originalOrder->high_risk,
            'manufacturer_id' => $originalOrder->manufacturer_id,
            'deposit_amount' => $originalOrder->deposit_amount,
            'deposit_vat' => $originalOrder->deposit_vat,
        ];

        $dbFactoryMain->createExtendedInsertValues($data);
        $dbFactoryMain->createExtendedInsertStatement('order_main_preview');

        $orderMain = new Order_main();
        $orderMain->recordOrderData($orderId, 'orders_preview', 'orders_returns_preview');

        $data = MailFactory::getPurchaseOrderDetails($orderId, true, true, $recurringOrder);
        echo $this->renderPartial('_previewOrder', ['from' => $from, 'data' => $data, 'recurringOrder' => $recurringOrder], true);
    }

    public function actionEditOrderPreviewSubmit($from)
    {
        $orderId = $_POST['order_id'];
        $recurringOrder = $_POST['recurring_order'];
        $tableSuffix = '';
        if($recurringOrder == 'true')
        {
            $tableSuffix = '_recurring';
        }
        $con = Yii::app()->db;

        //get the quantities to subtract
        $qtySql = <<<SQL
        select
        op.product_id,
        op.qty_ordered as qty,
        op.price
        from
        orders_preview op
        where
        op.order_id = {$orderId};
SQL;
        $quantities = $con->createCommand($qtySql)->queryAll();

        $deleteMainSql = <<<SQL
        DELETE FROM order_main{$tableSuffix} WHERE id = :orderId;
SQL;
        $deleteOrdersSql = <<<SQL
        DELETE FROM orders{$tableSuffix} WHERE order_id = :orderId;
SQL;
        $deleteOrdersReturnsSql = <<<SQL
        DELETE FROM orders_returns{$tableSuffix} WHERE order_id = :orderId;
SQL;

        $deleteComMain = $con->createCommand($deleteMainSql);
        $deleteComMain->bindValue(':orderId', $orderId, PDO::PARAM_INT);
        $deleteComMain->execute();

        $deleteComOrders = $con->createCommand($deleteOrdersSql);
        $deleteComOrders->bindValue(':orderId', $orderId, PDO::PARAM_INT);
        $deleteComOrders->execute();

        $deleteComOrdersReturns = $con->createCommand($deleteOrdersReturnsSql);
        $deleteComOrdersReturns->bindValue(':orderId', $orderId, PDO::PARAM_INT);
        $deleteComOrdersReturns->execute();

        $orderMainInsertSql = <<<SQL
        INSERT INTO order_main{$tableSuffix} SELECT * FROM order_main_preview WHERE id = :orderId;
SQL;
        $ordersInsertSql = <<<SQL
        INSERT INTO orders{$tableSuffix}
        (`order_id`,`product_id`,`qty_ordered`,`qty_picked`,`qty_picked_fcpo`,`qty_delivered`,`qty_delivered_fcpo`,`discount`,`price`,`gross_price`,`container_product_id`,`container_price`,`stock_location_id`,`pricing_engine_id`,`vat_percentage`,`vat_value`,`exchange_rate`,`converted_price`,`updated_date`,`updated_by`, product_sku, product_description, product_batch_code, product_expiry_date, uom_description)
        SELECT
        `order_id`,
        `product_id`,
        `qty_ordered`,
        `qty_picked`,
        `qty_picked_fcpo`,
        `qty_delivered`,
        `qty_delivered_fcpo`,
        `discount`,
        `price`,
        `gross_price`,
        `container_product_id`,
        `container_price`,
        `stock_location_id`,
        `pricing_engine_id`,
        `vat_percentage`,
        `vat_value`,
        `exchange_rate`,
        `converted_price`,
        `updated_date`,
        `updated_by`,
        product_sku, product_description, product_batch_code, product_expiry_date, uom_description
    FROM `orders_preview`
    WHERE order_id = :orderId;

SQL;
        $ordersReturnsInsertSql = <<<SQL
        INSERT INTO orders_returns{$tableSuffix}
        (`order_id`,`return_types_id`,`product_id`,`qty`,`price`,`container_product_id`,`container_price`,`qty_picked`,`qty_collected`,`vat_percentage`,`vat_value`,`exchange_rate`,`converted_price`,`updated_date`,`updated_by`, product_sku, product_description, product_batch_code, product_expiry_date, uom_description)
        SELECT
            `order_id`,
            `return_types_id`,
            `product_id`,
            `qty`,
            `price`,
            `container_product_id`,
            `container_price`,
            `qty_picked`,
            `qty_collected`,
            `vat_percentage`,
            `vat_value`,
            `exchange_rate`,
            `converted_price`,
            `updated_date`,
            `updated_by`,
            product_sku, product_description, product_batch_code, product_expiry_date, uom_description
        FROM `orders_returns_preview`
        WHERE order_id = :orderId;
SQL;

        $insertComMain = $con->createCommand($orderMainInsertSql);
        $insertComMain->bindValue(':orderId', $orderId, PDO::PARAM_INT);
        $insertComMain->execute();

        $insertComOrders = $con->createCommand($ordersInsertSql);
        $insertComOrders->bindValue(':orderId', $orderId, PDO::PARAM_INT);
        $insertComOrders->execute();

        $insertComOrdersReturns = $con->createCommand($ordersReturnsInsertSql);
        $insertComOrdersReturns->bindValue(':orderId', $orderId, PDO::PARAM_INT);
        $insertComOrdersReturns->execute();

        //get the trip sheet id to update the trip_sheets_stock table
        $tripSheetId = TripSheetsItems::model()->find('order_id = :orderId', [':orderId' => $orderId])->trip_sheet_id;
        if($from === 'planned')
        {
            //update the stock
            foreach($quantities as $qty)
            {
                $insertStockQtySql = <<<SQL
                insert into trip_sheets_stock (trip_sheet_id, product_id, planned_qty, accepted_qty, opening_qty, variance_id, product_indicator, opening_qty_product_price)
                values (:tripSheetId, :productId, :pQty, 0, :oQty, 0, 1, :oQtyProductPrice);
SQL;
                $insertStockQtyCom = $con->createCommand($insertStockQtySql);
                $insertStockQtyCom->bindValue(':tripSheetId', $tripSheetId, PDO::PARAM_INT);
                $insertStockQtyCom->bindValue(':productId', $qty['product_id'], PDO::PARAM_INT);
                $insertStockQtyCom->bindValue(':pQty', $qty['qty'], PDO::PARAM_INT);
                $insertStockQtyCom->bindValue(':oQty', $qty['qty'], PDO::PARAM_INT);
                $insertStockQtyCom->bindValue(':oQtyProductPrice', $qty['price'], PDO::PARAM_STR);

                try{
                    $insertStockQtyCom->execute();
                }catch(CDbException $e){
                    $qtyUpdateSql = <<<SQL
                    update trip_sheets_stock set opening_qty = {$qty['qty']} where product_id = {$qty['product_id']} and trip_sheet_id = {$tripSheetId} and product_indicator = 1;
SQL;
                    $stockUpdated = $con->createCommand($qtyUpdateSql)->execute();
                }

                CustomUtils::addEmptyParentChildTripSheetStock($qty['product_id'], $tripSheetId);
            }
        }

            //lastly update order_main order_status_id = 2;
            $con->createCommand('update order_main set order_status_id = 2 where id = ' . $orderId)->execute();

            CustomUtils::updateTripSheetValues($tripSheetId, 'opening_qty_product_price', 'opening_qty_value', 'opening_qty', true);
    }

    public function actionRepostToNav($orderId)
    {
        $con = Yii::app()->db;
        $sql = <<<SQL
        select account from order_main where id = :a
SQL;
        $command = $con->createCommand($sql);
        $command->bindParam(':a', $orderId);

        $outletId = $command->queryScalar();

        $includePath = str_replace('protected', '', Yii::app()->basePath) . 'schweppes_ws' . DIRECTORY_SEPARATOR . 'chr' . DIRECTORY_SEPARATOR;
        require_once($includePath . 'init.php');
        spl_autoload_register('my_autoloader');
        $preSalesOrders = new \mac\components\PreSalesOrders($orderId, $outletId);
        if($preSalesOrders->getResult())
        {
            $preSalesOrders->doPost();
        }

        $sql = <<<SQL
        select status, reason from soap_response where order_id = :a order by id desc limit 1;
SQL;
        $command = $con->createCommand($sql);
        $command->bindParam(':a', $orderId);

        $result = $command->queryRow();

        $reload = '';
        if($result['status'] == 'ERROR')
        {
            $reason = \mac\framework\FieldFormatter::format($result['reason'], FIELD_FORMATTER_NAV_ERROR);
            $what = 'danger';
            $html = <<<HTML
            <div class="alert alert-{$what}">{$reason}</div>
            {$reload}
HTML;
        }
        else
        {
            $what = 'success';
            $reload = <<<HTML
            <script type="text/javascript">
                $(document).ready(function(){
                    setTimeout(function(){ location.reload(); }, 3000);
                });
            </script>
HTML;
            $html = <<<HTML
            <div class="alert alert-{$what}">Successful, please wait...</div>
            {$reload}
HTML;
        }

        echo $html;
    }

    public function actionShowSAPDeltaResponse($id)
    {
        $sql = <<<SQL
        select sap_sales_document, status_code, status_message from delta_order where id = :id
SQL;
        $con = Yii::app()->db;
        $com = $con->createCommand($sql);
        $com->bindValue(':id', $id, PDO::PARAM_INT);
        $result = $com->queryRow();
        $sapSalesDocument = $result['sap_sales_document'];
        $statusMessage = $result['status_message'];
        $statusCode = $result['status_code'];
        if($statusCode == '200')
        {
            $statusMessage .= '<br><span class="bold">SAP Sales Document #: </span><span class="italic">'.$sapSalesDocument.'</span>';
        }
        $icon = 'fa fa-check';
        $color = 'success';
        if($statusCode == '-1')
        {
            $icon = 'fa fa-close';
            $color = 'danger';
        }

        $html = <<<HTML
        <div class="center">
            <span class="{$icon} text-{$color}" style="font-size: 72px;"></span>
            <div class="alert alert-{$color}">{$statusMessage}</div>
        </div>
HTML;
        echo $html;
    }
}
