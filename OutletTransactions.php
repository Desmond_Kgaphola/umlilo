<?php

/**
 * This is the model class for table "outlet_transactions".
 *
 * The followings are the available columns in table 'outlet_transactions':
 * @property integer $id
 * @property integer $bakery_id
 * @property string $outlet_id
 * @property string $order_id
 * @property string $description
 * @property string $debit_amount
 * @property string $credit_amount
 * @property string $created
 * @property integer $create_employee_id
 * @property integer $payment_type_id
 * @property integer $payment_allocation_status_id
 */
class OutletTransactions extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'outlet_transactions';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('outlet_id, description, created', 'required'),
            array('bakery_id, create_employee_id', 'numerical', 'integerOnly' => true),
            array('payment_type_id', 'length', 'max' => 11),
            array('payment_type_id_new', 'length', 'max' => 11),//---DESMOND
            array('description', 'length', 'max' => 155),
            array('debit_amount, credit_amount', 'length', 'max' => 11),
            array('payment_allocation_status_id, payment_allocation_status_id', 'length', 'max' => 11),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, bakery_id, outlet_id, order_id, description, debit_amount, credit_amount, created, create_employee_id', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'bakery'      => array(self::BELONGS_TO, 'Bakery', 'bakery_id'),
            'outlet'      => array(self::BELONGS_TO, 'Outlet', 'outlet_id'),
            'paymentType' => array(self::BELONGS_TO, 'PaymentType', 'payment_type_id')
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id'                           => 'ID',
            'bakery_id'                    => 'Bakery',
            'outlet_id'                    => 'Outlet',
            'order_id'                     => 'Order',
            'payment_type_id'              => 'Current Payment Type',
            'payment_type_id_new'          => 'Payment Type',//---DESMOND
            'description'                  => 'Description',
            'debit_amount'                 => 'Debit Amount',
            'credit_amount'                => 'Credit Amount',
            'created'                      => 'Created',
            'create_employee_id'           => 'Create Employee',
            'payment_allocation_status_id' => 'Payment Allocation Status'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.
        $criteria = new CDbCriteria;
        $criteria->compare('id', $this->id);
        $criteria->compare('bakery_id', $this->bakery_id);
        $criteria->compare('outlet_id', $this->outlet_id, true);
        $criteria->compare('order_id', $this->order_id, true);
        $criteria->compare('payment_type_id', $this->payment_type_id, true);
        $criteria->compare('payment_type_id_new', $this->payment_type_id_new, true);//--- DESMOND
        $criteria->compare('description', $this->description, true);
        $criteria->compare('debit_amount', $this->debit_amount, true);
        $criteria->compare('credit_amount', $this->credit_amount, true);
        $criteria->compare('created', $this->created, true);
        $criteria->compare('create_employee_id', $this->create_employee_id);
        $criteria->compare('payment_allocation_status_id', $this->payment_allocation_status_id);
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return OutletTransactions the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function insertTransaction($orderId, $data)
    {
        $bakery_id = (isset($data->bakery_id)) ? $data->bakery_id : "";
        $outletId = (isset($data->outlet_id)) ? $data->outlet_id : "";
        // This differs depending if it is comming from submitDelivery or saveOrder method from the API
        if(isset($data->payment_type))
        {
            $payment_type_id = $data->payment_type;
        }
        elseif(isset($data->payment_method_id))
        {
            $payment_type_id = $data->payment_method_id;
        }
        else
        {
            $payment_type_id = -1;
        }
        $order_total_amount = (isset($data->order_total_amount)) ? $data->order_total_amount : 0.00;
        $order_total_amount = round($order_total_amount, 2, PHP_ROUND_HALF_DOWN);
        $order_taker_employee_nr = (isset($data->employee_id)) ? $data->employee_id : "";
        // This differs depending if it is comming from submitDelivery or saveOrder method from the API
        if(isset($data->cashcollected))
        {
            $cash_collected = $data->cashcollected;
        }
        elseif(isset($data->cash_collected))
        {
            $cash_collected = $data->cash_collected;
        }
        else
        {
            $cash_collected = 0.00;
        }
        $ref = 'Order #' . addslashes($orderId);
        if(isset($data->order_status))
        {
            if($data->order_status == 13)
            {
                $fromBackendRef = '';
                if(isset($data->tripsheetId) && $data->tripsheetId === 0)
                {
                    $fromBackendRef = ' - ' . addslashes($data->comments);
                }
                $ref = 'Credit Note #' . addslashes($orderId) . $fromBackendRef;
                $creditAmount = $order_total_amount;
                $order_total_amount = $cash_collected;
                $cash_collected = $creditAmount;
            }
        }
        $connection = Yii::app()->db;
        $sql = "INSERT INTO
					`outlet_transactions` 
					(
						`bakery_id`, 
						`outlet_id`, 
						`order_id`, 
						`description`,
						`payment_type_id`,
						`debit_amount`, 
						`credit_amount`,
						`create_employee_id`,
						`payment_allocation_status_id`
					) 
					VALUES 
					(
						:bakeryId,
						:outletId,
						:orderId,
						:ref,
						:paymentTypeId,
						:orderTotalAmount,
						:cashCollected,
						:orderTakerEmployeeId,
						'0'
					)";
        $command = $connection->createCommand($sql)
                ->bindValue(':bakeryId', addslashes($bakery_id), PDO::PARAM_INT)
                ->bindValue(':outletId', addslashes($outletId), PDO::PARAM_INT)
                ->bindValue(':orderId', addslashes($orderId), PDO::PARAM_INT)
                ->bindValue(':ref', $ref, PDO::PARAM_STR)
                ->bindValue(':paymentTypeId', addslashes($payment_type_id), PDO::PARAM_INT)
                ->bindValue(':orderTotalAmount', addslashes($order_total_amount), PDO::PARAM_STR)
                ->bindValue(':cashCollected', addslashes($cash_collected), PDO::PARAM_STR)
                ->bindValue(':orderTakerEmployeeId', addslashes($order_taker_employee_nr), PDO::PARAM_INT);
        //$command->execute();
        if($command->execute())
        {
            // VAN SALES auto allocate
            if(isset($data->order_status))
            {
                if($data->order_status == 5)//only allocate on delivery if cash collected is equal to the order total amount
                {
                    if(floatval($order_total_amount) == floatval($cash_collected))
                    {
                        $_POST['id'] = Yii::app()->db->getLastInsertID();
                        $_POST['credit_amount'] = floatval($cash_collected);
                        $_POST['debit_amount'] = floatval($order_total_amount);
                        $outletTransactionResponse = OutletTransactions::savePaymentAllocation($outletId);
                    }
                }
            }
            // PRE SALES auto allocate
            if($data->Type == 'submitDelivery')//only allocate on delivery if cash collected is equal to the order total amount
            {
                $bakery = Bakery::model()->findByPk($bakery_id);
                if($bakery->auto_allocate_transactions == '1')
                {
                    //hier moet die check wees om te se of ons moet auto allocate of nie
                    if(floatval($order_total_amount) == floatval($cash_collected))
                    {
                        $_POST['id'] = Yii::app()->db->getLastInsertID();
                        $_POST['credit_amount'] = floatval($cash_collected);
                        $_POST['debit_amount'] = floatval($order_total_amount);
                        $outletTransactionResponse = OutletTransactions::savePaymentAllocation($outletId);
                    }
                }
            }
            
        }
        // Update outlet available credit
        $outletCredit = new OutletCredit();
        return $outletCredit->updateAvailableCredit($outletId);
    }

    public function getOutletIdFromPK($PK)
    {
        $sql = "SELECT
					outlet_id
				FROM 
					outlet_transactions 
				WHERE 
					id = :outletId";
        $stmt = Yii::app()->db->createCommand($sql)
                ->bindValue(':outletId', addslashes($PK), PDO::PARAM_INT);
        return $stmt->queryScalar();
    }

    public function getOutletBalance($outletId)
    {
        $sql = "SELECT
					COALESCE(SUM(`credit_amount`) - SUM(`debit_amount`),0) AS balance
				FROM
					outlet_transactions
				WHERE
					outlet_id = :outletId AND
					payment_allocation_status_id  = 1";
        $stmt = Yii::app()->db->createCommand($sql)
                ->bindValue(':outletId', addslashes($outletId), PDO::PARAM_INT);
        return $stmt->queryScalar();
    }

    public static function doAgaAnalysisLogic($outletId, $debitAmount, $creditAmount)
    {
        $payment_type=Outlet::getPaymentType($outletId);
        $con = Yii::app()->db;
        $termSql = <<<SQL
        select available_credit, payment_terms_days from outlet_credit where outlet_id = :a
SQL;
        $command = $con->createCommand($termSql);
        $command->bindParam(':a', $outletId, PDO::PARAM_INT);
        $term = $command->queryRow();
        if(empty($term))
        {
            $termInsertSql = <<<SQL
            insert into outlet_credit (outlet_id, available_credit, credit_limit, payment_terms_days, block, created_by, created_date) 
                values (:outletId, 0, 0, :paymentType, 0, 0, NOW())
SQL;
            $con->createCommand($termInsertSql)
                    ->bindValue(':outletId', $outletId, PDO::PARAM_INT)
                    ->bindValue(':paymentType', $payment_type, PDO::PARAM_INT)
                    ->execute();
            $term = $command->queryRow();
        }
        $availableBalance = floatval($term['available_credit']);
        $amountDue = 0;
        $debitMinusCredit = $debitAmount - $creditAmount;
        if($debitMinusCredit > 0)
        {
            $amountDue = $debitMinusCredit;
        }
        if($availableBalance > 0)
        {
            if($amountDue > $availableBalance)
            {
                $amountDue = ($availableBalance - $amountDue) * (-1);
            }
            else
            {
                $amountDue = 0;
            }
        }
        $allocatedDate = date('Y-m-d H:i:s');
        $dueDate = null;
        if($amountDue > 0)
        {
            $dueDate = date('Y-m-d', strtotime('+' . $term['payment_terms_days'] . ' days'));
        }
        $offset = $amountDue;
        $userId = CustomUtils::getCurrentLoggedInUserID();
        if($creditAmount > 0)
        {
            $offsetBalance = floatval($creditAmount);
            $prevSql = <<<SQL
            select * from outlet_transactions where outlet_id = :a and allocated_date <= :b and offset > 0;
SQL;
            $command = $con->createCommand($prevSql);
            $command->bindParam(':a', $outletId, PDO::PARAM_INT);
            $command->bindParam(':b', $allocatedDate, PDO::PARAM_STR);
            $prev = $command->queryAll();
            if(count($prev) > 0)
            {
                foreach($prev as $item)
                {
                    if($offsetBalance > 0)
                    {
                        if(floatval($item['offset']) <= $offsetBalance)
                        {
                            $sql = <<<SQL
                    update outlet_transactions set offset = 0, due_date = null where id = :a
SQL;
                            $command = $con->createCommand($sql);
                            $command->bindParam(':a', $item['id'], PDO::PARAM_INT);
                            $command->execute();

//                            $offsetBalance -= $item['offset'];
                        }
                        else
                        {
                            $newDueDate = date('Y-m-d', strtotime('+' . $term['payment_terms_days'] . ' days'));
                            $remaining = floatval($item['offset']) - $offsetBalance;
                            $sql = <<<SQL
                    update outlet_transactions set offset = :a where id = :b
SQL;
                            $command = $con->createCommand($sql);
                            $command->bindParam(':a', $remaining, PDO::PARAM_STR);
//                            $command->bindParam(':b', $newDueDate, PDO::PARAM_STR);
                            $command->bindParam(':b', $item['id'], PDO::PARAM_INT);
                            $command->execute();
                        }
                        $offsetBalance -= $item['offset'];
                    }
                    if($offsetBalance <= 0)
                    {
                        break;
                    }
                }
            }
            if($offsetBalance > 0 && $debitAmount > $offsetBalance)
            {
                $dueDate = date('Y-m-d', strtotime('+' . $term['payment_terms_days'] . ' days'));
                $offset = $debitAmount - $offsetBalance;
            }
            //when not all prior debt has been settled
            if($offsetBalance <= 0)
            {
                $amountDue = $debitAmount;
                $offset = $amountDue;
            }
        }
        //check whether we need to unblock the outlet if all offsets prior to today are zero
        $unblockSql = <<<SQL
        SELECT
            ot.outlet_id, SUM(ot.`offset`) AS due_amount
        FROM
            outlet_transactions ot
        WHERE
            due_date < NOW() AND ot.outlet_id = :a
        GROUP BY ot.outlet_id
        HAVING SUM(ot.`offset`) > 0;
SQL;
        $command = $con->createCommand($unblockSql);
        $command->bindParam(':a', $outletId, PDO::PARAM_INT);
        $unblock = $command->queryAll();
        if(empty($unblock))
        {
            $updateSql = <<<SQL
            UPDATE outlet_credit set block = 0 where outlet_id = :a
SQL;
            $command = $con->createCommand($updateSql);
            $command->bindParam(':a', $outletId, PDO::PARAM_INT);
            $updated = $command->execute();
        }
        $ret = [
            'term'           => $term['payment_terms_days'],
            'due_amount'     => $amountDue,
            'due_date'       => $dueDate,
            'offset'         => $offset,
            'allocated_date' => $allocatedDate,
            'user_id'        => $userId,
            'balance'        => $availableBalance
        ];
        return $ret;
    }

    public static function savePaymentAllocation($outletId)
    {
       // var_dump($_POST);
        if(!isset($_POST['credit_amount']) || !isset($_POST['debit_amount']))
        {
            $output = array(
                'id'               => 0,
                'available_credit' => 0,
                'error' => true,
                'msg' => 'Credit or Debit amount not set in POST'
            );
            return json_encode($output);
        }
        
        
        $creditAmount = (float)($_POST['credit_amount']);
        $debitAmount = (float)($_POST['debit_amount']);
        $details = self::doAgaAnalysisLogic($outletId, $debitAmount, $creditAmount);
        //---======='//
 
        $term = $details['term'];
        $amountDue = $details['due_amount'];
        $dueDate = (is_null($details['due_date'])) ? '' : 'due_date = :dueDate,';
        $offset = $details['offset'];
        $balance = $details['balance'];
        $allocatedDate = $details['allocated_date'];
        $userId = (is_null($details['user_id'])) ? 0 : $details['user_id'];
            // $payment_type = (isset($_POST['payment_type']) || $_POST['payment_type'] == 0) ? $details['payment_type'] : $_POST['payment_type'];
            if(isset($_POST['payment_type']) &&  $_POST['payment_type'] == 0)
            {
               // $payment_type_updated_by = 0;
                //$payment_type = $details['payment_type'];
                $sql = "UPDATE
                            `outlet_transactions`
                        SET
                            `credit_amount` = :creditAmount,
                            `debit_amount` = :debitAmount,
                            `payment_allocation_status_id` = '1',
                            term = :term,
                            due_amount = :amountDue,
                            $dueDate
                            offset = :offset,
                            balance_prior_to_allocation = :balance ,
                            allocated_date = :allocatedDate,
                            allocated_by = :userId
                        WHERE
                            `id` = :id";
                $rawDataCMD = Yii::app()->db->createCommand($sql);
                $rawDataCMD->bindValue(':creditAmount', addslashes($_POST['credit_amount']), PDO::PARAM_STR);
                $rawDataCMD->bindValue(':debitAmount', addslashes($_POST['debit_amount']), PDO::PARAM_STR);
                $rawDataCMD->bindValue(':term', $term, PDO::PARAM_STR);
                $rawDataCMD->bindValue(':amountDue', $amountDue, PDO::PARAM_STR);
            }
            else
            {
                $payment_type_updated_by = Yii::app()->user->id;
                $payment_type = $_POST['payment_type'];
                $sql = "UPDATE
                            `outlet_transactions`
                        SET
                            `credit_amount` = :creditAmount,
                            `debit_amount` = :debitAmount,
                            `payment_type_id` = :payment_type_id_new,
                            `payment_type_updated_by` = :payment_type_updated_by,
                            `payment_allocation_status_id` = '1',
                            term = :term,
                            due_amount = :amountDue,
                            $dueDate
                            offset = :offset,
                            balance_prior_to_allocation = :balance ,
                            allocated_date = :allocatedDate,
                            allocated_by = :userId
                        WHERE
                            `id` = :id";
                $rawDataCMD = Yii::app()->db->createCommand($sql);
                $rawDataCMD->bindValue(':creditAmount', addslashes($_POST['credit_amount']), PDO::PARAM_STR);
                $rawDataCMD->bindValue(':debitAmount', addslashes($_POST['debit_amount']), PDO::PARAM_STR);
                $rawDataCMD->bindValue(':payment_type_id_new',  $payment_type, PDO::PARAM_STR);
                $rawDataCMD->bindValue(':payment_type_updated_by',  $payment_type_updated_by, PDO::PARAM_STR);
                $rawDataCMD->bindValue(':term', $term, PDO::PARAM_STR);
                $rawDataCMD->bindValue(':amountDue', $amountDue, PDO::PARAM_STR);
            }
    
            //===========================//

        if(!is_null($details['due_date']))
        {
            $rawDataCMD->bindValue(':dueDate', $details['due_date'], PDO::PARAM_STR);                
        }
        $rawDataCMD->bindValue(':offset', $offset, PDO::PARAM_STR);
        $rawDataCMD->bindValue(':balance', $balance, PDO::PARAM_STR);
        $rawDataCMD->bindValue(':allocatedDate', $allocatedDate, PDO::PARAM_STR);
        $rawDataCMD->bindValue(':userId', $userId, PDO::PARAM_INT);
        $rawDataCMD->bindValue(':id', addslashes($_POST['id']), PDO::PARAM_INT);
        $rawDataCMD->query();
        // Get outlet id
        $outletTransactions = new OutletTransactions();
        if(isset($_POST['id']) && $_POST['id'] > 0)
        {
            $outletId = $outletTransactions->getOutletIdFromPK($_POST['id']); 
        }
        // Update outlet available credit
        $outletCredit = new OutletCredit();
        $availableCredit = $outletCredit->updateAvailableCredit($outletId);
        $output = array(
            'id'               => $_POST['id'],
            'available_credit' => number_format($availableCredit, 2, '.', '')
        );
        return json_encode($output);
    }
}
