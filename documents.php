
<?php
//$filename = 'C:\xampp\htdocs\sih\umlilo_git\images\api\PreSalesSignatures\1560424764806_2763_47';
////$data = base64_encode(file_get_contents('C:\Users\Christo\Pictures\tmp\FB_IMG_1516628730216.jpg'));
//$contents = base64_decode($data);
//$im = imagecreatefromstring($contents);
//if ($im !== false)
//{
//    imagejpeg($im, $filename);
//    imagedestroy($im);
//}
//else
//{
//    die('error');
//}
//die();
//?>

<form method="post" action="">
<table class="customFilterTable" id="date_filter_table">
    <tr>
        <td class="customTableFilterData">
            <span class="filterLabel">Customer</span>&nbsp;&nbsp;
            <input class="" type="text" id="customer" name="customer" value="<?php if(isset($_POST['customer']))echo $_POST['customer']; ?>" data-toggle="tooltip" title="Search by customer name, account # or order number" />
            &nbsp;&nbsp;&nbsp;
            <span class="filterLabel">From</span>&nbsp;&nbsp;
            <input class="filter_date" type="text" id="start_date" name="start_date" value="<?php echo $results['start_date']; ?>" />
            &nbsp;&nbsp;&nbsp;
            <span class="filterLabel">To</span>&nbsp;&nbsp;
            &nbsp;<input class="filter_date" type="text" id="end_date" name="end_date" value="<?php echo $results['end_date']; ?>" />
        </td>
        <td class="customTableFilterData right" style="vertical-align: bottom;"><button id="9" type="submit" name="filter" class="btn btn-primary btn-sm">Search</button></td>
    </tr>
 </table>
</form>



<table class="customTable">
<tbody>
<tr >
    <td class="customTableSubHeading fit">Creation</td>
    <td class="customTableSubHeading">Document</td>
    <td class="customTableSubHeading fit">Type</td>
    <td class="customTableSubHeading fit">Account #</td>
    <td class="customTableSubHeading">Customer</td>
    
    <td class="customTableSubHeading fit center">Action</td>
</tr>
<?php 
if(!empty($results['documents'])){
    // $type=$results['type'];
    foreach($results['documents'] as $doc):

?>
<tr id="sfd_call_cycle_<?php echo $doc['id'];?>">
  <td class="customTableData fit"><?php echo $doc['created_on'];?> </td>
  <td class="customTableData"><?php if($doc['type'] ==4){echo $doc['name'];}else{echo $doc['name'].' '.$doc['reference'];}?></td>
  <td class="customTableData fit"><?php if($doc['type'] ==4){ echo 'FORM';}else{echo $doc['name'];}?></td>
  <td class="customTableData fit"><?php echo $doc['account_number'];?></td>
   <td class="customTableData"><?php echo $doc['customer'];?></td>
  <td class="customTableData table_action fit center">
      <span class="send_attachment" data-order-id="<?php echo $doc['id'];?>" data-doc-type="<?php echo $doc['type']; ?>">
          <i class="fa fa-envelope-o fa-lg text-primary" data-toggle="tooltip" title='email this document'></i>
      </span>
      <?php
     // $orderStatusId = 4;
      $orderId = $doc['reference'];
      switch($doc['type']){
          case 1:
              $orderStatusId = 4;
              break;
          case 2:
              $orderStatusId = 5;
              break;
          case 3:
              $orderStatusId = $doc['order_status_id'];
              break;
          case 4:
              $orderStatusId = $doc['order_status_id'];
              break;
          default:
                $orderStatusId = 4;
              break;
      }
      if($doc['outlet_id'] == 0)
      {
          $orderStatusId = 0;
      }
      
       ?>
         <a href="<?php echo Yii::app()->getBaseUrl(true) . '/index.php/admin/distribution/showPDF/orderStatusId/' . $orderStatusId . '/orderId/' . FieldFormatter::format(FIELD_FORMATTER_CREATE_HASH_LINK, $orderId); ?>" target="_blank" class="fa fa-file-pdf-o fa-lg text-danger" data-toggle="tooltip" title="open pdf"></a>
   
    </td>
</tr>
<?php
endforeach;
}else{ 
?>
 <tr>
     <td class="customTableData" colspan="6">
        <div id="error_display" class="alert alert-danger">No documents found.</div>
    </td>
</tr>   
<?php    
}
?>
</tbody>
</table>



<script type='text/javascript'>
$(document).ready(function(){
   $("#diag").dialog({
        modal: true,
             buttons: {
              Close: function() {
                  $(this).dialog("close");
              }
           }
    });

    $("#diag").dialog();
    $("#diag").dialog( "option", "width", 800 );
    $("#diag").dialog( "option", "height", 550 );
    $("#diag").dialog("option", "position", "center");
    $("#diag").dialog("close");
});  

 $('.send_attachment').click(function(){
    var orderId = $(this).attr('data-order-id');
    var docType = $(this).attr('data-doc-type');
    //console.log(orderId);
    //alert(orderId);
    //alert(docType);
    $("#diag").html('Loading...');
    $("#diag").load('document/email/orderId/'+orderId+'/docType/'+docType,
                    function(response, status, xhr ){
                        if(status == 'error')
                        {
                            checkHttpStatus(response, xhr);
                        }
                    }
               );
    $("#diag").dialog('option', 'title', 'Email');
    $("#diag").dialog("open");
 });

 $('#send_attachment').click(function(){
    $("#diag").load('attachement/send/',
                    function(response, status, xhr ){
                          if ( status == "error" ) {
                                alert('Connection timed out after long inactivity! Please click OK and login again.');
                                location.reload(true);
                          }
                    }
               );
    $("#diag").dialog('option', 'title', 'Create Shipment');
    $("#diag").dialog("open");
 });
 
 var customers = [<?php echo $results['customers']; ?>];
 $("#customer").autocomplete({source:customers});

function showPDF(orderStatusId, orderId)
{
    window.open('distribution/showPDF/orderStatusId/' + orderStatusId + '/orderId/' + orderId, '_blank');
}
</script>
