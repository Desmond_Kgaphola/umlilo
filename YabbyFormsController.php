<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class YabbyFormsController extends Controller
{
    
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';
    public $checklistID = '';
    
    //public $layout = '//layouts/main';
    public function actionIndex()
    {
        $model = new Checklist('search');
        $model->unsetAttributes();  // clear any default values
        //showing active records by default
        $model->status_id = '';
        if(isset($_GET['Checklist']))
        {
            $model->attributes = $_GET['Checklist'];
        }
        $sql="SELECT  COUNT(c.id) AS numPending
FROM `checklist` c
LEFT JOIN `checklist_types` ct ON c.`checklist_type_id` = ct.id
WHERE c.status_id=2 AND ct.`exclude_on_form`=0 AND c.`request_approval_by` =:id";
        $numQry = Yii::app()->db->createCommand($sql);
        $numQry->bindValue(':id', Yii::app()->user->id, PDO::PARAM_INT);
        $numPending = $numQry->queryRow();
        $this->render('checklistManagement', array(
            'model' => $model,
            'numPending' =>$numPending
        ));
    }
    public function actionGetPendingList()
    {
        $sql="SELECT c.*
FROM `checklist` c
LEFT JOIN `checklist_types` ct ON c.`checklist_type_id` = ct.id
WHERE c.status_id=2 AND ct.`exclude_on_form`=0 AND c.`request_approval_by` =:id";
        $numQry = Yii::app()->db->createCommand($sql);
        $numQry->bindValue(':id', Yii::app()->user->id, PDO::PARAM_INT);
        $chkPending = $numQry->queryAll();
        $this->renderPartial('_allPendingChecklists', array(
            'chkPending' => $chkPending
        ));
    }
    public function actionPendingChecklists($oid = null)
    {
        $con = Yii::app()->db;
        $bakery=array();
        if($oid!=null)
        {
            $sql="select bakery_id from outlet where id=:id";
            $bakeryQry = $con->createCommand($sql);
            $bakeryQry->bindValue(':id', $oid, PDO::PARAM_INT);
            $bakery = $bakeryQry->queryAll();
            
        }
        $model = new Checklist('search');
        
        
        $model = new Checklist('search');
        if(isset($_GET['Checklist']))
        {
            $model->attributes = $_GET['Checklist'];
        }
        
        $this->renderPartial('_pendingChecklists', array(
            'models' => $model,'bakery_id'=>$bakery[0]['bakery_id']
        ));
    }
    
    public function actionRefuel()
    {
        $model = new Checklist('refuelSearch');
        $model->unsetAttributes();  // clear any default values
        //showing active records by default
        $model->status_id = 1;
        if(isset($_GET['Checklist']))
        {
            $model->attributes = $_GET['Checklist'];
        }
        $this->render('refuelForm', array(
            'model' => $model,
        ));
    }
    
    public function actionVehicleChecklist()
    {
        $model = new Checklist('vehicleChecklistSearch');
        $model->unsetAttributes();  // clear any default values
        //showing active records by default
        $model->status_id = 1;
        if(isset($_GET['Checklist']))
        {
            $model->attributes = $_GET['Checklist'];
        }
        $this->render('vehicleChecklist', array(
            'model' => $model
        ));
    }

    public function actionGetCurrentDayVehicleChecklistPDFs()
    {
        echo '<div class="alert alert-info">To be implement soon...</div>';
    }
    
    public function actionProcessVehicleChecklistImage()
    {
        if(Yii::app()->request->isPostRequest)
        {
            $parentFolder = 'images';
            $subFolder = "vehiclechecklistimages";
            $folder = $_SERVER['DOCUMENT_ROOT'] . Yii::app()->request->baseUrl . '/' . $parentFolder . '/' . $subFolder . '/';
            $imgUrl = "https://" . $_SERVER['HTTP_HOST'] . Yii::app()->request->baseUrl . '/' . $parentFolder . '/' . $subFolder . '/';
            foreach($_FILES['upload-image']['tmp_name'] as $index => $tmpName)
            {
                $name = basename($_FILES["upload-image"]["name"][$index]);
                $tmp_name = $_FILES["upload-image"]['tmp_name'][$index];
                if(move_uploaded_file($tmp_name, $folder . $name))
                {
                    $model = new VehicleChecklistImageUrl;
                    $model->vehicle_description = $_POST['vehicle-desc'][$index];
                    $model->image_url = $imgUrl . $name;
                    $model->created_by = Yii::app()->user->id;
                    $model->create_datetime = date('Y-m-d H:i:s');
                    $model->save();
                }
            }
        }
        echo 1;
    }
    
    public function actionViewChecklistImages($id)
    {
        $con = Yii::app()->db;
        $sql = <<<SQL
       SELECT
           c_i.id,
           c_i.checklist_image_path AS image_path,
           c_i.uploaded_date AS date_time,
           c_i.status_id,
           e.name AS employee_name,
           c.checklist_name,
           c.id AS `checklist_id`
       FROM
           checklist_images c_i
           JOIN checklist c ON c.id = c_i.checklist_id
           LEFT JOIN employee e ON e.id = c_i.uploaded_by
       WHERE checklist_id = :a AND c_i.status_id=1
SQL;
        $checklistImageQry = $con->createCommand($sql);
        $checklistImageQry->bindValue(':a', $id, PDO::PARAM_INT);
        $checklistImage = $checklistImageQry->queryAll();
        $checklist = ['media' => $checklistImage];
        $this->renderPartial('_viewImportChecklistImage', array('data' => $checklist));
    }
    
    public function actionImportChecklistImages($id)
    {
        if(Yii::app()->request->isPostRequest)
        {
            $parentFolder = 'images';
            $subFolder = "checklistimages";
            $folder = $_SERVER['DOCUMENT_ROOT'] . Yii::app()->request->baseUrl . '/' . $parentFolder . '/' . $subFolder . '/';
            $imgUrl = "https://" . $_SERVER['HTTP_HOST'] . Yii::app()->request->baseUrl . '/' . $parentFolder . '/' . $subFolder . '/';
            foreach($_FILES['upload-image']['tmp_name'] as $index => $tmpName)
            {
                $name = basename($_FILES["upload-image"]["name"][$index]);
                $tmp_name = $_FILES["upload-image"]['tmp_name'][$index];
                if(move_uploaded_file($tmp_name, $folder . $name))
                {
                    $model = new ChecklistImages;
                    $model->checklist_id = $id;
                    $model->checklist_image_name = $_POST['checklist-image-desc'][$index];
                    $model->checklist_image_path = $imgUrl . $name;
                    $model->uploaded_by = Yii::app()->user->id;
                    $model->uploaded_date = date('Y-m-d H:i:s');
                    $model->status_id = 1;
                    $model->save();
                }
            }
            echo "1";
        }
        else
        {
            $this->renderPartial('_importChecklistImage', array('id' => $id));
        }
    }
    
    public function actionListChecklistImages()
    {
        $model = new ChecklistImages('search');
        $model->unsetAttributes();
        if(isset($_GET['ChecklistImages']))
        {
            $model->attributes = $_GET['ChecklistImages'];
        }
        if(isset($_GET['success']) && $_GET['success'] === '1')
        {
            Yii::app()->user->setFlash('success', 'Checklist document(s) uploaded successfully');
        }
        $this->render('listchecklistImages', array('model' => $model));
    }
    
    public function actionDeleteGridviewChecklistImage($id)
    {
        if(!Yii::app()->request->isPostRequest)
        {
            $model = ChecklistImages::model()->findByPk($id);
            if($model->status_id == 1)
            {
                $model->status_id = 0;
            }
            else
            {
                $model->status_id = 1;
            }
            $model->save();
            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if(!isset($_GET['ajax']))
            {
                $this->redirect(array('listChecklistImages'));
            }
            
        }
        else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }
    
    public function actionDeleteChecklistImage($id)
    {
        $model = ChecklistImages::model()->findByPk($id);
        $model->status_id = 0;
        if($model->save())
        {
            echo "1";
        }
        else
        {
            echo "0";
        }
    }
    
    public function actionUploadChecklistImages()
    {
        $this->render('uploadChecklistImage');
    }
    
    public function actionYabbyForm()
    {
        if(isset($_GET['id']))
        {
            //            Yii::app()->clientScript->registerCoreScript('jquery');
            $yabby = new YabbyFormManager;
            //Get form details
            $form_data = $yabby->getFormName($_GET['id']);
            $additional_option_types = $yabby->getAdditionalOptionTypes($_GET['id']);
            $form_elements_two = $yabby->getAllFormElementsByFormId($_GET['id']);
            $form_elements = $yabby->getFormElementsByFormId($_GET['id']);
            //might end up deleting this function call
            $KpiSubGroup = $yabby->getKpiSubGroupDropdown();
            $KpiSubGroupTab = $yabby->getKpiSubGroupDropdownTab();
            
            if($form_data[0]['checklist_type_id'] == '1')
            {
                $page = array('index');
            }
            elseif($form_data[0]['checklist_type_id'] == '2')
            {
                $page = array('refuel');
            }
            elseif($form_data[0]['checklist_type_id'] == '3')
            {
                $page = array('vehicleChecklist');
            }
            elseif($form_data[0]['checklist_type_id'] >'4')
            {
                $page = array('index');
            }
            
            
            //echo "<pre>";var_dump($additional_option_types);die;
            $this->render('yabbyForm', array('form_data' => $form_data, 'form' => $form_elements_two, 'form_elements' => $form_elements, 'kpi_subs' => $KpiSubGroup, 'kpi_group' => $KpiSubGroupTab, 'page' => $page, 'additional_option_types' => $additional_option_types,));
            //$this->render('yabbyForm', array('form_data' => $form_data, 'form_elements' => $form_elements));
        }
        else
        {
            $this->actionIndex();
        }
    }
    
    public function actionProductYabbyForm($id)
    {
        if(isset($_GET['id']))
        {
            $page = array('ProductPriceChecklist');
            $yabby = new YabbyFormManager;
            //Get form details
            $form_data = $yabby->getFormName($_GET['id']);
            $additional_option_types = $yabby->getAdditionalOptionTypes($_GET['id']);
            $form_elements_two = $yabby->getAllFormElementsByFormId($_GET['id']);
            $form_elements = $yabby->getFormElementsByFormId($_GET['id']);
            //might end up deleting this function call
            $KpiSubGroup = $yabby->getKpiSubGroupDropdown();
            $KpiSubGroupTab = $yabby->getKpiSubGroupDropdownTab();
            $this->render('productYabbyForm', array('form_data' => $form_data, 'form' => $form_elements_two, 'form_elements' => $form_elements, 'kpi_subs' => $KpiSubGroup, 'kpi_group' => $KpiSubGroupTab, 'page' => $page, 'additional_option_types' => $additional_option_types));
        }
        else
        {
            $this->actionIndex();
        }
    }
    public function actionGetSelectedSubDropdown()
    {
        $yabby = new YabbyFormManager;
        $KpiSubGroup = $yabby->getKpiSubGroupDropdown();
    }
    
    public function actionChecklistManagement()
    {
        $brands = array();
        $this->render('checklistManagement', array('brands' => $brands));
    }
    private function doChecklistValidation($post,$id=0)
    {
        
        if(intval($post['Checklist']['status_id']) == 0) return;
        
        $whereParams = array(
            ':checklist_name' => $post['Checklist']['checklist_name'],
            ':status_id' => $post['Checklist']['status_id']
        );
        $extraWhereString = '';
        if($id>0)
        {
            $whereParams[':id'] =$id;
            $extraWhereString = ' AND id !=:id';
        }
        
        $hasRecord = Yii::app()->db->createCommand()
        ->select('count(*)')
        ->from('checklist')
        ->where('checklist_name=:checklist_name and status_id=:status_id' . $extraWhereString, $whereParams)
        ->queryScalar();
        
        if($hasRecord > 0)
        {
            Yii::app()->user->setFlash('error', 'A form like this already exists, please supply different parameters and try again');
            $this->redirect(array('index'));
        }
    }
    public function actionAddChecklist()
    {
        $model = new Checklist('create');
        $cat=ChecklistCategories::model()->getCategoryList($model);
        
        
        $condition = Yii::app()->user->bakery != 0 ? array('status_id' => 1, 'bakery_id' => Yii::app()->user->bakery) : array('status_id' => 1);
        $empGrp = EmployeeGroup::model()->findAll('status_id =1');
        $checklistTypes = ChecklistTypes::model()->findAll('status_id =1 and exclude_on_form=0');
        $employee = Employee::model()->findAllByAttributes($condition);
        $bakeryWhere = '';
        if(Yii::app()->user->role > 1)
        {
            $bakeryWhere = 'id = ' . Yii::app()->user->bakery;
        }
        else
        {
            $bakeryWhere .= CustomUtils::getEmployeeBakeryIds('id', '');
        }
        
        $bakeries = Bakery::model()->findAll($bakeryWhere);
        $selectedChecklistBakeries = array();
        $employeeListArray = array();
        $status=0;
        $comment='';
        foreach($empGrp as $grp)
        {
            foreach($employee as $emp)
            {
                if($emp['employee_group_id'] == $grp['id'])
                {
                    $employeeListArray[$grp['description']][] = array('id' => $emp['id'], 'name' => $emp['name']);
                }
            }
        }
        $schedule = ChecklistNotificationSchedule::model()->findAll('status_id =1');
        $selected_emp = array();
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);
        if(isset($_POST['Checklist']))
        {
            $this->doChecklistValidation($_POST);
            
            if((!isset( $_POST['bakery_list']) ))
            {
                Yii::app()->user->setFlash('error', 'Please select a branch.');
            }
            else
            {
                $model->attributes = $_POST['Checklist'];
                $model->created_by=Yii::app()->user->id;
                $model->created_date=date("Y-m-d H:i:s");
                $model->request_approval_by = $_POST['Checklist']['request_approval_by'];
                $model->remove_customer_on_submission = $_POST['Checklist']['remove_customer_on_submission'];
                $model->checklist_type_id = $_POST['Checklist']['checklist_type_id'];
                
                if($model->request_approval_by>0)
                {
                    $model->status_id=0;
                    $status=0;
                    $comment='Form Created';
                }
                else {
                    $status=$model->status_id;
                    $comment='Form Created';
                }
                if ($model->save())
                {
                    $id = Yii::app()->db->getLastInsertId();
                    
                    //template saving
                    if(isset($_POST['template_id']) && $_POST['template_id'] > 0)
                    {
                        $_POST['form']['template_id'] = $_POST['template_id'];
                        $_POST['form']['form_id'] = $id;
                        $this->actionAppendForm(true);
                        unset($_POST['form']['template_id'], $_POST['form']['form_id']);
                    }
                    $SQL = "INSERT INTO `checklist_reviews_logs`(`date_created`,`status_id`, `checklist_id`, `employee_id`, `reason`)
                 VALUES ('".date('Y-m-d H:i:s')."',".$status.",".$id.",".Yii::app()->user->id.",'".$comment."')";
                    $insert_command = Yii::app()->db->createCommand($SQL);
                    $insert_command->execute();
                    $exceptionArray = isset($_POST['exception_emp']) ? $_POST['exception_emp'] : array();
                    $this->saveExceptionRecipients($id, $exceptionArray, $selected_emp);
                    $selected_bakery = isset($_POST['bakery_list']) ? $_POST['bakery_list'] : array();
                    if(count($selected_bakery)>0)
                    {
                        foreach($bakeries as $bakery)
                        {
                            if(in_array($bakery->id, $selected_bakery) && !in_array($bakery->id, $selectedChecklistBakeries))
                            {
                                $saveChecklistBakery = new ChecklistBakeries();
                                $saveChecklistBakery->checklist_id = $id;
                                $saveChecklistBakery->bakery_id = $bakery->id;
                                $saveChecklistBakery->save();
                            }
                            elseif(!in_array($bakery->id, $selected_bakery) && in_array($bakery->id, $selectedChecklistBakeries))
                            {
                                ChecklistBakeries::model()->deleteAllByAttributes(array('checklist_id' => $id, 'bakery_id' => $bakery->id));
                            }
                        }
                    }
                    else
                    {
                        if(Yii::app()->user->role==2)
                        {
                            $saveChecklistBakery = new ChecklistBakeries();
                            $saveChecklistBakery->checklist_id = $id;
                            $saveChecklistBakery->bakery_id = Yii::app()->user->bakery;
                            $saveChecklistBakery->save();
                        }
                    }
                    
                    if($_POST['Checklist']['default_all_customers'] == '0')
                    {
                        if(isset($_POST['customers']) && $_POST['customers'] != '')
                        {
                            $valid = $this->actionPasteValidation($id, '1');
                            if(!$valid['valid'])
                            {
                                Yii::app()->user->setFlash('error', $valid['msg']);
                                $this->redirect(['yabbyForms/update/id/'.$id]);
                                die();
                            }
                            else
                            {
                                $this->actionPasteUpdate($id, '1');
                            }
                        }
                    }
                    
                    if((!isset( $_POST['formGroups'][0]) ||  $_POST['formGroups'][0]=="") && $model->default_all_employees==0)
                    {
                        Yii::app()->user->setFlash('error', 'Please select a Form Group or Default this form to all employees.');
                        
                    }
                    elseif(isset( $_POST['formGroups'][0]) && $model->default_all_employees==0)
                    {
                        $formGroups = explode(",", $_POST['formGroups'][0]);
                        if(count($formGroups)>0 && $formGroups[0]!='')
                        {
                            foreach($formGroups as $groupList)
                            {
                                $SQL = "INSERT INTO `checklist_form_groups`(`checklist_id`,`form_group_id`, `updated_by`, `updated_datetime`)
                                 VALUES (".$model->id.",".$groupList.",".Yii::app()->user->id.",'".date('Y-m-d H:i:s')."')";
                                $insert_command = Yii::app()->db->createCommand($SQL);
                                $insert_command->execute();
                            }
                            
                        }
                        
                    }
                    
                    $this->redirect(array('index'));
                }
            }
        }
        $this->render('addChecklist', array(
            'model' => $model,
            'employeeListArray' => $employeeListArray,
            'selected_emp' => $selected_emp,
            'schedule' => $schedule,
            'bakeryList' => $bakeries,
            'selectedChecklistBakeries' => $selectedChecklistBakeries,
            'checklistTypes'=>$checklistTypes
        ));
    }
    
    public function loadModel($id)
    {
        $model = Checklist::model()->findByPk((int)$id);
        if($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
            return $model;
    }
    
    public function actionUpdate($id)
    {
        if(isset($_POST['Checklist']))
        {
            if($_POST['Checklist']['default_all_customers'] == '0')
            {
                if(isset($_POST['customers']) && $_POST['customers'] != '')
                {
                    $valid = $this->actionPasteValidation($id, '1');
                    if(!$valid['valid'])
                    {
                        Yii::app()->user->setFlash('error', $valid['msg']);
                        $this->redirect(['yabbyForms/update/id/'.$id]);
                        die();
                    }
                    else
                    {
                        $this->actionPasteUpdate($id, '1');
                    }
                }
            }
        }
        $model = $this->loadModel($id);
        $connect = Yii::app()->db;
        $condition = Yii::app()->user->bakery != 0 ? array('status_id' => 1, 'bakery_id' => Yii::app()->user->bakery) : array('status_id' => 1);
        $empGrp = EmployeeGroup::model()->findAll('status_id =1');
        $checklistTypes = ChecklistTypes::model()->findAll('status_id =1 and exclude_on_form=0');
        
        $employee = Employee::model()->findAllByAttributes($condition);
        $bakeryWhere = '';
        if(Yii::app()->user->role > 1)
        {
            $bakeryWhere = 'id = ' . Yii::app()->user->bakery;
        }
        else
        {
            $bakeryWhere .= CustomUtils::getEmployeeBakeryIds('id', '');
        }
        
        $bakeries = Bakery::model()->findAll($bakeryWhere);
        $sql = <<<SQL
               SELECT
            fg.id,fg.description
            FROM `checklist_form_groups` cfg
            LEFT JOIN `form_groups` fg ON cfg.form_group_id=fg.id
            WHERE cfg.checklist_id=:a;
SQL;
        $command = $connect->createCommand($sql);
        $command->bindValue(':a', $id, PDO::PARAM_INT);
        $formGroups = $command->queryAll();
        
        
        $modelChecklistBakeries = ChecklistBakeries::model()->findAllByAttributes(array('checklist_id' => $id));
        $selectedChecklistBakeries = array();
        
        $updatedBakeries=array();
        foreach($modelChecklistBakeries as $bakery)
        {
            $selectedChecklistBakeries[] = $bakery->bakery_id;
        }
        $employeeListArray = array();
        foreach($empGrp as $grp)
        {
            foreach($employee as $emp)
            {
                if($emp['employee_group_id'] == $grp['id'])
                {
                    $employeeListArray[$grp['description']][] = array('id' => $emp['id'], 'name' => $emp['name']);
                }
            }
        }
        $schedule = ChecklistNotificationSchedule::model()->findAll('status_id =1');
        $sql = <<<SQL
    SELECT
        employee_id
    FROM
        checklist_notification_recipients
    WHERE checklist_id =:a;
SQL;
        $command = $connect->createCommand($sql);
        $command->bindValue(':a', $id, PDO::PARAM_INT);
        $tmpArray = $command->queryAll();
        $selected_emp = array();
        foreach($tmpArray as $tmp)
        {
            $selected_emp[] = $tmp['employee_id'];
        }
        if($model->checklist_type_id == '1')
        {
            $model->checklist_type_id = isset($_POST['Checklist']['checklist_type_id'])?$_POST['Checklist']['checklist_type_id']:1;
            
            $array = array('index');
        }
        elseif($model->checklist_type_id == '2')
        {
            $model->checklist_type_id = isset($_POST['Checklist']['checklist_type_id'])?$_POST['Checklist']['checklist_type_id']:2;
            
            $array = array('refuel');
        }
        elseif($model->checklist_type_id == '3')
        {
            $model->checklist_type_id = isset($_POST['Checklist']['checklist_type_id'])?$_POST['Checklist']['checklist_type_id']:3;
            
            $array = array('vehicleChecklist');
        }
        elseif($model->checklist_type_id == '4')
        {
            $model->checklist_type_id = isset($_POST['Checklist']['checklist_type_id'])?$_POST['Checklist']['checklist_type_id']:4;
            
            $array = array('productPriceChecklist');
        }
        if($model->checklist_type_id>4)
        {
            $model->checklist_type_id = isset($_POST['Checklist']['checklist_type_id'])?$_POST['Checklist']['checklist_type_id']:$model->checklist_type_id;
            
            $array = array('index');
        }
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);
        $status=0;
        $comment='';
        if(isset($_POST['Checklist']))
        {
            $this->doChecklistValidation($_POST,$id);
            
            if((!isset( $_POST['bakery_list']) ))
            {
                Yii::app()->user->setFlash('error', 'Please select atleast one branch for the form.');
                
            }
            
            $model->attributes = $_POST['Checklist'];
            $model->updated = date("Y-m-d H:i:s");
            $model->updated_by = Yii::app()->user->id;
            $model->request_approval_by = isset($_POST['Checklist']['request_approval_by'])?$_POST['Checklist']['request_approval_by']:0;
            $model->remove_customer_on_submission = isset($_POST['Checklist']['remove_customer_on_submission'])?$_POST['Checklist']['remove_customer_on_submission']:0;
            $model->checklist_category_id = $_POST['Checklist']['checklist_category_id'];
            
            
            if($model->request_approval_by>0)
            {
                $model->status_id=0;
                $status=0;
                $comment='Form Updated';
            }
            else {
                $status=$model->status_id;
                $comment='Form Updated';
            }
            if($model->save())
            {
                //template saving
                if(isset($_POST['template_id']) && $_POST['template_id'] > 0)
                {
                    $_POST['form']['template_id'] = $_POST['template_id'];
                    $_POST['form']['form_id'] = $model->id;
                    $this->actionAppendForm(true);
                    unset($_POST['form']['template_id'], $_POST['form']['form_id']);
                }
                $SQL = "INSERT INTO `checklist_reviews_logs`(`date_created`,`status_id`, `checklist_id`, `employee_id`, `reason`)
                 VALUES ('".date('Y-m-d H:i:s')."',".$status.",". $model->id.",".Yii::app()->user->id.",'".$comment."')";
                $insert_command = Yii::app()->db->createCommand($SQL);
                $insert_command->execute();
                $exceptionArray = isset($_POST['exception_emp']) ? $_POST['exception_emp'] : array();
                $this->saveExceptionRecipients($id, $exceptionArray, $selected_emp);
                $selected_bakery = isset($_POST['bakery_list']) ? $_POST['bakery_list'] : array();
                
                $bakeryWhere = '';
                if(Yii::app()->user->role > 1)
                {
                    $bakeryWhere = 'id = ' . Yii::app()->user->bakery;
                }
                else
                {
                    $bakeryWhere .= CustomUtils::getEmployeeBakeryIds('id', '');
                }
                
                $permissionsAllBakeries = Bakery::model()->findAll($bakeryWhere);
                $permissionBakeries=[];
                foreach($permissionsAllBakeries as $pBakery)
                {
                    $permissionBakeries[]=$pBakery['id'];
                }
                
                foreach($selectedChecklistBakeries as $key => $value)
                {
                    foreach($permissionBakeries as $permBakery)
                    {
                        if($permBakery == $value)
                        {
                            unset($selectedChecklistBakeries[$key]);
                        }
                    }
                }
                
                if(Yii::app()->user->role==1)
                {
                    ChecklistBakeries::model()->deleteAllByAttributes(array('checklist_id' => $id));
                    
                }
                elseif(Yii::app()->user->role==2)
                {
                    ChecklistBakeries::model()->deleteAllByAttributes(array('checklist_id' => $id,'bakery_id'=>Yii::app()->user->bakery));
                }
                if(count($selected_bakery)>0)
                {
                    foreach($selected_bakery as $bakery)
                    {
                        $updatedBakeries[]=$bakery;
                        $saveChecklistBakery = new ChecklistBakeries();
                        $saveChecklistBakery->checklist_id = $id;
                        $saveChecklistBakery->bakery_id = $bakery;
                        $saveChecklistBakery->save();
                        
                    }
                    if(count($selectedChecklistBakeries)>0)
                    {
                        foreach($selectedChecklistBakeries as $bakery)
                        {
                            $updatedBakeries[]=$bakery;
                            $saveChecklistBakery = new ChecklistBakeries();
                            $saveChecklistBakery->checklist_id = $id;
                            $saveChecklistBakery->bakery_id = $bakery;
                            try{
                                $saveChecklistBakery->save();
                            }catch(CDbException $e){
                                
                            }
                            
                        }
                    }
                }
                else {
                    if(Yii::app()->user->role==2)
                    {
                        $saveChecklistBakery = new ChecklistBakeries();
                        $saveChecklistBakery->checklist_id = $id;
                        $saveChecklistBakery->bakery_id = Yii::app()->user->bakery;
                        $saveChecklistBakery->save();
                    }
                    
                }
                
                /*foreach($bakeries as $bakery)
                 {
                 
                 if(in_array($bakery->id, $selected_bakery) && !in_array($bakery->id, $selectedChecklistBakeries))
                 {
                 $saveChecklistBakery = new ChecklistBakeries();
                 $saveChecklistBakery->checklist_id = $id;
                 $saveChecklistBakery->bakery_id = $bakery->id;
                 $saveChecklistBakery->save();
                 }
                 elseif(!in_array($bakery->id, $selected_bakery) && in_array($bakery->id, $selectedChecklistBakeries))
                 {
                 ChecklistBakeries::model()->deleteAllByAttributes(array('checklist_id' => $id, 'bakery_id' => $bakery->id));
                 }
                 
                 }*/
                $con = Yii::app()->db;
                $sql="select bakery_id from outlet_checklists where checklist_id=".$model->id;
                $outletc = $con->createCommand($sql);
                $outletChecklist = $outletc->queryAll();
                $removedChecklist=array();
                foreach($outletChecklist as $checklistOutlets)
                {
                    if(!in_array($checklistOutlets['bakery_id'], $updatedBakeries))
                    {
                        $removedChecklist[]=$checklistOutlets['bakery_id'];
                    }
                    
                }
                if(count($removedChecklist)>0)
                {
                    foreach($removedChecklist as $remove)
                    {
                        $sql='delete from outlet_checklists where checklist_id='.$model->id.' and bakery_id='.$remove;
                        $command = $con->createCommand($sql);
                        $command->execute();
                        
                    }
                }
                
                
                if((!isset( $_POST['formGroups'][0]) ||  $_POST['formGroups'][0]=="") && $model->default_all_employees==0)
                {
                    Yii::app()->user->setFlash('error', 'Please select a Form Group or Default this form to all employees.');
                    
                }
                elseif(isset( $_POST['formGroups'][0]) && $model->default_all_employees==0)
                {
                    
                    $formGroups = explode(",", $_POST['formGroups'][0]);
                    $sql='delete from checklist_form_groups where checklist_id='.$model->id;
                    $command = $con->createCommand($sql);
                    $command->execute();
                    if(count($formGroups)>0 && $formGroups[0]!='')
                    {
                        
                        foreach($formGroups as $groupList)
                        {
                            $SQL = "INSERT INTO `checklist_form_groups`(`checklist_id`,`form_group_id`, `updated_by`, `updated_datetime`)
                                 VALUES (".$model->id.",".$groupList.",".Yii::app()->user->id.",'".date('Y-m-d H:i:s')."')";
                            $insert_command = Yii::app()->db->createCommand($SQL);
                            $insert_command->execute();
                        }
                        
                    }
                    $this->redirect($array);
                }
                elseif(!isset( $_POST['formGroups'][0]) && $model->default_all_employees==1)
                {
                    $sql='delete from checklist_form_groups where checklist_id='.$model->id;
                    $command = $con->createCommand($sql);
                    $command->execute();
                    $this->redirect($array);
                }
                
            }
        }
        
        
        if($model->checklist_type_id == '4')
        {
            $sql="SELECT COUNT(*) AS numChecklists FROM `checklist_items` WHERE `checklist_id`=".$id." AND `product_id`!=0";
            $checklistItemsQry = $connect->createCommand($sql);
            $checklistProductItems = $checklistItemsQry->queryAll();
            $this->render('updatePriceForm', array(
                'model' => $model, 'array' => $array, 'employeeListArray' => $employeeListArray, 'selected_emp' => $selected_emp, 'schedule' => $schedule, 'bakeryList' => $bakeries, 'selectedChecklistBakeries' => $selectedChecklistBakeries,'checklistProductItems'=>$checklistProductItems
            ));
        }
        
        else{
            $this->render('update', array(
                'model' => $model, 'array' => $array,'formGroups'=>$formGroups, 'employeeListArray' => $employeeListArray, 'selected_emp' => $selected_emp, 'schedule' => $schedule, 'bakeryList' => $bakeries, 'selectedChecklistBakeries' => $selectedChecklistBakeries,'checklistTypes'=>$checklistTypes
            ));
        }
        
        
    }
    
    public function actionDelete($id)
    {
        if(!Yii::app()->request->isPostRequest)
        {
            // we only allow deletion via POST request
            //            $checklist = $this->loadModel($id);
            //            $checklist->status_id = 0;
            //            $checklist->save();
            $model = Checklist::model()->findByPk($id);
            $status='';
            $comment='';
            if($model->status_id == 1)
            {
                $model->status_id = 0;
                $status=0;
                $comment='Status changed to inactive';
            }
            elseif($model->status_id == 0 && $model->request_approval_by>0)
            {
                $model->status_id =2;
                $status=2;
                $comment='Status changed to pending approval';
            }
            elseif($model->status_id > 0 && $model->request_approval_by>0)
            {
                $model->status_id =0;
                $status=0;
                $comment='Status changed to inactive';
            }
            elseif($model->status_id == 0 && $model->request_approval_by==0)
            {
                $model->status_id = 1;
                $status=2;
                $comment='Status changed to active';
            }
            $model->save();
            
            $SQL = "INSERT INTO `checklist_reviews_logs`(`date_created`,`status_id`, `checklist_id`, `employee_id`, `reason`)
                 VALUES ('".date('Y-m-d H:i:s')."',".$status.",".$id.",".Yii::app()->user->id.",'".$comment."')";
            $insert_command = Yii::app()->db->createCommand($SQL);
            $insert_command->execute();
            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if(!isset($_GET['ajax']))
            {
                $chkType= ChecklistTypes::model()->findByPk($model->checklist_type_id);
                if($chkType['exclude_on_form'] !=1)
                {
                    $checkChecklist =  array('index') ;
                }
                elseif( $chkType['exclude_on_form'] ==1 && $model->checklist_type_id == '4')
                {
                    $checkChecklist =  array('productPriceChecklist');
                }
                else {
                    $checkChecklist =  array('refuel');
                }
                
                // $checkChecklist = ($model->checklist_type_id == '1') ? array('index') : array('refuel');
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : $checkChecklist);
            }
        }
        else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }
    
    public function actionDeactivateChecklist()
    {
        if(isset($_GET[id]))
        {
            $yabby = new YabbyFormManager;
            $yabby->deactivateChecklist($_GET);
            Yii::app()->user->setFlash('success', 'Checklist form was deactivated successfully');
        }
        $this->actionIndex();
    }
    
    public function actionSaveForm()
    {
        $yabby = new YabbyFormManager;
        $yabby->saveFormItems($_POST);
    }
    
    public function actionSaveKPIGroup()
    {
        $yabby = new YabbyFormManager;
        $yabby->saveKIPgroup($_POST);
    }
    
    public function actionSaveKPISubGroup()
    {
        $yabby = new YabbyFormManager;
        $yabby->SaveKPISubGroup($_POST);
    }
    
    public function actionupdateKPIGroupUrl()
    {
        $yabby = new YabbyFormManager;
        $yabby->updateKPIGroupUrl();
    }
    
    public function actionDeleteItem()
    {
        $yabby = new YabbyFormManager;
        $yabby->deleteItem($_POST);
    }
    
    public function actionDeleteAdditionalOption()
    {
        $yabby = new YabbyFormManager;
        $yabby->deleteAdditionalOption($_POST);
    }
    
    public function actionDeleteChkItemAndChkOption()
    {
        $yabby = new YabbyFormManager;
        $yabby->deleteChkItemAndChkOption($_POST);
    }
    
    public function actionGetChecklistId($id)
    {
        Yii::app()->session['checklist_id'] = $id;
        $getChecklistName = Checklist::model()->findByPk($id);
        Yii::app()->session['checklist_name'] = $getChecklistName->checklist_name;
        Yii::app()->session['checklist_type_id'] = $getChecklistName->checklist_type_id;
        $this->actionCustomerChecklistManagement($id);
    }
    
    public function actionCustomerChecklistManagement($id){
        
        //get selected branches
        $bakery_id = Yii::app()->user->bakery;
        $sql='select bakery_id from checklist_bakeries where checklist_id='.$id;
        $checklist_bakeries=Yii::app()->db->createCommand($sql)->queryAll();
        $first_option = isset($_GET['first_option']) ? $_GET['first_option'] : '';
        $second_option = isset($_GET['second_option']) ? $_GET['second_option'] : '';
        $formBakeries='';
        if(count($checklist_bakeries)>0)
        {
            foreach($checklist_bakeries as $cBakery)
            {
                $formBakeries.=','.$cBakery['bakery_id'];
            }
        }
        $formBakeries=trim($formBakeries,',');
        
        if(!isset($_GET['isIntialView'])){
            $checklistID =  Yii::app()->session['checklist_id'];
            $model = new OutletChecklists('searchBakeryOutlets');
            $model->unsetAttributes();
            $model->checklist_id = $checklistID;
            $model->branchList=$formBakeries;
        }
        elseif($_GET['first_option'] == 0 && $_GET['second_option'] == 0)
        {
            $model = new Outlet('searchForms');
            $model->unsetAttributes();
            $model->branchList=$formBakeries;
        }
        elseif($_GET['first_option'] == 0 && $_GET['second_option'] == 1)
        {
            $checklistID =  Yii::app()->session['checklist_id'];
            $model = new OutletChecklists('searchBakeryOutlets');
            $model->unsetAttributes();
            $model->checklist_id = $checklistID;
            $model->branchList=$formBakeries;
        }
        elseif($_GET['first_option'] > 0 && $_GET['second_option'] == 0)
        {
            $model = new Outlet('searchForms');
            $model->unsetAttributes();
            $model->branchList=$formBakeries;
            
        }
        else
        {
            $model = new Outlet('searchForms');
            $model->unsetAttributes();
            $model->branchList=$formBakeries;
            if($_GET['first_option'] == 1)
            {
                $model->outlet_group_id = $_GET['second_option'];
            }
            elseif($_GET['first_option'] == 2)
            {
                $model->route_id = $_GET['second_option'];
            }
        }
        
        if(isset($_GET['Outlet']))
        {
            $model->attributes = $_GET['Outlet'];
        }
        if(isset($_GET['OutletChecklists']))
        {
            $model->attributes = $_GET['OutletChecklists'];
        }
        
        $formName = Yii::app()->session['checklist_name'];
        
        $this->render('customerChecklistMgmt', array("model"=> $model,'first_option'=>$first_option,'second_option'=>$second_option, 'formName' => $formName,'checklist_bakeries'=>$checklist_bakeries,'id'=>$id));
    }
    
    public function actionGetSelection(){
        $sql = "";
        if(isset ($_POST['category_id'])){
            $category_id = $_POST['category_id'];
            if($category_id == 1)
            {
                $sql = "SELECT id, description FROM outlet_group WHERE status_id =1";
            }
            elseif($category_id == 2){
                $sql='select bakery_id from checklist_bakeries where checklist_id='.$_POST['id'];
                $checklist_bakeries=Yii::app()->db->createCommand($sql)->queryAll();
                $bakaries='';
                if(count($checklist_bakeries)>0)
                {
                    foreach($checklist_bakeries as $bakery)
                    {
                        $bakaries.=' OR bakery_id='.$bakery['bakery_id'];
                    }
                    $bakaries=trim($bakaries,' OR');
                    $sql = "SELECT id, area AS `description` FROM route WHERE status_id =1 AND ".$bakaries;
                    
                }
                /*$bakery = (Yii::app()->user->bakery == 0) ? '': 'AND bakery_id ='.Yii::app()->user->bakery;
                 $sql = "SELECT id, area AS `description` FROM route WHERE status_id =1 $bakery";*/
            }
            $category = Yii::app()->db->createCommand($sql)->queryAll();
            echo json_encode($category);
        }
    }
    
    public function isChecked($id)
    {
        $model = OutletChecklists::model()->findAllByAttributes(array('id' => $id));
        return $model;
    }
    
    public function actionSaveChecklistForm()
    {
        //$model = OutletChecklists::model()->findAllByAttributes(array('id' => $id));
        // return $model;
        
        $this->render('_saveChecklistForm', array($id = $_GET['id']));
    }
    
    public function isCheckedCustomer($id)
    {
        $model = OutletChecklists::model()->findAllByAttributes(array('outlet_id' => $id, 'checklist_id' => Yii::app()->session['checklist_id']));
        return $model;
    }
    
    /**
     *  Use this function to render the controls needed for constructing your form Jaco Smit
     */
    public function renderControl($name, $type, $id, $value, $title, $label, $attr1, $class, $required, $outletId)
    {
        $db = Yii::app()->db;
        $ControlOptions = [];
        $SearchOptionsSQL = <<<SQL
        SELECT *
        FROM checklist_options co
        WHERE co.checklist_items_id = {$id}
        AND co.status_id = 1;
SQL;
        
        $SearchOptionsCmd = $db->createCommand($SearchOptionsSQL);
        $SearchOptionsRes = $SearchOptionsCmd->queryAll();
        
        $control = '';
        $requiredFill = '';
        if($required)
        {
            $requiredFill = 'data-required="true"';
        }
        
        if(strlen($label) > 0)
        {
            $control .= '<label>'.$label.'</label>';
        }
        $i = 0;
        foreach($SearchOptionsRes AS $Options)
        {
            $optionId = $Options['id'];
            if($type === 'text' || $type === 'email')
            {
                $control .= <<<HTML
                <input type="{$type}" name="{$name}" id="{$id}" title="{$title}" value="{$value}" $attr1 class="form-control {$class}" {$requiredFill}>
HTML;
            }
            elseif($type === 'checkbox')
            {
                $control .= <<<HTML
                <div class="checkbox">
                    <label>
                        <input type="{$type}" name="{$name}[]" title="{$title}" value="{$optionId}" $attr1 {$requiredFill}>&nbsp;{$Options['description']}
                    </label>
                </div>
HTML;
            }
            elseif($type === 'radio')
            {
                $control .= <<<HTML
                <div class="radio">
                    <label>
                        <input type="{$type}" name="{$name}" title="{$title}" value="{$optionId}" $attr1 {$requiredFill}>&nbsp;{$Options['description']}
                    </label>
                </div>
HTML;
            }
            elseif($type === 'textarea')
            {
                $control .= <<<HTML
                <textarea name="{$name}" id="{$id}" $attr1 class="form-control {$class}" {$requiredFill}>{$value}</textarea>
HTML;
            }
            elseif($type === 'date')
            {
                $date = date('Y-m-d');
                $control .= <<<HTML
                <input type="text" name="{$name}" id="{$id}" $attr1 value="{$date}" class="hasDatePicker form-control {$class}" {$requiredFill}>
                <script>
                   $('#{$id}').datepicker({ minDate: '-1m', dateFormat: 'yy-mm-dd' });
                </script>
HTML;
            }
            elseif($type === 'time')
            {
                $time = date('H:i:s');
                $control .= <<<HTML
                <input type="text" name="{$name}" id="{$id}" $attr1 value="{$time}" autocomplete="off" class="time ui-timepicker-input form-control {$class}" {$requiredFill}>
                <script>
                $('#{$id}').timepicker({
                    'disableTimeRanges': [
                      //  ['1pm', '2pm']
                        ],
                    'showDuration': false,
                    'timeFormat': 'H:i:s'
                    });
                </script>
HTML;
            }
            elseif($type === 'photo' || $type === 'document')
            {
                $control .= <<<HTML
                <input type="file" name="{$name}" id="{$id}" $attr1 class="form-control {$class}" {$requiredFill}>
HTML;
            }
            elseif($type === 'number')
            {
                $control .= <<<HTML
                <input type="number" name="{$name}" id="{$id}" $attr1 class="form-control {$class}" {$requiredFill} onkeypress="return event.charCode == 0 || event.charCode == 8 || event.charCode == 9 ||  (  event.charCode >= 44 && event.charCode <= 57)">
HTML;
            }
            elseif($type === 'select')
            {
                $control .= <<<HTML
                <option value="{$optionId}">{$Options['description']}</option>
HTML;
            }
            
            ++$i;
        }
        if($type === 'select')
        {
            $control = <<<HTML
            <select name="{$name}" id="{$id}" $attr1 class="form-control {$class}" {$requiredFill}>
                 <option value="">Please Select</option>
                {$control}
            </select>
HTML;
        }
        if($type === 'radio' || $type === 'checkbox')
        {
            $control .= '<input '.$requiredFill.' type="hidden" id="'.$type.'_'.$name.'" name="'.$type.'_'.$name.'" value="" />';
        }
        if($type === 'product')
        {
            $productsSql = <<<SQL
            SELECT
            p.id,
            CONCAT(p.sku, ' - ', ' (', uom.description, ') ', p.description, ' - ', p.batch_code, ' [', p.expiry_date, ']') AS description
            FROM
            products p
            INNER JOIN bakery b ON b.id = p.bakery_id
            INNER JOIN outlet o ON o.id = {$outletId} AND o.bakery_id = b.id
            INNER JOIN units_of_measure uom ON uom.id = p.units_of_measure_id
            WHERE
            p.status_id = 1
            ORDER BY p.description, uom.description;
SQL;
            
            $products = $db->createCommand($productsSql)->queryAll();
            $control = <<<HTML
            <select name="{$name}" id="{$name}" class="form-control" {$requiredFill}>
HTML;
            foreach($products as $product)
            {
                $control .= <<<HTML
                <option value={$product['id']}>{$product['description']}</option>
HTML;
            }
            
            $control .= '</select>';
        }
        
        return $control;
    }
    
    public function actionSaveCustomerList()
    {
        if(isset($_GET['selectedIds']))
        {
            foreach($_GET['selectedIds'] as $id)
            {
                $ifOutletExist = OutletChecklists::model()->findAllByAttributes(array('outlet_id' => $id, 'checklist_id' => Yii::app()->session['checklist_id']));
                if(empty($ifOutletExist))
                {
                    $outletInfor = Outlet::model()->findByPk($id);
                    $model = new OutletChecklists;
                    $model->checklist_id = Yii::app()->session['checklist_id'];
                    $model->outlet_id = $id;
                    $model->bakery_id = $outletInfor->bakery_id;
                    $model->datetime = date('Y-m-d H:i:s');
                    $model->save();
                }
            }
        }
        $this->actionCustomerChecklistManagement(Yii::app()->session['checklist_id']);
    }
    
    public function actionSaveOutletChecklist()
    {
        //get all outlets linked to this checklist from outlets_checklists
        //place all retrieved ids from outlets_checklists in an array and match with what form returned
        //if id from outlets_checklists does not exist in array from form delete.
        if(isset($_GET['selectedIdRemove']))
        {
            $model = OutletChecklists::model()->findAllByAttributes(array('checklist_id' => Yii::app()->session['checklist_id']));
            $outletsLinked = array();
            foreach($model as $details)
            {
                if(!in_array($details->id, $_GET['selectedIdRemove']))
                {
                    OutletChecklists::model()->deleteByPk($details->id);
                }
            }
        }
        else
        {
            OutletChecklists::model()->deleteAllByAttributes(array('checklist_id' => Yii::app()->session['checklist_id']));
        }
        $this->actionCustomerChecklistManagement( Yii::app()->session['checklist_id']);
    }
    
    
    public function actionCaptureForm($id)
    {
        $db = Yii::app()->db;
        // $model = ChecklistItems::model()->findAllByAttributes('checklist_id ='.$id);
        $SearchSQL = " SELECT DISTINCT c.id,c.checklist_name, c.description as description_html, ci.id AS item_id, ci.description AS question, ci.mandatory,cot.description
      FROM checklist  c
      INNER JOIN checklist_items ci ON c.id = ci.checklist_id
      INNER JOIN checklist_options co ON ci.id = co.checklist_items_id
      INNER JOIN checklist_option_types cot ON co.option_type_id = cot.id

      WHERE c.id = {$id}
      AND c.status_id is true
      AND ci.status_id is true
      AND co.status_id is true
      ORDER BY ci.ordering ASC;";
        $SearchCmd = $db->createCommand($SearchSQL);
        $SearchRes = $SearchCmd->queryAll();
        $description_html = "";
        foreach($SearchRes as $name)
        {
            $formName = $name['checklist_name'];
            $description_html = $name['description_html'];
        }
        //        var_dump($SearchRes);
        $this->renderPartial('_captureForm', array(
            'models'   => $SearchRes,
            'formName' => $formName,
            'description_html' => $description_html
        ));
    }
    
    public function actionGetNotificationRecipients()
    {
        $db = Yii::app()->db;
        $getAttributes = (Yii::app()->user->bakery != 0) ? array('status_id' => '1', 'bakery_id' => Yii::app()->user->bakery) : array('status_id' => '1');
        $model = Employee::model()->findAllByAttributes($getAttributes, 'email !=""');
        $bakery = (Yii::app()->user->bakery != 0) ? " WHERE bakery_id =" . Yii::app()->user->bakery : "";
        //Get all recipients previously saved
        $recipientSQL = "SELECT employee_id  FROM `vehicle_checklist_image_recipient` $bakery";
        $recipientCmd = $db->createCommand($recipientSQL);
        $recipientRes = $recipientCmd->queryAll();
        $recipients = array();
        if(isset($recipientRes))
        {
            foreach($recipientRes as $res)
            {
                $recipients[] = $res['employee_id'];
            }
        }
        if(isset($_POST['recipient']))
        {
            //Delete all recipients previously saved
            $delCmd = $db->createCommand("DELETE FROM `vehicle_checklist_image_recipient` $bakery")->execute();
            if(!empty($_POST['recipient']))
            {
                foreach($_POST['recipient'] as $emp)
                {
                    $exp = explode("_", $emp);
                    //Save all recipients selected
                    $sql = "INSERT INTO `vehicle_checklist_image_recipient` VALUES({$exp[0]},'{$exp[1]}',{$exp[2]})";
                    $db->createCommand($sql)->execute();
                }
                Yii::app()->user->setFlash('success', 'Recipient(s) saved sucessfully');
                $this->redirect(array('vehicleChecklist'));
            }
            else
            {
                Yii::app()->user->setFlash('error', 'Please select atleast one recipients to receive the report');
            }
            
        }
        $this->render('manageNotification', array('model' => $model, 'recipients' => $recipients));
    }
    
    public function saveExceptionRecipients($id, $exceptionArray, $selectedArray)
    {
        $connect = Yii::app()->db;
        if(isset($exceptionArray))
        {
            foreach($selectedArray as $chk)
            {
                if(!in_array($chk, $exceptionArray))
                {
                    $sql = "DELETE FROM `checklist_notification_recipients`
                            WHERE checklist_id ={$id} AND employee_id ={$chk}";
                    $command = $connect->createCommand($sql);
                    $command->execute();
                }
            }
            foreach($exceptionArray as $emp)
            {
                $sql = <<<SQL
            INSERT IGNORE INTO `checklist_notification_recipients`
            (`checklist_id`,`employee_id`)
            VALUES
            (:a, :b);
SQL;
                $command = $connect->createCommand($sql);
                $command->bindValue(':a', $id, PDO::PARAM_INT);
                $command->bindValue(':b', $emp, PDO::PARAM_STR);
                $command->execute();
            }
        }
        if(!isset($exceptionArray) && isset($selectedArray))
        {
            $sql = "DELETE FROM `checklist_notification_recipients` WHERE checklist_id ={$id}";
            $command = $connect->createCommand($sql);
            $command->execute();
        }
    }
    public function actionImportChecklistCustomers($id)
    {
        $outletList='';
        $checklistOutlets=OutletChecklists::model()->findAllByAttributes(array('checklist_id'=>$id));
        if(count($checklistOutlets)>0)
        {
            foreach($checklistOutlets as $outlets)
            {
                $outletList.= $outlets['outlet_id']. "&#10;";
            }
        }
        
        $this->renderPartial('_importCheklistCustomers', array(
            'id' => $id,
            'outletList' => $outletList
        ));
        
    }
    public function actionIsBusyDownloading()
    {
        
        sleep(2);
        if (isset($_SESSION['Checklist']))
            echo 1;
            else
                echo 0;
    }
    
    public function actionStart($id)
    {
        $bakery_id=Yii::app()->user->bakery;
        
        set_time_limit(0);
        // get a reference to the path of PHPExcel classes
        $phpExcelPath = Yii::getPathOfAlias('application.extensions.phpexcel.Classes');
        include ($phpExcelPath . DIRECTORY_SEPARATOR . 'PHPExcel.php');
        $templatePath = Yii::app()->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR;
        $templateFile = $templatePath . 'FormCustomerList.xls';
        $checklistCustomers=[];
        $customers=[];
        $bakeryList=[];
        
        $con = Yii::app()->db;
        $role=Yii::app()->user->role;
        $sql="";
        $bakery_sql="";
        $outletChecklist_sql="";
        // get customers
        if($role==1)
        {
            $sql='SELECT cb.bakery_id
                FROM checklist_bakeries cb
                LEFT JOIN `bakery` b ON cb.bakery_id=b.id
                WHERE b.status_id=1 AND checklist_id='.$id;
            $checklist_bakeries=Yii::app()->db->createCommand($sql)->queryAll();
            $bakaries='';
            $bakerySelected='';
            
            if(count($checklist_bakeries)>0)
            {
                foreach($checklist_bakeries as $bakery)
                {
                    $bakaries.=','.$bakery['bakery_id'];
                    $bakerySelected.=','.$bakery['bakery_id'];
                }
                $bakaries=trim($bakaries,',');
                $bakerySelected=trim($bakerySelected,',');
            }
            $outletChecklist_bakery=$bakaries!='' ? ' AND bakery_id IN ('. $bakaries.')':'';
            $bakaries= $bakaries!='' ? ' AND o.bakery_id IN ('. $bakaries.')':'';
            $sql = "SELECT o.id,o.account_number,o.description,b.description as branch,o.bakery_id, og.description AS outlet_group,
            opg.description AS outlet_price_group,
            r.area AS route
                FROM `outlet` o
                LEFT JOIN `bakery` b ON o.bakery_id=b.id
                LEFT JOIN  outlet_group og ON og.id = o.outlet_group_id
                LEFT JOIN  outlet_price_group opg ON opg.id = o.outlet_price_group_id
                LEFT JOIN  route r ON r.id = o.route_id
                WHERE o.status_id =1 ".$bakaries;
            
            $bakerySelected=  $bakerySelected!=''?' AND id IN('. $bakerySelected.')':'';
            $bakery_sql="SELECT id,description,physical_address FROM `bakery` WHERE status_id =1 ".$bakerySelected;
            
            
            $outletChecklist_sql="SELECT * FROM outlet_checklists WHERE checklist_id=".$id." ".$outletChecklist_bakery;
        }
        elseif($role==2){
            $sql = "SELECT o.id,o.account_number,o.description,b.description as branch,o.bakery_id, og.description AS outlet_group,
            opg.description AS outlet_price_group,
            r.area AS route
                FROM `outlet` o
                LEFT JOIN `bakery` b ON o.bakery_id=b.id
                LEFT JOIN  outlet_group og ON og.id = o.outlet_group_id
                LEFT JOIN  outlet_price_group opg ON opg.id = o.outlet_price_group_id
                LEFT JOIN  route r ON r.id = o.route_id
                WHERE o.status_id =1 AND b.status_id=1 AND o.bakery_id=".$bakery_id;
            $bakery_sql="SELECT id,description,physical_address FROM `bakery` WHERE status_id =1 AND id=".$bakery_id;
            $outletChecklist_sql="SELECT * FROM `outlet_checklists` WHERE checklist_id=".$id." AND bakery_id=".$bakery_id;
        }
        
        
        $customers = Yii::app()->db->createCommand($sql)->queryAll();
        $bakeryList = Yii::app()->db->createCommand($bakery_sql)->queryAll();
        $checklistCustomers = Yii::app()->db->createCommand($outletChecklist_sql)->queryAll();
        
        
        $objReader = new PHPExcel_Reader_Excel2007();
        $objReader = new PHPExcel_Reader_Excel5();
        $objPHPExcel = $objReader->load($templateFile);
        $activeSheet = $objPHPExcel->setActiveSheetIndex(0);
        $activeSheet->setCellValueByColumnAndRow(0, 1, 'LIST OF FORM CUSTOMER LIST');
        $row = 3;
        
        foreach($checklistCustomers as $data)
        {
            $activeSheet->setCellValueByColumnAndRow(0, $row, $data['outlet_id']);
            ++$row;
        }
        $activeSheet = $objPHPExcel->setActiveSheetIndex(1);
        $row = 3;
        $activeSheet->setCellValueByColumnAndRow(0, 1, 'LIST OF CUSTOMERS');
        foreach($customers as $data)
        {
            $activeSheet->setCellValueByColumnAndRow(0, $row, $data['id']);
            $activeSheet->setCellValueByColumnAndRow(1, $row, $data['account_number']);
            $activeSheet->setCellValueByColumnAndRow(2, $row, $data['description']);
            $activeSheet->setCellValueByColumnAndRow(3, $row, $data['outlet_group']);
            $activeSheet->setCellValueByColumnAndRow(4, $row, $data['outlet_price_group']);
            $activeSheet->setCellValueByColumnAndRow(5, $row, $data['route']);
            $activeSheet->setCellValueByColumnAndRow(6, $row, $data['branch']);
            ++$row;
        }
        
        $activeSheet = $objPHPExcel->setActiveSheetIndex(0);
        $objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);
        $objWriter->setPreCalculateFormulas(false);
        header('Content-type: application/vnd.ms-excel');
        //        header('Content-Disposition: attachment; filename="products.xlsx"');
        //  header('Content-Disposition: attachment; filename="ProductBasePricingEngine.xls"');
        $download_file_name="FormCustomerList".trim(date('Y_m_d_H:i:s')).".xls";
        header('Content-Disposition: attachment; filename="'.$download_file_name.'"');
        $objWriter->save('php://output');
        
        
    }
    public function actionImportValidation($id)
    {
        $con = Yii::app()->db;
        $search = '';
        $replace = '';
        #INITIALIZE..
        ini_set("memory_limit", "-1");
        set_time_limit(0);
        $eid = Yii::app()->user->id;
        $bakery = Yii::app()->user->bakery;
        $bakery_id = Yii::app()->user->bakery;
        $folder = Yii::app()->basePath . "/../files/";
        $iChecklistCustomers=[];
        $iCustomers=[];
        $iBakeryList=[];
        
        $con = Yii::app()->db;
        $role=Yii::app()->user->role;
        $sql="";
        $bakery_sql="";
        $outletChecklist_sql="";
        $mimetypes = array(
            'text/xls',
            'application/excel',
            'application/vnd.ms-excel',
            'application/vnd.msexcel',
            'application/octet-stream',
        );
        $columns = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K');
        $mFatal = '';
        $mWarning = '';
        $count = $uCounter = $iCounter = 0;
        
        // get customers
        if($role==1)
        {
            $sql='select bakery_id from checklist_bakeries where checklist_id='.$id;
            $checklist_bakeries=Yii::app()->db->createCommand($sql)->queryAll();
            $bakaries='';
            $bakerySelected='';
            
            if(count($checklist_bakeries)>0)
            {
                foreach($checklist_bakeries as $bakery)
                {
                    $bakaries.=','.$bakery['bakery_id'];
                    $bakerySelected.=','.$bakery['bakery_id'];
                }
                $bakaries=trim($bakaries,',');
                $bakerySelected=trim($bakerySelected,',');
            }
            $outletChecklist_bakery=$bakaries!='' ? ' AND bakery_id IN ('. $bakaries.')':'';
            $bakaries= $bakaries!='' ? ' AND o.bakery_id IN ('. $bakaries.')':'';
            $sql = "SELECT o.id,o.account_number,o.description,b.description as branch,o.bakery_id
                FROM `outlet` o
                LEFT JOIN `bakery` b ON o.bakery_id=b.id
                WHERE o.status_id =1 ".$bakaries;
            
            $bakerySelected=  $bakerySelected!=''?' AND id IN('. $bakerySelected.')':'';
            $bakery_sql="SELECT id,description,physical_address FROM `bakery` WHERE status_id =1 ".$bakerySelected;
            
            
            $outletChecklist_sql="SELECT * FROM outlet_checklists WHERE checklist_id=".$id." ".$outletChecklist_bakery;
        }
        elseif($role==2){
            $sql = "SELECT o.id,o.account_number,o.description,b.description as branch,o.bakery_id
                FROM `outlet` o
                LEFT JOIN `bakery` b ON o.bakery_id=b.id
                WHERE o.status_id =1 AND o.bakery_id=".$bakery_id;
            $bakery_sql="SELECT id,description,physical_address FROM `bakery` WHERE status_id =1 AND id=".$bakery_id;
            $outletChecklist_sql="SELECT * FROM `outlet_checklists`WHERE bakery_id=".$bakery_id;
        }
        
        
        $customersData = Yii::app()->db->createCommand($sql)->queryAll();
        foreach($customersData as $val)
        {
            $iCustomers[] = $val['id'];
        }
        
        $bakeryListData = Yii::app()->db->createCommand($bakery_sql)->queryAll();
        foreach($bakeryListData as $val)
        {
            $iBakeryList[] = $val['id'];
        }
        
        $checklistCustomersData = Yii::app()->db->createCommand($outletChecklist_sql)->queryAll();
        foreach($checklistCustomersData as $val)
        {
            $iChecklistCustomers[] = $val['id'];
        }
        
        
        #PHPExcel
        //        spl_autoload_unregister(array('YiiBase', 'autoload'));
        $phpExcelPath = Yii::getPathOfAlias('ext.phpexcel.Classes');
        include($phpExcelPath . DIRECTORY_SEPARATOR . 'PHPExcel.php');
        $objPHPExcel = new PHPExcel();
        #Reading
        
        if(is_uploaded_file($_FILES['customer_list_file']['tmp_name']))
        {
            
            if(move_uploaded_file($_FILES['customer_list_file']['tmp_name'], $folder . $_FILES['customer_list_file']['name']))
            {
                $filename = $folder . $_FILES['customer_list_file']['name'];
                $objPHPExcel = PHPExcel_IOFactory::load($filename);
                $msg = '';
                foreach($objPHPExcel->getWorksheetIterator() as $worksheet)
                {
                    //Worksheet info
                    $worksheetTitle = $worksheet->getTitle();
                    $highestRow = $worksheet->getHighestRow();
                    $highestColumn = $worksheet->getHighestColumn();
                    $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
                    $nrColumns = ord($highestColumn) - 64;
                    
                    //Worksheet data
                    for($row = 3; $row <= $highestRow; ++$row)
                    {
                        ################################
                        #Column A: checklist customer  ID
                        $cell = $worksheet->getCellByColumnAndRow(0, $row);
                        $val = $cell->getValue();
                        $dataType = PHPExcel_Cell_DataType::dataTypeForValue($val);
                        
                        if(!in_array(trim($val), $iCustomers))
                        {
                            if($val != "" && $val > 0 && $dataType != 'n')
                            {
                                
                                $mFatal .= '- ' . $columns[0] . $row . ': A number required. <br/>';
                                
                            }
                            elseif($val != "" && $val > 0 && $dataType == 'n')
                            {
                                
                                $mFatal .= '- ' . $columns[0] . $row . ': invalid customer id<br/>';
                                
                            }
                            else
                            {
                                $iCounter++;
                            }
                        }
                        else
                        {
                            $uCounter++;
                        }
                        
                        
                        
                    }
                    //Stop after reading the first worksheet
                    break;
                }
            }
        }
        
        #OUTPUT
        
        
        if($mFatal != '' or $mWarning != '')
        {
            $disabled = '';
            if($mFatal != '')
            {
                echo "<div class='alert alert-danger'><b>FATAL ERROR!<br/>You can not import this file as it contains errors:</b><br/>" . $mFatal . "</div>";
                $part = '<input type="button" onClick="goBackToTwo()" class="btn btn-primary btn-sm" value="Back" />
                             <input type="button" id="next3" class="btn btn-primary btn-sm" value="Next" disabled="true"/>';
                $disabled = 'disabled="true"';
            }
            if($mWarning != '')
            {
                echo "<div class='alert alert-warning'><b>WARNING!<br/>Please note that you are about to:</b><br/>" . $mWarning . "</div>";
                $part = '<input type="button" onClick="goBackToTwo()" class="btn btn-primary btn-sm" value="Back" />
                             <input type="button" onClick="goToLastStep()" class="btn btn-primary btn-sm" value="Continue" ' . $disabled . '/>';
            }
            echo '<table id="date_filter_table customTable" width="100%">
                        <tr><td>
                            ' . $part . '
                        </td></tr>
                     </table> <script type="text/javascript">reloadCustomers()</script>
                     ';
        }
        else
        {
            $count = $uCounter + $iCounter;
            //            $msg = $count . ' records found: ' . $iCounter . ' new and ' . $uCounter . ' existing products';
            $msg = $count . ' records found';
            echo "<div class='alert alert-success'><span class='fa fa-check-circle fa-fw'></span>&nbsp;<span class='bold'>Uploaded file is correct and ready to be imported!</span> ".$msg."</div>";
            echo '<table id="date_filter_table customTable" width="100%">
                        <tr><td>
                            <input type="button" onClick="goBackToTwo()" class="btn btn-primary btn-sm" value="Back" disabled="true" />
                            <input type="button" onClick="goToLastStep()" class="btn btn-primary btn-sm" value="Continue"/>
                        </td></tr>
                     </table> <script type="text/javascript">reloadCustomers()</script>
                     ';
        }
        #FREE
        //        spl_autoload_register(array('YiiBase', 'autoload'));
        die;
        
    }
    
    public function actionLoadChecklistOutlets($id)
    {
        //get the outlet_checklists
        $outletCheckLists = OutletChecklists::model()->findAll('checklist_id = :checkListId', [':checkListId' => $id]);
        $outletList = '';
        $i = 0;
        foreach($outletCheckLists as $list)
        {
            $outletList .= ($i == 0) ? $list->outlet_id : chr(10) .  $list->outlet_id;
            ++$i;
        }
        $outletListVal = '';
        if($outletList != '')
        {
            $outletListVal = $outletList;
        }
        $this->renderPartial('loadChecklist', array(
            'outletListVal' => $outletListVal
        ));

    }
    //----UM-2354 ----//
    public function actionDownloadCustomerList()
    {
    
       $con = Yii::app()->db;
       $search = '';
       $replace = '';
       #INITIALIZE..
       ini_set("memory_limit", "-1");
       set_time_limit(0);
       $eid = Yii::app()->user->id;
       $bakery = Yii::app()->user->bakery;
       $bakery_id = Yii::app()->user->bakery;
       $folder = Yii::app()->basePath . "/../files/";
       $iChecklistCustomers=[];
       $iCustomers=[];
       $iBakeryList=[];
       
       $con = Yii::app()->db;
       $role=Yii::app()->user->role;
       $sql="";
       $bakery_sql="";
       $outletChecklist_sql="";
       $mimetypes = array(
           'text/xls',
           'application/excel',
           'application/vnd.ms-excel',
           'application/vnd.msexcel',
           'application/octet-stream',
       );
       $columns = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K');
       $mFatal = '';
       $mWarning = '';
       $count = $uCounter = $iCounter = 0;


       
   
       #PHPExcel
       //        spl_autoload_unregister(array('YiiBase', 'autoload'));
       $phpExcelPath = Yii::getPathOfAlias('ext.phpexcel.Classes');
       include($phpExcelPath . DIRECTORY_SEPARATOR . 'PHPExcel.php');

       $i = 0;
       $idx = 1;
       $excel = new PHPExcel();

       $objWorkSheet = $excel->createSheet($i); //Setting index when creating
       // STYLE EXCEL
       $estiloTituloReporte = array('font' => array('name' => 'Verdana', 'bold' => true, 'italic' => false, 'strike' => false, 'size' => 10, 'color' => array('rgb' => 'FFFFFF')), 'fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('argb' => 'FF244062')), 'borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => 'FFFFFF'))), 'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, 'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER, 'rotation' => 0, 'wrap' => TRUE));
       $estiloTituloColumnas = array('font' => array('name' => 'Arial', 'bold' => true, 'size' => '11', 'color' => array('rgb' => 'FFFFFF')), 'fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('argb' => 'FFA5A5A5')), 'borders' => array('top' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM, 'color' => array('rgb' => '143860')), 'right' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM, 'color' => array('rgb' => '143860')), 'bottom' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM, 'color' => array('rgb' => '143860'))), 'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, 'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER, 'wrap' => TRUE));
       $estiloInformacion = new PHPExcel_Style();
       $estiloInformacion->applyFromArray(array('font' => array('name' => 'Arial', 'color' => array('rgb' => '000000')), 'fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('argb' => 'FFDCE6F1')), 'borders' => array('right' => array('style' => PHPExcel_Style_Border::BORDER_THIN, 'color' => array('rgb' => '3a2a47')), 'bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN, 'color' => array('rgb' => '3a2a47')))));
        //$objWorkSheet->getStyle('A')->applyFromArray($estiloTituloColumnas);
        $objWorkSheet->getStyle('A1:E1')->applyFromArray($estiloTituloReporte);
       //===============================================================//H-1
        $objWorkSheet->setTitle("Customers");
        $objWorkSheet->getColumnDimension("A")->setWidth(10);
        $objWorkSheet->getColumnDimension("B")->setWidth(35);
        $objWorkSheet->getColumnDimension("C")->setWidth(35);
        $objWorkSheet->getColumnDimension("D")->setWidth(35);
        $objWorkSheet->getColumnDimension("E")->setWidth(20);
       $objWorkSheet->getStyle("A1:Z1")->applyFromArray(array("font" => array("bold" => true)));        
       
       $objWorkSheet->setCellValue('A1', 'ID')
              ->setCellValue('B1', 'Account Number')
              ->setCellValue('C1', 'Customer Name')
              ->setCellValue('D1', 'Customer Group')
              ->setCellValue('E1', 'Branch');
       if($bakery_id != 0)
       {
                $outlets_sql = "SELECT o.id,o.account_number,o.description,b.description as branch,o.address, o.contact_tel,
                og.description oulet_group, opg.description price_group
            FROM `outlet` o
            INNER JOIN `bakery` b ON o.bakery_id=b.id
            INNER JOIN `outlet_group` og ON o.outlet_group_id=og.id
            INNER JOIN `outlet_price_group` opg ON o.outlet_price_group_id=opg.id
            /*INNER JOIN `route` r ON o.route_id=r.id*/
            WHERE o.status_id =1 AND o.approved=1 AND o.bakery_id='".$bakery_id."'";
       }
       else
       {
                $outlets_sql = "SELECT o.id,o.account_number,o.description,b.description as branch,o.address, o.contact_tel,
                og.description oulet_group, opg.description price_group
            FROM `outlet` o
            INNER JOIN `bakery` b ON o.bakery_id=b.id
            INNER JOIN `outlet_group` og ON o.outlet_group_id=og.id
            INNER JOIN `outlet_price_group` opg ON o.outlet_price_group_id=opg.id
            /*INNER JOIN `route` r ON o.route_id=r.id*/
            WHERE o.status_id =1 AND o.approved=1";

       }       

       $customersData = Yii::app()->db->createCommand($outlets_sql)->queryAll();
       $iCustomers = array();
       $idx = 1;
       foreach($customersData as $val)
       {
          
           $idx++;
           $objWorkSheet->getStyle('A'.$idx)->applyFromArray(array("font" => array("bold" => true,'color' => array('rgb' => 'FFFFFF')),'fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('argb' => 'FFA5A5A5'))));
           $objWorkSheet->setCellValue('A'.$idx, $val['id'])
                  ->setCellValue('B'.$idx, $val['account_number'])
                  ->setCellValue('C'.$idx, $val['description'])
                  ->setCellValue('D'.$idx, $val['oulet_group'])
                  ->setCellValue('E'.$idx, $val['branch']);
                  if($bakery_id != 0)
                  {
                    $branch= $val['branch'];
                  }
                  else
                  {
                    $branch= 'All Branches';
                  }

       }       
       
       $objWriter = new PHPExcel_Writer_Excel2007($excel);
       $objWriter->setPreCalculateFormulas(false);
       $downloadFilename = $branch.' - Customers List '.date('Y-M-d').'.xlsx';
       header('Content-Disposition: attachment; filename="'.$downloadFilename.'"');
       $objWriter->save('php://output');


    }
    //---END-UM-2354 ----//
    /*===============================//
    /   END DOWNLOAD CUSTOMERS
    /
    /==================================*/
    public function actionPasteValidation($id, $fromAddEdit = '0')
    {
        $con = Yii::app()->db;
        $iCustomers=[];
        $role=Yii::app()->user->role;
        $mFatal='';
        $count = $uCounter = $iCounter = 0;
        $bakery_id=Yii::app()->user->bakery;
        // get customers
        if($role==1)
        {
            $sql='select bakery_id from checklist_bakeries where checklist_id='.$id;
            $checklist_bakeries=Yii::app()->db->createCommand($sql)->queryAll();
            $bakaries='';
            $bakerySelected='';
            
            if(count($checklist_bakeries)>0)
            {
                foreach($checklist_bakeries as $bakery)
                {
                    $bakaries.=','.$bakery['bakery_id'];
                    $bakerySelected.=','.$bakery['bakery_id'];
                }
                $bakaries=trim($bakaries,',');
                $bakerySelected=trim($bakerySelected,',');
            }
            $outletChecklist_bakery=$bakaries!='' ? ' AND bakery_id IN ('. $bakaries.')':'';
            $bakaries= $bakaries!='' ? ' AND o.bakery_id IN ('. $bakaries.')':'';
            $sql = "SELECT o.id,o.account_number,o.description,b.description as branch,o.bakery_id
                FROM `outlet` o
                LEFT JOIN `bakery` b ON o.bakery_id=b.id
                WHERE o.status_id =1 ".$bakaries;
            
        }
        elseif($role==2){
            $sql = "SELECT o.id,o.account_number,o.description,b.description as branch,o.bakery_id
                FROM `outlet` o
                LEFT JOIN `bakery` b ON o.bakery_id=b.id
                WHERE o.status_id =1 AND o.bakery_id=".$bakery_id;
        }
        
        
        $customersData = Yii::app()->db->createCommand($sql)->queryAll();
        foreach($customersData as $val)
        {
            $iCustomers[] = $val['id'];
        }
        
        $customerList=trim($_POST['customers']);
        //replace all tabs with breaks
        $customerList=  trim(preg_replace('/\s+/', "\n", $customerList));
        $customerArray = explode("\n", $customerList);
        
        if(count($customerArray)>0 && $customerArray[0] !='')
        {
            
            foreach($customerArray as $customer)
            {
                
                
                $non_numeric=preg_match('/^[0-9]*$/',$customer);
                
                if(!in_array(trim($customer), $iCustomers))
                {
                    
                    
                    if(($customer != "" || $customer > 0) && !$non_numeric)
                    {
                        $mFatal.='- '.$customer.' : A number required. <br/>';
                    }
                    elseif(($customer != "" || $customer > 0) && $non_numeric)
                    {
                        $mFatal .= '- '.$customer.': invalid customer id<br/>';
                    }
                    else
                    {
                        $iCounter++;
                    }
                }
                else
                {
                    $uCounter++;
                }
            }
            
        }
        else {
            $mFatal.='Please provide a list of customers you would like to link to this form';
        }
        if($mFatal != '')
        {
            $disabled = '';
            $html = '';
            if($mFatal != '')
            {
                $html .= <<<HTML
                <div class='alert alert-danger'><b>FATAL ERROR!<br/>You can not update the customer list as it contains errors:</b><br/>{$mFatal}</div>
HTML;
                $part = '<input type="button" onClick="pasteGoBackToTwo()" class="btn btn-primary btn-sm" value="Back" />
                              <input type="button" id="paste_next4" class="btn btn-primary btn-sm" value="Next" disabled="true"/>';
                $disabled = 'disabled="true"';
                if($fromAddEdit == '1')
                {
                    return ['valid' => false, 'msg' => trim($html)];
                    die();
                }
            }
            if($fromAddEdit == '0')
            {
                $html .= <<<HTML
                <table id="date_filter_table customTable" width="100%">
                    <tr><td>{$part}</td></tr>
                </table>
                <script type="text/javascript">reloadCustomers()</script>
HTML;
                echo $html;
            }
        }
        else
        {
            $count = $uCounter + $iCounter;
            //            $msg = $count . ' records found: ' . $iCounter . ' new and ' . $uCounter . ' existing products';
            $msg = $count . ' records found';
            $html = <<<HTML
           <div class='alert alert-success'><span class='fa fa-check-circle fa-fw'></span>&nbsp;<span class='bold'>The list is correct and ready to be saved!</span>{$msg}</div>
HTML;
            if($fromAddEdit == '0')
            {
                $html .= <<<HTML
                <table id="date_filter_table customTable" width="100%">
                    <tr><td>
                        <input type="button" onClick="" class="btn btn-primary btn-sm" value="Back" disabled="true" />
                        <input type="button" onClick="pasteGoToLastStep()" class="btn btn-primary btn-sm" value="Continue"/>
                    </td></tr>
                    </table>
                <script type="text/javascript">reloadCustomers()</script>
HTML;
                
                echo $html;
            }
            else
            {
                return ['valid' => true, 'msg' => ''];
            }
        }
        #FREE
        //        spl_autoload_register(array('YiiBase', 'autoload'));
        die;
        
        
        
        die;
    }
    public function actionImportUpdate($id)
    {
        $con = Yii::app()->db;
        $search = '';
        $replace = '';
        #INITIALIZE..
        ini_set("memory_limit", "-1");
        set_time_limit(0);
        $eid = Yii::app()->user->id;
        $bakery = Yii::app()->user->bakery;
        $bakery_id= Yii::app()->user->bakery;
        $folder = Yii::app()->basePath . "/../files/";
        $iChecklistCustomers=[];
        $iCustomers=[];
        $iBakeryList=[];
        
        $con = Yii::app()->db;
        $role=Yii::app()->user->role;
        $sql="";
        $bakery_sql="";
        $outletChecklist_sql="";
        $mimetypes = array(
            'text/xls',
            'application/excel',
            'application/vnd.ms-excel',
            'application/vnd.msexcel',
            'application/octet-stream',
        );
        $columns = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K');
        $mFatal = '';
        $mWarning = '';
        $count = $uCounter = $iCounter = 0;
        
        // get customers
        if($role==1)
        {
            $sql='select bakery_id from checklist_bakeries where checklist_id='.$id;
            $checklist_bakeries=Yii::app()->db->createCommand($sql)->queryAll();
            $bakaries='';
            $bakerySelected='';
            
            if(count($checklist_bakeries)>0)
            {
                foreach($checklist_bakeries as $bakery)
                {
                    $bakaries.=','.$bakery['bakery_id'];
                    $bakerySelected.=','.$bakery['bakery_id'];
                }
                $bakaries=trim($bakaries,',');
                $bakerySelected=trim($bakerySelected,',');
            }
            $outletChecklist_bakery=$bakaries!='' ? ' AND bakery_id IN ('. $bakaries.')':'';
            $bakaries= $bakaries!='' ? ' AND o.bakery_id IN ('. $bakaries.')':'';
            $sql = "SELECT o.id,o.account_number,o.description,b.description as branch,o.bakery_id
                FROM `outlet` o
                LEFT JOIN `bakery` b ON o.bakery_id=b.id
                WHERE o.status_id =1 ".$bakaries;
            
            $bakerySelected=  $bakerySelected!=''?' AND id IN('. $bakerySelected.')':'';
            $bakery_sql="SELECT id,description,physical_address FROM `bakery` WHERE status_id =1 ".$bakerySelected;
            
            
            $outletChecklist_sql="SELECT * FROM outlet_checklists WHERE checklist_id=".$id." ".$outletChecklist_bakery;
        }
        elseif($role==2){
            $sql = "SELECT o.id,o.account_number,o.description,b.description as branch,o.bakery_id
                FROM `outlet` o
                LEFT JOIN `bakery` b ON o.bakery_id=b.id
                WHERE o.status_id =1 AND o.bakery_id=".$bakery_id;
            $bakery_sql="SELECT id,description,physical_address FROM `bakery` WHERE status_id =1 AND id=".$bakery_id;
            $outletChecklist_sql="SELECT * FROM `outlet_checklists` WHERE checklist_id=".$id." AND bakery_id=".$bakery_id;
        }
        
        
        $customersData = Yii::app()->db->createCommand($sql)->queryAll();
        foreach($customersData as $val)
        {
            $iCustomers[] = $val['id'];
        }
        
        $bakeryListData = Yii::app()->db->createCommand($bakery_sql)->queryAll();
        foreach($bakeryListData as $val)
        {
            $iBakeryList[] = $val['id'];
        }
        
        $checklistCustomersData = Yii::app()->db->createCommand($outletChecklist_sql)->queryAll();
        
        
        foreach($checklistCustomersData as $val)
        {
            $sql="DELETE FROM outlet_checklists WHERE id =".$val['id'];
            $con->createCommand($sql)->execute();
            $iChecklistCustomers[] = $val['id'];
        }
        
        $newCustomerList=[];
        #PHPExcel
        //        spl_autoload_unregister(array('YiiBase', 'autoload'));
        $phpExcelPath = Yii::getPathOfAlias('ext.phpexcel.Classes');
        include($phpExcelPath . DIRECTORY_SEPARATOR . 'PHPExcel.php');
        $objPHPExcel = new PHPExcel();
        #Reading
        $numberOfRecords = 0;
        if(is_uploaded_file($_FILES['customer_list_file']['tmp_name']))
        {
            if(move_uploaded_file($_FILES['customer_list_file']['tmp_name'], $folder . $_FILES['customer_list_file']['name']))
            {
                $filename = $folder . $_FILES['customer_list_file']['name'];
                $objPHPExcel = PHPExcel_IOFactory::load($filename);
                $extendedInsert = [];
                $bindings = [];
                
                foreach($objPHPExcel->getWorksheetIterator() as $worksheet)
                {
                    //Worksheet info
                    $worksheetTitle = $worksheet->getTitle();
                    $highestRow = $worksheet->getHighestRow();
                    $numberOfRecords = $highestRow - 2;
                    $highestColumn = $worksheet->getHighestColumn();
                    $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
                    $nrColumns = ord($highestColumn) - 64;
                    //Worksheet data
                    $i = 0;
                    for($row = 3; $row <= $highestRow; ++$row)
                    {
                        ################################
                        #Column A: checklist customer  ID
                        $cell = $worksheet->getCellByColumnAndRow(0, $row);
                        $coid = $cell->getValue();
                        $newCustomerList[]=$coid;
                        if(in_array(trim($coid), $iChecklistCustomers))
                            $action = 'Update';
                            else
                                $action = 'Insert';
                                ################################
                                #Column B: customer ID
                                $cell = $worksheet->getCellByColumnAndRow(0, $row);
                                $customer_id = str_replace($search, $replace, $cell->getValue());
                                $connect = Yii::app()->db;
                                $sql="select bakery_id from outlet where id=".$customer_id;
                                $branch=Outlet::model()->findByPk($customer_id);
                                
                                $createDate = date('Y-m-d h:i:s');
                                
                                $bindings[$i] = [
                                    
                                    ':checklist_id' . $row => [$id, PDO::PARAM_INT, 'checklist_id'],
                                    ':outlet_id' . $row => [$customer_id, PDO::PARAM_INT, 'outlet_id'],
                                    ':bakery_id' . $row => [$branch['bakery_id'], PDO::PARAM_INT, 'bakery_id'],
                                    ':datetime' . $row => [$createDate, PDO::PARAM_STR, 'datetime']
                                ];
                                unset($customer_id, $branch_id);
                                ++$i;
                    }
                    //Stop after reading the first worksheet
                    break;
                }
                
                $autTable = AuthTables::model()->with('fields')->find('table_key = \'outlet_checklists\'');
                $loggedInEmployee = Employee::model()->with('group.role')->findByPk(Yii::app()->user->id);
                $editableFields = [];
                if(count($autTable)>0)
                {
                    foreach($autTable['fields'] as $fields)
                    {
                        $editableFields[] = $fields['field'];
                    }
                }
                
                
                $values = [];
                $updateValues = [];
                foreach($bindings as $arr)
                {
                    
                    
                    $keys = array_keys($arr);
                    $values[] = '(' . implode(',',$keys) . ')';
                    $tmpUpdateValues = [];
                    foreach($arr as $key => $details)
                    {
                        $column = $details[2];
                        
                        $tmpUpdateValues[] = $column . '=VALUES(' . $column . ')';
                        
                        
                    }
                    
                    $updateValues[] = implode(',', $tmpUpdateValues);
                }
                
                $chunkSize = 1000;
                $chunkValues = array_chunk($values, $chunkSize);
                $chunkBindings = array_chunk($bindings, $chunkSize);
                $sqlCommands = [];
                foreach($chunkValues as $chunk)
                {
                    $sql = <<<SQL
                INSERT INTO outlet_checklists
                (`checklist_id`,`outlet_id`,`bakery_id`,`datetime`)
                VALUES
SQL;
                    $chunkString = implode(',',$chunk);
                    $sql .= $chunkString;
                    
                    
                    $chunkUpdateString = $updateValues[0];
                    $sql .= ' ON DUPLICATE KEY UPDATE ' . $chunkUpdateString ;
                    //   echo $sql . '<br><br>';
                    
                    
                    $sqlCommand = $con->createCommand($sql);
                    
                    $sqlCommands[] = $sqlCommand;
                }
                
                
                $i = 0;
                foreach($sqlCommands as $command)
                {
                    for($b = 0; $b < count($chunkBindings[$i]); $b++)
                    {
                        $rowBinding = $chunkBindings[$i][$b];
                        foreach($rowBinding as $key => $details)
                        {
                            $value = $details[0];
                            $PDOType = $details[1];
                            $command->bindValue($key, $value, $PDOType);
                        }
                    }
                    
                    $command->execute();
                    
                    ++$i;
                }
                $maxIdSql = <<<SQL
                SELECT
                    id
                FROM
                    outlet_checklists
                ORDER BY id DESC
                LIMIT 1;
SQL;
                $maxId = $con->createCommand($maxIdSql)->queryScalar();
                $newMaxId = $maxId + 1;
                $maxIdUpdateSql = <<<SQL
                ALTER TABLE outlet_checklists AUTO_INCREMENT={$newMaxId};
SQL;
                //$con->createCommand($maxIdUpdateSql)->execute();
            }
        }
        
        
        
        $message="";
        
        echo '<div class="alert alert-success"><span class="fa fa-check-circle fa-fw"></span>&nbsp;'.$numberOfRecords.' Records processed'.$message.'</div>';
        die;
        
    }
    public function actionPasteUpdate($id, $fromAddEdit = '0')
    {
        $con = Yii::app()->db;
        $iCustomers=[];
        $role=Yii::app()->user->role;
        $mFatal='';
        $count = $uCounter = $iCounter = 0;
        $bakery_id=Yii::app()->user->bakery;
        // get customers
        if($role==1)
        {
            $sql='select bakery_id from checklist_bakeries where checklist_id='.$id;
            $checklist_bakeries=Yii::app()->db->createCommand($sql)->queryAll();
            $bakaries='';
            $bakerySelected='';
            
            if(count($checklist_bakeries)>0)
            {
                foreach($checklist_bakeries as $bakery)
                {
                    $bakaries.=','.$bakery['bakery_id'];
                    $bakerySelected.=','.$bakery['bakery_id'];
                }
                $bakaries=trim($bakaries,',');
                $bakerySelected=trim($bakerySelected,',');
            }
            $outletChecklist_bakery=$bakaries!='' ? ' AND bakery_id IN ('. $bakaries.')':'';
            $bakaries= $bakaries!='' ? ' AND o.bakery_id IN ('. $bakaries.')':'';
            $sql = "SELECT o.id,o.account_number,o.description,b.description as branch,o.bakery_id
                FROM `outlet` o
                LEFT JOIN `bakery` b ON o.bakery_id=b.id
                WHERE o.status_id =1 ".$bakaries;
            
            $bakerySelected=  $bakerySelected!=''?' AND id IN('. $bakerySelected.')':'';
            $bakery_sql="SELECT id,description,physical_address FROM `bakery` WHERE status_id =1 ".$bakerySelected;
            
            
            $outletChecklist_sql="SELECT * FROM outlet_checklists WHERE checklist_id=".$id." ".$outletChecklist_bakery;
        }
        elseif($role==2){
            $sql = "SELECT o.id,o.account_number,o.description,b.description as branch,o.bakery_id
                FROM `outlet` o
                LEFT JOIN `bakery` b ON o.bakery_id=b.id
                WHERE o.status_id =1 AND o.bakery_id=".$bakery_id;
            $bakery_sql="SELECT id,description,physical_address FROM `bakery` WHERE status_id =1 AND id=".$bakery_id;
            $outletChecklist_sql="SELECT * FROM `outlet_checklists` WHERE checklist_id=".$id." AND bakery_id=".$bakery_id;
        }
        
        
        $customersData = Yii::app()->db->createCommand($sql)->queryAll();
        foreach($customersData as $val)
        {
            $iCustomers[] = $val['id'];
        }
        
        $checklistCustomersData = Yii::app()->db->createCommand($outletChecklist_sql)->queryAll();
        
        
        foreach($checklistCustomersData as $val)
        {
            $sql="DELETE FROM outlet_checklists WHERE id =".$val['id'];
            $con->createCommand($sql)->execute();
            $iChecklistCustomers[] = $val['id'];
        }
        
        $customerList=trim($_POST['customers']);
        //replace all tabs with breaks
        $customerList=  trim(preg_replace('/\s+/', "\n", $customerList));
        $customerArray = explode("\n", $customerList);
        $bindings = [];
        $numberOfRecords = 0;
        if(count($customerArray)>0)
        {
            $i = 0;
            $row=1;
            foreach($customerArray as $customer)
            {
                if($customer !=='' || $customer==0)
                {
                    $branch=Outlet::model()->findByPk($customer);
                    $createDate = date('Y-m-d h:i:s');
                    
                    $bindings[$i] = [
                        ':checklist_id' . $row => [$id, PDO::PARAM_INT, 'checklist_id'],
                        ':outlet_id' . $row => [$customer, PDO::PARAM_INT, 'outlet_id'],
                        ':bakery_id' . $row => [$branch['bakery_id'], PDO::PARAM_INT, 'bakery_id'],
                        ':datetime' . $row => [$createDate, PDO::PARAM_STR, 'datetime']
                    ];
                    ++$i;
                    ++$row;
                    ++$numberOfRecords;
                }
                
                
            }
        }
        
        $autTable = AuthTables::model()->with('fields')->find('table_key = \'outlet_checklists\'');
        $loggedInEmployee = Employee::model()->with('group.role')->findByPk(Yii::app()->user->id);
        $editableFields = [];
        if(count($autTable)>0)
        {
            foreach($autTable['fields'] as $fields)
            {
                $editableFields[] = $fields['field'];
            }
        }
        
        
        $values = [];
        $updateValues = [];
        foreach($bindings as $arr)
        {
            
            
            $keys = array_keys($arr);
            $values[] = '(' . implode(',',$keys) . ')';
            $tmpUpdateValues = [];
            foreach($arr as $key => $details)
            {
                $column = $details[2];
                
                $tmpUpdateValues[] = $column . '=VALUES(' . $column . ')';
                
                
            }
            
            $updateValues[] = implode(',', $tmpUpdateValues);
        }
        
        $chunkSize = 1000;
        $chunkValues = array_chunk($values, $chunkSize);
        $chunkBindings = array_chunk($bindings, $chunkSize);
        $sqlCommands = [];
        foreach($chunkValues as $chunk)
        {
            $sql = <<<SQL
                INSERT INTO outlet_checklists
                (`checklist_id`,`outlet_id`,`bakery_id`,`datetime`)
                VALUES
SQL;
            $chunkString = implode(',',$chunk);
            $sql .= $chunkString;
            
            
            $chunkUpdateString = $updateValues[0];
            $sql .= ' ON DUPLICATE KEY UPDATE ' . $chunkUpdateString ;
            //   echo $sql . '<br><br>';
            
            
            $sqlCommand = $con->createCommand($sql);
            
            $sqlCommands[] = $sqlCommand;
        }
        
        
        $i = 0;
        foreach($sqlCommands as $command)
        {
            for($b = 0; $b < count($chunkBindings[$i]); $b++)
            {
                $rowBinding = $chunkBindings[$i][$b];
                foreach($rowBinding as $key => $details)
                {
                    $value = $details[0];
                    $PDOType = $details[1];
                    $command->bindValue($key, $value, $PDOType);
                }
            }
            
            $command->execute();
            
            ++$i;
        }
        $maxIdSql = <<<SQL
                SELECT
                    id
                FROM
                    outlet_checklists
                ORDER BY id DESC
                LIMIT 1;
SQL;
        $maxId = $con->createCommand($maxIdSql)->queryScalar();
        $newMaxId = $maxId + 1;
        $maxIdUpdateSql = <<<SQL
                ALTER TABLE outlet_checklists AUTO_INCREMENT={$newMaxId};
SQL;
        //$con->createCommand($maxIdUpdateSql)->execute();
        
        
        
        $message="";
        
        if($fromAddEdit == '0')
        {
            echo '<div class="alert alert-success"><span class="fa fa-check-circle fa-fw"></span>&nbsp;'.$numberOfRecords.' Records processed'.$message.'</div>';
            die();
        }
        
        
    }
    public function actioncopyForm()
    {
        $id=$_POST['id'];
        $checklist=[];
        
        //Get check list
        $sql='SELECT * FROM checklist where id='.$id;
        $checklist=Yii::app()->db->createCommand($sql)->queryRow();
        
        
        //Get checklist Items
        $sql='SELECT * FROM `checklist_items` WHERE `checklist_id`='.$id;
        $checklist['checklist_items']=Yii::app()->db->createCommand($sql)->queryAll();
        
        if(count($checklist['checklist_items'])>0)
        {
            foreach($checklist['checklist_items'] as $key=>$value)
            {
                //Get checklist options
                $sql="SELECT * FROM `checklist_options` WHERE `checklist_items_id`=".$value['id'];
                $checklist['checklist_items'][$key]['checklist_options']=Yii::app()->db->createCommand($sql)->queryAll();
                if(count($checklist['checklist_items'][$key]['checklist_options'])>0)
                {
                    $checklist_options=$checklist['checklist_items'][$key]['checklist_options'];
                    foreach($checklist_options as $option_key=>$option_value)
                    {
                        //Get checklist option groups
                        $sql="SELECT * FROM `checklist_option_groups` WHERE `checklist_option_id`=".$option_value['id'];
                        $checklist['checklist_items'][$key]['checklist_options'][$option_key]['checklist_option_groups']=Yii::app()->db->createCommand($sql)->queryAll();
                        
                        if(count($checklist['checklist_items'][$key]['checklist_options'][$option_key]['checklist_option_groups'])>0)
                        {
                            $checklist_option_groups=$checklist['checklist_items'][$key]['checklist_options'][$option_key]['checklist_option_groups'];
                            foreach($checklist_option_groups as $option_group_key=>$option_group_value)
                            {
                                //Get checklist option groups score
                                $sql="SELECT * FROM `checklist_option_scores` WHERE `checklist_option_groups_id`=".$option_group_value['id'];
                                $checklist['checklist_items'][$key]['checklist_options'][$option_key]['checklist_option_groups'][$option_group_key]['checklist_option_scores']=Yii::app()->db->createCommand($sql)->queryAll();
                                
                                //Get compliance
                                $sql="SELECT * FROM `checklist_compliance_calculation` WHERE `checklist_option_group_id`=".$option_group_value['id'];
                                $checklist['checklist_items'][$key]['checklist_options'][$option_key]['checklist_option_groups'][$option_group_key]['checklist_compliance_calculation']=Yii::app()->db->createCommand($sql)->queryAll();
                            }
                        }
                        
                        // checklist additional
                        //Get checklist additional options
                        //echo $key."--";
                        $sql="SELECT * FROM `checklist_additional_options` WHERE `checklist_id`=".$id. " AND checklist_item_id=".$value['id']." AND `checklist_options_id`=".$option_value['id'];
                        $checklist['checklist_items'][$key]['checklist_options'][$option_key]['checklist_additional_options']=Yii::app()->db->createCommand($sql)->queryAll();
                    }
                    
                }
            }
        }
        
        
        // Copy function
        $yabby = new YabbyFormManager;
        $yabby->copyForm($checklist);
        
        Yii::app()->user->setFlash('success', $checklist['checklist_name'].' - Copy is successfully created');
        die ( json_encode ( array ('checklist'=>$checklist
        ) ) );
    }
    public function actionProductPriceChecklist()
    {
        
        $model = new Checklist('productChecklistSearch');
        $model->unsetAttributes();  // clear any default values
        //showing active records by default
        $model->status_id = 1;
        if(isset($_GET['Checklist']))
        {
            $model->attributes = $_GET['Checklist'];
        }
        $this->render('productPriceForm', array(
            'model' => $model
        ));
    }
    
    public function actionaddProductChecklist()
    {
        $model = new Checklist('create');
        $condition = Yii::app()->user->bakery != 0 ? array('status_id' => 1, 'bakery_id' => Yii::app()->user->bakery) : array('status_id' => 1);
        $empGrp = EmployeeGroup::model()->findAll('status_id =1');
        $employee = Employee::model()->findAllByAttributes($condition);
        $bakeries = Bakery::model()->findAllByAttributes(array('status_id' => 1), array('order' => 'description ASC'));
        $checklistProductItems=[];
        $selectedChecklistBakeries = array();
        $employeeListArray = array();
        foreach($empGrp as $grp)
        {
            foreach($employee as $emp)
            {
                if($emp['employee_group_id'] == $grp['id'])
                {
                    $employeeListArray[$grp['description']][] = array('id' => $emp['id'], 'name' => $emp['name']);
                }
            }
        }
        $schedule = ChecklistNotificationSchedule::model()->findAll('status_id =1');
        $selected_emp = array();
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);
        if(isset($_POST['Checklist']))
        {
            
            $model->attributes = $_POST['Checklist'];
            $model->created_by=Yii::app()->user->id;
            $model->created_date=date("Y-m-d H:i:s");
            $model->checklist_type_id=4;
            $model->checklist_category_id = $_POST['Checklist']['checklist_category_id'];
            if ($model->save()){
                $id = Yii::app()->db->getLastInsertId();
                $exceptionArray = isset($_POST['exception_emp']) ? $_POST['exception_emp'] : array();
                $this->saveExceptionRecipients($id, $exceptionArray, $selected_emp);
                $selected_bakery = isset($_POST['bakery_list']) ? $_POST['bakery_list'] : array();
                foreach($bakeries as $bakery)
                {
                    if(in_array($bakery->id, $selected_bakery) && !in_array($bakery->id, $selectedChecklistBakeries))
                    {
                        $saveChecklistBakery = new ChecklistBakeries();
                        $saveChecklistBakery->checklist_id = $id;
                        $saveChecklistBakery->bakery_id = $bakery->id;
                        $saveChecklistBakery->save();
                    }
                    elseif(!in_array($bakery->id, $selected_bakery) && in_array($bakery->id, $selectedChecklistBakeries))
                    {
                        ChecklistBakeries::model()->deleteAllByAttributes(array('checklist_id' => $id, 'bakery_id' => $bakery->id));
                    }
                }
                $this->redirect(array('productPriceChecklist'));
            }
        }
        $this->render('addProductChecklist', array(
            'model' => $model, 'employeeListArray' => $employeeListArray, 'selected_emp' => $selected_emp, 'schedule' => $schedule, 'bakeryList' => $bakeries, 'selectedChecklistBakeries' => $selectedChecklistBakeries,'checklistProductItems'=>$checklistProductItems
        ));
    }
    
    public function actionRequestApproal($id,$form,$link)
    {
        
        
        $con = Yii::app()->db;
        $sql='UPDATE checklist set status_id=2 where id='.$id;
        $command = $con->createCommand($sql);
        $command->execute();
        
        $SQL = "INSERT INTO `checklist_reviews_logs`(`date_created`,`status_id`, `checklist_id`, `employee_id`, `reason`)
                 VALUES ('".date('Y-m-d H:i:s')."',2,".$id.",".Yii::app()->user->id.",'Form Requires Approval')";
        $insert_command = Yii::app()->db->createCommand($SQL);
        $insert_command->execute();
        
        $checklist=Checklist::model()->findByPk($id);
        $user= Employee::model()->findByPk($checklist ['request_approval_by']);
        $requester=Employee::model()->findByPk(Yii::app()->user->id);
        $content = '
				<p>You have been assigned to review Form: '.$checklist ['description'].' by '.$requester['name'].'</p>
				<p>
					To review this form <a href="'. Yii::app()->getBaseUrl(true) . '/index.php/admin/yabbyForms/'.$link.'/id/'.$id.'">click here</a>.
				</p>
			';
        $SIHFrom = Yii::app()->params['adminEmail'];
        $headers = "From: {$SIHFrom}\r\nReply-To: {$SIHFrom} \r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=UTF-8\r\n";
        if(@mail($user['email'], "Form Review Request", $content, $headers))
        {
            $communication_id =$this->addCommunicationLog($user['email'], 'SENT', $content);
        }
        else
        {
            $communication_id =$this->addCommunicationLog($user['email'], 'CREATED', '');
            
        }
        
        
        Yii::app()->user->setFlash('success','Form status has been updated');
        $array = array($form);
        $this->redirect($array);
    }
    public function addCommunicationLog($email,$status,$message)
    {
        $date=date("Y-m-d H:i:s");
        $sql = "INSERT INTO `communication_log` (email,status,message,communication_date) VALUES (:email,:status,:message,:communication_date)";
        $stmt = Yii::app()->db->createCommand($sql);
        $stmt->bindValue(':email', $email, PDO::PARAM_STR);
        $stmt->bindValue(':status', $status, PDO::PARAM_STR);
        $stmt->bindValue(':message', $message, PDO::PARAM_STR);
        $stmt->bindValue(':communication_date', $date, PDO::PARAM_STR);
        $stmt->execute();
        $id = Yii::app()->db->getLastInsertID();
        return $id;
    }
    public function actionUnlockApproal($id,$form,$link)
    {
        $con = Yii::app()->db;
        $sql='UPDATE checklist set status_id=0 where id='.$id;
        $command = $con->createCommand($sql);
        $command->execute();
        
        $SQL = "INSERT INTO `checklist_reviews_logs`(`date_created`,`status_id`, `checklist_id`, `employee_id`, `reason`)
                 VALUES ('".date('Y-m-d H:i:s')."',0,".$id.",".Yii::app()->user->id.",'Form is unlocked for editing')";
        $insert_command = Yii::app()->db->createCommand($SQL);
        $insert_command->execute();
        
        
        $checklist=Checklist::model()->findByPk($id);
        $user_id=0;
        if($checklist ['created_by'] !='' && $checklist ['updated_by']=='')
        {
            $user_id=$checklist ['created_by'];
        }
        elseif($checklist ['created_by'] !='' && $checklist ['updated_by']!='')
        {
            $user_id=$checklist ['updated_by'];
        }
        $user= Employee::model()->findByPk($user_id);
        $content = '
				<p>'.$checklist ['description'].' Form has been unlocked for editing.</p>
				<p>
					To review this form <a href="'. Yii::app()->getBaseUrl(true) . '/index.php/admin/yabbyForms/'.$link.'/id/'.$id.'">click here</a>.
				</p>
			';
        $SIHFrom = Yii::app()->params['adminEmail'];
        $headers = "From: {$SIHFrom}\r\nReply-To: {$SIHFrom} \r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=UTF-8\r\n";
        if(@mail($user['email'], "Unlocked Form", $content, $headers))
        {
            $communication_id =$this->addCommunicationLog($user['email'], 'SENT', $content);
        }
        else
        {
            $communication_id =$this->addCommunicationLog($user['email'], 'CREATED', '');
            
        }
        Yii::app()->user->setFlash('success','Form status has been updated');
        $array = array($form);
        $this->redirect($array);
    }
    public function actionApproveChecklist($id)
    {
        $con = Yii::app()->db;
        $sql='UPDATE checklist set status_id=1 where id='.$id;
        $command = $con->createCommand($sql);
        $command->execute();
        $SQL = "INSERT INTO `checklist_reviews_logs`(`date_created`,`status_id`, `checklist_id`, `employee_id`, `reason`)
                 VALUES ('".date('Y-m-d H:i:s')."',1,".$id.",".Yii::app()->user->id.",'".$_POST['comment']."')";
        $insert_command = Yii::app()->db->createCommand($SQL);
        $insert_command->execute();
        
        $checklist=Checklist::model()->findByPk($id);
        $user_id=0;
        if($checklist ['created_by'] !='' && $checklist ['updated_by']=='')
        {
            $user_id=$checklist ['created_by'];
        }
        elseif($checklist ['created_by'] !='' && $checklist ['updated_by']!='')
        {
            $user_id=$checklist ['updated_by'];
        }
        $user= Employee::model()->findByPk($user_id);
        $reviewer= Employee::model()->findByPk($checklist ['request_approval_by']);
        $content = '
				<p>'.$checklist ['description'].' Form Has been Approved by '.$reviewer['name'].'</p>
				<p>
					To view this form <a href="'. Yii::app()->getBaseUrl(true) . '/index.php/admin/yabbyForms/'.$_POST['link'].'/id/'.$id.'">click here</a>.
				</p>
			';
        $SIHFrom = Yii::app()->params['adminEmail'];
        $headers = "From: {$SIHFrom}\r\nReply-To: {$SIHFrom} \r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=UTF-8\r\n";
        if(@mail($user['email'], "Form Approved", $content, $headers))
        {
            $communication_id =$this->addCommunicationLog($user['email'], 'SENT', $content);
        }
        else
        {
            $communication_id =$this->addCommunicationLog($user['email'], 'CREATED', '');
            
        }
        
        Yii::app()->user->setFlash('success','Form status has been updated');
        die();
    }
    
    public function actionDeclineChecklist($id)
    {
        $con = Yii::app()->db;
        $sql ='UPDATE checklist set status_id=4 where id='.$id;
        $command = $con->createCommand($sql);
        $command->execute();
        $SQL = "INSERT INTO `checklist_reviews_logs`(`date_created`,`status_id`, `checklist_id`, `employee_id`, `reason`)
                 VALUES ('".date('Y-m-d H:i:s')."',4,".$id.",".Yii::app()->user->id.",'".$_POST['comment']."')";
        $insert_command = Yii::app()->db->createCommand($SQL);
        $insert_command->execute();
        
        $checklist = Checklist::model()->findByPk($id);
        $user_id=0;
        if($checklist ['created_by'] !='' && $checklist ['updated_by']=='')
        {
            $user_id=$checklist ['created_by'];
        }
        elseif($checklist ['created_by'] !='' && $checklist ['updated_by']!='')
        {
            $user_id=$checklist ['updated_by'];
        }
        $user = Employee::model()->findByPk($user_id);
        $reviewer = Employee::model()->findByPk($checklist ['request_approval_by']);
        $content = '
				<p>'.$checklist ['description'].' Form has been declined by '.$reviewer['name'].'.</p>
                <p><b>Reason:</b></p>
                <p>'.$_POST['comment'].'</p>
				<p>
					To view this form <a href="'. Yii::app()->getBaseUrl(true) . '/index.php/admin/yabbyForms/'.$_POST['link'].'/id/'.$id.'">click here</a>.
				</p>
			';
        $SIHFrom = Yii::app()->params['adminEmail'];
        $headers = "From: {$SIHFrom}\r\nReply-To: {$SIHFrom} \r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=UTF-8\r\n";
        if(@mail($user['email'], "Form Declined", $content, $headers))
        {
            $communication_id = $this->addCommunicationLog($user['email'], 'SENT', $content);
        }
        else
        {
            $communication_id = $this->addCommunicationLog($user['email'], 'CREATED', '');
            
        }
        
        Yii::app()->user->setFlash('success','Form status has been updated');
        die();
    }
    public function actionHistoryLog($id)
    {
        $con = Yii::app()->db;

        $checklist_id = (int) $id;

        $sql="SELECT crl.*,cs.`checklist_status_type`,e.name
FROM `checklist_reviews_logs` crl
LEFT JOIN `checklist_statuses` cs ON crl.status_id=cs.`checklist_status_id`
JOIN `employee` e ON crl.employee_id=e.id
WHERE crl.`checklist_id`=".$id." ORDER BY crl.`date_created`  DESC";
        $logs=$con->createCommand($sql)->queryAll();
        
        $numberOfCustomersSql = <<<SQL
        select
            c.default_all_customers,
            count(oc.outlet_id) as num_customers
        from
            checklist c
        left join outlet_checklists oc on
            oc.checklist_id = c.id
        where
            c.id = :checklistId
        group by
            c.default_all_customers;
SQL;
        $numberOfCustomersCom = $con->createCommand($numberOfCustomersSql);
        $numberOfCustomersCom->bindValue(':checklistId', $checklist_id, PDO::PARAM_INT);
        $numberOfCustomers = $numberOfCustomersCom->queryRow();

        $numberOfQuestionsSql = <<<SQL
        select
            count(ci.id) as num_question
        from
            checklist c
        left join checklist_items ci on
            ci.checklist_id = c.id
        where
            c.id = :checklistId
            and ci.status_id = 1;
SQL;
        $numberOfQuestionsCom = $con->createCommand($numberOfQuestionsSql);
        $numberOfQuestionsCom->bindValue(':checklistId', $checklist_id, PDO::PARAM_INT);
        $numberOfQuestions = $numberOfQuestionsCom->queryScalar();

        $this->renderPartial('_history_log', array(
            'id' => $id,
            'logHistory'=>$logs,
            'numberOfCustomers' => $numberOfCustomers,
            'numberOfQuestions' => $numberOfQuestions
        ));
        
    }
    public function actionTerminateForm()
    {
        $id =$_POST['id'];
        $model = Checklist::model()->findByPk($id);
        $model->status_id = 6;
        $model->terminated_by = Yii::app()->user->id;
        $model->terminated_datetime =date("Y-m-d H:i:s");
        $status  = 6;
        $model->save();
        $comment='Form Terminated';
        
        $SQL = "INSERT INTO `checklist_reviews_logs`(`date_created`,`status_id`, `checklist_id`, `employee_id`, `reason`)
                 VALUES ('".date('Y-m-d H:i:s')."',".$status.",".$id.",".Yii::app()->user->id.",'".$comment."')";
        $insert_command = Yii::app()->db->createCommand($SQL);
        $insert_command->execute();
        die ( json_encode ( array ('status'=>'success'
        ) ) );
    }
    public function actionBulkStatus()
    {
        $con = Yii::app()->db;
        $formIDs=$_POST['formIDs'];
        if(count($formIDs)>0)
        {
            foreach($formIDs as $id)
            {
                $model = Checklist::model()->findByPk($id);
                $status='';
                $comment='';
                if($model->status_id == 1)
                {
                    $model->status_id = 0;
                    $status=0;
                    $comment='Status changed to inactive';
                }
                elseif($model->status_id == 0 && $model->request_approval_by>0)
                {
                    $model->status_id =2;
                    $status=2;
                    $comment='Status changed to pending approval';
                }
                elseif($model->status_id > 0 && $model->request_approval_by>0)
                {
                    $model->status_id =0;
                    $status=0;
                    $comment='Status changed to inactive';
                }
                elseif($model->status_id == 0 && $model->request_approval_by==0)
                {
                    $model->status_id = 1;
                    $status=2;
                    $comment='Status changed to active';
                }
                $model->save();
                
                $SQL = "INSERT INTO `checklist_reviews_logs`(`date_created`,`status_id`, `checklist_id`, `employee_id`, `reason`)
                 VALUES ('".date('Y-m-d H:i:s')."',".$status.",".$id.",".Yii::app()->user->id.",'".$comment."')";
                $insert_command = Yii::app()->db->createCommand($SQL);
                $insert_command->execute();
            }
        }
        die ( json_encode ( array ('status'=>'success'
        ) ) );
    }
    public function actionBulkTerminate()
    {
        $formIDs=$_POST['formIDs'];
        if(count($formIDs)>0)
        {
            foreach($formIDs as $id)
            {
                $model = Checklist::model()->findByPk($id);
                $model->status_id = 6;
                $status=6;
                $model->save();
                $comment='Form Terminated';
                $SQL = "INSERT INTO `checklist_reviews_logs`(`date_created`,`status_id`, `checklist_id`, `employee_id`, `reason`)
                 VALUES ('".date('Y-m-d H:i:s')."',".$status.",".$id.",".Yii::app()->user->id.",'".$comment."')";
                $insert_command = Yii::app()->db->createCommand($SQL);
                $insert_command->execute();
            }
        }
        die ( json_encode ( array ('status'=>'success'
        ) ) );
    }
    public function actionBulkDuplicate()
    {
        $formIDs=$_POST['formIDs'];
        if(count($formIDs)>0)
        {
            foreach($formIDs as $id)
            {
                $checklist=[];
                
                //Get check list
                $sql='SELECT * FROM checklist where id='.$id;
                $checklist=Yii::app()->db->createCommand($sql)->queryRow();
                
                
                
                
                //Get checklist Items
                $sql='SELECT * FROM `checklist_items` WHERE `checklist_id`='.$id;
                $checklist['checklist_items']=Yii::app()->db->createCommand($sql)->queryAll();
                
                if(count($checklist['checklist_items'])>0)
                {
                    foreach($checklist['checklist_items'] as $key=>$value)
                    {
                        //Get checklist options
                        $sql="SELECT * FROM `checklist_options` WHERE `checklist_items_id`=".$value['id'];
                        $checklist['checklist_items'][$key]['checklist_options']=Yii::app()->db->createCommand($sql)->queryAll();
                        if(count($checklist['checklist_items'][$key]['checklist_options'])>0)
                        {
                            $checklist_options=$checklist['checklist_items'][$key]['checklist_options'];
                            foreach($checklist_options as $option_key=>$option_value)
                            {
                                //Get checklist option groups
                                $sql="SELECT * FROM `checklist_option_groups` WHERE `checklist_option_id`=".$option_value['id'];
                                $checklist['checklist_items'][$key]['checklist_options'][$option_key]['checklist_option_groups']=Yii::app()->db->createCommand($sql)->queryAll();
                                
                                if(count($checklist['checklist_items'][$key]['checklist_options'][$option_key]['checklist_option_groups'])>0)
                                {
                                    $checklist_option_groups=$checklist['checklist_items'][$key]['checklist_options'][$option_key]['checklist_option_groups'];
                                    foreach($checklist_option_groups as $option_group_key=>$option_group_value)
                                    {
                                        //Get checklist option groups score
                                        $sql="SELECT * FROM `checklist_option_scores` WHERE `checklist_option_groups_id`=".$option_group_value['id'];
                                        $checklist['checklist_items'][$key]['checklist_options'][$option_key]['checklist_option_groups'][$option_group_key]['checklist_option_scores']=Yii::app()->db->createCommand($sql)->queryAll();
                                        
                                        //Get compliance
                                        $sql="SELECT * FROM `checklist_compliance_calculation` WHERE `checklist_option_group_id`=".$option_group_value['id'];
                                        $checklist['checklist_items'][$key]['checklist_options'][$option_key]['checklist_option_groups'][$option_group_key]['checklist_compliance_calculation']=Yii::app()->db->createCommand($sql)->queryAll();
                                        
                                        
                                    }
                                }
                            }
                            
                        }
                        
                        
                        //Get checklist additiona options
                        $sql="SELECT * FROM `checklist_additional_options` WHERE `checklist_id`=".$id. " AND checklist_item_id=".$value['id'];
                        $checklist['checklist_items'][$key]['checklist_additional_options']=Yii::app()->db->createCommand($sql)->queryAll();
                        
                    }
                }
                
                // Copy function
                $yabby = new YabbyFormManager;
                $yabby->copyForm($checklist);
                
            }
        }
        die ( json_encode ( array ('status'=>'success'
        ) ) );
    }
    
    public function actionBulkImageUpload()
    {
        $formIDs=$_POST['formIDs'];
        
        $this->renderPartial('_bulkImportChecklistImage', array('formIDs' => $formIDs));
        
    }
    public function actionBulkImageUploadSave()
    {
        $formIDs=explode(",", $_POST['formIDs']);
        if(count($formIDs)>0)
        {
            
            $images=[];
            $parentFolder = 'images';
            $subFolder = "checklistimages";
            $folder = $_SERVER['DOCUMENT_ROOT'] . Yii::app()->request->baseUrl . '/' . $parentFolder . '/' . $subFolder . '/';
            $imgUrl = "http://" . $_SERVER['HTTP_HOST'] . Yii::app()->request->baseUrl . '/' . $parentFolder . '/' . $subFolder . '/';
            foreach($_FILES['upload-image']['tmp_name'] as $index => $tmpName)
            {
                $name = basename($_FILES["upload-image"]["name"][$index]);
                
                $tmp_name = $_FILES["upload-image"]['tmp_name'][$index];
                if(move_uploaded_file($tmp_name, $folder . $name))
                {
                    $images[]=array(
                        'image_name' =>$_POST['checklist-image-desc'][$index],
                        'image_path'=> $imgUrl . $name
                    );
                    
                }
            }
            foreach($formIDs as $id)
            {
                
                if(count($images)>0)
                {
                    foreach ($images as $image)
                    {
                        $model = new ChecklistImages;
                        $model->checklist_id = $id;
                        $model->checklist_image_name = $image['image_name'];
                        $model->checklist_image_path = $image['image_path'];
                        $model->uploaded_by = Yii::app()->user->id;
                        $model->uploaded_date = date('Y-m-d H:i:s');
                        $model->status_id = 1;
                        $model->save();
                    }
                }
                
                
            }
        }
        
        echo "1";
    }
    public function actionBulkCustomerImport()
    {
        $formIDs=$_POST['formIDs'];
        
        $this->renderPartial('_bulkImportCheklistCustomers', array('formIDs' => $formIDs));
        
    }
    public function actionbulkFormUpdateImport()
    {
        $formIDs=$_POST['formIDs'];
        
        $this->renderPartial('_bulkFormUpdateImport', array('formIDs' => $formIDs));
        
    }
    public function actionstartDownloadUpdateAllFormSheet($ids)
    {
        set_time_limit(0);
        
        // get a reference to the path of PHPExcel classes
        $phpExcelPath = Yii::getPathOfAlias('application.extensions.phpexcel.Classes');
        include ($phpExcelPath . DIRECTORY_SEPARATOR . 'PHPExcel.php');
        $templatePath = Yii::app()->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR;
        $templateFile = $templatePath . 'formsUpdate.xls';
        $con = Yii::app()->db;
        $bakeryId = Yii::app()->user->bakery;
        $objReader = new PHPExcel_Reader_Excel5();
        $objPHPExcel = $objReader->load($templateFile);
        
        $strIds='';
        $ids=json_decode($ids) ;
        foreach($ids as $id)
        {
            $strIds.=','.$id;
        }
        $strIds=trim($strIds,',');
        $sql='select * from checklist where id IN('. $strIds.')';
        $checklist=Yii::app()->db->createCommand($sql)->queryAll();
        $bakeriesStr='';
        $formGroupsStr='';
        
        $questions=array();
        $optionsList=array();
        $additional_answers_list=array();
        if(count($checklist)>0)
        {
            foreach ($checklist as $key => $value)
            {
                //get Checklist Items
                //  $sql='select * from checklist_items where checklist_id='.$value['id'] .' and status_id=1';
                $sql='SELECT ci.*
FROM checklist_items ci
LEFT JOIN `checklist_options` co ON co.`checklist_items_id`=ci.id
WHERE ci.checklist_id='.$value['id'] .' AND co.`option_type_id` !=""
GROUP BY ci.id';
                $checklistItems=Yii::app()->db->createCommand($sql)->queryAll();
                if(count($checklistItems)>0)
                {
                    $countItems=0;
                    foreach($checklistItems as $items)
                    {
                        $checklist[$key]['checklist_item'][$countItems]=$items;
                        $questions[]=$items;
                        //get item options
                        $sql='select * from checklist_options where checklist_items_id='.$items['id'];
                        $checklistOptions=Yii::app()->db->createCommand($sql)->queryAll();
                        if(count($checklistOptions)>0)
                        {
                            $countOptions=0;
                            foreach($checklistOptions as $option)
                            {
                                $optionsList[]=$option;
                                
                                $checklist[$key]['checklist_item'][$countItems]['options'][$countOptions]=$option;
                                //get item options
                                $sql='select * from checklist_additional_options where checklist_options_id='.$option['id'] .' and status_id=1';
                                $checklistAdditionalAnswers=Yii::app()->db->createCommand($sql)->queryAll();
                                
                                if(count($checklistAdditionalAnswers)>0)
                                {
                                    foreach($checklistAdditionalAnswers as $answers)
                                    {
                                        $additional_answers_list[]=$answers;
                                        $checklist[$key]['checklist_item'][$countItems]['options'][$countOptions]['additional_answers'][]=$answers;
                                        
                                    }
                                }
                                else
                                {
                                    $checklist[$key]['checklist_item'][$countItems]['options'][$countOptions]['additional_answers']=array();
                                }
                                
                                
                                $countOptions++;
                            }
                        }
                        $countItems++;
                        
                    }
                    
                }
            }
        }
        // Form Sheet
        $row=3;
        $activeSheet = $objPHPExcel->setActiveSheetIndex(0);
        foreach($checklist as $chk)
        {
            $activeSheet->setCellValueByColumnAndRow(0, $row, $chk['checklist_name']);
            
            if(isset($chk['checklist_item']) && count($chk['checklist_item'])>0)
            {
                foreach($chk['checklist_item'] as $checklist_item)
                {
                    $activeSheet->setCellValueByColumnAndRow(0, $row, $chk['id']);
                    $activeSheet->setCellValueByColumnAndRow(1, $row, $chk['checklist_name']);
                    $activeSheet->setCellValueByColumnAndRow(2, $row, $chk['description']);
                    $activeSheet->setCellValueByColumnAndRow(3, $row, $chk['status_id']);
                    $activeSheet->setCellValueByColumnAndRow(4, $row, $checklist_item['id']);
                    $activeSheet->setCellValueByColumnAndRow(5, $row, $checklist_item['description']);
                    $activeSheet->setCellValueByColumnAndRow(7, $row, $checklist_item['ordering']);
                    $activeSheet->setCellValueByColumnAndRow(8, $row, $checklist_item['mandatory']);
                    $activeSheet->setCellValueByColumnAndRow(9, $row, $checklist_item['status_id']);
                    $optionsIDs='';
                    $optionsStr='';
                    $optionsStatus='';
                    $additional_answersIDs='';
                    $additional_answersStr='';
                    
                    if(isset($checklist_item['options']) && count($checklist_item['options'])>0)
                    {
                        foreach($checklist_item['options'] as $options){
                            $activeSheet->setCellValueByColumnAndRow(6, $row, $options['option_type_id']);
                            $optionsIDs.=$options['id'].'|';
                            $optionsStatus.=$options['status_id'].'|';
                            if($options['option_type_id']==2 || $options['option_type_id']==3 || $options['option_type_id']==7)
                            {
                                $optionsStr.=$options['description'].'|';
                            }
                            else {
                                $optionsStr.='';
                            }
                            
                            
                            if(count($options['additional_answers'])>0)
                            {
                                foreach($options['additional_answers'] as $additional_answers)
                                {
                                    $additional_answersIDs.='['.$additional_answers['id'].']|';
                                    $additional_answersStr.='['.$additional_answers['description'].'|'.$additional_answers['option_type_id'].'|'.$additional_answers['mandatory'].']|';
                                }
                                
                                
                            }
                            else
                            {
                                $additional_answersIDs.='[]|';
                                $additional_answersStr.='[]|';
                            }
                            $additional_answersIDs= trim($additional_answersIDs,'|');
                            $additional_answersIDs.='~';
                            $additional_answersStr= trim($additional_answersStr,'|');
                            $additional_answersStr.='~';
                            
                        }
                    }
                    
                    $optionsIDs= trim($optionsIDs,'|');
                    $optionsStr= trim($optionsStr,'|');
                    $optionsStatus= trim($optionsStatus,'|');
                    
                    
                    if (preg_match('~[0-9]+~', $additional_answersIDs)) {
                        $additional_answersIDs= trim($additional_answersIDs,'~');
                    }
                    else
                        $additional_answersIDs='';
                        
                        if (preg_match('~[0-9]+~', $additional_answersStr)) {
                            $additional_answersStr= trim($additional_answersStr,'~');
                        }
                        else
                            $additional_answersStr='';
                            
                            $activeSheet->setCellValueByColumnAndRow(10, $row, $optionsIDs);
                            $activeSheet->setCellValueByColumnAndRow(11, $row, $optionsStr);
                            $activeSheet->setCellValueByColumnAndRow(12, $row, $optionsStatus);
                            $activeSheet->setCellValueByColumnAndRow(13, $row, $additional_answersStr);
                            ++$row;
                }
                
            }
        }
        //Add Existing questions
        $activeSheet = $objPHPExcel->setActiveSheetIndex(1);
        $activeSheet->setCellValueByColumnAndRow(0, 1, 'LIST OF EXISTING FORM QUESTIONS ');
        $row = 3;
        if(count($questions)>0)
        {
            foreach($questions as $question)
            {
                $activeSheet->setCellValueByColumnAndRow(0, $row, $question['id']);
                $activeSheet->setCellValueByColumnAndRow(1, $row, $question['description']);
                $activeSheet->setCellValueByColumnAndRow(2, $row, $question['status_id']);
                ++$row;
            }
        }
        
        //Add Existing options
        $activeSheet = $objPHPExcel->setActiveSheetIndex(2);
        $activeSheet->setCellValueByColumnAndRow(0, 1, 'LIST OF EXISTING FORM OPTIONS ');
        $row = 3;
        if(count($optionsList)>0)
        {
            foreach($optionsList as $optionsL)
            {
                
                $activeSheet->setCellValueByColumnAndRow(0, $row, $optionsL['id']);
                $activeSheet->setCellValueByColumnAndRow(1, $row, $optionsL['description']);
                $activeSheet->setCellValueByColumnAndRow(2, $row, $optionsL['option_type_id']);
                $activeSheet->setCellValueByColumnAndRow(3, $row, $optionsL['status_id']);
                $activeSheet->setCellValueByColumnAndRow(4, $row, $optionsL['checklist_items_id']);
                ++$row;
            }
        }
        
        //Add Existing additional answers
        $activeSheet = $objPHPExcel->setActiveSheetIndex(3);
        $activeSheet->setCellValueByColumnAndRow(0, 1, 'LIST OF EXISTING FORM ADDITIONAL ANSWERS ');
        $row = 3;
        if(count($additional_answers_list)>0)
        {
            foreach($additional_answers_list as $add_Answers)
            {
                $activeSheet->setCellValueByColumnAndRow(0, $row, $add_Answers['id']);
                $activeSheet->setCellValueByColumnAndRow(1, $row, $add_Answers['description']);
                $activeSheet->setCellValueByColumnAndRow(2, $row, $add_Answers['option_type_id']);
                $activeSheet->setCellValueByColumnAndRow(3, $row, $add_Answers['checklist_options_id']);
                ++$row;
            }
        }
        
        
        //Add checklist Items Option Types
        $activeSheet = $objPHPExcel->setActiveSheetIndex(4);
        $activeSheet->setCellValueByColumnAndRow(0, 1, 'LIST OF FORM ITEM TYPES ');
        $row = 3;
        $optionTypes = ChecklistOptionTypes::model()->findAllByAttributes(array(),array('order' => 'id ASC'));
        if(count($optionTypes)>0)
        {
            foreach($optionTypes as $data)
            {
                if($data['id']==18)
                {
                    continue;
                }
                $activeSheet->setCellValueByColumnAndRow(0, $row, $data['id']);
                $activeSheet->setCellValueByColumnAndRow(1, $row, $data['description']);
                ++$row;
            }
        }
        
        //Add checklist Additonal Questions Types
        $activeSheet = $objPHPExcel->setActiveSheetIndex(5);
        $activeSheet->setCellValueByColumnAndRow(0, 1, 'LIST OF FORM ADDITIONAL QUESTION TYPES');
        $row = 3;
        $additionaQuestionsArray[1] = "Single Line Text";
        $additionaQuestionsArray[8] = "Paragraph Text";
        $additionaQuestionsArray[13] = "GPS";
        $additionaQuestionsArray[9] = "Time";
        $additionaQuestionsArray[17] = "Product";
        $additionaQuestionsArray[11] = "123 Number";
        $additionaQuestionsArray[10] = "Date";
        $additionaQuestionsArray[14] = "Photo";
        $additionaQuestionsArray[16] = "Signature";
        $additionaQuestionsArray[15] = "Barcode";
        foreach($additionaQuestionsArray as $key => $value)
        {
            $activeSheet->setCellValueByColumnAndRow(0, $row, $key);
            $activeSheet->setCellValueByColumnAndRow(1, $row, $value);
            ++$row;
        }
        
        //Download File
        $activeSheet = $objPHPExcel->setActiveSheetIndex(0);
        $objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);
        $objWriter->setPreCalculateFormulas(false);
        header('Content-type: application/vnd.ms-excel');
        $download_file_name="FormsUpdate".trim(date('Y_m_d_H:i:s')).".xls";
        header('Content-Disposition: attachment; filename="'.$download_file_name.'"');
        $objWriter->save('php://output');
        echo '<pre>';
        
        
    }
    
    public function actionImportUpdateFormAllValidation($id)
    {
        $con = Yii::app()->db;
        $search = '';
        $replace = '';
        $iForms = [];
        #INITIALIZE..
        ini_set("memory_limit", "-1");
        set_time_limit(0);
        $bakery = Yii::app()->user->bakery;
        $folder = Yii::app()->basePath . "/../files/";
        $mimetypes = array(
            'text/xls',
            'application/excel',
            'application/vnd.ms-excel',
            'application/vnd.msexcel',
            'application/octet-stream',
        );
        $columns = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L','M','N');
        $mFatal = '';
        $mWarning = '';
        $count = $uCounter = $iCounter = 0;
        
        //List of Item Option Types
        $iItemTypes = [];
        $optionTypes = ChecklistOptionTypes::model()->findAllByAttributes(array(),array('order' => 'description ASC'));
        foreach($optionTypes as $data)
        {
            $iItemTypes[] = $data['id'];
        }
        //List of Additional Questions types
        $iAdditionalQuestionTypes = [1,8,13,9,17,11,10,14,16,15];
        
        #PHPExcel
        //spl_autoload_unregister(array('YiiBase', 'autoload'));
        $phpExcelPath = Yii::getPathOfAlias('ext.phpexcel.Classes');
        include($phpExcelPath . DIRECTORY_SEPARATOR . 'PHPExcel.php');
        $objPHPExcel = new PHPExcel();
        $formArray=[];
        
        $iChecklistItems=[];
        $iChecklistOptions=[];
        $iAdditionalAnswers=[];
        //get Checklist Items
        
        $strIds='';
        $ids=explode("_",$id);
        foreach($ids as $id)
        {
            $strIds.=','.$id;
        }
        $strIds=trim($strIds,',');
        
        $sql='select * from checklist_items where checklist_id IN('. $strIds.')';
        $checklistItems=Yii::app()->db->createCommand($sql)->queryAll();
        if(count($checklistItems)>0)
        {
            foreach($checklistItems as $items)
            {
                $iChecklistItems[]=$items['id'];
                
                $sql='select * from checklist_options where checklist_items_id='.$items['id'];
                $checklistOptions=Yii::app()->db->createCommand($sql)->queryAll();
                
                if(count($checklistOptions)>0)
                {
                    foreach($checklistOptions as $option)
                    {
                        $iChecklistOptions[]=$option['id'];
                        
                        $sql='select * from checklist_additional_options where checklist_options_id='.$option['id'];
                        $checklistAdditionalAnswers=Yii::app()->db->createCommand($sql)->queryAll();
                        if(count($checklistAdditionalAnswers)>0)
                        {
                            foreach($checklistAdditionalAnswers as $answers)
                            {
                                $iAdditionalAnswers[]=$answers['id'];
                                
                            }
                        }
                    }
                }
            }
        }
        #Reading
        if(is_uploaded_file($_FILES['form_file']['tmp_name']))
        {
            if(move_uploaded_file($_FILES['form_file']['tmp_name'], $folder . $_FILES['form_file']['name']))
            {
                $filename = $folder . $_FILES['form_file']['name'];
                $objPHPExcel = PHPExcel_IOFactory::load($filename);
                $msg = '';
                foreach($objPHPExcel->getWorksheetIterator() as $worksheet)
                {
                    //Worksheet info
                    $worksheetTitle = $worksheet->getTitle();
                    $highestRow = $worksheet->getHighestRow();
                    $highestColumn = $worksheet->getHighestColumn();
                    $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
                    $nrColumns = ord($highestColumn) - 64;
                    //Worksheet data
                    for($row = 3; $row <= $highestRow; ++$row)
                    {
                        ################################
                        #Column A: ID
                        $cell = $worksheet->getCellByColumnAndRow(0, $row);
                        $form_id = str_replace($search, $replace, $cell->getValue());
                        
                        if($form_id != $form_id)
                        {
                            $mFatal .= '- ' . $columns[0] . $row . ': Invalid Form ID. <br/>';
                        }
                        if($form_id == '')
                        {
                            $mFatal .= '- ' . $columns[0] . $row . ': Form ID cannot be empty. <br/>';
                        }
                        ################################
                        #Column B: name required
                        $cell = $worksheet->getCellByColumnAndRow(1, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        $checklist_name = $val;
                        if(trim($val) == "")
                        {
                            $mFatal .= '- ' . $columns[1] . $row . ': checklist name is required. <br/>';
                        }
                        ################################
                        #Column C: description required
                        $cell = $worksheet->getCellByColumnAndRow(2, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        $checklist_description = $val;
                        if(trim($val) == "")
                        {
                            $mFatal .= '- ' . $columns[2] . $row . ': checklist description is required. <br/>';
                        }
                        
                        ################################
                        #Column D: status  required
                        $cell = $worksheet->getCellByColumnAndRow(3, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        $status = $val;
                        $dataType = PHPExcel_Cell_DataType::dataTypeForValue($val);
                        if(trim($val) == "")
                        {
                            $mFatal .= '- ' . $columns[3] . $row . ': status is required. <br/>';
                        }
                        elseif($val != ""){
                            
                            if($dataType != 'n')
                            {
                                $mFatal .= '- ' . $columns[3] . $row . ': invalid input number. status has to be a number. <br/>';
                            }
                            
                        }
                        
                        ################################
                        #Column E: Question id
                        $cell = $worksheet->getCellByColumnAndRow(4, $row);
                        $val = $cell->getValue();
                        $product_id=$val;
                        $dataType = PHPExcel_Cell_DataType::dataTypeForValue($val);
                        if($val != "" && $dataType != 'n'){
                            $mFatal .= '- ' . $columns[4] . $row . ': invalid input number. Question id has to be a number. <br/>';
                        }
                        else if($val != "" && $dataType== 'n'){
                            if(!in_array(trim($val), $iChecklistItems))
                            {
                                $mFatal .= '- ' . $columns[4] . $row . ': invalid question id. <br/>';
                            }
                        }
                        ################################
                        #Column F: Question  required
                        $cell = $worksheet->getCellByColumnAndRow(5, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        $question = $val;
                        $dataType = PHPExcel_Cell_DataType::dataTypeForValue($val);
                        if(trim($val) == "")
                        {
                            $mFatal .= '- ' . $columns[5] . $row . ': a question is required. <br/>';
                        }
                        
                        ################################
                        #Column G: question type  required
                        $cell = $worksheet->getCellByColumnAndRow(6, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        $question_type = $val;
                        $dataType = PHPExcel_Cell_DataType::dataTypeForValue($val);
                        if(trim($val) == "")
                        {
                            $mFatal .= '- ' . $columns[6] . $row . ': question type is required. <br/>';
                        }
                        elseif($val != ""){
                            
                            if($dataType != 'n')
                            {
                                $mFatal .= '- ' . $columns[6] . $row . ': invalid input number. question type has to be a number. <br/>';
                            }
                            elseif($dataType == 'n')
                            {
                                if(!in_array(trim($val), $iItemTypes) )
                                {
                                    $mFatal .= '- ' . $columns[6] . $row . ': question type id ' .$val .' is invalid <br/>';
                                }
                            }
                        }
                        
                        ################################
                        #Column H: order required
                        $cell = $worksheet->getCellByColumnAndRow(7, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        $question_order = $val;
                        $dataType = PHPExcel_Cell_DataType::dataTypeForValue($val);
                        if(trim($val) == "")
                        {
                            $mFatal .= '- ' . $columns[7] . $row . ': question order is required. <br/>';
                        }
                        elseif($val != ""){
                            
                            if($dataType != 'n')
                            {
                                $mFatal .= '- ' . $columns[7] . $row . ': invalid input number. question order has to be a number. <br/>';
                            }
                            
                        }
                        
                        ################################
                        #Column I: mandatory  required
                        $cell = $worksheet->getCellByColumnAndRow(8, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        $question_mandatory = $val;
                        $dataType = PHPExcel_Cell_DataType::dataTypeForValue($val);
                        if(trim($val) == "")
                        {
                            $mFatal .= '- ' . $columns[8] . $row . ': please indicate if this question is mandatory. <br/>';
                        }
                        elseif($val != ""){
                            
                            if($dataType != 'n')
                            {
                                $mFatal .= '- ' . $columns[8] . $row . ': invalid input number. mandatory status has to be a number. <br/>';
                            }
                            elseif($dataType == 'n')
                            {
                                if((int)$val>1)
                                {
                                    $mFatal .= '- ' . $columns[8] . $row . ': invalid input. mandatory status has to be 0 or 1. <br/>';
                                }
                            }
                        }
                        
                        ################################
                        #Column J: question status  required
                        $cell = $worksheet->getCellByColumnAndRow(9, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        $questionStatus = $val;
                        $dataType = PHPExcel_Cell_DataType::dataTypeForValue($val);
                        if(trim($val) == "")
                        {
                            $mFatal .= '- ' . $columns[9] . $row . ': question status is required. <br/>';
                        }
                        elseif($val != ""){
                            
                            if($dataType != 'n')
                            {
                                $mFatal .= '- ' . $columns[9] . $row . ': question invalid input number. status has to be a number. <br/>';
                            }
                            
                        }
                        
                        ################################
                        #Column J: option id required
                        $cell = $worksheet->getCellByColumnAndRow(10, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        
                        $dataType = PHPExcel_Cell_DataType::dataTypeForValue($val);
                        if($val !="")
                        {
                            $splitoptions = explode('|', $val);
                            if(count($splitoptions)>0)
                            {
                                foreach ($splitoptions as $option)
                                {
                                    if(!in_array(trim($option), $iChecklistOptions) )
                                    {
                                        $mFatal .= '- ' . $columns[10] . $row . ': Option ID  ' .$val .' is invalid <br/>';
                                    }
                                    
                                }
                            }
                        }
                        
                        ################################
                        #Column L: option(s) required
                        $cell = $worksheet->getCellByColumnAndRow(11, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        $optionsList = [];
                        $dataType = PHPExcel_Cell_DataType::dataTypeForValue($val);
                        if($question_type==2 || $question_type==3 || $question_type==7)
                        {
                            if($val =="")
                            {
                                $mFatal .= '- ' . $columns[11] . $row . ': option  is required. <br/>';
                            }
                            elseif($val !="")
                            {
                                $splitoptions = explode('|', $val);
                                if(count($splitoptions)>0)
                                {
                                    foreach ($splitoptions as $option)
                                    {
                                        if($option=="")
                                        {
                                            $mFatal .= '- ' . $columns[11] . $row . ': option  is required. <br/>';
                                        }
                                        else
                                        {
                                            $optionsList[] = $option;
                                        }
                                        
                                    }
                                }
                            }
                            
                        }
                        
                        ################################
                        #Column M: option(s) status required
                        $cell = $worksheet->getCellByColumnAndRow(12, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        $optionsListStatus = [];
                        $dataType = PHPExcel_Cell_DataType::dataTypeForValue($val);
                        if($val =="")
                        {
                            $mFatal .= '- ' . $columns[12] . $row . ': option(s) status  is required. <br/>';
                        }
                        elseif($val !="")
                        {
                            $splitOptionsStatuses = explode('|', $val);
                            if(count($splitOptionsStatuses)>0)
                            {
                                foreach ($splitOptionsStatuses as $optionStatus)
                                {
                                    if($optionStatus=="")
                                    {
                                        $mFatal .= '- ' . $columns[12] . $row . ': option status  is required. <br/>';
                                    }
                                    else
                                    {
                                        $optionsListStatus[] = $optionStatus;
                                    }
                                    
                                }
                            }
                        }
                        ################################
                        #Column N: additional questions required
                        $cell = $worksheet->getCellByColumnAndRow(13, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        $dataType = PHPExcel_Cell_DataType::dataTypeForValue($val);
                        $additionalQuestions = [];
                        if(trim($val) != "")
                        {
                            $countOptionAdditionalQuestion=0;
                            $splitoptionsAddQuestion = explode('~', $val);
                            foreach($splitoptionsAddQuestion as $splitOpAddQuestionsKey=>$splitOpAddQuestionsValue)
                            {
                                
                                $countAddQuestion = 0;
                                $addQuestionArray = preg_split('/(\[[^]]+\])/', trim($splitOpAddQuestionsValue), -1, PREG_SPLIT_DELIM_CAPTURE);
                                foreach($addQuestionArray as $addArray)
                                {
                                    if($addArray !='' && $addArray!=null && $addArray!="[]")
                                    {
                                        $addArray = str_replace(array('[',']'),'',$addArray);
                                        $splitAddQuestions = explode('|', $addArray);
                                        if(count($splitAddQuestions)>0)
                                        {
                                            
                                            $countAddQuestionOptions = 0;
                                            $keyAnswer='';
                                            $keyAnswerType='';
                                            $keyMandatory='';
                                            foreach ($splitAddQuestions as $key=>$value)
                                            {
                                                if($key==0)
                                                {
                                                    $keyAnswer=$value;
                                                }
                                                elseif($key==1)
                                                {
                                                    $keyAnswerType=$value;
                                                }
                                                elseif ($key==2)
                                                {
                                                    $keyMandatory=$value;
                                                }
                                            }
                                            if($keyAnswer=="" && $keyAnswerType=="")
                                            {
                                                continue;
                                            }
                                            
                                            if($keyAnswer=="")
                                            {
                                                $mFatal .= '- ' . $columns[13] . $row . ': additional answer is required. <br/>';
                                            }
                                            else {
                                                $additionalQuestions[$countOptionAdditionalQuestion][$countAddQuestion]['additional_answer']=$keyAnswer;
                                                
                                            }
                                            
                                            if($keyAnswerType == "")
                                            {
                                                $mFatal .= '- ' . $columns[13] . $row . ': additional answer type is required. <br/>';
                                            }
                                            elseif($keyAnswerType != "")
                                            {
                                                
                                                $dataType = PHPExcel_Cell_DataType::dataTypeForValue($keyAnswerType);
                                                
                                                if($dataType != 'n')
                                                {
                                                    $mFatal .= '- ' . $columns[13] . $row . ': invalid input number.additional question type has to be a number. <br/>';
                                                }
                                                elseif($dataType == 'n')
                                                {
                                                    if(!in_array(trim($keyAnswerType), $iAdditionalQuestionTypes) )
                                                    {
                                                        $mFatal .= '- ' . $columns[13] . $row . ': question type type id ' .$keyAnswerType .' is invalid <br/>';
                                                    }
                                                    else {
                                                        
                                                        $additionalQuestions[$countOptionAdditionalQuestion][$countAddQuestion]['additional_answer_type']=$keyAnswerType;
                                                    }
                                                }
                                                
                                                
                                                
                                                
                                            }
                                            
                                            
                                            if($keyMandatory == "")
                                            {
                                                $mFatal .= '- ' . $columns[13] . $row . ': additional answer mandatory status is required. <br/>';
                                            }
                                            elseif($value != "")
                                            {
                                                $dataType = PHPExcel_Cell_DataType::dataTypeForValue($keyMandatory);
                                                
                                                if($dataType != 'n')
                                                {
                                                    $mFatal .= '- ' . $columns[13] . $row . ':additional answer invalid input number. mandatory status has to be a number. <br/>';
                                                }
                                                elseif($dataType == 'n')
                                                {
                                                    if((int)$keyMandatory>1)
                                                    {
                                                        $mFatal .= '- ' . $columns[13] . $row . ': invalid input. additional answer mandatory status has to be 0 or 1. <br/>';
                                                    }
                                                    else {
                                                        $additionalQuestions[$countOptionAdditionalQuestion][$countAddQuestion]['additional_answer_mandatory']=$value;
                                                    }
                                                }
                                                
                                                
                                            }
                                            if(count($additionalQuestions[$countOptionAdditionalQuestion][$countAddQuestion])!=3)
                                            {
                                                $mFatal .= 'Row - ' .$row . ': invalid format. additional answer format is invalid. <br/>';
                                                
                                            }
                                            
                                            
                                            
                                            
                                            
                                        }
                                        $countAddQuestion++;
                                    }
                                    
                                }
                                
                                $countOptionAdditionalQuestion++;
                            }
                        }
                        
                        
                        $checklist= Checklist::model()->findByPk($id);
                        $checklistBranch = ChecklistBakeries::model()->findAllByAttributes(array('checklist_id'=>$id),array('order' => 'bakery_id ASC'));
                        $branch_list=[];
                        if(count($checklistBranch)>0)
                        {
                            foreach($checklistBranch as $data)
                            {
                                $branch_list[]=$data['bakery_id'];
                            }
                        }
                        $formGroup_list=[];
                        $sql='select * from checklist_form_groups where checklist_id='.$id;
                        $checklistFormGroups=Yii::app()->db->createCommand($sql)->queryAll();
                        if(count($checklistFormGroups)>0)
                        {
                            foreach($checklistFormGroups as $data)
                            {
                                $formGroup_list[]=$data['form_group_id'];
                            }
                        }
                        
                        if(count($additionalQuestions)>0)
                        {
                            foreach ($additionalQuestions as $key=>$value)
                            {
                                
                                if(!isset($value['additional_answer'])  && !isset($value['additional_answer_type']))
                                {
                                    unset($additionalQuestions[$key]);
                                }
                            }
                        }
                        //setting array
                        if(count($formArray)>0)
                        {
                            if(isset($formArray[$checklist_name]))
                            {
                                $formArray[$checklist_name]['checklist'][]=array(
                                    'question' => $question,
                                    'question_order' => $question_order,
                                    'question_manatory' => $question_mandatory,
                                    'question_type' => $question_type,
                                    'question_status' => $questionStatus,
                                    'options' => $optionsList,
                                    'options_statuses' => $optionsListStatus,
                                    'additional_answers' => $additionalQuestions
                                );
                            }
                            else
                            {
                                $formArray[$checklist_name][]=array(
                                    'form' => array(
                                        'name' => $checklist_name,
                                        'description' => $checklist_description,
                                        'branch' => $branch_list,
                                        'default_all_employees' => $checklist['default_all_employees'],
                                        'form_groups' => $formGroup_list,
                                        'start_date' => $checklist['effective_date'],
                                        'end_date' => $checklist['expiry_date'],
                                        'default_all_customers' => $checklist['default_all_customers'],
                                        'pre_populate' => $checklist['prepopulate_form'],
                                        'image_approve' => $checklist['image_approval'],
                                        'form_category' => $checklist['checklist_category_id'],
                                        'approval' => $checklist['request_approval_by'],
                                        'history_days' => $checklist['history_duration'],
                                        'results_days' => $checklist['duration'],
                                        'status' => $checklist['status_id']
                                    ));
                                $formArray[$checklist_name]['checklist'][]=array(
                                    'question' => $question,
                                    'question_order' => $question_order,
                                    'question_manatory' => $question_mandatory,
                                    'question_type' => $question_type,
                                    'question_status' => $questionStatus,
                                    'options' => $optionsList,
                                    'options_statuses' => $optionsListStatus,
                                    'additional_answers' => $additionalQuestions
                                );
                            }
                            
                            
                        }
                        else
                        {
                            $formArray[$checklist_name][]=array(
                                'form' => array(
                                    'name' => $checklist_name,
                                    'description' => $checklist_description,
                                    'branch' => $branch_list,
                                    'default_all_employees' => $checklist['default_all_employees'],
                                    'form_groups' => $formGroup_list,
                                    'start_date' => $checklist['effective_date'],
                                    'end_date' => $checklist['expiry_date'],
                                    'default_all_customers' => $checklist['default_all_customers'],
                                    'pre_populate' => $checklist['prepopulate_form'],
                                    'image_approve' => $checklist['image_approval'],
                                    'form_category' => $checklist['checklist_category_id'],
                                    'approval' => $checklist['request_approval_by'],
                                    'history_days' => $checklist['history_duration'],
                                    'results_days' => $checklist['duration'],
                                    'status' => $checklist['status_id']
                                ));
                            $formArray[$checklist_name]['checklist'][]=array(
                                'question' => $question,
                                'question_order' => $question_order,
                                'question_manatory' => $question_mandatory,
                                'question_type' => $question_type,
                                'question_status' => $questionStatus,
                                'options' => $optionsList,
                                'options_statuses' => $optionsListStatus,
                                'additional_answers' => $additionalQuestions
                            );
                        }
                        
                        
                        
                    }
                    //Stop after reading the first worksheet
                    break;
                }
            }
        }
        
        
        
        #OUTPUT
        if($mFatal != '' or $mWarning != '')
        {
            $disabled = '';
            if($mFatal != '')
            {
                echo "<div class='alert alert-danger'><b>FATAL ERROR!<br/>You can not import this file as it contains errors:</b><br/>" . $mFatal . "</div>";
                $part = '<input type="button" onClick="goBackToTwo()" class="btn btn-primary btn-sm" value="Back" />
                             <input type="button" id="next3" class="btn btn-primary btn-sm" value="Next" disabled="true"/>';
                $disabled = 'disabled="true"';
            }
            if($mWarning != '')
            {
                echo "<div class='alert alert-warning'><b>WARNING!<br/>Please note that you are about to:</b><br/>" . $mWarning . "</div>";
                $part = '<input type="button" onClick="goBackToTwo()" class="btn btn-primary btn-sm" value="Back" />
                             <input type="button" onClick="goToLastStep()" class="btn btn-primary btn-sm" value="Continue" ' . $disabled . '/>';
            }
            echo '<table id="date_filter_table customTable" width="100%">
                        <tr><td>
                            ' . $part . '
                        </td></tr>
                     </table> <script type="text/javascript">reloadCustomers()</script>
                     ';
        }
        else
        {
            $count = $uCounter + $iCounter;
            $msg = $count . ' records found';
            $this->renderPartial('_updateAllPreview', array(
                'formArray' => $formArray
            ));
            
            
            echo '<table id="date_filter_table customTable" width="100%">
                        <tr><td>
                            <input type="button" onClick="goBackToTwo()" class="btn btn-primary btn-sm" value="Back" disabled="true" />
                            <input type="button" onClick="goToLastStep()" class="btn btn-primary btn-sm" value="Continue"/>
                        </td></tr>
                     </table> <script type="text/javascript">reloadCustomers()</script>
                     ';
            
        }
        
        die;
        
    }
    public function actionBulkstart($ids)
    {
        $ids=json_decode($ids);
        
        $bakery_id=Yii::app()->user->bakery;
        
        set_time_limit(0);
        // get a reference to the path of PHPExcel classes
        $phpExcelPath = Yii::getPathOfAlias('application.extensions.phpexcel.Classes');
        include ($phpExcelPath . DIRECTORY_SEPARATOR . 'PHPExcel.php');
        $templatePath = Yii::app()->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR;
        $templateFile = $templatePath . 'FormCustomerList.xls';
        $checklistCustomers=[];
        $customers=[];
        $bakeryList=[];
        
        $con = Yii::app()->db;
        $role=Yii::app()->user->role;
        $sql="";
        $bakery_sql="";
        $outletChecklist_sql="";
        // get customers
        if($role==1)
        {
            $formBakeries='';
            if(count($ids)>0)
            {
                foreach($ids as $id)
                {
                    $formBakeries.=','.$id;
                }
            }
            $formBakeries=trim($formBakeries,',');
            $sql='SELECT bakery_id
                    FROM checklist_bakeries cb
                    LEFT JOIN `bakery` b ON cb.bakery_id=b.id
                    WHERE b.status_id=1 AND checklist_id IN ('.$formBakeries .')';
            $checklist_bakeries=Yii::app()->db->createCommand($sql)->queryAll();
            $bakaries='';
            $bakerySelected='';
            
            if(count($checklist_bakeries)>0)
            {
                foreach($checklist_bakeries as $bakery)
                {
                    $bakaries.=','.$bakery['bakery_id'];
                    $bakerySelected.=','.$bakery['bakery_id'];
                }
                $bakaries=trim($bakaries,',');
                $bakerySelected=trim($bakerySelected,',');
            }
            $outletChecklist_bakery=$bakaries!='' ? ' AND bakery_id IN ('. $bakaries.')':'';
            $bakaries= $bakaries!='' ? ' AND o.bakery_id IN ('. $bakaries.')':'';
            $sql = "SELECT o.id,o.account_number,o.description,b.description as branch,o.bakery_id, og.description AS outlet_group,
            opg.description AS outlet_price_group,
            r.area AS route
                FROM `outlet` o
                LEFT JOIN `bakery` b ON o.bakery_id=b.id
                LEFT JOIN  outlet_group og ON og.id = o.outlet_group_id
                LEFT JOIN  outlet_price_group opg ON opg.id = o.outlet_price_group_id
                LEFT JOIN  route r ON r.id = o.route_id
                WHERE o.status_id =1 ".$bakaries;
            
            $bakerySelected=  $bakerySelected!=''?' AND id IN('. $bakerySelected.')':'';
            $bakery_sql="SELECT id,description,physical_address FROM `bakery` WHERE status_id =1 ".$bakerySelected;
            
            
            $outletChecklist_sql="SELECT * FROM outlet_checklists WHERE checklist_id=".$id." ".$outletChecklist_bakery;
            
            
            
        }
        elseif($role==2){
            $sql = "SELECT o.id,o.account_number,o.description,b.description as branch,o.bakery_id, og.description AS outlet_group,
            opg.description AS outlet_price_group,
            r.area AS route
                FROM `outlet` o
                LEFT JOIN `bakery` b ON o.bakery_id=b.id
                LEFT JOIN  outlet_group og ON og.id = o.outlet_group_id
                LEFT JOIN  outlet_price_group opg ON opg.id = o.outlet_price_group_id
                LEFT JOIN  route r ON r.id = o.route_id
                WHERE o.status_id =1 AND o.bakery_id=".$bakery_id;
            $bakery_sql="SELECT id,description,physical_address FROM `bakery` WHERE status_id =1 AND id=".$bakery_id;
            $outletChecklist_sql="SELECT * FROM `outlet_checklists`WHERE bakery_id=".$bakery_id;
        }
        
        $customers = Yii::app()->db->createCommand($sql)->queryAll();
        $bakeryList = Yii::app()->db->createCommand($bakery_sql)->queryAll();
        $checklistCustomers = Yii::app()->db->createCommand($outletChecklist_sql)->queryAll();
        
        
        $objReader = new PHPExcel_Reader_Excel2007();
        $objReader = new PHPExcel_Reader_Excel5();
        $objPHPExcel = $objReader->load($templateFile);
        $activeSheet = $objPHPExcel->setActiveSheetIndex(0);
        $activeSheet->setCellValueByColumnAndRow(0, 1, 'LIST OF FORM CUSTOMER LIST');
        $row = 3;
        
        foreach($checklistCustomers as $data)
        {
            $activeSheet->setCellValueByColumnAndRow(0, $row, $data['outlet_id']);
            ++$row;
        }
        $activeSheet = $objPHPExcel->setActiveSheetIndex(1);
        $row = 3;
        $activeSheet->setCellValueByColumnAndRow(0, 1, 'LIST OF CUSTOMERS');
        foreach($customers as $data)
        {
            $activeSheet->setCellValueByColumnAndRow(0, $row, $data['id']);
            $activeSheet->setCellValueByColumnAndRow(1, $row, $data['account_number']);
            $activeSheet->setCellValueByColumnAndRow(2, $row, $data['description']);
            $activeSheet->setCellValueByColumnAndRow(3, $row, $data['outlet_group']);
            $activeSheet->setCellValueByColumnAndRow(4, $row, $data['outlet_price_group']);
            $activeSheet->setCellValueByColumnAndRow(5, $row, $data['route']);
            $activeSheet->setCellValueByColumnAndRow(6, $row, $data['branch']);
            ++$row;
        }
        
        $activeSheet = $objPHPExcel->setActiveSheetIndex(0);
        $objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);
        $objWriter->setPreCalculateFormulas(false);
        header('Content-type: application/vnd.ms-excel');
        //        header('Content-Disposition: attachment; filename="products.xlsx"');
        //  header('Content-Disposition: attachment; filename="ProductBasePricingEngine.xls"');
        $download_file_name="FormCustomerList".trim(date('Y_m_d_H:i:s')).".xls";
        header('Content-Disposition: attachment; filename="'.$download_file_name.'"');
        $objWriter->save('php://output');
    }
    public function actionBulkImportValidation()
    {
        
        $con = Yii::app()->db;
        $search = '';
        $replace = '';
        #INITIALIZE..
        ini_set("memory_limit", "-1");
        set_time_limit(0);
        $eid = Yii::app()->user->id;
        $bakery = Yii::app()->user->bakery;
        $bakery_id = Yii::app()->user->bakery;
        $folder = Yii::app()->basePath . "/../files/";
        $iChecklistCustomers=[];
        $iCustomers=[];
        $iBakeryList=[];
        
        $con = Yii::app()->db;
        $role=Yii::app()->user->role;
        $sql="";
        $bakery_sql="";
        $outletChecklist_sql="";
        $mimetypes = array(
            'text/xls',
            'application/excel',
            'application/vnd.ms-excel',
            'application/vnd.msexcel',
            'application/octet-stream',
        );
        $columns = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K');
        $mFatal = '';
        $mWarning = '';
        $count = $uCounter = $iCounter = 0;
        $ids=explode(",", $_POST['formIDs']);
        // get customers
        if($role==1)
        {
            $formBakeries='';
            if(count($ids)>0)
            {
                foreach($ids as $id)
                {
                    $formBakeries.=','.$id;
                }
            }
            $formBakeries=trim($formBakeries,',');
            $sql='SELECT bakery_id
                    FROM checklist_bakeries cb
                    LEFT JOIN `bakery` b ON cb.bakery_id=b.id
                    WHERE b.status_id=1 AND checklist_id IN ('.$formBakeries .')';
            $checklist_bakeries=Yii::app()->db->createCommand($sql)->queryAll();
            $bakaries='';
            $bakerySelected='';
            
            if(count($checklist_bakeries)>0)
            {
                foreach($checklist_bakeries as $bakery)
                {
                    $bakaries.=','.$bakery['bakery_id'];
                    $bakerySelected.=','.$bakery['bakery_id'];
                }
                $bakaries=trim($bakaries,',');
                $bakerySelected=trim($bakerySelected,',');
            }
            $outletChecklist_bakery=$bakaries!='' ? ' AND bakery_id IN ('. $bakaries.')':'';
            $bakaries= $bakaries!='' ? ' AND o.bakery_id IN ('. $bakaries.')':'';
            $sql = "SELECT o.id,o.account_number,o.description,b.description as branch,o.bakery_id
                FROM `outlet` o
                LEFT JOIN `bakery` b ON o.bakery_id=b.id
                WHERE o.status_id =1 ".$bakaries;
        }
        elseif($role==2){
            $sql = "SELECT o.id,o.account_number,o.description,b.description as branch,o.bakery_id
                FROM `outlet` o
                LEFT JOIN `bakery` b ON o.bakery_id=b.id
                WHERE o.status_id =1 AND o.bakery_id=".$bakery_id;
            $bakery_sql="SELECT id,description,physical_address FROM `bakery` WHERE status_id =1 AND id=".$bakery_id;
            $outletChecklist_sql="SELECT * FROM `outlet_checklists`WHERE bakery_id=".$bakery_id;
        }
        
        
        $customersData = Yii::app()->db->createCommand($sql)->queryAll();
        foreach($customersData as $val)
        {
            $iCustomers[] = $val['id'];
        }
        #PHPExcel
        //        spl_autoload_unregister(array('YiiBase', 'autoload'));
        $phpExcelPath = Yii::getPathOfAlias('ext.phpexcel.Classes');
        include($phpExcelPath . DIRECTORY_SEPARATOR . 'PHPExcel.php');
        $objPHPExcel = new PHPExcel();
        #Reading
        
        if(is_uploaded_file($_FILES['customer_list_file']['tmp_name']))
        {
            
            if(move_uploaded_file($_FILES['customer_list_file']['tmp_name'], $folder . $_FILES['customer_list_file']['name']))
            {
                $filename = $folder . $_FILES['customer_list_file']['name'];
                $objPHPExcel = PHPExcel_IOFactory::load($filename);
                $msg = '';
                foreach($objPHPExcel->getWorksheetIterator() as $worksheet)
                {
                    //Worksheet info
                    $worksheetTitle = $worksheet->getTitle();
                    $highestRow = $worksheet->getHighestRow();
                    $highestColumn = $worksheet->getHighestColumn();
                    $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
                    $nrColumns = ord($highestColumn) - 64;
                    
                    //Worksheet data
                    for($row = 3; $row <= $highestRow; ++$row)
                    {
                        ################################
                        #Column A: checklist customer  ID
                        $cell = $worksheet->getCellByColumnAndRow(0, $row);
                        $val = $cell->getValue();
                        $dataType = PHPExcel_Cell_DataType::dataTypeForValue($val);
                        
                        if(!in_array(trim($val), $iCustomers))
                        {
                            
                            if(($val != "" || $val > 0) && $dataType != 'n')
                            {
                                
                                $mFatal .= '- ' . $columns[0] . $row . ': A number required. <br/>';
                                
                            }
                            elseif(($val != "" || $val > 0) && $dataType == 'n')
                            {
                                
                                $mFatal .= '- ' . $columns[0] . $row . ': invalid customer id<br/>';
                                
                            }
                            else
                            {
                                $iCounter++;
                            }
                        }
                        else
                        {
                            $uCounter++;
                        }
                        
                        
                        
                    }
                    //Stop after reading the first worksheet
                    break;
                }
            }
        }
        
        #OUTPUT
        $count = $uCounter + $iCounter;
        if($count==0)
        {
            $mFatal .= '- : No customer IDs found.<br/>';
            
        }
        
        if($mFatal != '' or $mWarning != '')
        {
            $disabled = '';
            if($mFatal != '')
            {
                echo "<div class='alert alert-danger'><b>FATAL ERROR!<br/>You can not import this file as it contains errors:</b><br/>" . $mFatal . "</div>";
                $part = '<input type="button" onClick="goBackToTwo()" class="btn btn-primary btn-sm" value="Back" />
                             <input type="button" id="next3" class="btn btn-primary btn-sm" value="Next" disabled="true"/>';
                $disabled = 'disabled="true"';
            }
            if($mWarning != '')
            {
                echo "<div class='alert alert-warning'><b>WARNING!<br/>Please note that you are about to:</b><br/>" . $mWarning . "</div>";
                $part = '<input type="button" onClick="goBackToTwo()" class="btn btn-primary btn-sm" value="Back" />
                             <input type="button" onClick="goToLastStep()" class="btn btn-primary btn-sm" value="Continue" ' . $disabled . '/>';
            }
            echo '<table id="date_filter_table customTable" width="100%">
                        <tr><td>
                            ' . $part . '
                        </td></tr>
                     </table> <script type="text/javascript">reloadCustomers()</script>
                     ';
        }
        else
        {
            
            //            $msg = $count . ' records found: ' . $iCounter . ' new and ' . $uCounter . ' existing products';
            $msg = $count . ' records found';
            echo "<div class='alert alert-success'><span class='fa fa-check-circle fa-fw'></span>&nbsp;<span class='bold'>Uploaded file is correct and ready to be imported!</span> ".$msg."</div>";
            echo '<table id="date_filter_table customTable" width="100%">
                        <tr><td>
                            <input type="button" onClick="goBackToTwo()" class="btn btn-primary btn-sm" value="Back" disabled="true" />
                            <input type="button" onClick="goToLastStep()" class="btn btn-primary btn-sm" value="Continue"/>
                        </td></tr>
                     </table> <script type="text/javascript">reloadCustomers()</script>
                     ';
        }
        #FREE
        //        spl_autoload_register(array('YiiBase', 'autoload'));
        die;
    }
    public function actionBulkImportUpdate()
    {
        $con = Yii::app()->db;
        $search = '';
        $replace = '';
        #INITIALIZE..
        ini_set("memory_limit", "-1");
        set_time_limit(0);
        $eid = Yii::app()->user->id;
        $bakery = Yii::app()->user->bakery;
        $folder = Yii::app()->basePath . "/../files/";
        $iChecklistCustomers=[];
        $iCustomers=[];
        $iBakeryList=[];
        
        $con = Yii::app()->db;
        $role=Yii::app()->user->role;
        $sql="";
        $bakery_sql="";
        $outletChecklist_sql="";
        $mimetypes = array(
            'text/xls',
            'application/excel',
            'application/vnd.ms-excel',
            'application/vnd.msexcel',
            'application/octet-stream',
        );
        $columns = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K');
        $mFatal = '';
        $mWarning = '';
        $count = $uCounter = $iCounter = 0;
        $ids=explode(",", $_POST['formIDs']);
        // get customers
        if($role==1)
        {
            $formBakeries='';
            if(count($ids)>0)
            {
                foreach($ids as $id)
                {
                    $formBakeries.=','.$id;
                }
            }
            $formBakeries=trim($formBakeries,',');
            $sql='SELECT bakery_id
                    FROM checklist_bakeries cb
                    LEFT JOIN `bakery` b ON cb.bakery_id=b.id
                    WHERE b.status_id=1 AND checklist_id IN ('.$formBakeries .')';
            $checklist_bakeries=Yii::app()->db->createCommand($sql)->queryAll();
            $bakaries='';
            $bakerySelected='';
            
            if(count($checklist_bakeries)>0)
            {
                foreach($checklist_bakeries as $bakery)
                {
                    $bakaries.=','.$bakery['bakery_id'];
                    $bakerySelected.=','.$bakery['bakery_id'];
                }
                $bakaries=trim($bakaries,',');
                $bakerySelected=trim($bakerySelected,',');
            }
            $outletChecklist_bakery=$bakaries!='' ? ' AND bakery_id IN ('. $bakaries.')':'';
            $bakaries= $bakaries!='' ? ' AND o.bakery_id IN ('. $bakaries.')':'';
            $sql = "SELECT o.id,o.account_number,o.description,b.description as branch,o.bakery_id
                FROM `outlet` o
                LEFT JOIN `bakery` b ON o.bakery_id=b.id
                WHERE o.status_id =1 ".$bakaries;
            
            $bakerySelected=  $bakerySelected!=''?' AND id IN('. $bakerySelected.')':'';
            $bakery_sql="SELECT id,description,physical_address FROM `bakery` WHERE status_id =1 ".$bakerySelected;
            
            
            $outletChecklist_sql="SELECT * FROM outlet_checklists WHERE checklist_id IN (".$formBakeries .") ".$outletChecklist_bakery;
        }
        elseif($role==2){
            $formBakeries='';
            if(count($ids)>0)
            {
                foreach($ids as $id)
                {
                    $formBakeries.=','.$id;
                }
            }
            $formBakeries=trim($formBakeries,',');
            $sql = "SELECT o.id,o.account_number,o.description,b.description as branch,o.bakery_id
                FROM `outlet` o
                LEFT JOIN `bakery` b ON o.bakery_id=b.id
                WHERE o.status_id =1 AND o.bakery_id=".$bakery_id;
            $bakery_sql="SELECT id,description,physical_address FROM `bakery` WHERE status_id =1 AND id=".$bakery_id;
            $outletChecklist_sql="SELECT * FROM `outlet_checklists` WHERE checklist_id IN (".$formBakeries .") AND bakery_id=".$bakery_id;
        }
        
        
        $customersData = Yii::app()->db->createCommand($sql)->queryAll();
        foreach($customersData as $val)
        {
            $iCustomers[] = $val['id'];
        }
        
        $bakeryListData = Yii::app()->db->createCommand($bakery_sql)->queryAll();
        foreach($bakeryListData as $val)
        {
            $iBakeryList[] = $val['id'];
        }
        
        $checklistCustomersData = Yii::app()->db->createCommand($outletChecklist_sql)->queryAll();
        
        
        foreach($checklistCustomersData as $val)
        {
            $sql="DELETE FROM outlet_checklists WHERE id =".$val['id'];
            $con->createCommand($sql)->execute();
            $iChecklistCustomers[] = $val['id'];
        }
        
        $newCustomerList=[];
        #PHPExcel
        //        spl_autoload_unregister(array('YiiBase', 'autoload'));
        $phpExcelPath = Yii::getPathOfAlias('ext.phpexcel.Classes');
        include($phpExcelPath . DIRECTORY_SEPARATOR . 'PHPExcel.php');
        $objPHPExcel = new PHPExcel();
        #Reading
        $numberOfRecords = 0;
        if(is_uploaded_file($_FILES['customer_list_file']['tmp_name']))
        {
            if(move_uploaded_file($_FILES['customer_list_file']['tmp_name'], $folder . $_FILES['customer_list_file']['name']))
            {
                $filename = $folder . $_FILES['customer_list_file']['name'];
                $objPHPExcel = PHPExcel_IOFactory::load($filename);
                $extendedInsert = [];
                $bindings = [];
                $i = 0;
                $icount=1;
                foreach($ids as $form_id)
                {
                    foreach($objPHPExcel->getWorksheetIterator() as $worksheet)
                    {
                        //Worksheet info
                        $worksheetTitle = $worksheet->getTitle();
                        $highestRow = $worksheet->getHighestRow();
                        $numberOfRecords = $highestRow - 2;
                        $highestColumn = $worksheet->getHighestColumn();
                        $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
                        $nrColumns = ord($highestColumn) - 64;
                        //Worksheet data
                        
                        for($row = 3; $row <= $highestRow; ++$row)
                        {
                            
                            ################################
                            #Column A: checklist customer  ID
                            $cell = $worksheet->getCellByColumnAndRow(0, $row);
                            $customer_id = $cell->getValue();
                            
                            //Get branch
                            $outlet=Outlet::model()->findByPk($customer_id);
                            $sql="SELECT bakery_id FROM `checklist_bakeries` WHERE checklist_id=".$form_id." AND bakery_id=".$outlet['bakery_id'];
                            
                            $checklist_bakery = Yii::app()->db->createCommand($sql)->queryRow();
                            
                            if(count($checklist_bakery)>0 && isset($checklist_bakery['bakery_id']))
                            {
                                
                                $createDate = date('Y-m-d h:i:s');
                                $bindings[$i] = [
                                    
                                    ':checklist_id' . $icount => [$form_id, PDO::PARAM_INT, 'checklist_id'],
                                    ':outlet_id' . $icount => [$customer_id, PDO::PARAM_INT, 'outlet_id'],
                                    ':bakery_id' . $icount => [$checklist_bakery['bakery_id'], PDO::PARAM_INT, 'bakery_id'],
                                    ':datetime' . $icount => [$createDate, PDO::PARAM_STR, 'datetime']
                                ];
                                
                                $icount++;
                                
                            }
                            ++$i;
                        }
                        
                        //Stop after reading the first worksheet
                        break;
                    }
                }
                
                
                $autTable = AuthTables::model()->with('fields')->find('table_key = \'outlet_checklists\'');
                $loggedInEmployee = Employee::model()->with('group.role')->findByPk(Yii::app()->user->id);
                $editableFields = [];
                if(count($autTable)>0)
                {
                    foreach($autTable['fields'] as $fields)
                    {
                        $editableFields[] = $fields['field'];
                    }
                }
                
                $values = [];
                $updateValues = [];
                
                foreach($bindings as $arr)
                {
                    
                    $keys = array_keys($arr);
                    $values[] = '(' . implode(',',$keys) . ')';
                    $tmpUpdateValues = [];
                    foreach($arr as $key => $details)
                    {
                        $column = $details[2];
                        
                        $tmpUpdateValues[] = $column . '=VALUES(' . $column . ')';
                        
                        
                    }
                    
                    $updateValues[] = implode(',', $tmpUpdateValues);
                }
                
                $chunkSize = 1000;
                $chunkValues = array_chunk($values, $chunkSize);
                $chunkBindings = array_chunk($bindings, $chunkSize);
                $sqlCommands = [];
                
                foreach($chunkValues as $chunk)
                {
                    $sql = <<<SQL
                INSERT INTO outlet_checklists
                (`checklist_id`,`outlet_id`,`bakery_id`,`datetime`)
                VALUES
SQL;
                    $chunkString = implode(',',$chunk);
                    $sql .= $chunkString;
                    
                    
                    $chunkUpdateString = $updateValues[0];
                    $sql .= ' ON DUPLICATE KEY UPDATE ' . $chunkUpdateString ;
                    // echo $sql . '<br><br>';
                    
                    
                    $sqlCommand = $con->createCommand($sql);
                    
                    $sqlCommands[] = $sqlCommand;
                }
                
                $i = 0;
                foreach($sqlCommands as $command)
                {
                    
                    for($b = 0; $b < count($chunkBindings[$i]); $b++)
                    {
                        $rowBinding = $chunkBindings[$i][$b];
                        foreach($rowBinding as $key => $details)
                        {
                            $value = $details[0];
                            $PDOType = $details[1];
                            $command->bindValue($key, $value, $PDOType);
                        }
                    }
                    
                    $command->execute();
                    
                    ++$i;
                }
                $maxIdSql = <<<SQL
                SELECT
                    id
                FROM
                    outlet_checklists
                ORDER BY id DESC
                LIMIT 1;
SQL;
                $maxId = $con->createCommand($maxIdSql)->queryScalar();
                $newMaxId = $maxId + 1;
                $maxIdUpdateSql = <<<SQL
                ALTER TABLE outlet_checklists AUTO_INCREMENT={$newMaxId};
SQL;
                //$con->createCommand($maxIdUpdateSql)->execute();
            }
        }
        
        
        
        $message="";
        
        echo '<div class="alert alert-success"><span class="fa fa-check-circle fa-fw"></span>&nbsp;'.$numberOfRecords.' Records processed'.$message.'</div>';
        die;
        
    }
    
    public function actionBulkPasteValidation()
    {
        $ids=$_POST['formIDs'];
        
        
        $con = Yii::app()->db;
        $iCustomers=[];
        $role=Yii::app()->user->role;
        $mFatal='';
        $count = $uCounter = $iCounter = 0;
        
        if($role==1)
        {
            $formBakeries='';
            if(count($ids)>0)
            {
                foreach($ids as $id)
                {
                    $formBakeries.=','.$id;
                }
            }
            $formBakeries=trim($formBakeries,',');
            $sql='SELECT bakery_id
                    FROM checklist_bakeries cb
                    LEFT JOIN `bakery` b ON cb.bakery_id=b.id
                    WHERE b.status_id=1 AND checklist_id IN ('.$formBakeries .')';
            $checklist_bakeries=Yii::app()->db->createCommand($sql)->queryAll();
            $bakaries='';
            $bakerySelected='';
            
            if(count($checklist_bakeries)>0)
            {
                foreach($checklist_bakeries as $bakery)
                {
                    $bakaries.=','.$bakery['bakery_id'];
                    $bakerySelected.=','.$bakery['bakery_id'];
                }
                $bakaries=trim($bakaries,',');
                $bakerySelected=trim($bakerySelected,',');
            }
            $outletChecklist_bakery=$bakaries!='' ? ' AND bakery_id IN ('. $bakaries.')':'';
            $bakaries= $bakaries!='' ? ' AND o.bakery_id IN ('. $bakaries.')':'';
            $sql = "SELECT o.id,o.account_number,o.description,b.description as branch,o.bakery_id
                FROM `outlet` o
                LEFT JOIN `bakery` b ON o.bakery_id=b.id
                WHERE o.status_id =1 ".$bakaries;
        }
        elseif($role==2){
            $sql = "SELECT o.id,o.account_number,o.description,b.description as branch,o.bakery_id
                FROM `outlet` o
                LEFT JOIN `bakery` b ON o.bakery_id=b.id
                WHERE o.status_id =1 AND o.bakery_id=".$bakery_id;
            $bakery_sql="SELECT id,description,physical_address FROM `bakery` WHERE status_id =1 AND id=".$bakery_id;
            $outletChecklist_sql="SELECT * FROM `outlet_checklists`WHERE bakery_id=".$bakery_id;
        }
        
        
        $customersData = Yii::app()->db->createCommand($sql)->queryAll();
        foreach($customersData as $val)
        {
            $iCustomers[] = $val['id'];
        }
        
        $customerList=trim($_POST['customers']);
        //replace all tabs with breaks
        $customerList=  trim(preg_replace('/\s+/', "\n", $customerList));
        $customerArray = explode("\n", $customerList);
        
        if(count($customerArray)>0 && $customerArray[0] !='')
        {
            
            foreach($customerArray as $customer)
            {
                
                
                $non_numeric=preg_match('/^[0-9]*$/',$customer);
                
                if(!in_array(trim($customer), $iCustomers))
                {
                    
                    
                    if(($customer != "" || $customer > 0) && !$non_numeric)
                    {
                        $mFatal.='- '.$customer.' : A number required. <br/>';
                    }
                    elseif(($customer != "" || $customer > 0) && $non_numeric)
                    {
                        $mFatal .= '- '.$customer.': invalid customer id<br/>';
                    }
                    else
                    {
                        $iCounter++;
                    }
                }
                else
                {
                    $uCounter++;
                }
            }
            
        }
        else {
            $mFatal.='Please provide a list of customers you would like to link to these forms';
        }
        if($mFatal != '')
        {
            $disabled = '';
            if($mFatal != '')
            {
                echo "<div class='alert alert-danger'><b>FATAL ERROR!<br/>You can not update the customer list as it contains errors:</b><br/>" . $mFatal . "</div>";
                $part = '<input type="button" onClick="pasteGoBackToTwo()" class="btn btn-primary btn-sm" value="Back" />
                             <input type="button" id="paste_next4" class="btn btn-primary btn-sm" value="Next" disabled="true"/>';
                $disabled = 'disabled="true"';
            }
            echo '<table id="date_filter_table customTable" width="100%">
                        <tr><td>
                            ' . $part . '
                        </td></tr>
                     </table> <script type="text/javascript">reloadCustomers()</script>
                     ';
        }
        else
        {
            $count = $uCounter + $iCounter;
            //            $msg = $count . ' records found: ' . $iCounter . ' new and ' . $uCounter . ' existing products';
            $msg = $count . ' records found';
            echo "<div class='alert alert-success'><span class='fa fa-check-circle fa-fw'></span>&nbsp;<span class='bold'>The list is correct and ready to be saved!</span> ".$msg."</div>";
            echo '<table id="date_filter_table customTable" width="100%">
                        <tr><td>
                            <input type="button" onClick="" class="btn btn-primary btn-sm" value="Back" disabled="true" />
                            <input type="button" onClick="pasteGoToLastStep()" class="btn btn-primary btn-sm" value="Continue"/>
                        </td></tr>
                     </table> <script type="text/javascript">reloadCustomers()</script>
                     ';
        }
        #FREE
        //        spl_autoload_register(array('YiiBase', 'autoload'));
        die;
    }
    public function actionBulkPasteUpdate()
    {
        $ids=$_POST['formIDs'];
        $con = Yii::app()->db;
        $iCustomers=[];
        $role=Yii::app()->user->role;
        $mFatal='';
        $count = $uCounter = $iCounter = 0;
        
        if($role==1)
        {
            $formBakeries='';
            if(count($ids)>0)
            {
                foreach($ids as $id)
                {
                    $formBakeries.=','.$id;
                }
            }
            $formBakeries=trim($formBakeries,',');
            $sql='SELECT bakery_id
                    FROM checklist_bakeries cb
                    LEFT JOIN `bakery` b ON cb.bakery_id=b.id
                    WHERE b.status_id=1 AND checklist_id IN ('.$formBakeries .')';
            $checklist_bakeries=Yii::app()->db->createCommand($sql)->queryAll();
            $bakaries='';
            $bakerySelected='';
            
            if(count($checklist_bakeries)>0)
            {
                foreach($checklist_bakeries as $bakery)
                {
                    $bakaries.=','.$bakery['bakery_id'];
                    $bakerySelected.=','.$bakery['bakery_id'];
                }
                $bakaries=trim($bakaries,',');
                $bakerySelected=trim($bakerySelected,',');
            }
            $outletChecklist_bakery=$bakaries!='' ? ' AND bakery_id IN ('. $bakaries.')':'';
            $bakaries= $bakaries!='' ? ' AND o.bakery_id IN ('. $bakaries.')':'';
            $sql = "SELECT o.id,o.account_number,o.description,b.description as branch,o.bakery_id
                FROM `outlet` o
                LEFT JOIN `bakery` b ON o.bakery_id=b.id
                WHERE o.status_id =1 ".$bakaries;
            
            $bakerySelected=  $bakerySelected!=''?' AND id IN('. $bakerySelected.')':'';
            $bakery_sql="SELECT id,description,physical_address FROM `bakery` WHERE status_id =1 ".$bakerySelected;
            
            
            $outletChecklist_sql="SELECT * FROM outlet_checklists WHERE checklist_id IN (".$formBakeries .") ".$outletChecklist_bakery;
        }
        elseif($role==2){
            $formBakeries='';
            if(count($ids)>0)
            {
                foreach($ids as $id)
                {
                    $formBakeries.=','.$id;
                }
            }
            $formBakeries=trim($formBakeries,',');
            $sql = "SELECT o.id,o.account_number,o.description,b.description as branch,o.bakery_id
                FROM `outlet` o
                LEFT JOIN `bakery` b ON o.bakery_id=b.id
                WHERE o.status_id =1 AND o.bakery_id=".$bakery_id;
            $bakery_sql="SELECT id,description,physical_address FROM `bakery` WHERE status_id =1 AND id=".$bakery_id;
            $outletChecklist_sql="SELECT * FROM `outlet_checklists` WHERE checklist_id IN (".$formBakeries .") AND bakery_id=".$bakery_id;
        }
        
        
        $customersData = Yii::app()->db->createCommand($sql)->queryAll();
        foreach($customersData as $val)
        {
            $iCustomers[] = $val['id'];
        }
        
        $bakeryListData = Yii::app()->db->createCommand($bakery_sql)->queryAll();
        foreach($bakeryListData as $val)
        {
            $iBakeryList[] = $val['id'];
        }
        
        $checklistCustomersData = Yii::app()->db->createCommand($outletChecklist_sql)->queryAll();
        
        
        foreach($checklistCustomersData as $val)
        {
            $sql="DELETE FROM outlet_checklists WHERE id =".$val['id'];
            $con->createCommand($sql)->execute();
            $iChecklistCustomers[] = $val['id'];
        }
        
        $customerList=trim($_POST['customers']);
        //replace all tabs with breaks
        $customerList=  trim(preg_replace('/\s+/', "\n", $customerList));
        $customerArray = explode("\n", $customerList);
        $bindings = [];
        $numberOfRecords = 0;
        $i = 0;
        $row=1;
        foreach($ids as $form_id)
        {
            if(count($customerArray)>0)
            {
                foreach($customerArray as $customer_id)
                {
                    if($customer_id !=='' || $customer_id==0)
                    {
                        //Get branch
                        $outlet=Outlet::model()->findByPk($customer_id);
                        $sql="SELECT bakery_id FROM `checklist_bakeries` WHERE checklist_id=".$form_id." AND bakery_id=".$outlet['bakery_id'];
                        $checklist_bakery = Yii::app()->db->createCommand($sql)->queryRow();
                        if(count($checklist_bakery)>0 && isset($checklist_bakery['bakery_id']))
                        {
                            
                            $createDate = date('Y-m-d h:i:s');
                            $bindings[$i] = [
                                
                                ':checklist_id' . $row => [$form_id, PDO::PARAM_INT, 'checklist_id'],
                                ':outlet_id' . $row => [$customer_id, PDO::PARAM_INT, 'outlet_id'],
                                ':bakery_id' . $row => [$checklist_bakery['bakery_id'], PDO::PARAM_INT, 'bakery_id'],
                                ':datetime' . $row => [$createDate, PDO::PARAM_STR, 'datetime']
                            ];
                            
                            $row++;
                            ++$numberOfRecords;
                        }
                        ++$i;
                        
                    }
                }
            }
        }
        $autTable = AuthTables::model()->with('fields')->find('table_key = \'outlet_checklists\'');
        $loggedInEmployee = Employee::model()->with('group.role')->findByPk(Yii::app()->user->id);
        $editableFields = [];
        if(count($autTable)>0)
        {
            foreach($autTable['fields'] as $fields)
            {
                $editableFields[] = $fields['field'];
            }
        }
        
        
        $values = [];
        $updateValues = [];
        foreach($bindings as $arr)
        {
            
            
            $keys = array_keys($arr);
            $values[] = '(' . implode(',',$keys) . ')';
            $tmpUpdateValues = [];
            foreach($arr as $key => $details)
            {
                $column = $details[2];
                
                $tmpUpdateValues[] = $column . '=VALUES(' . $column . ')';
                
                
            }
            
            $updateValues[] = implode(',', $tmpUpdateValues);
        }
        
        $chunkSize = 1000;
        $chunkValues = array_chunk($values, $chunkSize);
        $chunkBindings = array_chunk($bindings, $chunkSize);
        $sqlCommands = [];
        foreach($chunkValues as $chunk)
        {
            $sql = <<<SQL
                INSERT INTO outlet_checklists
                (`checklist_id`,`outlet_id`,`bakery_id`,`datetime`)
                VALUES
SQL;
            $chunkString = implode(',',$chunk);
            $sql .= $chunkString;
            
            
            $chunkUpdateString = $updateValues[0];
            $sql .= ' ON DUPLICATE KEY UPDATE ' . $chunkUpdateString ;
            //   echo $sql . '<br><br>';
            
            
            $sqlCommand = $con->createCommand($sql);
            
            $sqlCommands[] = $sqlCommand;
        }
        
        
        $i = 0;
        foreach($sqlCommands as $command)
        {
            for($b = 0; $b < count($chunkBindings[$i]); $b++)
            {
                $rowBinding = $chunkBindings[$i][$b];
                foreach($rowBinding as $key => $details)
                {
                    $value = $details[0];
                    $PDOType = $details[1];
                    $command->bindValue($key, $value, $PDOType);
                }
            }
            
            $command->execute();
            
            ++$i;
        }
        $maxIdSql = <<<SQL
                SELECT
                    id
                FROM
                    outlet_checklists
                ORDER BY id DESC
                LIMIT 1;
SQL;
        $maxId = $con->createCommand($maxIdSql)->queryScalar();
        $newMaxId = $maxId + 1;
        $maxIdUpdateSql = <<<SQL
                ALTER TABLE outlet_checklists AUTO_INCREMENT={$newMaxId};
SQL;
        //$con->createCommand($maxIdUpdateSql)->execute();
        
        
        
        $message="";
        
        echo '<div class="alert alert-success"><span class="fa fa-check-circle fa-fw"></span>&nbsp;'.$numberOfRecords.' Records processed'.$message.'</div>';
        die;
        
    }
    public function actionGetFormGroups()
    {
        $sql="  SELECT id,description FROM form_groups WHERE status_id=1";
        $formGroups = Yii::app()->db->createCommand($sql)->queryAll();
        die ( json_encode ( array ('formGroups'=>$formGroups
        ) ) );
        
        
        
    }
    
    public function actionImportBulkForms()
    {
        
        $this->renderPartial('_importBulkForms', array());
    }
    public function actionStartDownloadFormSheet()
    {
        set_time_limit(0);
        
        // get a reference to the path of PHPExcel classes
        $phpExcelPath = Yii::getPathOfAlias('application.extensions.phpexcel.Classes');
        include ($phpExcelPath . DIRECTORY_SEPARATOR . 'PHPExcel.php');
        $templatePath = Yii::app()->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR;
        $templateFile = $templatePath . 'forms.xls';
        $con = Yii::app()->db;
        $bakeryId = Yii::app()->user->bakery;
        $objReader = new PHPExcel_Reader_Excel5();
        $objPHPExcel = $objReader->load($templateFile);
        
        $checklistTypes = ChecklistTypes::model()->findAll('status_id =1 and exclude_on_form=0');
        $activeSheet = $objPHPExcel->setActiveSheetIndex(1);
        $activeSheet->setCellValueByColumnAndRow(0, 1, 'LIST OF FORM TYPES');
        $row = 3;
        foreach($checklistTypes as $data)
        {
            $activeSheet->setCellValueByColumnAndRow(0, $row, $data['id']);
            $activeSheet->setCellValueByColumnAndRow(1, $row, $data['description']);
            ++$row;
        }
        
        // Add bakeries to list
        $activeSheet = $objPHPExcel->setActiveSheetIndex(2);
        $activeSheet->setCellValueByColumnAndRow(0, 1, 'LIST OF BRANCHES');
        $row = 3;
        $bakeryWhere = '';
        if(Yii::app()->user->role > 1)
        {
            $bakeryWhere = 'id = ' . Yii::app()->user->bakery;
        }
        else
        {
            $bakeryWhere .= CustomUtils::getEmployeeBakeryIds('id', '');
        }
        
        $bakeries = Bakery::model()->findAll($bakeryWhere);
        
        foreach($bakeries as $data)
        {
            $activeSheet->setCellValueByColumnAndRow(0, $row, $data['id']);
            $activeSheet->setCellValueByColumnAndRow(1, $row, $data['description']);
            ++$row;
        }
        
        //Add Form Groups
        $activeSheet = $objPHPExcel->setActiveSheetIndex(3);
        $activeSheet->setCellValueByColumnAndRow(0, 1, 'LIST OF FORM GROUPS');
        $row = 3;
        $formGroups = FormGroups::model()->findAllByAttributes(array('status_id' => 1), array('order' => 'id ASC'));
        if(count($formGroups)>0)
        {
            foreach($formGroups as $data)
            {
                $activeSheet->setCellValueByColumnAndRow(0, $row, $data['id']);
                $activeSheet->setCellValueByColumnAndRow(1, $row, $data['description']);
                ++$row;
            }
        }
        
        //Add Form Categories
        $activeSheet = $objPHPExcel->setActiveSheetIndex(4);
        $activeSheet->setCellValueByColumnAndRow(0, 1, 'LIST OF FORM CATEGORIES');
        $row = 3;
        $emptyChecklist = new Checklist('create');
        $checklistCategories = ChecklistCategories::model()->getCategoryList($emptyChecklist);
        
        // $checklistCategories = ChecklistCategories::model()->findAllByAttributes(array('status_id' => 1), array('order' => 'id ASC'));
        if(count($checklistCategories)>0)
        {
            foreach($checklistCategories as $key=>$value)
            {
                $activeSheet->setCellValueByColumnAndRow(0, $row, $key);
                $activeSheet->setCellValueByColumnAndRow(1, $row, $value);
                ++$row;
            }
        }
        
        //Add approval List
        $activeSheet = $objPHPExcel->setActiveSheetIndex(5);
        $activeSheet->setCellValueByColumnAndRow(0, 1, 'LIST OF EMPLOYEES ');
        $row = 3;
        $approvalList = Checklist::model()->getApprovalList();
        if(count($approvalList)>0)
        {
            foreach($approvalList as $key => $value)
            {
                $activeSheet->setCellValueByColumnAndRow(0, $row, $key);
                $activeSheet->setCellValueByColumnAndRow(1, $row, $value);
                ++$row;
            }
        }
        
        
        //Add checklist Items Option Types
        $activeSheet = $objPHPExcel->setActiveSheetIndex(6);
        $activeSheet->setCellValueByColumnAndRow(0, 1, 'LIST OF FORM ITEM TYPES ');
        $row = 3;
        $optionTypes = ChecklistOptionTypes::model()->findAllByAttributes(array(),array('order' => 'id ASC'));
        if(count($optionTypes)>0)
        {
            foreach($optionTypes as $data)
            {
                if($data['id']==18)
                {
                    continue;
                }
                $activeSheet->setCellValueByColumnAndRow(0, $row, $data['id']);
                $activeSheet->setCellValueByColumnAndRow(1, $row, $data['description']);
                ++$row;
            }
        }
        
        //Add checklist Additonal Questions Types
        $activeSheet = $objPHPExcel->setActiveSheetIndex(7);
        $activeSheet->setCellValueByColumnAndRow(0, 1, 'LIST OF FORM ADDITIONAL QUESTION TYPES');
        $row = 3;
        $additionaQuestionsArray[1] = "Single Line Text";
        $additionaQuestionsArray[8] = "Paragraph Text";
        $additionaQuestionsArray[13] = "GPS";
        $additionaQuestionsArray[9] = "Time";
        $additionaQuestionsArray[17] = "Product";
        $additionaQuestionsArray[11] = "123 Number";
        $additionaQuestionsArray[10] = "Date";
        $additionaQuestionsArray[14] = "Photo";
        $additionaQuestionsArray[16] = "Signature";
        $additionaQuestionsArray[15] = "Barcode";
        foreach($additionaQuestionsArray as $key => $value)
        {
            $activeSheet->setCellValueByColumnAndRow(0, $row, $key);
            $activeSheet->setCellValueByColumnAndRow(1, $row, $value);
            ++$row;
        }
        
        //Download File
        $activeSheet = $objPHPExcel->setActiveSheetIndex(0);
        $objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);
        $objWriter->setPreCalculateFormulas(false);
        header('Content-type: application/vnd.ms-excel');
        $download_file_name="Forms".trim(date('Y_m_d_H:i:s')).".xls";
        header('Content-Disposition: attachment; filename="'.$download_file_name.'"');
        $objWriter->save('php://output');
    }
    
    public function actionImportFormValidation()
    {
        
        $con = Yii::app()->db;
        $search = '';
        $replace = '';
        $iForms = [];
        #INITIALIZE..
        ini_set("memory_limit", "-1");
        set_time_limit(0);
        $bakery = Yii::app()->user->bakery;
        $folder = Yii::app()->basePath . "/../files/";
        $mimetypes = array(
            'text/xls',
            'application/excel',
            'application/vnd.ms-excel',
            'application/vnd.msexcel',
            'application/octet-stream',
        );
        $columns = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U');
        $mFatal = '';
        $mWarning = '';
        $count = $uCounter = $iCounter = 0;
        
        //List of Available branches
        $iBakeries = [];
        
        $bakeryWhere = '';
        if(Yii::app()->user->role > 1)
        {
            $bakeryWhere = 'id = ' . Yii::app()->user->bakery;
        }
        else
        {
            $bakeryWhere .= CustomUtils::getEmployeeBakeryIds('id', '');
        }
        
        $bakeries = Bakery::model()->findAll($bakeryWhere);
        foreach($bakeries as $data)
        {
            $iBakeries[] = $data['id'];
        }
        $checklistTypes = ChecklistTypes::model()->findAll('status_id =1 AND exclude_on_form=0');
        $iChecklistTypeIds=[];
        $iChecklistTypeApprovalStatus=[];
        foreach($checklistTypes as $val)
        {
            $iChecklistTypeIds[] = $val['id'];
            $iChecklistTypeApprovalStatus[$val['id']] = $val['approval_required'];
        }
        
        //List of form Groups
        $iFormGroups = [];
        $formGroups = FormGroups::model()->findAllByAttributes(array('status_id' => 1));
        foreach($formGroups as $data)
        {
            $iFormGroups[] = $data['id'];
        }
        
        $iCustomers = [];
        $employeeDetails= Employee:: model()->findByPk(Yii::app()->user->id);
        
        if(Yii::app()->user->role==1)
        {
            $bakeryWhere = '';
            $bakeryWhere .= CustomUtils::getEmployeeBakeryIds('b.id', '');
            if($bakeryWhere !='')
            {
                $bakeryWhere =' and  '.$bakeryWhere;
            }
            $sql = "SELECT o.id
                FROM `outlet` o
                LEFT JOIN `bakery` b ON o.bakery_id=b.id
                 WHERE o.status_id =1 {$bakeryWhere}
                GROUP BY o.id";
        }
        else{
            $sql = "SELECT o.id
                FROM `outlet` o
                LEFT JOIN `bakery` b ON o.bakery_id=b.id
                WHERE o.status_id =1 AND b.status_id=1 AND o.bakery_id=".$employeeDetails['bakery_id']. "
                GROUP BY o.id";
        }
        $outlets = Yii::app()->db->createCommand($sql)->queryAll();
        foreach($outlets as $data)
        {
            $iCustomers[] = $data['id'];
        }
        
        //List of Form Categories
        $iFormCategories = [];
        //  $checklistCategories = ChecklistCategories::model()->findAllByAttributes(array('status_id' => 1));
        $emptyChecklist = new Checklist('create');
        $checklistCategories = ChecklistCategories::model()->getCategoryList($emptyChecklist);
        
        foreach($checklistCategories as $data=>$value)
        {
            $iFormCategories[] = $data;
        }
        
        //List Of Employees
        $iEmployees = [];
        $approvalList = Checklist::model()->getApprovalList();
        foreach($approvalList as $key => $value)
        {
            $iEmployees[] = $key;
        }
        
        //List of Item Option Types
        $iItemTypes = [];
        $optionTypes = ChecklistOptionTypes::model()->findAllByAttributes(array(),array('order' => 'description ASC'));
        foreach($optionTypes as $data)
        {
            $iItemTypes[] = $data['id'];
        }
        
        //List of Additional Questions types
        $iAdditionalQuestionTypes = [1,8,13,9,17,11,10,14,16,15];
        
        #PHPExcel
        //        spl_autoload_unregister(array('YiiBase', 'autoload'));
        $phpExcelPath = Yii::getPathOfAlias('ext.phpexcel.Classes');
        include($phpExcelPath . DIRECTORY_SEPARATOR . 'PHPExcel.php');
        $objPHPExcel = new PHPExcel();
        $formArray=[];
        #Reading
        if(is_uploaded_file($_FILES['form_file']['tmp_name']))
        {
            if(move_uploaded_file($_FILES['form_file']['tmp_name'], $folder . $_FILES['form_file']['name']))
            {
                $filename = $folder . $_FILES['form_file']['name'];
                $objPHPExcel = PHPExcel_IOFactory::load($filename);
                $msg = '';
                foreach($objPHPExcel->getWorksheetIterator() as $worksheet)
                {
                    //Worksheet info
                    $worksheetTitle = $worksheet->getTitle();
                    $highestRow = $worksheet->getHighestRow();
                    $highestColumn = $worksheet->getHighestColumn();
                    $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
                    $nrColumns = ord($highestColumn) - 64;
                    //Worksheet data
                    $duplicateSequence=[];
                    for($row = 3; $row <= $highestRow; ++$row)
                    {
                        ################################
                        #Column A: name required
                        $cell = $worksheet->getCellByColumnAndRow(0, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        $checklist_name = $val;
                        $dataType = PHPExcel_Cell_DataType::dataTypeForValue($val);
                        if(trim($val) == "")
                        {
                            $mFatal .= '- ' . $columns[0] . $row . ': checklist name is required. <br/>';
                            $iCounter++;
                        }
                        else
                        {
                            $uCounter++;
                            
                        }
                        
                        // check for duplicate checklist_name
                        $sql = "SELECT c.checklist_name
                        FROM `checklist` c
                        WHERE c.status_id <> 6 AND c.checklist_name= '".$checklist_name. "'";
                        
                        $checklist_nameComm = Yii::app()->db->createCommand($sql)->queryAll();
                        if(count($checklist_nameComm) > 0)
                        {
                            $mFatal .= '- ' . $columns[0] . $row . ': form name '.$checklist_name.' already exists and cannot be used.<br/>';
                            $iCounter++;
                        }
                        
                        ################################
                        #Column B: description required
                        $cell = $worksheet->getCellByColumnAndRow(1, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        $checklist_description = $val;
                        $dataType = PHPExcel_Cell_DataType::dataTypeForValue($val);
                        if(trim($val) == "")
                        {
                            $mFatal .= '- ' . $columns[1] . $row . ': checklist description is required. <br/>';
                        }
                        ################################
                        #Column C: form type  required
                        $cell = $worksheet->getCellByColumnAndRow(2, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        $formTypeId = $val;
                        $dataType = PHPExcel_Cell_DataType::dataTypeForValue($val);
                        if(trim($val) == "")
                        {
                            $mFatal .= '- ' . $columns[2] . $row . ': checklist type id is required. <br/>';
                        }
                        else {
                            
                            if(!in_array(trim($val), $iChecklistTypeIds))
                            {
                                $mFatal .= '- ' . $columns[2] . $row . ': form type id ' .$val .' is invalid <br/>';
                            }
                            
                        }
                        ################################
                        #Column D: branch required
                        $cell = $worksheet->getCellByColumnAndRow(3, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        $branch=$val;
                        $branch_list = [];
                        $dataType = PHPExcel_Cell_DataType::dataTypeForValue($val);
                        if(trim($val) == "")
                        {
                            $mFatal .= '- ' . $columns[3] . $row . ': branch id(s) are required. <br/>';
                        }
                        elseif(trim($val) != "")
                        {
                            
                            $bakeryIDs = explode('|', $val);
                            if(count($bakeryIDs)>0)
                            {
                                foreach ($bakeryIDs as $bID)
                                {
                                    if(!in_array(trim($bID), $iBakeries))
                                    {
                                        $mFatal .= '- ' . $columns[3] . $row . ': branch id ' .$bID .' is invalid <br/>';
                                    }
                                    else
                                    {
                                        $branch_list[]=$bID;
                                    }
                                    
                                }
                            }
                            
                        }
                        
                        ################################
                        #Column E: default all employees required
                        $cell = $worksheet->getCellByColumnAndRow(4, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        $default_all_employees = $val;
                        $dataType = PHPExcel_Cell_DataType::dataTypeForValue($val);
                        if(trim($val) == "")
                        {
                            $mFatal .= '- ' . $columns[4] . $row . ': please indicate if this form must be defaulted to all employees. <br/>';
                        }
                        elseif($val != ""){
                            
                            if($dataType != 'n')
                            {
                                $mFatal .= '- ' . $columns[4] . $row . ': invalid input number. Default all employees  has to be a number. <br/>';
                            }
                            elseif($dataType == 'n')
                            {
                                if((int)$val>1)
                                {
                                    $mFatal .= '- ' . $columns[4] . $row . ': invalid input. Default all employees  has to be 0 or 1. <br/>';
                                }
                            }
                        }
                        
                        ################################
                        #Column F: form groups required
                        $cell = $worksheet->getCellByColumnAndRow(5, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        $formGroupsStr = $val;
                        $formGroup_list = [];
                        $dataType = PHPExcel_Cell_DataType::dataTypeForValue($val);
                        if($default_all_employees==0 && $val=="")
                        {
                            $mFatal .= '- ' . $columns[5] . $row . ': Please indicate the form group(s) that this form must be assigned to.<br/>';
                        }
                        if(trim($val) != "" && $default_all_employees==0)
                        {
                            $formGroupIDs = explode('|', $val);
                            if(count($formGroupIDs)>0)
                            {
                                
                                foreach ($formGroupIDs as $fgID)
                                {
                                    if(!in_array(trim($fgID), $iFormGroups))
                                    {
                                        $mFatal .= '- ' . $columns[5] . $row . ': form group id ' .$fgID .' is invalid <br/>';
                                    }
                                    else{
                                        $formGroup_list[]=$fgID;
                                    }
                                    
                                }
                            }
                            
                        }
                        if(trim($val) != "" && $default_all_employees==1)
                        {
                            $mWarning.= '- ' . $columns[5] . $row . ': Please note that the provided form groups will be disregarded because default to all employees is 1. <br/>';
                        }
                        
                        ################################
                        #Column G: start date required
                        $cell = $worksheet->getCellByColumnAndRow(6, $row);
                        $val = $cell->getValue();
                        $dataType = PHPExcel_Cell_DataType::dataTypeForValue($val);
                        $start_date = PHPExcel_Style_NumberFormat::toFormattedString($val, 'yyyy-mm-dd');
                        
                        if(trim($val) == "")
                        {
                            $mFatal .= '- ' . $columns[6] . $row . ': start date is required. <br/>';
                        }
                        if($start_date!="9999-01-01")
                        {
                            if (strtotime($start_date)=='' ||strtotime($start_date)==null )
                            {
                                $mFatal .= '- ' . $columns[6] . $row . ': invalid start date format. <br/>';
                                
                            }
                        }
                        
                        
                        ################################
                        #Column H: end date required
                        $cell = $worksheet->getCellByColumnAndRow(7, $row);
                        $val = $cell->getValue();
                        $end_date = PHPExcel_Style_NumberFormat::toFormattedString($val, 'yyyy-mm-dd');
                        $dataType = PHPExcel_Cell_DataType::dataTypeForValue($val);
                        if(trim($val) == "")
                        {
                            $mFatal .= '- ' . $columns[7] . $row . ': end date is required. <br/>';
                        }
                        
                        if(trim($end_date) !="9999-01-01")
                        {
                            if (strtotime($end_date)=='' || strtotime($end_date)==null)
                            {
                                $mFatal .= '- ' . $columns[7] . $row . ': invalid end date format. <br/>';
                                
                            }
                        }
                        
                        ################################
                        #Column I: default all customers required
                        $cell = $worksheet->getCellByColumnAndRow(8, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        $default_all_customers = $val;
                        $dataType = PHPExcel_Cell_DataType::dataTypeForValue($val);
                        if(trim($val) == "")
                        {
                            $mFatal .= '- ' . $columns[8] . $row . ': please indicate if this form must be defaulted to all customers. <br/>';
                        }
                        elseif($val != ""){
                            
                            if($dataType != 'n')
                            {
                                $mFatal .= '- ' . $columns[8] . $row . ': invalid input number. Default all customers  has to be a number. <br/>';
                            }
                            elseif($dataType == 'n')
                            {
                                if((int)$val>1)
                                {
                                    $mFatal .= '- ' . $columns[8] . $row . ': invalid input. Default all customers  has to be 0 or 1. <br/>';
                                }
                            }
                        }
                        
                        ################################
                        #Column J: customers ids required
                        $cell = $worksheet->getCellByColumnAndRow(9, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        $customersStr = $val;
                        $customers_list = [];
                        $dataType = PHPExcel_Cell_DataType::dataTypeForValue($val);
                        if($default_all_customers==0 && $val=="")
                        {
                            $mFatal .= '- ' . $columns[9] . $row . ': Please indicate the customer(s) that this form must be assigned to.<br/>';
                        }
                        if(trim($val) != "" && $default_all_customers==0)
                        {
                            $customerIDs = explode('|', $val);
                            if(count($customerIDs)>0)
                            {
                                
                                foreach ($customerIDs as $cID)
                                {
                                    if(!in_array(trim($cID), $iCustomers))
                                    {
                                        $mFatal .= '- ' . $columns[9] . $row . ': customer id ' .$cID .' is invalid <br/>';
                                    }
                                    else{
                                        $customerDetails = Outlet::model()->findByPk($cID);
                                        if(!in_array($customerDetails['bakery_id'],$branch_list))
                                        {
                                            $mFatal .= '- ' . $columns[9] . $row . ': customer id ' .$cID .' does not belong to the provided branch(s). <br/>';
                                            
                                        }
                                        else{
                                            $customers_list[]=$cID;
                                        }
                                        
                                    }
                                    
                                }
                            }
                            
                        }
                        if(trim($val) != "" && $default_all_customers==1)
                        {
                            $mWarning.= '- ' . $columns[9] . $row . ': Please note that the provided customers will be disregarded because default to all customers is 1. <br/>';
                        }
                        ################################
                        #Column K: pre-populate form required
                        $cell = $worksheet->getCellByColumnAndRow(10, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        $pre_populate = $val;
                        $dataType = PHPExcel_Cell_DataType::dataTypeForValue($val);
                        if(trim($val) == "")
                        {
                            $mFatal .= '- ' . $columns[10] . $row . ': please indicate if this form must be pre-populated. <br/>';
                        }
                        elseif($val != ""){
                            
                            if($dataType != 'n')
                            {
                                $mFatal .= '- ' . $columns[10] . $row . ': invalid input number. pre-populate form has to be a number. <br/>';
                            }
                            elseif($dataType == 'n')
                            {
                                if((int)$val>1)
                                {
                                    $mFatal .= '- ' . $columns[10] . $row . ': invalid input. pre-populate form  has to be 0 or 1. <br/>';
                                }
                            }
                        }
                        
                        ################################
                        #Column L: form image requires approval  required
                        $cell = $worksheet->getCellByColumnAndRow(11, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        $image_approval = $val;
                        $dataType = PHPExcel_Cell_DataType::dataTypeForValue($val);
                        if(trim($val) == "")
                        {
                            $mFatal .= '- ' . $columns[11] . $row . ': please indicate if images captured in this form reqire approval. <br/>';
                        }
                        elseif($val != ""){
                            
                            if($dataType != 'n')
                            {
                                $mFatal .= '- ' . $columns[11] . $row . ': invalid input number. form images approval has to be a number. <br/>';
                            }
                            elseif($dataType == 'n')
                            {
                                if((int)$val>1)
                                {
                                    $mFatal .= '- ' . $columns[11] . $row . ': invalid input. form images approval  has to be 0 or 1. <br/>';
                                }
                            }
                        }                        
                        
                        ################################
                        #Column M: form category required
                        $cell = $worksheet->getCellByColumnAndRow(12, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        $formCategory = $val;
                        $dataType = PHPExcel_Cell_DataType::dataTypeForValue($val);
                        if(trim($val) != "")
                        {
                            if(!in_array(trim($val), $iFormCategories))
                            {
                                $mFatal .= '- ' . $columns[12] . $row . ': form category id ' .$val .' is invalid <br/>';
                            }
                        }
                        
                        ################################
                        #Column N: requires approval  required
                        $cell = $worksheet->getCellByColumnAndRow(13, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        $dataType = PHPExcel_Cell_DataType::dataTypeForValue($val);
                        $requiresApproval = $val;
                        if(trim($val) == "" || trim($val) == "-" || trim($val) == "0" )
                        {
                           //var_dump($iChecklistTypeApprovalStatus); echo $formTypeId; die;
                            if(isset($iChecklistTypeApprovalStatus[$formTypeId]))
                            {
                                if($iChecklistTypeApprovalStatus[$formTypeId] == 1)
                                {
                                  $mFatal .= '- ' . $columns[13] . $row . ': The selected Form Type requires Approval of this form, Please indicate who Authorizes this Form. <br/>';
                                }                              
                                elseif(trim($val) == "" && $iChecklistTypeApprovalStatus[$formTypeId] == 0)
                                {
                                  // Happy if Type id is not set and does not require Approval
                                }
                            }
                        }
                        elseif($val != ""){
                            
                            if($dataType != 'n')
                            {
                                $mFatal .= '- ' . $columns[13] . $row . ': invalid input number. approval request has to be a number. <br/>';
                            }
                            elseif($dataType == 'n')
                            {
                                if(!in_array(trim($val), $iEmployees) && $val !=0)
                                {
                                    $mFatal .= '- ' . $columns[13] . $row . ': employee id ' .$val .' is invalid <br/>';
                                }
                            }
                        }
                        
                        ################################
                        #Column O: form history days  required
                        $cell = $worksheet->getCellByColumnAndRow(14, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        $history_days = $val;
                        $dataType = PHPExcel_Cell_DataType::dataTypeForValue($val);
                        if(trim($val) == "")
                        {
                            $mFatal .= '- ' . $columns[14] . $row . ': please indicate the history duration of this form. <br/>';
                        }
                        elseif($val != ""){
                            
                            if($dataType != 'n')
                            {
                                $mFatal .= '- ' . $columns[14] . $row . ': form history duration has to be a number. <br/>';
                            }
                            
                        }
                        
                        ################################
                        #Column P: results availability days  required
                        $cell = $worksheet->getCellByColumnAndRow(15, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        $results_days = $val;
                        $dataType = PHPExcel_Cell_DataType::dataTypeForValue($val);
                        if(trim($val) == "")
                        {
                            $mFatal .= '- ' . $columns[15] . $row . ': please indicate the results availability days for this form. <br/>';
                        }
                        elseif($val != ""){
                            
                            if($dataType != 'n')
                            {
                                $mFatal .= '- ' . $columns[15] . $row . ': results availability days should to be a number. <br/>';
                            }
                            
                        }
                        
                        
                        ################################
                        #Column Q: status  required
                        $cell = $worksheet->getCellByColumnAndRow(16, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        $status = $val;
                        $dataType = PHPExcel_Cell_DataType::dataTypeForValue($val);
                        if(trim($val) == "")
                        {
                            $mFatal .= '- ' . $columns[16] . $row . ': status is required. <br/>';
                        }
                        elseif($val != ""){
                            
                            if($dataType != 'n')
                            {
                                $mFatal .= '- ' . $columns[16] . $row . ': invalid input number. status has to be a number. <br/>';
                            }
                            elseif($dataType == 'n')
                            {
                                if((int)$val>1)
                                {
                                    $mFatal .= '- ' . $columns[16] . $row . ': invalid input. status has to be 0 or 1. <br/>';
                                }
                            }
                            /*  $checklistRecord= Checklist::model() -> findByAttributes(array('checklist_name'=>$checklist_name,'status_id'=>$status));
                            
                            if(count($checklistRecord) > 0)
                            {
                            $mFatal .= '- ' . $columns[0] . $row . ': A form with this form name already exists. <br/>';
                            
                            }*/
                        }
                        
                        
                        
                        ################################
                        #Column R: Question  required
                        $cell = $worksheet->getCellByColumnAndRow(17, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        $question = $val;
                        $dataType = PHPExcel_Cell_DataType::dataTypeForValue($val);
                        if(trim($val) == "")
                        {
                            $mFatal .= '- ' . $columns[17] . $row . ': a question is required. <br/>';
                        }
                        
                        ################################
                        #Column S: order required
                        $cell = $worksheet->getCellByColumnAndRow(18, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        $question_order = $val;
                        $dataType = PHPExcel_Cell_DataType::dataTypeForValue($val);
                        if(trim($val) == "")
                        {
                            $mFatal .= '- ' . $columns[18] . $row . ': question order is required. <br/>';
                        }
                        elseif($val != ""){
                            
                            if($dataType != 'n')
                            {
                                $mFatal .= '- ' . $columns[18] . $row . ': invalid input number. question order has to be a number. <br/>';
                            }
                            
                        }
                        
                        ################################
                        #Column T: mandatory  required
                        $cell = $worksheet->getCellByColumnAndRow(19, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        $question_mandatory = $val;
                        $dataType = PHPExcel_Cell_DataType::dataTypeForValue($val);
                        if(trim($val) == "")
                        {
                            $mFatal .= '- ' . $columns[19] . $row . ': please indicate if this question is mandatory. <br/>';
                        }
                        elseif($val != ""){
                            
                            if($dataType != 'n')
                            {
                                $mFatal .= '- ' . $columns[19] . $row . ': invalid input number. mandatory status has to be a number. <br/>';
                            }
                            elseif($dataType == 'n')
                            {
                                if((int)$val>1)
                                {
                                    $mFatal .= '- ' . $columns[19] . $row . ': invalid input. mandatory status has to be 0 or 1. <br/>';
                                }
                            }
                        }
                        
                        ################################
                        #Column U: question type  required
                        $cell = $worksheet->getCellByColumnAndRow(20, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        $question_type = $val;
                        $dataType = PHPExcel_Cell_DataType::dataTypeForValue($val);
                        if(trim($val) == "")
                        {
                            $mFatal .= '- ' . $columns[20] . $row . ': question type is required. <br/>';
                        }
                        elseif($val != ""){
                            
                            if($dataType != 'n')
                            {
                                $mFatal .= '- ' . $columns[20] . $row . ': invalid input number. question type has to be a number. <br/>';
                            }
                            elseif($dataType == 'n')
                            {
                                if(!in_array(trim($val), $iItemTypes) )
                                {
                                    $mFatal .= '- ' . $columns[20] . $row . ': question type id ' .$val .' is invalid <br/>';
                                }
                            }
                        }
                        
                        
                        //populating array for duplicate sequence check;
                        $duplicateSequence[]=array(
                            'form_id'=>$checklist_name,
                            'sequence'=>$question_order,
                            'status'=>1,
                            'row' => $row
                        );
                        ################################
                        #Column V: option(s) required
                        $cell = $worksheet->getCellByColumnAndRow(21, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        $optionsList = [];
                        $dataType = PHPExcel_Cell_DataType::dataTypeForValue($val);
                        if($question_type==2 || $question_type==3 || $question_type==7)
                        {
                            if($val =="" || $val == '0' || $val == "-")
                            {
                                $mFatal .= '- ' . $columns[21] . $row . ': option  is required. <br/>';
                            }
                            elseif($val !="")
                            {
                                $splitoptions = explode('|', $val);
                                if(count($splitoptions)>0)
                                {
                                    foreach ($splitoptions as $option)
                                    {
                                        if($option=="")
                                        {
                                            $mFatal .= '- ' . $columns[21] . $row . ': option  is required. <br/>';
                                        }
                                        else
                                        {
                                            $optionsList[] = $option;
                                        }
                                        
                                    }
                                }
                            }
                            
                        }
                        elseif($question_type!=2 || $question_type!=3 || $question_type!=7)
                        {
                            if($val !="")
                            {
                                if($val == '0' || $val == "-")
                                {
                                    $val = "";
                                }
                                else{
                                    $mFatal .= '- ' . $columns[21] . $row . ': this question type does not require an option. <br/>';
                                }
                            }
                        }
                        
                        ################################
                        #Column W: additional questions required
                        $cell = $worksheet->getCellByColumnAndRow(22, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        $dataType = PHPExcel_Cell_DataType::dataTypeForValue($val);
                        $additionalQuestions = [];
                        if(trim($val) != "")
                        {
                            $countOptionAdditionalQuestion=0;
                            $splitoptionsAddQuestion = explode('~', $val);
                            
                            foreach($splitoptionsAddQuestion as $splitOpAddQuestionsKey=>$splitOpAddQuestionsValue)
                            {
                                
                                $countAddQuestion = 0;
                                $addQuestionArray = preg_split('/(\[[^]]+\])/', trim($splitOpAddQuestionsValue), -1, PREG_SPLIT_DELIM_CAPTURE);
                                
                                if($addQuestionArray=="" || $addQuestionArray==null || $addQuestionArray=="|")
                                {
                                    continue;
                                }
                                else
                                {
                                    
                                    foreach($addQuestionArray as $addArray)
                                    {
                                        
                                        if($addArray !='' && $addArray!=null && $addArray!="[]" && $addArray!="|")
                                        {
                                            $addArray = str_replace(array('[',']'),'',$addArray);
                                            $splitAddQuestions = explode('|', $addArray);
                                            
                                            if(count($splitAddQuestions)<3 || count($splitAddQuestions)>3)
                                            {
                                                $mFatal .= 'Row - ' .$row . ': invalid format. additional answer format is invalid. <br/>';
                                            }
                                            elseif(count($splitAddQuestions)==3)
                                            {
                                                $countAddQuestionOptions = 0;
                                                $keyAnswer="";
                                                $keyAnswerType="";
                                                $keyMandatory="";
                                                
                                                foreach ($splitAddQuestions as $key=>$value)
                                                {
                                                    if($key==0)
                                                    {
                                                        $keyAnswer=$value;
                                                    }
                                                    elseif($key==1)
                                                    {
                                                        $keyAnswerType=$value;
                                                    }
                                                    elseif ($key==2)
                                                    {
                                                        $keyMandatory=$value;
                                                    }
                                                    
                                                }
                                                
                                                
                                                if($keyAnswer=="" && $keyAnswerType=="")
                                                {
                                                    continue;
                                                }
                                                
                                                if($keyAnswer=="")
                                                {
                                                    $mFatal .= '- ' . $columns[22] . $row . ': additional answer is required. <br/>';
                                                }
                                                else {
                                                    
                                                    $additionalQuestions[$countOptionAdditionalQuestion][$countAddQuestion]['additional_answer']=$keyAnswer;
                                                    
                                                }
                                                
                                                if($keyAnswerType == "")
                                                {
                                                    $mFatal .= '- ' . $columns[22] . $row . ': additional answer type is required. <br/>';
                                                }
                                                elseif($keyAnswerType != "")
                                                {
                                                    
                                                    $dataType = PHPExcel_Cell_DataType::dataTypeForValue($keyAnswerType);
                                                    
                                                    if($dataType != 'n')
                                                    {
                                                        $mFatal .= '- ' . $columns[22] . $row . ': invalid input number.additional question type has to be a number. <br/>';
                                                    }
                                                    elseif($dataType == 'n')
                                                    {
                                                        if(!in_array(trim($keyAnswerType), $iAdditionalQuestionTypes) )
                                                        {
                                                            $mFatal .= '- ' . $columns[22] . $row . ': question type type id ' .$keyAnswerType .' is invalid <br/>';
                                                        }
                                                        else {
                                                            
                                                            $additionalQuestions[$countOptionAdditionalQuestion][$countAddQuestion]['additional_answer_type']=$keyAnswerType;
                                                        }
                                                    }
                                                }
                                                
                                                
                                                if($keyMandatory == "")
                                                {
                                                    $mFatal .= '- ' . $columns[22] . $row . ': additional answer mandatory status is required. <br/>';
                                                }
                                                elseif($value != "")
                                                {
                                                    $dataType = PHPExcel_Cell_DataType::dataTypeForValue($keyMandatory);
                                                    
                                                    if($dataType != 'n' || $val == '0' || $val == "-")
                                                    {
                                                        $mFatal .= '- ' . $columns[22] . $row . ':additional answer invalid input number. mandatory status has to be a number. <br/>';
                                                    }
                                                    elseif($dataType == 'n')
                                                    {
                                                        if((int)$keyMandatory>1)
                                                        {
                                                            $mFatal .= '- ' . $columns[22] . $row . ': invalid input. additional answer mandatory status has to be 0 or 1. <br/>';
                                                        }
                                                        else {
                                                            $additionalQuestions[$countOptionAdditionalQuestion][$countAddQuestion]['additional_answer_mandatory']=$value;
                                                        }
                                                    }
                                                    
                                                    
                                                }
                                                if(count($additionalQuestions[$countOptionAdditionalQuestion][$countAddQuestion])!=3)
                                                {
                                                    $mFatal .= 'Row - ' .$row . ': invalid format. additional answer format is invalid. <br/>';
                                                    
                                                }
                                                
                                                
                                                
                                                
                                                
                                            }
                                            $countAddQuestion++;
                                        }
                                        
                                    }
                                    
                                    $countOptionAdditionalQuestion++;
                                }
                                
                            }
                        }
                        
                        
                        
                        $duplicateCheckList = Checklist::model()->findAllByAttributes(array('checklist_name' => $checklist_name,'default_all_employees'=>$default_all_employees,'effective_date'=>$start_date,'expiry_date'=>$end_date,'default_all_customers'=>$default_all_customers,'checklist_category_id'=>$formCategory), array('order' => 'id ASC'));
                        
                        if(count($duplicateCheckList)>0)
                        {
                            foreach($duplicateCheckList as $chkList)
                            {
                                if($chkList['status_id']!=6)
                                {
                                    $mFatal .= '- '. $row . ': This form already exists in the database.<br/>';
                                    
                                }
                            }
                            
                        }
                        
                        //setting array
                        $formArray[$checklist_name][$branch][$default_all_employees][$formGroupsStr][$start_date][$end_date][$default_all_customers][$formCategory][]=array(
                            'form' => array(
                                'name' => $checklist_name,
                                'description' => $checklist_description,
                                'branch' => $branch_list,
                                'default_all_employees' => $default_all_employees,
                                'form_groups' => $formGroup_list,
                                'start_date' => $start_date,
                                'end_date' => $end_date,
                                'default_all_customers' => $default_all_customers,
                                'pre_populate' => $pre_populate,
                                'image_approve' => $image_approval,
                                'form_category' => $formCategory,
                                'approval' => $requiresApproval,
                                'history_days' => $history_days,
                                'results_days' => $results_days,
                                'status' => $status
                            ),
                            'checklist' => array(
                                'question' => $question,
                                'question_order' => $question_order,
                                'question_manatory' => $question_mandatory,
                                'question_type' => $question_type,
                                'options' => $optionsList,
                                'additional_answers' => $additionalQuestions
                            )
                            
                        );
                        
                        
                    }
                    if(count($duplicateSequence)>0)
                    {
                        foreach($duplicateSequence as $key=>$duplicate)
                        {
                            if($this->checkDuplicates($duplicateSequence, $duplicate['form_id'], $duplicate['sequence'] , $duplicate['status'],$duplicate['row'],$key))
                            {
                                $mFatal .= 'Row - ' .$duplicate['row'] . ': this form question contains a duplicate sequence. <br/>';
                            }
                            
                        }
                    }
                    //Stop after reading the first worksheet
                    break;
                }
            }
        }
        
        
        
        
        
        #OUTPUT
        if($mFatal != '' or $mWarning != '')
        {
            $disabled = '';
            if($mFatal != '')
            {
                echo "<div class='alert alert-danger'><b>FATAL ERROR!<br/>You can not import this file as it contains errors:</b><br/>" . $mFatal . "</div>";
                $part = '<input type="button" onClick="goBackToTwo()" class="btn btn-primary btn-sm" value="Back" />
                             <input type="button" id="next3" class="btn btn-primary btn-sm" value="Next" disabled="true"/>';
                $disabled = 'disabled="true"';
            }
            if($mWarning != '')
            {
                echo "<div class='alert alert-warning'><b>WARNING!<br/>Please note that you are about to:</b><br/>" . $mWarning . "</div>";
                $part = '<input type="button" onClick="goBackToTwo()" class="btn btn-primary btn-sm" value="Back" />
                             <input type="button" onClick="goToLastStep()" class="btn btn-primary btn-sm" value="Continue" ' . $disabled . '/>';
            }
            echo '<table id="date_filter_table customTable" width="100%">
                        <tr><td>
                            ' . $part . '
                        </td></tr>
                     </table> <script type="text/javascript">reloadCustomers()</script>
                     ';
        }
        else
        {
            $count = $uCounter + $iCounter;
            $msg = $count . ' records found';
            
            $this->renderPartial('_preview', array(
                'formArray' => $formArray
            ));
            
            
            echo '<table id="date_filter_table customTable" width="100%">
                        <tr><td>
                            <input type="button" onClick="goBackToTwo()" class="btn btn-primary btn-sm" value="Back" disabled="true" />
                            <input type="button" onClick="goToLastStep()" class="btn btn-primary btn-sm" value="Continue"/>
                        </td></tr>
                     </table> <script type="text/javascript">reloadCustomers()</script>
                     ';
            
        }
        
        die;
    }
    public function getBranches($ids)
    {
        $bakery_names='';
        $bakeries = Bakery::model()->findAll(['condition' => 'id IN('.$ids.') ', 'order' => 'description']);
        if(count($bakeries)>0)
        {
            $bakery_names.='<ul>';
            foreach($bakeries as $bakery)
            {
                $bakery_names.='<li>'.$bakery['description'].'</li>';
            }
            $bakery_names.='</ul>';
        }
        return $bakery_names;
    }
    
    public function getFormGroups($ids)
    {
        $form_froup_names='';
        $formGroups = FormGroups::model()->findAll(['condition' => 'id IN('.$ids.') ', 'order' => 'description']);
        if(count($formGroups)>0)
        {
            $form_froup_names.='<ul>';
            
            foreach($formGroups as $formGroup)
            {
                $form_froup_names.='<li>'.$formGroup['description'].'</li>';
            }
            $form_froup_names.='</ul>';
        }
        
        return $form_froup_names;
    }
    public function actionImportFormUpdate()
    {
        $con = Yii::app()->db;
        $search = '';
        $replace = '';
        $iForms = [];
        #INITIALIZE..
        ini_set("memory_limit", "-1");
        set_time_limit(0);
        $bakery = Yii::app()->user->bakery;
        $folder = Yii::app()->basePath . "/../files/";
        $mimetypes = array(
            'text/xls',
            'application/excel',
            'application/vnd.ms-excel',
            'application/vnd.msexcel',
            'application/octet-stream',
        );
        $columns = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T');
        $mFatal = '';
        $mWarning = '';
        $count = $uCounter = $iCounter = 0;
        #PHPExcel
        //        spl_autoload_unregister(array('YiiBase', 'autoload'));
        $phpExcelPath = Yii::getPathOfAlias('ext.phpexcel.Classes');
        include($phpExcelPath . DIRECTORY_SEPARATOR . 'PHPExcel.php');
        $objPHPExcel = new PHPExcel();
        $formArray=[];
        #Reading
        if(is_uploaded_file($_FILES['form_file']['tmp_name']))
        {
            if(move_uploaded_file($_FILES['form_file']['tmp_name'], $folder . $_FILES['form_file']['name']))
            {
                $filename = $folder . $_FILES['form_file']['name'];
                $objPHPExcel = PHPExcel_IOFactory::load($filename);
                $msg = '';
                foreach($objPHPExcel->getWorksheetIterator() as $worksheet)
                {
                    //Worksheet info
                    $worksheetTitle = $worksheet->getTitle();
                    $highestRow = $worksheet->getHighestRow();
                    $highestColumn = $worksheet->getHighestColumn();
                    $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
                    $nrColumns = ord($highestColumn) - 64;
                    //Worksheet data
                    for($row = 3; $row <= $highestRow; ++$row)
                    {
                        ################################
                        #Column A: name required
                        $cell = $worksheet->getCellByColumnAndRow(0, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        $checklist_name = $val;
                        
                        
                        ################################
                        #Column B: description required
                        $cell = $worksheet->getCellByColumnAndRow(1, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        $checklist_description = $val;
                        
                        ################################
                        #Column B: form type required
                        $cell = $worksheet->getCellByColumnAndRow(2, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        $form_types = $val;
                        
                        ################################
                        #Column C: branch required
                        $cell = $worksheet->getCellByColumnAndRow(3, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        $branch=$val;
                        $branch_list = [];
                        
                        if(trim($val) != "")
                        {
                            
                            $bakeryIDs = explode('|', $val);
                            if(count($bakeryIDs)>0)
                            {
                                foreach ($bakeryIDs as $bID)
                                {
                                    $branch_list[]=$bID;
                                }
                            }
                            
                        }
                        
                        ################################
                        #Column D: default all employees required
                        $cell = $worksheet->getCellByColumnAndRow(4, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        $default_all_employees = $val;
                        
                        
                        ################################
                        #Column E: form groups required
                        $cell = $worksheet->getCellByColumnAndRow(5, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        $formGroupsStr = $val;
                        $formGroup_list = [];
                        if(trim($val) != "")
                        {
                            $formGroupIDs = explode('|', $val);
                            if(count($formGroupIDs)>0)
                            {
                                
                                foreach ($formGroupIDs as $fgID)
                                {
                                    $formGroup_list[]=$fgID;
                                    
                                }
                            }
                            
                        }
                        
                        ################################
                        #Column F: start date required
                        $cell = $worksheet->getCellByColumnAndRow(6, $row);
                        $val = $cell->getValue();
                        $start_date = PHPExcel_Style_NumberFormat::toFormattedString($val, 'yyyy-mm-dd');
                        
                        ################################
                        #Column G: end date required
                        $cell = $worksheet->getCellByColumnAndRow(7, $row);
                        $val = $cell->getValue();
                        $end_date = PHPExcel_Style_NumberFormat::toFormattedString($val, 'yyyy-mm-dd');
                        
                        ################################
                        #Column H: default all customers required
                        $cell = $worksheet->getCellByColumnAndRow(8, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        $default_all_customers = $val;
                        
                        ################################
                        #Column I: customer IDS required
                        $cell = $worksheet->getCellByColumnAndRow(9, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        $customersStr = $val;
                        $customer_list = [];
                        if(trim($val) != "")
                        {
                            $customerIDs = explode('|', $val);
                            if(count($customerIDs)>0)
                            {
                                
                                foreach ($customerIDs as $cID)
                                {
                                    $customerDetails= Outlet:: model()->findByPk($cID);
                                    $customer_list[]=array(
                                        'bakery_id' =>$customerDetails['bakery_id'],
                                        'id' =>$cID
                                    );
                                    
                                }
                            }
                            
                        }
                        
                        ################################
                        #Column I: pre-populate form required
                        $cell = $worksheet->getCellByColumnAndRow(10, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        $pre_populate = $val;
                        
                        ################################
                        #Column J: form image requires approval  required
                        $cell = $worksheet->getCellByColumnAndRow(11, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        $image_approval = $val;
                        
                        ################################
                        #Column K: form category required
                        $cell = $worksheet->getCellByColumnAndRow(12, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        $formCategory = $val;
                        
                        ################################
                        #Column L: requires approval  required
                        $cell = $worksheet->getCellByColumnAndRow(13, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        $requiresApproval = $val;
                        
                        ################################
                        #Column M: form history days  required
                        $cell = $worksheet->getCellByColumnAndRow(14, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        $history_days = $val;
                        
                        
                        ################################
                        #Column N: results availability days  required
                        $cell = $worksheet->getCellByColumnAndRow(15, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        $results_days = $val;
                        
                        
                        ################################
                        #Column O: status  required
                        $cell = $worksheet->getCellByColumnAndRow(16, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        $status = $val;
                        
                        ################################
                        #Column P: Question  required
                        $cell = $worksheet->getCellByColumnAndRow(17, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        $question = $val;
                        
                        ################################
                        #Column Q: order required
                        $cell = $worksheet->getCellByColumnAndRow(18, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        $question_order = $val;
                        
                        ################################
                        #Column R: mandatory  required
                        $cell = $worksheet->getCellByColumnAndRow(19, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        $question_mandatory = $val;
                        
                        ################################
                        #Column S: question type  required
                        $cell = $worksheet->getCellByColumnAndRow(20, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        $question_type = $val;
                        
                        ################################
                        #Column T: option(s) required
                        $cell = $worksheet->getCellByColumnAndRow(21, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        $optionsList = [];
                        
                        if($val !="")
                        {
                            $splitoptions = explode('|', $val);
                            if(count($splitoptions)>0)
                            {
                                foreach ($splitoptions as $option)
                                {
                                    $optionsList[] = $option;
                                    
                                }
                            }
                        }
                        ################################
                        #Column U: additional questions required
                        $cell = $worksheet->getCellByColumnAndRow(22, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        $additionalQuestions = [];
                        if(trim($val) != "")
                        {
                            $countOptionAdditionalQuestion=0;
                            $splitoptionsAddQuestion = explode('~', $val);
                            foreach($splitoptionsAddQuestion as $splitOpAddQuestions)
                            {
                                $countAddQuestion = 0;
                                $addQuestionArray = preg_split('/(\[[^]]+\])/', trim($splitOpAddQuestions), -1, PREG_SPLIT_DELIM_CAPTURE);
                                
                                foreach($addQuestionArray as $addArray)
                                {
                                    if($addArray !='' || $addArray!=null)
                                    {
                                        
                                        $addArray = str_replace(array('[',']'),'',$addArray);
                                        $splitAddQuestions = explode('|', $addArray);
                                        if(count($splitAddQuestions)>0)
                                        {
                                            $countAddQuestionOptions = 0;
                                            foreach ($splitAddQuestions as $key=>$value)
                                            {
                                                if($key==0)
                                                {
                                                    $additionalQuestions[$countOptionAdditionalQuestion][$countAddQuestion]['additional_answer']=$value;
                                                }
                                                if($key==1)
                                                {
                                                    $additionalQuestions[$countOptionAdditionalQuestion][$countAddQuestion]['additional_answer_type']=$value;
                                                    
                                                }
                                                
                                                if($key==2)
                                                {
                                                    $additionalQuestions[$countOptionAdditionalQuestion][$countAddQuestion]['additional_answer_mandatory']=$value;
                                                    
                                                    
                                                }
                                                
                                            }
                                        }
                                        $countAddQuestion++;
                                    }
                                    
                                }
                                
                                $countOptionAdditionalQuestion++;
                            }
                            
                            
                            
                            
                        }
                        
                        //setting array
                        $formArray[$checklist_name][$branch][$default_all_employees][$formGroupsStr][$start_date][$end_date][$default_all_customers][$formCategory][]=array(
                            'form' => array(
                                'name' => $checklist_name,
                                'description' => $checklist_description,
                                'checklist_type_id' => $form_types,
                                'branch' => $branch_list,
                                'default_all_employees' => $default_all_employees,
                                'form_groups' => $formGroup_list,
                                'start_date' => $start_date,
                                'end_date' => $end_date,
                                'default_all_customers' => $default_all_customers,
                                'customersList' => $customer_list,
                                'pre_populate' => $pre_populate,
                                'image_approve' => $image_approval,
                                'form_category' => $formCategory,
                                'approval' => $requiresApproval,
                                'history_days' => $history_days,
                                'results_days' => $results_days,
                                'status' => $status
                            ),
                            'checklist' => array(
                                'question' => $question,
                                'question_order' => $question_order,
                                'question_manatory' => $question_mandatory,
                                'question_type' => $question_type,
                                'options' => $optionsList,
                                'additional_answers' => $additionalQuestions
                            )
                            
                        );
                        
                        
                    }
                    //Stop after reading the first worksheet
                    break;
                }
            }
        }
        /* echo '<pre>';
        var_dump($formArray); 
        echo '</pre>';
        die(); */
        $countFormsNum=0;
        $sql_outlet_checklists = array();
        $sql_checklist_form_groups = array();
        $sql_checklist_reviews_logs = array();
        
        foreach($formArray as $chkName)
        {
            foreach($chkName as $chkBranch)
            {
                foreach($chkBranch as $defaultEmp)
                {
                    foreach($defaultEmp as $chkFormGroups)
                    {
                        foreach($chkFormGroups as $chkStartDate)
                        {
                            foreach($chkStartDate as $chkEndDate)
                            {
                                
                                foreach($chkEndDate as $defaultCustomers)
                                {
                                    
                                    foreach($defaultCustomers as $forms)
                                    {
                                        $formInfo = $forms[0]['form'];
                                        $countFormsNum++;
                                        $model = new Checklist('create');
                                        $model->checklist_name=$formInfo['name'];
                                        $model->checklist_type_id=$formInfo['checklist_type_id'];
                                        $model->description=$formInfo['description'];
                                        $model->created_by=Yii::app()->user->id;
                                        $model->created_date=date("Y-m-d H:i:s");
                                        $model->effective_date=$formInfo['start_date'];
                                        $model->expiry_date=$formInfo['end_date'];
                                        $model->status_id=$formInfo['status'];
                                        $model->default_all_customers=$formInfo['default_all_customers'];
                                        $model->prepopulate_form=$formInfo['pre_populate'];
                                        $model->checklist_category_id=$formInfo['form_category'];
                                        $model->request_approval_by=$formInfo['approval'];
                                        $model->history_duration=$formInfo['history_days'];
                                        $model->duration=$formInfo['results_days'];
                                        $model->image_approval=$formInfo['image_approve'];
                                        $model->default_all_employees=$formInfo['default_all_employees'];
                                        if($model->request_approval_by>0)
                                        {
                                            $model->status_id=0;
                                            $formStatus=0;
                                            $comment='Form Created';
                                        }
                                        else {
                                            $status=$model->status_id;
                                            $comment='Form Created';
                                        }
                                        if ($model->save()){
                                            $id = Yii::app()->db->getLastInsertId();
                                            
                                            $sql_checklist_reviews_logs[] = "('".date('Y-m-d H:i:s')."',".$status.",".$id.",".Yii::app()->user->id.",'".$comment."')";
                                            
                                            $SQL = "INSERT INTO `checklist_reviews_logs`(`date_created`,`status_id`, `checklist_id`, `employee_id`, `reason`) VALUES ('".date('Y-m-d H:i:s')."',".$status.",".$id.",".Yii::app()->user->id.",'".$comment."')";
                                            $insert_command = Yii::app()->db->createCommand($SQL);
                                            $insert_command->execute();
                                            
                                            if(count($formInfo['branch'])>0)
                                            {
                                                foreach($formInfo['branch'] as $bakery)
                                                {
                                                    $saveChecklistBakery = new ChecklistBakeries();
                                                    $saveChecklistBakery->checklist_id = $id;
                                                    $saveChecklistBakery->bakery_id = $bakery;
                                                    $saveChecklistBakery->save();
                                                    if(count($formInfo['customersList'])>0)
                                                    {
                                                        foreach($formInfo['customersList'] as $customerList)
                                                        {
                                                            if($customerList['bakery_id']==$bakery)
                                                            {
                                                                $sql_outlet_checklists[] = "(".$model->id.",".$customerList['id'].",".$bakery.",'".date('Y-m-d H:i:s')."')";
                                                                
                                                                $SQL = "INSERT INTO `outlet_checklists`(`checklist_id`,`outlet_id`, `bakery_id`, `datetime`) VALUES (".$model->id.",".$customerList['id'].",".$bakery.",'".date('Y-m-d H:i:s')."')";
                                                                $insert_command = Yii::app()->db->createCommand($SQL);
                                                                
                                                                $insert_command->execute();
                                                            }
                                                            
                                                        }
                                                    }
                                                }
                                            }
                                            if(count($formInfo['form_groups'])>0)
                                            {
                                                foreach($formInfo['form_groups'] as $groupList)
                                                {
                                                    //$sql_checklist_form_groups[] = "(".$model->id.",".$groupList.",".Yii::app()->user->id.",'".date('Y-m-d H:i:s')."')";
                                                    $SQL = "INSERT INTO `checklist_form_groups`(`checklist_id`,`form_group_id`, `updated_by`, `updated_datetime`) VALUES (".$model->id.",".$groupList.",".Yii::app()->user->id.",'".date('Y-m-d H:i:s')."')";
                                                    $insert_command = Yii::app()->db->createCommand($SQL);
                                                    $insert_command->execute();
                                                }
                                            }
                                        }
                                        

                                        foreach ($forms as $checklists)
                                        {
                                            
                                            $sql = "INSERT INTO `checklist_items`(`checklist_id`,`description`, `ordering`, `mandatory`,`status_id`)
                                   VALUES (:checklist_id,:description,:ordering,:mandatory,:status_id)";
                                            $command = Yii::app()->db->createCommand($sql);
                                            $command->bindValue(':checklist_id', $id, PDO::PARAM_STR);
                                            $command->bindValue(':description', $checklists['checklist']['question'], PDO::PARAM_INT);
                                            $command->bindValue(':ordering',$checklists['checklist']['question_order'], PDO::PARAM_INT);
                                            $command->bindValue(':mandatory', $checklists['checklist']['question_manatory'], PDO::PARAM_INT);
                                            $command->bindValue(':status_id', 1, PDO::PARAM_INT);
                                            $command->execute();
                                            $itemID = Yii::app()->db->getLastInsertId();
                                            
                                            
                                            $bindings_count = 0;
                                            if ($itemID){
                                                // $itemID = Yii::app()->db->getLastInsertId();
                                                
                                                if(count($checklists['checklist']['options'])>0)
                                                {
                                                    $countOrder=1;
                                                    foreach($checklists['checklist']['options'] as $key=>$value)
                                                    {
                                                        /*
                                                        $bindings[$bindings_count] = [
                                                            ':checklist_items_id' . $row => [$itemID, PDO::PARAM_STR, 'checklist_items_id'],
                                                            ':description' . $row => [$value, PDO::PARAM_STR, 'description'],
                                                            ':option_type_id' . $row => [$checklists['checklist']['question_type'], PDO::PARAM_INT, 'option_type_id'],
                                                            ':ordering' . $row => [$countOrder, PDO::PARAM_INT, 'ordering'],
                                                            ':status_id' . $row => [1, PDO::PARAM_INT, 'status_id']
                                                            
                                                        ];
                                                        */
                                                        
                                                        $SQL = "INSERT INTO `checklist_options`(`checklist_items_id`,`description`, `option_type_id`, `ordering`,`status_id`)
                                                        VALUES (:checklist_items_id,:description,:option_type_id,:ordering,:status_id)";
                                                        $command = Yii::app()->db->createCommand($SQL);
                                                        $command->bindValue(':checklist_items_id', $itemID, PDO::PARAM_STR);
                                                        $command->bindValue(':description', $value, PDO::PARAM_STR);
                                                        $command->bindValue(':option_type_id', $checklists['checklist']['question_type'], PDO::PARAM_INT);
                                                        $command->bindValue(':ordering', $countOrder, PDO::PARAM_INT);
                                                        $command->bindValue(':status_id', 1, PDO::PARAM_INT);
                                                        $command->execute();
                                                        $optionID = Yii::app()->db->getLastInsertId();
                                                        
                                                        
                                                        if ($optionID){
                                                            // $optionID = Yii::app()->db->getLastInsertId();
                                                            if(isset($checklists['checklist']['additional_answers'][$key]) && count($checklists['checklist']['additional_answers'][$key])>0)
                                                            {
                                                                foreach($checklists['checklist']['additional_answers'][$key] as $additionalOptions)
                                                                {
                                                                    if($additionalOptions['additional_answer']!='' && $additionalOptions['additional_answer_type']!='')
                                                                    {
                                                                        
                                                                        $SQL = "INSERT INTO `checklist_additional_options`(`checklist_id`,`checklist_item_id`, `option_type_id`, `status_id`, `checklist_options_id`, `mandatory`,`description`)
                                                             VALUES (:checklist_id,:checklist_item_id,:option_type_id,:status_id,:checklist_options_id,:mandatory,:description)";
                                                                        
                                                                        $command = Yii::app()->db->createCommand($SQL);
                                                                        $command->bindValue(':checklist_id',$id, PDO::PARAM_INT);
                                                                        $command->bindValue(':checklist_item_id', $itemID, PDO::PARAM_INT);
                                                                        $command->bindValue(':option_type_id', $additionalOptions['additional_answer_type'], PDO::PARAM_INT);
                                                                        $command->bindValue(':status_id', 1, PDO::PARAM_INT);
                                                                        $command->bindValue(':checklist_options_id', $optionID, PDO::PARAM_INT);
                                                                        $command->bindValue(':mandatory', $additionalOptions['additional_answer_mandatory'], PDO::PARAM_INT);
                                                                        $command->bindValue(':description', $additionalOptions['additional_answer'], PDO::PARAM_STR);
                                                                        $command->execute();
                                                                    } 
                                                                }
                                                                
                                                            }
                                                        }
                                                        $countOrder++;
                                                        $bindings_count++;
                                                    }
                                                }
                                                else {
                                                    
                                                    
                                                    $SQL = "INSERT INTO `checklist_options`(`checklist_items_id`,`description`, `option_type_id`, `ordering`,`status_id`)
                                                        VALUES (:checklist_items_id,:description,:option_type_id,:ordering,:status_id)";
                                                    $command = Yii::app()->db->createCommand($SQL);
                                                    $command->bindValue(':checklist_items_id', $itemID, PDO::PARAM_STR);
                                                    $command->bindValue(':description', ' ', PDO::PARAM_STR);
                                                    $command->bindValue(':option_type_id', $checklists['checklist']['question_type'], PDO::PARAM_INT);
                                                    $command->bindValue(':ordering', 1, PDO::PARAM_INT);
                                                    $command->bindValue(':status_id', 1, PDO::PARAM_INT);
                                                    $command->execute();
                                                    $optionID = Yii::app()->db->getLastInsertId();
                                                    
                                                    if ($optionID){
                                                        //$optionID = Yii::app()->db->getLastInsertId();
                                                        if(isset($checklists['checklist']['additional_answers'][0]) && count($checklists['checklist']['additional_answers'][0])>0)
                                                        {
                                                            foreach($checklists['checklist']['additional_answers'][0] as $additionalOptions)
                                                            {
                                                                if($additionalOptions['additional_answer']!='' && $additionalOptions['additional_answer_type']!='')
                                                                {
                                                                    
                                                                    $SQL = "INSERT INTO `checklist_additional_options`(`checklist_id`,`checklist_item_id`, `option_type_id`, `status_id`, `checklist_options_id`, `mandatory`,`description`)
                                                             VALUES (:checklist_id,:checklist_item_id,:option_type_id,:status_id,:checklist_options_id,:mandatory,:description)";
                                                                    
                                                                    $command = Yii::app()->db->createCommand($SQL);
                                                                    $command->bindValue(':checklist_id',$id, PDO::PARAM_INT);
                                                                    $command->bindValue(':checklist_item_id', $itemID, PDO::PARAM_INT);
                                                                    $command->bindValue(':option_type_id',$additionalOptions['additional_answer_type'], PDO::PARAM_INT);
                                                                    $command->bindValue(':status_id', 1, PDO::PARAM_INT);
                                                                    $command->bindValue(':checklist_options_id', $optionID, PDO::PARAM_INT);
                                                                    $command->bindValue(':mandatory', $additionalOptions['additional_answer_mandatory'], PDO::PARAM_INT);
                                                                    $command->bindValue(':description', $additionalOptions['additional_answer'], PDO::PARAM_STR);
                                                                    $command->execute();
                                                                }
                                                                
                                                            }
                                                            
                                                        }
                                                    }
                                                    $bindings_count++;
                                                }
                                            }
                                        }
                                        
                                        
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        /*
        //$sql = Yii::app()->db->createCommand($sql_checklist_form_groups); INSERT INTO `checklist_reviews_logs`(`date_created`,`status_id`, `checklist_id`, `employee_id`, `reason`) VALUES 
        //sql = Yii::app()->db->createCommand($sql_outlet_checklists); INSERT INTO `outlet_checklists`(`checklist_id`,`outlet_id`, `bakery_id`, `datetime`) VALUES 
        //$sql = Yii::app()->db->createCommand($sql_checklist_reviews_logs); INSERT INTO `checklist_form_groups`(`checklist_id`,`form_group_id`, `updated_by`, `updated_datetime`) VALUES 
        
        echo "<pre>";
        var_dump($sql_checklist_form_groups);
        var_dump($sql_outlet_checklists);
        var_dump($sql_checklist_reviews_logs);
        echo "</pre>";
        */
        echo '<div class="alert alert-success"><span class="fa fa-check-circle fa-fw"></span>&nbsp;'.$countFormsNum.' Records processed</div>';
        die;
        
    }
    
    public function extendedInserts($table, $columnList, $bindings)
    {
        $con = Yii::app()->db;
        $values = [];
        $updateValues = [];
        
        foreach($bindings as $arr)
        {

            $keys = array_keys($arr);
            $values[] = '(' . implode(',',$keys) . ')';
            $tmpUpdateValues = [];
            foreach($arr as $key => $details)
            {
                $column = $details[2];
                $tmpUpdateValues[] = $column . '=VALUES(' . $column . ')';
            }
            
            $updateValues[] = implode(',', $tmpUpdateValues);
        }
        $chunkSize = 1000;
        $chunkValues = array_chunk($values, $chunkSize);
        $chunkBindings = array_chunk($bindings, $chunkSize);
        $sqlCommands = [];
        foreach($chunkValues as $chunk)
        {
            
            $columnList=["id","sku","description","manufacturer_number","supplier_id","product_type_id","units","units_of_measure_id","price","cost_price","weight","orderable","returnable","returnable_on_pre_sale","status_id","vat","created_at","created_time","batch_code","expiry_date","bakery_id","created_by","barcode","exclude_van_sale"];
         
            
            $strColumns="";
            
            foreach($columnList as $editableColumnsList)
            {
                $strColumns.="`".$editableColumnsList."`,";
            }
            $strColumns=trim($strColumns,',');
            $sql = <<<SQL
                INSERT INTO {$table}
                ($strColumns)
                VALUES
SQL;
            
            $chunkString = implode(',',$chunk);
            
            $sql .= $chunkString;
            
            
            $chunkUpdateString = $updateValues[0];
            $sql .= ' ON DUPLICATE KEY UPDATE ' . $chunkUpdateString . ';';
            
            $sqlCommand = $con->createCommand($sql);
            
            $sqlCommands[] = $sqlCommand;
        }
        $i = 0;
        foreach($sqlCommands as $command)
        {
            for($b = 0; $b < count($chunkBindings[$i]); $b++)
            {
                $rowBinding = $chunkBindings[$i][$b];
                foreach($rowBinding as $key => $details)
                {
                    $value = $details[0];
                    $PDOType = $details[1];
                    $command->bindValue($key, $value, $PDOType);
                }
            }
            
            
            $command->execute();
            
            ++$i;
        }
    }
    
    public function actionimportUpdateForm($id)
    {
        
        $this->renderPartial('_importUpdateForm', array('id'=>$id));
    }
    public function actionStartDownloadUpdateFormSheet($id)
    {
        set_time_limit(0);
        
        // get a reference to the path of PHPExcel classes
        $phpExcelPath = Yii::getPathOfAlias('application.extensions.phpexcel.Classes');
        include ($phpExcelPath . DIRECTORY_SEPARATOR . 'PHPExcel.php');
        $templatePath = Yii::app()->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR;
        $templateFile = $templatePath . 'formsUpdate.xls';
        $con = Yii::app()->db;
        $bakeryId = Yii::app()->user->bakery;
        $objReader = new PHPExcel_Reader_Excel5();
        $objPHPExcel = $objReader->load($templateFile);
        
        //Get checklist Array
        $sql='select * from checklist where id='.$id;
        $checklist=Yii::app()->db->createCommand($sql)->queryRow();
        $bakeriesStr='';
        $formGroupsStr='';
        
        $questions=array();
        $optionsList=array();
        $additional_answers_list=array();
        if(count($checklist)>0)
        {
            
            
            //get Checklist Items
            $sql='select * from checklist_items where checklist_id='.$id;
            $checklistItems=Yii::app()->db->createCommand($sql)->queryAll();
            if(count($checklistItems)>0)
            {
                $countItems=0;
                foreach($checklistItems as $items)
                {
                    $checklist['checklist_item'][$countItems]=$items;
                    $questions[]=$items;
                    //get item options
                    $sql='select * from checklist_options where checklist_items_id='.$items['id'];
                    $checklistOptions=Yii::app()->db->createCommand($sql)->queryAll();
                    if(count($checklistOptions)>0)
                    {
                        $countOptions=0;
                        foreach($checklistOptions as $option)
                        {
                            $optionsList[]=$option;
                            
                            $checklist['checklist_item'][$countItems]['options'][$countOptions]=$option;
                            //get item options
                            $sql='select * from checklist_additional_options where checklist_options_id='.$option['id'];
                            $checklistAdditionalAnswers=Yii::app()->db->createCommand($sql)->queryAll();
                            
                            if(count($checklistAdditionalAnswers)>0)
                            {
                                foreach($checklistAdditionalAnswers as $answers)
                                {
                                    $additional_answers_list[]=$answers;
                                    $checklist['checklist_item'][$countItems]['options'][$countOptions]['additional_answers'][]=$answers;
                                    
                                }
                            }
                            else
                            {
                                $checklist['checklist_item'][$countItems]['options'][$countOptions]['additional_answers']=array();
                            }
                            
                            
                            $countOptions++;
                        }
                    }
                    $countItems++;
                    
                }
                
            }
            
            
        }
        
        
        // Form Sheet
        
        $activeSheet = $objPHPExcel->setActiveSheetIndex(0);
        $activeSheet->setCellValueByColumnAndRow(0, 1, $checklist['checklist_name']);
        $row=3;
        if(isset($checklist) && count($checklist)>0)
        {
            $activeSheet->setCellValueByColumnAndRow(0, $row, $checklist['id']);
            $activeSheet->setCellValueByColumnAndRow(1, $row, $checklist['checklist_name']);
            $activeSheet->setCellValueByColumnAndRow(2, $row, $checklist['description']);
            $activeSheet->setCellValueByColumnAndRow(3, $row, $checklist['status_id']);
            if(isset($checklist['checklist_item']) && count($checklist['checklist_item'])>0)
            {
                foreach($checklist['checklist_item'] as $checklist_item)
                {
                    $activeSheet->setCellValueByColumnAndRow(0, $row, $checklist['id']);
                    $activeSheet->setCellValueByColumnAndRow(1, $row, $checklist['checklist_name']);
                    $activeSheet->setCellValueByColumnAndRow(2, $row, $checklist['description']);
                    $activeSheet->setCellValueByColumnAndRow(3, $row, $checklist['status_id']);
                    $activeSheet->setCellValueByColumnAndRow(4, $row, $checklist_item['id']);
                    $activeSheet->setCellValueByColumnAndRow(5, $row, $checklist_item['description']);
                    $activeSheet->setCellValueByColumnAndRow(7, $row, $checklist_item['ordering']);
                    $activeSheet->setCellValueByColumnAndRow(8, $row, $checklist_item['mandatory']);
                    $activeSheet->setCellValueByColumnAndRow(9, $row, $checklist_item['status_id']);
                    
                    $optionsIDs='';
                    $optionsStr='';
                    $optionsStatus='';
                    $additional_answersIDs='';
                    $additional_answersStr='';
                    
                    if(isset($checklist_item['options']) && count($checklist_item['options'])>0)
                    {
                        foreach($checklist_item['options'] as $options){
                            $activeSheet->setCellValueByColumnAndRow(6, $row, $options['option_type_id']);
                            $optionsIDs.=$options['id'].'|';
                            $optionsStatus.=$options['status_id'].'|';
                            if($options['option_type_id']==2 || $options['option_type_id']==3 || $options['option_type_id']==7)
                            {
                                $optionsStr.=$options['description'].'|';
                            }
                            else {
                                $optionsStr.='';
                            }
                            
                            
                            if(count($options['additional_answers'])>0)
                            {
                                foreach($options['additional_answers'] as $additional_answers)
                                {
                                    $additional_answersIDs.='['.$additional_answers['id'].']|';
                                    $additional_answersStr.='['.$additional_answers['description'].'|'.$additional_answers['option_type_id'].'|'.$additional_answers['mandatory'].']|';
                                }
                                
                                
                            }
                            else
                            {
                                $additional_answersIDs.='[]|';
                                $additional_answersStr.='[]|';
                            }
                            $additional_answersIDs= trim($additional_answersIDs,'|');
                            $additional_answersIDs.='~';
                            $additional_answersStr= trim($additional_answersStr,'|');
                            $additional_answersStr.='~';
                            
                        }
                    }
                    
                    $optionsIDs= trim($optionsIDs,'|');
                    $optionsStr= trim($optionsStr,'|');
                    $optionsStatus= trim($optionsStatus,'|');
                    
                    
                    
                    if (preg_match('~[0-9]+~', $additional_answersIDs)) {
                        $additional_answersIDs= trim($additional_answersIDs,'~');
                    }
                    else
                        $additional_answersIDs='';
                        
                        if (preg_match('~[0-9]+~', $additional_answersStr)) {
                            $additional_answersStr= trim($additional_answersStr,'~');
                        }
                        else
                            $additional_answersStr='';
                            $activeSheet->setCellValueByColumnAndRow(10, $row, $optionsIDs);
                            $activeSheet->setCellValueByColumnAndRow(11, $row, $optionsStr);
                            $activeSheet->setCellValueByColumnAndRow(12, $row, $optionsStatus);
                            $activeSheet->setCellValueByColumnAndRow(13, $row, $additional_answersStr);
                            ++$row;
                }
                
            }
        }
        
        
        
        
        
        //Add Existing questions
        $activeSheet = $objPHPExcel->setActiveSheetIndex(1);
        $activeSheet->setCellValueByColumnAndRow(0, 1, 'LIST OF EXISTING FORM QUESTIONS ');
        $row = 3;
        if(count($questions)>0)
        {
            foreach($questions as $question)
            {
                $activeSheet->setCellValueByColumnAndRow(0, $row, $question['id']);
                $activeSheet->setCellValueByColumnAndRow(1, $row, $question['description']);
                $activeSheet->setCellValueByColumnAndRow(2, $row, $question['status_id']);
                
                ++$row;
            }
        }
        
        //Add Existing options
        $activeSheet = $objPHPExcel->setActiveSheetIndex(2);
        $activeSheet->setCellValueByColumnAndRow(0, 1, 'LIST OF EXISTING FORM OPTIONS ');
        $row = 3;
        if(count($optionsList)>0)
        {
            foreach($optionsList as $optionsL)
            {
                $activeSheet->setCellValueByColumnAndRow(0, $row, $optionsL['id']);
                $activeSheet->setCellValueByColumnAndRow(1, $row, $optionsL['description']);
                $activeSheet->setCellValueByColumnAndRow(2, $row, $optionsL['option_type_id']);
                $activeSheet->setCellValueByColumnAndRow(3, $row, $optionsL['status_id']);
                $activeSheet->setCellValueByColumnAndRow(4, $row, $optionsL['checklist_items_id']);
                ++$row;
            }
        }
        
        //Add Existing additional answers
        $activeSheet = $objPHPExcel->setActiveSheetIndex(3);
        $activeSheet->setCellValueByColumnAndRow(0, 1, 'LIST OF EXISTING FORM ADDITIONAL ANSWERS ');
        $row = 3;
        if(count($additional_answers_list)>0)
        {
            foreach($additional_answers_list as $add_Answers)
            {
                $activeSheet->setCellValueByColumnAndRow(0, $row, $add_Answers['id']);
                $activeSheet->setCellValueByColumnAndRow(1, $row, $add_Answers['description']);
                $activeSheet->setCellValueByColumnAndRow(2, $row, $add_Answers['option_type_id']);
                $activeSheet->setCellValueByColumnAndRow(3, $row, $add_Answers['checklist_options_id']);
                ++$row;
            }
        }
        
        
        //Add checklist Items Option Types
        $activeSheet = $objPHPExcel->setActiveSheetIndex(4);
        $activeSheet->setCellValueByColumnAndRow(0, 1, 'LIST OF FORM ITEM TYPES ');
        $row = 3;
        $optionTypes = ChecklistOptionTypes::model()->findAllByAttributes(array(),array('order' => 'id ASC'));
        if(count($optionTypes)>0)
        {
            foreach($optionTypes as $data)
            {
                if($data['id']==18)
                {
                    continue;
                }
                $activeSheet->setCellValueByColumnAndRow(0, $row, $data['id']);
                $activeSheet->setCellValueByColumnAndRow(1, $row, $data['description']);
                ++$row;
            }
        }
        
        //Add checklist Additonal Questions Types
        $activeSheet = $objPHPExcel->setActiveSheetIndex(5);
        $activeSheet->setCellValueByColumnAndRow(0, 1, 'LIST OF FORM ADDITIONAL QUESTION TYPES');
        $row = 3;
        $additionaQuestionsArray[1] = "Single Line Text";
        $additionaQuestionsArray[8] = "Paragraph Text";
        $additionaQuestionsArray[13] = "GPS";
        $additionaQuestionsArray[9] = "Time";
        $additionaQuestionsArray[17] = "Product";
        $additionaQuestionsArray[11] = "123 Number";
        $additionaQuestionsArray[10] = "Date";
        $additionaQuestionsArray[14] = "Photo";
        $additionaQuestionsArray[16] = "Signature";
        $additionaQuestionsArray[15] = "Barcode";
        foreach($additionaQuestionsArray as $key => $value)
        {
            $activeSheet->setCellValueByColumnAndRow(0, $row, $key);
            $activeSheet->setCellValueByColumnAndRow(1, $row, $value);
            ++$row;
        }
        
        //Download File
        $activeSheet = $objPHPExcel->setActiveSheetIndex(0);
        $objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);
        $objWriter->setPreCalculateFormulas(false);
        header('Content-type: application/vnd.ms-excel');
        $download_file_name="FormsUpdate".trim(date('Y_m_d_H:i:s')).".xls";
        header('Content-Disposition: attachment; filename="'.$download_file_name.'"');
        $objWriter->save('php://output');
        
    }
    
    public function actionImportUpdateFormValidation($id)
    {
        $con = Yii::app()->db;
        $search = '';
        $replace = '';
        $iForms = [];
        #INITIALIZE..
        ini_set("memory_limit", "-1");
        set_time_limit(0);
        $bakery = Yii::app()->user->bakery;
        $folder = Yii::app()->basePath . "/../files/";
        $mimetypes = array(
            'text/xls',
            'application/excel',
            'application/vnd.ms-excel',
            'application/vnd.msexcel',
            'application/octet-stream',
        );
        $columns = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L','M','N');
        $mFatal = '';
        $mWarning = '';
        $count = $uCounter = $iCounter = 0;
        
        //List of Item Option Types
        $iItemTypes = [];
        $optionTypes = ChecklistOptionTypes::model()->findAllByAttributes(array(),array('order' => 'description ASC'));
        foreach($optionTypes as $data)
        {
            $iItemTypes[] = $data['id'];
        }
        //List of Additional Questions types
        $iAdditionalQuestionTypes = [1,8,13,9,17,11,10,14,16,15];
        
        #PHPExcel
        //spl_autoload_unregister(array('YiiBase', 'autoload'));
        $phpExcelPath = Yii::getPathOfAlias('ext.phpexcel.Classes');
        include($phpExcelPath . DIRECTORY_SEPARATOR . 'PHPExcel.php');
        $objPHPExcel = new PHPExcel();
        $formArray=[];
        
        $iChecklistItems=[];
        $iChecklistOptions=[];
        $iAdditionalAnswers=[];
        //get Checklist Items
        $sql='select * from checklist_items where checklist_id='.$id;
        $checklistItems=Yii::app()->db->createCommand($sql)->queryAll();
        if(count($checklistItems)>0)
        {
            foreach($checklistItems as $items)
            {
                $iChecklistItems[]=$items['id'];
                
                $sql='select * from checklist_options where checklist_items_id='.$items['id'];
                $checklistOptions=Yii::app()->db->createCommand($sql)->queryAll();
                
                if(count($checklistOptions)>0)
                {
                    foreach($checklistOptions as $option)
                    {
                        $iChecklistOptions[]=$option['id'];
                        
                        $sql='select * from checklist_additional_options where checklist_options_id='.$option['id'];
                        $checklistAdditionalAnswers=Yii::app()->db->createCommand($sql)->queryAll();
                        if(count($checklistAdditionalAnswers)>0)
                        {
                            foreach($checklistAdditionalAnswers as $answers)
                            {
                                $iAdditionalAnswers[]=$answers['id'];
                                
                            }
                        }
                    }
                }
            }
        }
        
        
        #Reading
        if(is_uploaded_file($_FILES['form_file']['tmp_name']))
        {
            if(move_uploaded_file($_FILES['form_file']['tmp_name'], $folder . $_FILES['form_file']['name']))
            {
                $filename = $folder . $_FILES['form_file']['name'];
                $objPHPExcel = PHPExcel_IOFactory::load($filename);
                $msg = '';
                foreach($objPHPExcel->getWorksheetIterator() as $worksheet)
                {
                    //Worksheet info
                    $worksheetTitle = $worksheet->getTitle();
                    $highestRow = $worksheet->getHighestRow();
                    $highestColumn = $worksheet->getHighestColumn();
                    $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
                    $nrColumns = ord($highestColumn) - 64;
                    //Worksheet data
                    $duplicateSequence=[];
                    for($row = 3; $row <= $highestRow; ++$row)
                    {
                        ################################
                        #Column A: ID
                        $cell = $worksheet->getCellByColumnAndRow(0, $row);
                        $form_id = str_replace($search, $replace, $cell->getValue());
                        
                        if($form_id != $form_id)
                        {
                            $mFatal .= '- ' . $columns[0] . $row . ': Invalid Form ID. <br/>';
                        }
                        if($form_id == '')
                        {
                            $mFatal .= '- ' . $columns[0] . $row . ': Form ID cannot be empty. <br/>';
                        }
                        ################################
                        #Column B: name required
                        $cell = $worksheet->getCellByColumnAndRow(1, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        $checklist_name = $val;
                        if(trim($val) == "")
                        {
                            $mFatal .= '- ' . $columns[1] . $row . ': checklist name is required. <br/>';
                        }
                        ################################
                        #Column C: description required
                        $cell = $worksheet->getCellByColumnAndRow(2, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        $checklist_description = $val;
                        if(trim($val) == "")
                        {
                            $mFatal .= '- ' . $columns[2] . $row . ': checklist description is required. <br/>';
                        }
                        
                        ################################
                        #Column D: status  required
                        $cell = $worksheet->getCellByColumnAndRow(3, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        $status = $val;
                        $dataType = PHPExcel_Cell_DataType::dataTypeForValue($val);
                        if(trim($val) == "")
                        {
                            $mFatal .= '- ' . $columns[3] . $row . ': status is required. <br/>';
                        }
                        elseif($val != ""){
                            
                            if($dataType != 'n')
                            {
                                $mFatal .= '- ' . $columns[3] . $row . ': invalid input number. status has to be a number. <br/>';
                            }
                            
                        }
                        
                        ################################
                        #Column E: Question id
                        $cell = $worksheet->getCellByColumnAndRow(4, $row);
                        $val = $cell->getValue();
                        $product_id=$val;
                        $dataType = PHPExcel_Cell_DataType::dataTypeForValue($val);
                        if($val != "" && $dataType != 'n'){
                            $mFatal .= '- ' . $columns[4] . $row . ': invalid input number. Question id has to be a number. <br/>';
                        }
                        else if($val != "" && $dataType== 'n'){
                            if(!in_array(trim($val), $iChecklistItems))
                            {
                                $mFatal .= '- ' . $columns[4] . $row . ': invalid question id. <br/>';
                            }
                        }
                        ################################
                        #Column F: Question  required
                        $cell = $worksheet->getCellByColumnAndRow(5, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        $question = $val;
                        $dataType = PHPExcel_Cell_DataType::dataTypeForValue($val);
                        if(trim($val) == "")
                        {
                            $mFatal .= '- ' . $columns[5] . $row . ': a question is required. <br/>';
                        }
                        ################################
                        #Column G: question type  required
                        $cell = $worksheet->getCellByColumnAndRow(6, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        $question_type = $val;
                        $dataType = PHPExcel_Cell_DataType::dataTypeForValue($val);
                        if(trim($val) == "")
                        {
                            $mFatal .= '- ' . $columns[6] . $row . ': question type is required. <br/>';
                        }
                        elseif($val != ""){
                            
                            if($dataType != 'n')
                            {
                                $mFatal .= '- ' . $columns[6] . $row . ': invalid input number. question type has to be a number. <br/>';
                            }
                            elseif($dataType == 'n')
                            {
                                if(!in_array(trim($val), $iItemTypes) )
                                {
                                    $mFatal .= '- ' . $columns[6] . $row . ': question type id ' .$val .' is invalid <br/>';
                                }
                            }
                        }
                        
                        ################################
                        #Column H: order required
                        $cell = $worksheet->getCellByColumnAndRow(7, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        $question_order = $val;
                        $dataType = PHPExcel_Cell_DataType::dataTypeForValue($val);
                        if(trim($val) == "")
                        {
                            $mFatal .= '- ' . $columns[7] . $row . ': question order is required. <br/>';
                        }
                        elseif($val != ""){
                            
                            if($dataType != 'n')
                            {
                                $mFatal .= '- ' . $columns[7] . $row . ': invalid input number. question order has to be a number. <br/>';
                            }
                            
                        }
                        
                        ################################
                        #Column I: mandatory  required
                        $cell = $worksheet->getCellByColumnAndRow(8, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        $question_mandatory = $val;
                        $dataType = PHPExcel_Cell_DataType::dataTypeForValue($val);
                        if(trim($val) == "")
                        {
                            $mFatal .= '- ' . $columns[8] . $row . ': please indicate if this question is mandatory. <br/>';
                        }
                        elseif($val != ""){
                            
                            if($dataType != 'n')
                            {
                                $mFatal .= '- ' . $columns[8] . $row . ': invalid input number. mandatory status has to be a number. <br/>';
                            }
                            elseif($dataType == 'n')
                            {
                                if((int)$val>1)
                                {
                                    $mFatal .= '- ' . $columns[8] . $row . ': invalid input. mandatory status has to be 0 or 1. <br/>';
                                }
                            }
                        }
                        
                        ################################
                        #Column J: question status  required
                        $cell = $worksheet->getCellByColumnAndRow(9, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        $questionStatus = $val;
                        $dataType = PHPExcel_Cell_DataType::dataTypeForValue($val);
                        if(trim($val) == "")
                        {
                            $mFatal .= '- ' . $columns[9] . $row . ': question status is required. <br/>';
                        }
                        elseif($val != ""){
                            
                            if($dataType != 'n')
                            {
                                $mFatal .= '- ' . $columns[9] . $row . ': question invalid input number. status has to be a number. <br/>';
                            }
                            
                        }
                        //populating array for duplicate sequence check;
                        $duplicateSequence[]=array(
                            'form_id'=>$form_id,
                            'sequence'=>$question_order,
                            'status'=>$questionStatus,
                            'row' => $row
                        );
                        
                        ################################
                        #Column K: option id required
                        $cell = $worksheet->getCellByColumnAndRow(10, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        
                        $dataType = PHPExcel_Cell_DataType::dataTypeForValue($val);
                        if($val !="")
                        {
                            $splitoptions = explode('|', $val);
                            if(count($splitoptions)>0)
                            {
                                foreach ($splitoptions as $option)
                                {
                                    if(!in_array(trim($option), $iChecklistOptions) )
                                    {
                                        $mFatal .= '- ' . $columns[10] . $row . ': Option ID  ' .$val .' is invalid <br/>';
                                    }
                                    
                                }
                            }
                        }
                        
                        ################################
                        #Column L: option(s) required
                        $cell = $worksheet->getCellByColumnAndRow(11, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        $optionsList = [];
                        $dataType = PHPExcel_Cell_DataType::dataTypeForValue($val);
                        if($question_type==2 || $question_type==3 || $question_type==7)
                        {
                            if($val =="")
                            {
                                $mFatal .= '- ' . $columns[11] . $row . ': option  is required. <br/>';
                            }
                            elseif($val !="")
                            {
                                $splitoptions = explode('|', $val);
                                if(count($splitoptions)>0)
                                {
                                    foreach ($splitoptions as $option)
                                    {
                                        if($option=="")
                                        {
                                            $mFatal .= '- ' . $columns[11] . $row . ': option  is required. <br/>';
                                        }
                                        else
                                        {
                                            $optionsList[] = $option;
                                        }
                                        
                                    }
                                }
                            }
                            
                        }
                        ################################
                        #Column M: option(s) status required
                        $cell = $worksheet->getCellByColumnAndRow(12, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        $optionsListStatus = [];
                        $dataType = PHPExcel_Cell_DataType::dataTypeForValue($val);
                        if($val =="")
                        {
                            $mFatal .= '- ' . $columns[12] . $row . ': option(s) status  is required. <br/>';
                        }
                        elseif($val !="")
                        {
                            $splitOptionsStatuses = explode('|', $val);
                            $countInactiveOptionsStatus=0;
                            if(count($splitOptionsStatuses)>0)
                            {
                                foreach ($splitOptionsStatuses as $optionStatus)
                                {
                                    if($optionStatus=="")
                                    {
                                        $mFatal .= '- ' . $columns[12] . $row . ': option status  is required. <br/>';
                                    }
                                    else
                                    {
                                        if($optionStatus==0)
                                        {
                                            $countInactiveOptionsStatus++;
                                        }
                                        $optionsListStatus[] = $optionStatus;
                                    }
                                    
                                }
                                if(count($splitOptionsStatuses) == $countInactiveOptionsStatus && $questionStatus == 1)
                                {
                                    $mFatal .= '- ' . $columns[12] . $row . ': all options are inactive, either make one option active or change the question status to inactive.. <br/>';
                                }
                            }
                        }
                        
                        
                        
                        ################################
                        #Column L: additional questions required
                        $cell = $worksheet->getCellByColumnAndRow(13, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        $dataType = PHPExcel_Cell_DataType::dataTypeForValue($val);
                        $additionalQuestions = [];
                        if(trim($val) != "")
                        {
                            $countOptionAdditionalQuestion=0;
                            $splitoptionsAddQuestion = explode('~', $val);
                            foreach($splitoptionsAddQuestion as $splitOpAddQuestionsKey=>$splitOpAddQuestionsValue)
                            {
                                
                                $countAddQuestion = 0;
                                $addQuestionArray = preg_split('/(\[[^]]+\])/', trim($splitOpAddQuestionsValue), -1, PREG_SPLIT_DELIM_CAPTURE);
                                foreach($addQuestionArray as $addArray)
                                {
                                    if($addArray !='' && $addArray!=null && $addArray!="[]")
                                    {
                                        $addArray = str_replace(array('[',']'),'',$addArray);
                                        $splitAddQuestions = explode('|', $addArray);
                                        if(count($splitAddQuestions)>0)
                                        {
                                            
                                            $countAddQuestionOptions = 0;
                                            $keyAnswer='';
                                            $keyAnswerType='';
                                            $keyMandatory='';
                                            foreach ($splitAddQuestions as $key=>$value)
                                            {
                                                if($key==0)
                                                {
                                                    $keyAnswer=$value;
                                                }
                                                elseif($key==1)
                                                {
                                                    $keyAnswerType=$value;
                                                }
                                                elseif ($key==2)
                                                {
                                                    $keyMandatory=$value;
                                                }
                                            }
                                            if($keyAnswer=="" && $keyAnswerType=="")
                                            {
                                                continue;
                                            }
                                            
                                            if($keyAnswer=="")
                                            {
                                                $mFatal .= '- ' . $columns[13] . $row . ': additional answer is required. <br/>';
                                            }
                                            else {
                                                $additionalQuestions[$countOptionAdditionalQuestion][$countAddQuestion]['additional_answer']=$keyAnswer;
                                                
                                            }
                                            
                                            if($keyAnswerType == "")
                                            {
                                                $mFatal .= '- ' . $columns[13] . $row . ': additional answer type is required. <br/>';
                                            }
                                            elseif($keyAnswerType != "")
                                            {
                                                
                                                $dataType = PHPExcel_Cell_DataType::dataTypeForValue($keyAnswerType);
                                                
                                                if($dataType != 'n')
                                                {
                                                    $mFatal .= '- ' . $columns[13] . $row . ': invalid input number.additional question type has to be a number. <br/>';
                                                }
                                                elseif($dataType == 'n')
                                                {
                                                    if(!in_array(trim($keyAnswerType), $iAdditionalQuestionTypes) )
                                                    {
                                                        $mFatal .= '- ' . $columns[13] . $row . ': question type type id ' .$keyAnswerType .' is invalid <br/>';
                                                    }
                                                    else {
                                                        
                                                        $additionalQuestions[$countOptionAdditionalQuestion][$countAddQuestion]['additional_answer_type']=$keyAnswerType;
                                                    }
                                                }
                                                
                                                
                                                
                                                
                                            }
                                            
                                            
                                            if($keyMandatory == "")
                                            {
                                                $mFatal .= '- ' . $columns[13] . $row . ': additional answer mandatory status is required. <br/>';
                                            }
                                            elseif($value != "")
                                            {
                                                $dataType = PHPExcel_Cell_DataType::dataTypeForValue($keyMandatory);
                                                
                                                if($dataType != 'n')
                                                {
                                                    $mFatal .= '- ' . $columns[13] . $row . ':additional answer invalid input number. mandatory status has to be a number. <br/>';
                                                }
                                                elseif($dataType == 'n')
                                                {
                                                    if((int)$keyMandatory>1)
                                                    {
                                                        $mFatal .= '- ' . $columns[13] . $row . ': invalid input. additional answer mandatory status has to be 0 or 1. <br/>';
                                                    }
                                                    else {
                                                        $additionalQuestions[$countOptionAdditionalQuestion][$countAddQuestion]['additional_answer_mandatory']=$value;
                                                    }
                                                }
                                                
                                                
                                            }
                                            if(count($additionalQuestions[$countOptionAdditionalQuestion][$countAddQuestion])!=3)
                                            {
                                                $mFatal .= 'Row - ' .$row . ': invalid format. additional answer format is invalid. <br/>';
                                                
                                            }
                                            
                                            
                                            
                                            
                                            
                                        }
                                        $countAddQuestion++;
                                    }
                                    
                                }
                                
                                $countOptionAdditionalQuestion++;
                            }
                        }
                        
                        
                        $checklist= Checklist::model()->findByPk($id);
                        $checklistBranch = ChecklistBakeries::model()->findAllByAttributes(array('checklist_id'=>$id),array('order' => 'bakery_id ASC'));
                        $branch_list=[];
                        if(count($checklistBranch)>0)
                        {
                            foreach($checklistBranch as $data)
                            {
                                $branch_list[]=$data['bakery_id'];
                            }
                        }
                        $formGroup_list=[];
                        $sql='select * from checklist_form_groups where checklist_id='.$id;
                        $checklistFormGroups=Yii::app()->db->createCommand($sql)->queryAll();
                        if(count($checklistFormGroups)>0)
                        {
                            foreach($checklistFormGroups as $data)
                            {
                                $formGroup_list[]=$data['form_group_id'];
                            }
                        }
                        
                        if(count($additionalQuestions)>0)
                        {
                            foreach ($additionalQuestions as $key=>$value)
                            {
                                
                                if(!isset($value['additional_answer'])  && !isset($value['additional_answer_type']))
                                {
                                    unset($additionalQuestions[$key]);
                                }
                            }
                        }
                        //setting array
                        $formArray[]=array(
                            'form' => array(
                                'name' => $checklist_name,
                                'description' => $checklist_description,
                                'branch' => $branch_list,
                                'default_all_employees' => $checklist['default_all_employees'],
                                'form_groups' => $formGroup_list,
                                'start_date' => $checklist['effective_date'],
                                'end_date' => $checklist['expiry_date'],
                                'default_all_customers' => $checklist['default_all_customers'],
                                'pre_populate' => $checklist['prepopulate_form'],
                                'image_approve' => $checklist['image_approval'],
                                'form_category' => $checklist['checklist_category_id'],
                                'approval' => $checklist['request_approval_by'],
                                'history_days' => $checklist['history_duration'],
                                'results_days' => $checklist['duration'],
                                'status' => $checklist['status_id']
                            ),
                            'checklist' => array(
                                'question' => $question,
                                'question_order' => $question_order,
                                'question_manatory' => $question_mandatory,
                                'question_type' => $question_type,
                                'question_status' => $questionStatus,
                                'options' => $optionsList,
                                'options_statuses' => $optionsListStatus,
                                'additional_answers' => $additionalQuestions
                            )
                            
                        );
                        
                        
                        
                        
                    }
                    if(count($duplicateSequence)>0)
                    {
                        foreach($duplicateSequence as $key=>$duplicate)
                        {
                            if($this->checkDuplicates($duplicateSequence, $duplicate['form_id'], $duplicate['sequence'] , $duplicate['status'],$duplicate['row'],$key))
                            {
                                $mFatal .= 'Row - ' .$duplicate['row'] . ': this form question contains a duplicate sequence. <br/>';
                            }
                            
                        }
                    }
                    
                    
                    //Stop after reading the first worksheet
                    break;
                }
            }
        }
        
        
        
        #OUTPUT
        if($mFatal != '' or $mWarning != '')
        {
            $disabled = '';
            if($mFatal != '')
            {
                echo "<div class='alert alert-danger'><b>FATAL ERROR!<br/>You can not import this file as it contains errors:</b><br/>" . $mFatal . "</div>";
                $part = '<input type="button" onClick="goBackToTwo()" class="btn btn-primary btn-sm" value="Back" />
                             <input type="button" id="next3" class="btn btn-primary btn-sm" value="Next" disabled="true"/>';
                $disabled = 'disabled="true"';
            }
            if($mWarning != '')
            {
                echo "<div class='alert alert-warning'><b>WARNING!<br/>Please note that you are about to:</b><br/>" . $mWarning . "</div>";
                $part = '<input type="button" onClick="goBackToTwo()" class="btn btn-primary btn-sm" value="Back" />
                             <input type="button" onClick="goToLastStep()" class="btn btn-primary btn-sm" value="Continue" ' . $disabled . '/>';
            }
            echo '<table id="date_filter_table customTable" width="100%">
                        <tr><td>
                            ' . $part . '
                        </td></tr>
                     </table> <script type="text/javascript">reloadCustomers()</script>
                     ';
        }
        else
        {
            $count = $uCounter + $iCounter;
            $msg = $count . ' records found';
            
            $this->renderPartial('_updatePreview', array(
                'formArray' => $formArray
            ));
            
            
            echo '<table id="date_filter_table customTable" width="100%">
                        <tr><td>
                            <input type="button" onClick="goBackToTwo()" class="btn btn-primary btn-sm" value="Back" disabled="true" />
                            <input type="button" onClick="goToLastStep()" class="btn btn-primary btn-sm" value="Continue"/>
                        </td></tr>
                     </table> <script type="text/javascript">reloadCustomers()</script>
                     ';
            
        }
        
        die;
        
    }
    public function actionImportUpdateFormUpdate($id)
    {
        $con = Yii::app()->db;
        $search = '';
        $replace = '';
        $iForms = [];
        #INITIALIZE..
        ini_set("memory_limit", "-1");
        set_time_limit(0);
        $bakery = Yii::app()->user->bakery;
        $folder = Yii::app()->basePath . "/../files/";
        $mimetypes = array(
            'text/xls',
            'application/excel',
            'application/vnd.ms-excel',
            'application/vnd.msexcel',
            'application/octet-stream',
        );
        $columns = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T');
        $mFatal = '';
        $mWarning = '';
        $count = $uCounter = $iCounter = 0;
        #PHPExcel
        //        spl_autoload_unregister(array('YiiBase', 'autoload'));
        $phpExcelPath = Yii::getPathOfAlias('ext.phpexcel.Classes');
        include($phpExcelPath . DIRECTORY_SEPARATOR . 'PHPExcel.php');
        $objPHPExcel = new PHPExcel();
        $formArray=[];
        #Reading
        if(is_uploaded_file($_FILES['form_file']['tmp_name']))
        {
            if(move_uploaded_file($_FILES['form_file']['tmp_name'], $folder . $_FILES['form_file']['name']))
            {
                $filename = $folder . $_FILES['form_file']['name'];
                $objPHPExcel = PHPExcel_IOFactory::load($filename);
                $msg = '';
                foreach($objPHPExcel->getWorksheetIterator() as $worksheet)
                {
                    //Worksheet info
                    $worksheetTitle = $worksheet->getTitle();
                    $highestRow = $worksheet->getHighestRow();
                    $highestColumn = $worksheet->getHighestColumn();
                    $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
                    $nrColumns = ord($highestColumn) - 64;
                    //Worksheet data
                    for($row = 3; $row <= $highestRow; ++$row)
                    {
                        ################################
                        #Column A: ID required
                        $cell = $worksheet->getCellByColumnAndRow(0, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        $id = $val;
                        
                        ################################
                        #Column B: name required
                        $cell = $worksheet->getCellByColumnAndRow(1, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        $checklist_name = $val;
                        
                        ################################
                        #Column C: description required
                        $cell = $worksheet->getCellByColumnAndRow(2, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        $checklist_description = $val;
                        
                        ################################
                        #Column D: status  required
                        $cell = $worksheet->getCellByColumnAndRow(3, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        $status = $val;
                        
                        ################################
                        #Column E: Question id required
                        $cell = $worksheet->getCellByColumnAndRow(4, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        $question_id = $val;
                        
                        ################################
                        #Column F: Question  required
                        $cell = $worksheet->getCellByColumnAndRow(5, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        $question = $val;
                        
                        ################################
                        #Column G: question type  required
                        $cell = $worksheet->getCellByColumnAndRow(6, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        $question_type = $val;
                        
                        ################################
                        #Column H: order required
                        $cell = $worksheet->getCellByColumnAndRow(7, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        $question_order = $val;
                        
                        ################################
                        #Column I: mandatory  required
                        $cell = $worksheet->getCellByColumnAndRow(8, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        $question_mandatory = $val;
                        
                        ################################
                        #Column J: question status  required
                        $cell = $worksheet->getCellByColumnAndRow(9, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        $question_status = $val;
                        
                        
                        ################################
                        #Column K: option(s) id required
                        $cell = $worksheet->getCellByColumnAndRow(10, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        $optionsIDList = [];
                        
                        if($val !="")
                        {
                            $splitoptions = explode('|', $val);
                            if(count($splitoptions)>0)
                            {
                                foreach ($splitoptions as $option)
                                {
                                    $optionsIDList[] = $option;
                                    
                                }
                            }
                        }
                        
                        ################################
                        #Column L: option(s) required
                        $cell = $worksheet->getCellByColumnAndRow(11, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        $optionsList = [];
                        
                        if($val !="")
                        {
                            $splitoptions = explode('|', $val);
                            if(count($splitoptions)>0)
                            {
                                foreach ($splitoptions as $option)
                                {
                                    $optionsList[] = $option;
                                    
                                }
                            }
                        }
                        
                        ################################
                        #Column M: option(s) statuses required
                        $cell = $worksheet->getCellByColumnAndRow(12, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        $optionsListStatuses = [];
                        
                        if($val !="")
                        {
                            $splitOptionsStatuses = explode('|', $val);
                            if(count($splitOptionsStatuses)>0)
                            {
                                foreach ($splitOptionsStatuses as $optionStatus)
                                {
                                    $optionsListStatuses[] = $optionStatus;
                                    
                                }
                            }
                        }
                        ################################
                        #Column N: additional questions required
                        $cell = $worksheet->getCellByColumnAndRow(13, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        $additionalQuestions = [];
                        if(trim($val) != "")
                        {
                            $countOptionAdditionalQuestion=0;
                            $splitoptionsAddQuestion = explode('~', $val);
                            foreach($splitoptionsAddQuestion as $splitOpAddQuestions)
                            {
                                $countAddQuestion = 0;
                                $addQuestionArray = preg_split('/(\[[^]]+\])/', trim($splitOpAddQuestions), -1, PREG_SPLIT_DELIM_CAPTURE);
                                
                                foreach($addQuestionArray as $addArray)
                                {
                                    if($addArray !='' || $addArray!=null)
                                    {
                                        
                                        $addArray = str_replace(array('[',']'),'',$addArray);
                                        $splitAddQuestions = explode('|', $addArray);
                                        if(count($splitAddQuestions)>0)
                                        {
                                            $countAddQuestionOptions = 0;
                                            foreach ($splitAddQuestions as $key=>$value)
                                            {
                                                if($key==0)
                                                {
                                                    $additionalQuestions[$countOptionAdditionalQuestion][$countAddQuestion]['additional_answer']=$value;
                                                }
                                                if($key==1)
                                                {
                                                    $additionalQuestions[$countOptionAdditionalQuestion][$countAddQuestion]['additional_answer_type']=$value;
                                                    
                                                }
                                                
                                                if($key==2)
                                                {
                                                    $additionalQuestions[$countOptionAdditionalQuestion][$countAddQuestion]['additional_answer_mandatory']=$value;
                                                    
                                                    
                                                }
                                                
                                            }
                                        }
                                        $countAddQuestion++;
                                    }
                                    
                                }
                                
                                $countOptionAdditionalQuestion++;
                            }
                            
                            
                            
                            
                        }
                        
                        //setting array
                        $formArray[$id][]=array(
                            'form' => array(
                                'id'=>$id,
                                'name' => $checklist_name,
                                'description' => $checklist_description,
                                'status' => $status
                            ),
                            'checklist' => array(
                                'question_id'=>$question_id,
                                'question' => $question,
                                'question_order' => $question_order,
                                'question_manatory' => $question_mandatory,
                                'question_type' => $question_type,
                                'question_status' => $question_status,
                                'optionsID'=>$optionsIDList,
                                'options' => $optionsList,
                                'optionsStatuses' => $optionsListStatuses,
                                'additional_answers' => $additionalQuestions
                            )
                            
                        );
                    }
                    break;
                }
            }
        }
        $formDetails=[];
        $formChecklists=[];
        $countFormsNum=0;
        foreach ($formArray as $formChecklist)
        {
            foreach($formChecklist as $form)
            {
                $countFormsNum++;
                $formDetails=$form['form'];
                $formChecklists[]=$form['checklist'];
            }
        }
        //update form
        $checklist_details= Checklist::model()->findByPk($formDetails['id']);
        if(count($formDetails)>0)
        {
            
            $model = $this->loadModel($formDetails['id']);
            $model->checklist_name = $formDetails['name'];
            $model->description = $formDetails['description'];
            $model->status_id = $formDetails['status'];
            $model->updated = date("Y-m-d H:i:s");
            $model->updated_by = Yii::app()->user->id;
            if($checklist_details['request_approval_by']>0)
            {
                $model->status_id=0;
                $status=0;
                $comment='Form Updated';
            }
            else {
                $status=$model->status_id;
                $comment='Form Updated';
            }
            $model->save();
            $SQL = "INSERT INTO `checklist_reviews_logs`(`date_created`,`status_id`, `checklist_id`, `employee_id`, `reason`)
                     VALUES ('".date('Y-m-d H:i:s')."',".$status.",". $formDetails['id'].",".Yii::app()->user->id.",'".$comment."')";
            $insert_command = Yii::app()->db->createCommand($SQL);
            $insert_command->execute();
            $con = Yii::app()->db;
            if(count($formChecklists)>0)
            {
                $sql = "UPDATE `checklist_items` SET `status_id`=0 WHERE `checklist_id` = ".$formDetails['id'];
                $cmd = $con->createCommand($sql);
                $res = $cmd->execute();
                
                $sql = "DELETE FROM `checklist_additional_options` WHERE `checklist_id` = ".$formDetails['id'];
                $cmd = $con->createCommand($sql);
                $res = $cmd->execute();
                foreach($formChecklists as $checklists)
                {
                    
                    
                    if($checklists['question_id'])
                    {
                        //update question
                        ChecklistItems::model()->updateByPk($checklists['question_id'], ['description' => $checklists['question'], 'ordering' => $checklists['question_order'], 'mandatory' => $checklists['question_manatory'],'status_id' =>$checklists['question_status']]);
                        if($checklists['question_type']==2 || $checklists['question_type']==3 || $checklists['question_type']==7)
                        {
                            if(count($checklists['options'])>0)
                            {
                                foreach($checklists['options'] as $key =>$value)
                                {
                                    $option_status=$checklists['optionsStatuses'][$key];
                                    if(isset($checklists['optionsID'][$key]))
                                    {
                                        
                                        ChecklistOptions::model()->updateByPk($checklists['optionsID'][$key], ['description' => $value, 'option_type' => $checklists['question_type'], 'status_id' => $option_status]);
                                        
                                        if(isset($checklists['additional_answers'][$key]))
                                        {
                                            if(count($checklists['additional_answers'][$key]))
                                            {
                                                foreach($checklists['additional_answers'][$key] as $addAnswers)
                                                {
                                                    if($addAnswers['additional_answer']!='' && $addAnswers['additional_answer_type']!='')
                                                    {
                                                        $SQL = "INSERT INTO `checklist_additional_options`(`checklist_id`,`checklist_item_id`, `option_type_id`, `status_id`, `checklist_options_id`, `mandatory`,`description`)
                                                                     VALUES (:checklist_id,:checklist_item_id,:option_type_id,:status_id,:checklist_options_id,:mandatory,:description)";
                                                        $command = $con->createCommand($SQL);
                                                        $command->bindValue(':checklist_id',$formDetails['id'], PDO::PARAM_INT);
                                                        $command->bindValue(':checklist_item_id', $checklists['question_id'], PDO::PARAM_INT);
                                                        $command->bindValue(':option_type_id', $addAnswers['additional_answer_type'], PDO::PARAM_INT);
                                                        $command->bindValue(':status_id',$option_status, PDO::PARAM_INT);
                                                        $command->bindValue(':checklist_options_id',$checklists['optionsID'][$key], PDO::PARAM_INT);
                                                        $command->bindValue(':mandatory',$addAnswers['additional_answer_mandatory'], PDO::PARAM_INT);
                                                        $command->bindValue(':description',$addAnswers['additional_answer'], PDO::PARAM_STR);
                                                        $command->execute();
                                                        
                                                    }
                                                    
                                                }
                                                
                                                
                                            }
                                        }
                                    }
                                    else{
                                        $SQL = "INSERT INTO `checklist_options`(`checklist_items_id`,`description`, `option_type_id`, `ordering`,`status_id`)
                                                VALUES (:checklist_items_id,:description,:option_type_id,:ordering,:status_id)";
                                        $command = $con->createCommand($SQL);
                                        $command->bindValue(':checklist_items_id', $checklists['question_id'], PDO::PARAM_STR);
                                        $command->bindValue(':description', value, PDO::PARAM_STR);
                                        $command->bindValue(':option_type_id', $checklists['question_type'], PDO::PARAM_INT);
                                        $command->bindValue(':ordering',1, PDO::PARAM_INT);
                                        $command->bindValue(':status_id',$option_status, PDO::PARAM_INT);
                                        $command->execute();
                                        $optionID = Yii::app()->db->getLastInsertId();
                                        if(isset($checklists['additional_answers'][$key]) && count($checklists['additional_answers'][$key]))
                                        {
                                            foreach($checklists['additional_answers'][$key] as $addAnswers)
                                            {
                                                if($addAnswers['additional_answer'] !='' && $addAnswers['additional_answer_type']!='')
                                                {
                                                    $SQL = "INSERT INTO `checklist_additional_options`(`checklist_id`,`checklist_item_id`, `option_type_id`, `status_id`, `checklist_options_id`, `mandatory`,`description`)
                                                                VALUES (:checklist_id,:checklist_item_id,:option_type_id,:status_id,:checklist_options_id,:mandatory,:description)";
                                                    $command = $con->createCommand($SQL);
                                                    $command->bindValue(':checklist_id',$formDetails['id'], PDO::PARAM_INT);
                                                    $command->bindValue(':checklist_item_id', $checklists['question_id'], PDO::PARAM_INT);
                                                    $command->bindValue(':option_type_id', $addAnswers['additional_answer_type'], PDO::PARAM_INT);
                                                    $command->bindValue(':status_id',1, PDO::PARAM_INT);
                                                    $command->bindValue(':checklist_options_id',$optionID, PDO::PARAM_INT);
                                                    $command->bindValue(':mandatory',$addAnswers['additional_answer_mandatory'], PDO::PARAM_INT);
                                                    $command->bindValue(':description',$addAnswers['additional_answer'], PDO::PARAM_STR);
                                                    $command->execute();
                                                }
                                                
                                            }
                                            
                                            
                                        }
                                        
                                        
                                        
                                    }
                                }
                            }
                        }
                        else
                        {
                            $description='';
                            $option_status=$checklists['optionsStatuses'][0];
                            if(isset($checklists['optionsID'][0]))
                            {
                                $value="";
                                
                                ChecklistOptions::model()->updateByPk($checklists['optionsID'][0], ['description' => $value, 'option_type' => $checklists['question_type'], 'status_id' => $option_status]);
                                
                                if(isset($checklists['additional_answers'][0]) && count($checklists['additional_answers'][0]))
                                {
                                    foreach($checklists['additional_answers'][0] as $addAnswers)
                                    {
                                        if($addAnswers['additional_answer']!='' && $addAnswers['additional_answer_type']!='')
                                        {
                                            $SQL = "INSERT INTO `checklist_additional_options`(`checklist_id`,`checklist_item_id`, `option_type_id`, `status_id`, `checklist_options_id`, `mandatory`,`description`)
                                                         VALUES (:checklist_id,:checklist_item_id,:option_type_id,:status_id,:checklist_options_id,:mandatory,:description)";
                                            $command = $con->createCommand($SQL);
                                            $command->bindValue(':checklist_id', $formDetails['id'], PDO::PARAM_INT);
                                            $command->bindValue(':checklist_item_id', $checklists['question_id'], PDO::PARAM_INT);
                                            $command->bindValue(':option_type_id', $addAnswers['additional_answer_type'], PDO::PARAM_INT);
                                            $command->bindValue(':status_id', 1, PDO::PARAM_INT);
                                            $command->bindValue(':checklist_options_id', $checklists['optionsID'][0], PDO::PARAM_INT);
                                            $command->bindValue(':mandatory', $addAnswers['additional_answer_mandatory'], PDO::PARAM_INT);
                                            $command->bindValue(':description', $addAnswers['additional_answer'], PDO::PARAM_STR);
                                            $command->execute();
                                            
                                        }
                                        
                                    }
                                    
                                    
                                }
                                
                                
                            }
                            
                            else {
                                $SQL = "INSERT INTO `checklist_options`(`checklist_items_id`,`description`, `option_type_id`, `ordering`,`status_id`)
                                             VALUES (:checklist_items_id,:description,:option_type_id,:ordering,:status_id)";
                                $command = $con->createCommand($SQL);
                                $command->bindValue(':checklist_items_id', $checklists['question_id'], PDO::PARAM_INT);
                                $command->bindValue(':description', '', PDO::PARAM_STR);
                                $command->bindValue(':option_type_id', $checklists['question_type'], PDO::PARAM_INT);
                                $command->bindValue(':ordering', 1, PDO::PARAM_INT);
                                $command->bindValue(':status_id', $option_status, PDO::PARAM_INT);
                                $command->execute();
                                $optionID = Yii::app()->db->getLastInsertId();
                                if(isset($checklists['additional_answers'][0]) && count($checklists['additional_answers'][0]))
                                {
                                    foreach($checklists['additional_answers'][0] as $addAnswers)
                                    {
                                        if($addAnswers['additional_answer']!='' && $addAnswers['additional_answer_type']!='')
                                        {
                                            $SQL = "INSERT INTO `checklist_additional_options`(`checklist_id`,`checklist_item_id`, `option_type_id`, `status_id`, `checklist_options_id`, `mandatory`,`description`)
                                                          VALUES (:checklist_id,:checklist_item_id,:option_type_id,:status_id,:checklist_options_id,:mandatory,:description)";
                                            $command = $con->createCommand($SQL);
                                            $command->bindValue(':checklist_id', $formDetails['id'], PDO::PARAM_INT);
                                            $command->bindValue(':checklist_item_id', $checklists['question_id'], PDO::PARAM_INT);
                                            $command->bindValue(':option_type_id', $addAnswers['additional_answer_type'], PDO::PARAM_INT);
                                            $command->bindValue(':status_id', 1, PDO::PARAM_INT);
                                            $command->bindValue(':checklist_options_id', $optionID, PDO::PARAM_INT);
                                            $command->bindValue(':mandatory', $addAnswers['additional_answer_mandatory'], PDO::PARAM_INT);
                                            $command->bindValue(':description', $addAnswers['additional_answer'], PDO::PARAM_STR);
                                            $command->execute();
                                            
                                        }
                                        
                                    }
                                    
                                    
                                }
                            }
                        }
                    }
                    else{
                        $sql = "INSERT INTO `checklist_items`(`checklist_id`,`description`, `ordering`, `mandatory`,`status_id`)
                                   VALUES (:checklist_id,:description,:ordering,:mandatory,:status_id)";
                        $command = $con->createCommand($sql);
                        $command->bindValue(':checklist_id', $formDetails['id'], PDO::PARAM_STR);
                        $command->bindValue(':description', $checklists['question'], PDO::PARAM_INT);
                        $command->bindValue(':ordering', $checklists['question_order'], PDO::PARAM_INT);
                        $command->bindValue(':mandatory', $checklists['question_manatory'], PDO::PARAM_INT);
                        $command->bindValue(':status_id', $checklists['question_status'], PDO::PARAM_INT);
                        $command->execute();
                        $checklists['question_id'] = Yii::app()->db->getLastInsertId();
                        if($checklists['question_type']==2 || $checklists['question_type']==3 || $checklists['question_type']==7)
                        {
                            if(count($checklists['options'])>0)
                            {
                                foreach($checklists['options'] as $key =>$value)
                                {
                                    $option_status=$checklists['optionsStatuses'][$key];
                                    
                                    $SQL = "INSERT INTO `checklist_options`(`checklist_items_id`,`description`, `option_type_id`, `ordering`,`status_id`)
                                                        VALUES (:checklist_items_id,:description,:option_type_id,:ordering,:status_id)";
                                    $command = $con->createCommand($SQL);
                                    $command->bindValue(':checklist_items_id', $checklists['question_id'], PDO::PARAM_STR);
                                    $command->bindValue(':description', $value, PDO::PARAM_STR);
                                    $command->bindValue(':option_type_id', $checklists['question_type'], PDO::PARAM_INT);
                                    $command->bindValue(':ordering', 1, PDO::PARAM_INT);
                                    $command->bindValue(':status_id', $option_status, PDO::PARAM_INT);
                                    $command->execute();
                                    $optionID = Yii::app()->db->getLastInsertId();
                                    if(isset($checklists['additional_answers'][$key]) && count($checklists['additional_answers'][$key]))
                                    {
                                        foreach($checklists['additional_answers'][$key] as $addAnswers)
                                        {
                                            if($addAnswers['additional_answer']!='' && $addAnswers['additional_answer_type']!='')
                                            {
                                                $SQL = "INSERT INTO `checklist_additional_options`(`checklist_id`,`checklist_item_id`, `option_type_id`, `status_id`, `checklist_options_id`, `mandatory`,`description`)
                                                             VALUES (:checklist_id,:checklist_item_id,:option_type_id,:status_id,:checklist_options_id,:mandatory,:description)";
                                                
                                                $command = $con->createCommand($SQL);
                                                $command->bindValue(':checklist_id',$formDetails['id'], PDO::PARAM_INT);
                                                $command->bindValue(':checklist_item_id', $checklists['question_id'], PDO::PARAM_INT);
                                                $command->bindValue(':option_type_id', $addAnswers['additional_answer_type'], PDO::PARAM_INT);
                                                $command->bindValue(':status_id', 1, PDO::PARAM_INT);
                                                $command->bindValue(':checklist_options_id', $optionID, PDO::PARAM_INT);
                                                $command->bindValue(':mandatory', $addAnswers['additional_answer_mandatory'], PDO::PARAM_INT);
                                                $command->bindValue(':description', $addAnswers['additional_answer'], PDO::PARAM_STR);
                                                $command->execute();
                                            }
                                            
                                        }
                                        
                                        
                                    }
                                    
                                }
                            }
                        }
                        else
                        {
                            $description='';
                            
                            $option_status=$checklists['optionsStatuses'][0];
                            $SQL = "INSERT INTO `checklist_options`(`checklist_items_id`,`description`, `option_type_id`, `ordering`,`status_id`)
                                                        VALUES (:checklist_items_id,:description,:option_type_id,:ordering,:status_id)";
                            $command = $con->createCommand($SQL);
                            $command->bindValue(':checklist_items_id', $checklists['question_id'], PDO::PARAM_STR);
                            $command->bindValue(':description', '', PDO::PARAM_STR);
                            $command->bindValue(':option_type_id', $checklists['question_type'], PDO::PARAM_INT);
                            $command->bindValue(':ordering', 1, PDO::PARAM_INT);
                            $command->bindValue(':status_id', $option_status, PDO::PARAM_INT);
                            
                            $command->execute();
                            $optionID = Yii::app()->db->getLastInsertId();
                            if(isset($checklists['additional_answers'][0]) && count($checklists['additional_answers'][0]))
                            {
                                foreach($checklists['additional_answers'][0] as $addAnswers)
                                {
                                    if($addAnswers['additional_answer']!='' && $addAnswers['additional_answer_type']!='')
                                    {
                                        $SQL = "INSERT INTO `checklist_additional_options`(`checklist_id`,`checklist_item_id`, `option_type_id`, `status_id`, `checklist_options_id`, `mandatory`,`description`)
                                                       VALUES (:checklist_id,:checklist_item_id,:option_type_id,:status_id,:checklist_options_id,:mandatory,:description)";
                                        $command = $con->createCommand($SQL);
                                        $command->bindValue(':checklist_id',$formDetails['id'], PDO::PARAM_INT);
                                        $command->bindValue(':checklist_item_id', $checklists['question_id'], PDO::PARAM_INT);
                                        $command->bindValue(':option_type_id', $addAnswers['additional_answer_type'], PDO::PARAM_INT);
                                        $command->bindValue(':status_id', 1, PDO::PARAM_INT);
                                        $command->bindValue(':checklist_options_id', $optionID, PDO::PARAM_INT);
                                        $command->bindValue(':mandatory', $addAnswers['additional_answer_mandatory'], PDO::PARAM_INT);
                                        $command->bindValue(':description', $addAnswers['additional_answer'], PDO::PARAM_STR);
                                        $command->execute();
                                    }
                                    
                                }
                                
                                
                            }
                            
                        }
                        
                    }
                    
                }
            }
            
        }
        echo '<div class="alert alert-success"><span class="fa fa-check-circle fa-fw"></span>&nbsp;'.$countFormsNum.' Records processed</div>';
        die;
    }
    public function actionGetImagePath()
    {
        $url=$_POST['file_path'];
        $URLProcessed = str_replace($_SERVER['SERVER_NAME'] . 'https', 'https', $url);
        $str = '';
        if (strpos($url, "/") > 0) {
            $str = $URLProcessed;
        } else {
            $str = 'https://' . $_SERVER['SERVER_NAME'] . $URLProcessed;
        }
        $str=str_replace("http://","https://",$str);
        die ( json_encode ( array (
            'path'=>$str
        ) ) );
    }
    public function actionGetProductBrands()
    {
        
        $productBrand = [];
        $condition = 'status_id = 1';
        $productBrandList = ProductSupplier:: model()->findAll(['condition' => $condition,'order' => 'description']);
        foreach($productBrandList as $product_Brand)
        {
            $productBrand[] = array(
                'id' => $product_Brand['id'],
                'description' => $product_Brand['description']
            );
        }
        
        
        die ( json_encode ( array (
            'productBrand'=>$productBrand,
        ) ) );
    }
    
    public function actionGetProductCategories()
    {
        
        $productCategories = [];
        $condition = 'status_id = 1';
        $productCategoriesList= ProductType:: model()->findAll(['condition' => $condition,'order' => 'description']);
        foreach($productCategoriesList as $product_Cat)
        {
            $productCategories[] = array(
                'id' => $product_Cat['id'],
                'description' => $product_Cat['description']
            );
        }
        die ( json_encode ( array (
            'productCategories'=>$productCategories
        ) ) );
    }
    public function actionStartCustomers()
    {
        ini_set("memory_limit", "-1");
        set_time_limit(0);
        $bakery_id = Yii::app()->user->bakery;
        $employeeCustomers = [];
        $customers = [];
        $employeeDetails= Employee:: model()->findByPk(Yii::app()->user->id);
        $role = Yii::app()->user->role;
        
        
        
        if($role==1)
        {
            $bakeryWhere = '';
            $bakeryWhere .= CustomUtils::getEmployeeBakeryIds('b.id', '');
            if($bakeryWhere !='')
            {
                $bakeryWhere =' and  '.$bakeryWhere;
            }
            $sql = "SELECT o.id,o.account_number,o.description,b.description as branch,o.bakery_id, og.description AS outlet_group,
            opg.description AS outlet_price_group,
            r.area AS route
                FROM `outlet` o
                LEFT JOIN `bakery` b ON o.bakery_id=b.id
                LEFT JOIN  outlet_group og ON og.id = o.outlet_group_id
                LEFT JOIN  outlet_price_group opg ON opg.id = o.outlet_price_group_id
                LEFT JOIN  route r ON r.id = o.route_id
                WHERE o.status_id =1 {$bakeryWhere}
                GROUP BY o.id";
        }
        else{
            $sql = "SELECT o.id,o.account_number,o.description,b.description as branch,o.bakery_id, og.description AS outlet_group,
            opg.description AS outlet_price_group,
            r.area AS route
                FROM `outlet` o
                LEFT JOIN `bakery` b ON o.bakery_id=b.id
                LEFT JOIN  outlet_group og ON og.id = o.outlet_group_id
                LEFT JOIN  outlet_price_group opg ON opg.id = o.outlet_price_group_id
                LEFT JOIN  route r ON r.id = o.route_id
                WHERE o.status_id =1 AND b.status_id=1 AND o.bakery_id=".$employeeDetails['bakery_id']. "
                GROUP BY o.id";
        }
        $outlets = Yii::app()->db->createCommand($sql)->queryAll();
        #PHPExcel
        //        spl_autoload_unregister(array('YiiBase', 'autoload'));
        $phpExcelPath = Yii::getPathOfAlias('ext.phpexcel.Classes');
        include($phpExcelPath . DIRECTORY_SEPARATOR . 'PHPExcel.php');
        
        $i = 1;
        $idx = 2;
        $excel = new PHPExcel();
        $alp_pre = 65;
        $row=3;
        $objWorkSheet = $excel->createSheet(0);
        $objWorkSheet = $excel->setActiveSheetIndex(0);
        
        $objWorkSheet->setTitle("Customers");
        $objWorkSheet->getColumnDimension("A")->setWidth(10);
        $objWorkSheet->getColumnDimension("B")->setWidth(10);
        $objWorkSheet->getColumnDimension("C")->setWidth(35);
        $objWorkSheet->getColumnDimension("D")->setWidth(35);
        $objWorkSheet->getColumnDimension("E")->setWidth(12);
        $objWorkSheet->getColumnDimension("F")->setWidth(20);
        $objWorkSheet->getColumnDimension("G")->setWidth(18);
        $objWorkSheet->getColumnDimension("H")->setWidth(18);
        $objWorkSheet->getColumnDimension("I")->setWidth(35);
        $objWorkSheet->getColumnDimension("A")->setWidth(20);
        $objWorkSheet->setCellValue('A1', 'LIST OF CUSTOMERS');
        $objWorkSheet->mergeCells('A1:C1');
        $objWorkSheet->getStyle("A" . $idx . ":Z" . $idx)->applyFromArray(array("font" => array('color' => array('rgb' => 'FFFFFF'))));
        $objWorkSheet->getStyle('A2:I2')->applyFromArray( array( 'fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => '000000'))));
        $objWorkSheet->setCellValue('A2', 'ID')
        ->setCellValue('B2', 'ACCOUNT')
        ->setCellValue('C2', 'DESCRIPTION')
        ->setCellValue('D2', 'CUSTOMER GROUP')
        ->setCellValue('E2', 'PRICE GROUP')
        ->setCellValue('F2', 'ROUTE')
        ->setCellValue('G2', 'BRANCH');
        
        foreach($outlets as $data)
        {
            $objWorkSheet->setCellValue('A'.$row, $data['id']);
            $objWorkSheet->setCellValue('B'.$row, $data['account_number']);
            $objWorkSheet->setCellValue('C'.$row, $data['description']);
            $objWorkSheet->setCellValue('D'.$row, $data['outlet_group']);
            $objWorkSheet->setCellValue('E'.$row, $data['outlet_price_group']);
            $objWorkSheet->setCellValue('F'.$row, $data['route']);
            $objWorkSheet->setCellValue('G'.$row, $data['branch']);
            ++$row;
        }
        $objWorkSheet = $excel->setActiveSheetIndex();
        $excel->getSheetByName('Worksheet')
        ->setSheetState(PHPExcel_Worksheet::SHEETSTATE_HIDDEN);
        $objWriter = new PHPExcel_Writer_Excel2007($excel);
        $objWriter->setPreCalculateFormulas(false);
        setcookie('file_downloaded', 1, time()+20);
        $downloadFilename = 'CustomerList' . trim(date('Y_m_d_H')). '.xlsx';
        header('Content-Disposition: attachment; filename="'.$downloadFilename.'"');
        $_SESSION['EmployeeCustomer']='download';
        $objWriter->save('php://output');
        
        
        
    }
    public function actionAppendTemplate($id, $fromAddEdit = '0', $checklistTypeId = '')
    {
        
        $requiredApproval = 0;
        $formTemplates = ChecklistTemplates:: model() -> findAllByAttributes(array('checklist_type_id'=>1,'status_id'=>1));
        if (!empty($checklistTypeId))
        {
            $formTypes = ChecklistTypes:: model()->findAllByAttributes(array('id'=>$checklistTypeId,'status_id'=>1));            
            $requiredApproval = $formTypes[0]['approval_required'];
        }
        
        $this->renderPartial('_checklistTemplateSelection', array(
            'formTemplates' => $formTemplates, 'requiredApproval' => $requiredApproval,'id'=>$id,'fromAddEdit'=>$fromAddEdit
        ));
        
        
//        $formTemplates = ChecklistTemplates:: model() -> findAllByAttributes(array('checklist_type_id'=>1,'status_id'=>1));
//        $this->renderPartial('_checklistTemplateSelection', array(
//            'formTemplates' => $formTemplates,'id'=>$id,'fromAddEdit'=>$fromAddEdit
//        ));
        
    }
    public function actionGetTemplate($id, $fromAddEdit)
    {
        $checklist = new ChecklistTemplateManager();
        //Get form details
        $form_data = $checklist->getFormName($id);
        $additional_option_types = $checklist->getAdditionalOptionTypes($id);
        $form_elements_two = $checklist->getAllFormElementsByFormId($id);
        $form_elements = $checklist->getFormElementsByFormId($id);
        $KpiSubGroup = $checklist->getKpiSubGroupDropdown();
        $KpiSubGroupTab = $checklist->getKpiSubGroupDropdownTab();
        
        $this->renderPartial('checklist_preview', array('form_data' => $form_data, 'form' => $form_elements_two, 'form_elements' => $form_elements, 'kpi_subs' => $KpiSubGroup, 'kpi_group' => $KpiSubGroupTab,'additional_option_types' => $additional_option_types,'fromAddEdit' => $fromAddEdit));
        //$this->render('yabbyForm', array('form_data' => $form_data, 'form_elements' => $form_elements));
        
    }
    public function actionAppendForm($fromAddEdit = false)
    {
        $checklist = new ChecklistTemplateManager();
        $checklistTemplate=$checklist->getCopyTemplate($_POST['form']['template_id'],$_POST['form']['form_id']);
        $form_data = $checklist->getFormName($_POST['form']['template_id']);
        $yabby = new YabbyFormManager;
        $yabby->appendForm($checklistTemplate,$_POST['form']['form_id'],$form_data[0]['editable_questions']);
        if(!$fromAddEdit)
        {
            die ( json_encode ( array (
                'status'=>'success',
                'message' => 'Form has been updated'
            ) ) );
        }
    }
    public function actionViewApprover($id)
    {
        $sql = "SELECT e.`name`,b.`description`,e.email,c.checklist_name,c.description as form_description
                FROM `checklist` c
                LEFT JOIN `employee` e ON e.id= c.`request_approval_by`
                LEFT JOIN `bakery` b ON b.id=e.`bakery_id`
                WHERE c.id=".$id;
        
        $employeeDetails = Yii::app()->db->createCommand($sql)->queryRow();
        $this->renderPartial('employee_preview', array('employee' => $employeeDetails));
        
        
    }
    public function actionImportUpdateFormAllUpdate($id)
    {
        $con = Yii::app()->db;
        $search = '';
        $replace = '';
        $iForms = [];
        #INITIALIZE..
        ini_set("memory_limit", "-1");
        set_time_limit(0);
        $bakery = Yii::app()->user->bakery;
        $folder = Yii::app()->basePath . "/../files/";
        $mimetypes = array(
            'text/xls',
            'application/excel',
            'application/vnd.ms-excel',
            'application/vnd.msexcel',
            'application/octet-stream',
        );
        $columns = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T');
        $mFatal = '';
        $mWarning = '';
        $count = $uCounter = $iCounter = 0;
        #PHPExcel
        //        spl_autoload_unregister(array('YiiBase', 'autoload'));
        $phpExcelPath = Yii::getPathOfAlias('ext.phpexcel.Classes');
        include($phpExcelPath . DIRECTORY_SEPARATOR . 'PHPExcel.php');
        $objPHPExcel = new PHPExcel();
        $formArray=[];
        #Reading
        if(is_uploaded_file($_FILES['form_file']['tmp_name']))
        {
            if(move_uploaded_file($_FILES['form_file']['tmp_name'], $folder . $_FILES['form_file']['name']))
            {
                $filename = $folder . $_FILES['form_file']['name'];
                $objPHPExcel = PHPExcel_IOFactory::load($filename);
                $msg = '';
                foreach($objPHPExcel->getWorksheetIterator() as $worksheet)
                {
                    //Worksheet info
                    $worksheetTitle = $worksheet->getTitle();
                    $highestRow = $worksheet->getHighestRow();
                    $highestColumn = $worksheet->getHighestColumn();
                    $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
                    $nrColumns = ord($highestColumn) - 64;
                    //Worksheet data
                    for($row = 3; $row <= $highestRow; ++$row)
                    {
                        ################################
                        #Column A: ID required
                        $cell = $worksheet->getCellByColumnAndRow(0, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        $id = $val;
                        
                        ################################
                        #Column B: name required
                        $cell = $worksheet->getCellByColumnAndRow(1, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        $checklist_name = $val;
                        
                        ################################
                        #Column C: description required
                        $cell = $worksheet->getCellByColumnAndRow(2, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        $checklist_description = $val;
                        
                        ################################
                        #Column D: status  required
                        $cell = $worksheet->getCellByColumnAndRow(3, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        $status = $val;
                        
                        ################################
                        #Column E: Question id required
                        $cell = $worksheet->getCellByColumnAndRow(4, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        $question_id = $val;
                        ################################
                        #Column F: Question  required
                        $cell = $worksheet->getCellByColumnAndRow(5, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        $question = $val;
                        
                        ################################
                        #Column G: question type  required
                        $cell = $worksheet->getCellByColumnAndRow(6, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        $question_type = $val;
                        
                        ################################
                        #Column H: order required
                        $cell = $worksheet->getCellByColumnAndRow(7, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        $question_order = $val;
                        
                        ################################
                        #Column H: mandatory  required
                        $cell = $worksheet->getCellByColumnAndRow(8, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        $question_mandatory = $val;
                        
                        ################################
                        #Column J: question status  required
                        $cell = $worksheet->getCellByColumnAndRow(9, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        $question_status = $val;
                        
                        
                        ################################
                        #Column J: option(s) id required
                        $cell = $worksheet->getCellByColumnAndRow(10, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        $optionsIDList = [];
                        
                        if($val !="")
                        {
                            $splitoptions = explode('|', $val);
                            if(count($splitoptions)>0)
                            {
                                foreach ($splitoptions as $option)
                                {
                                    $optionsIDList[] = $option;
                                    
                                }
                            }
                        }
                        
                        ################################
                        #Column K: option(s) required
                        $cell = $worksheet->getCellByColumnAndRow(11, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        $optionsList = [];
                        
                        if($val !="")
                        {
                            $splitoptions = explode('|', $val);
                            if(count($splitoptions)>0)
                            {
                                foreach ($splitoptions as $option)
                                {
                                    $optionsList[] = $option;
                                    
                                }
                            }
                        }
                        
                        ################################
                        #Column M: option(s) statuses required
                        $cell = $worksheet->getCellByColumnAndRow(12, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        $optionsListStatuses = [];
                        
                        if($val !="")
                        {
                            $splitOptionsStatuses = explode('|', $val);
                            if(count($splitOptionsStatuses)>0)
                            {
                                foreach ($splitOptionsStatuses as $optionStatus)
                                {
                                    $optionsListStatuses[] = $optionStatus;
                                    
                                }
                            }
                        }
                        ################################
                        #Column L: additional questions required
                        $cell = $worksheet->getCellByColumnAndRow(13, $row);
                        $val = str_replace($search, $replace, $cell->getValue());
                        $additionalQuestions = [];
                        if(trim($val) != "")
                        {
                            $countOptionAdditionalQuestion=0;
                            $splitoptionsAddQuestion = explode('~', $val);
                            foreach($splitoptionsAddQuestion as $splitOpAddQuestions)
                            {
                                $countAddQuestion = 0;
                                $addQuestionArray = preg_split('/(\[[^]]+\])/', trim($splitOpAddQuestions), -1, PREG_SPLIT_DELIM_CAPTURE);
                                
                                foreach($addQuestionArray as $addArray)
                                {
                                    if($addArray !='' || $addArray!=null)
                                    {
                                        
                                        $addArray = str_replace(array('[',']'),'',$addArray);
                                        $splitAddQuestions = explode('|', $addArray);
                                        if(count($splitAddQuestions)>0)
                                        {
                                            $countAddQuestionOptions = 0;
                                            foreach ($splitAddQuestions as $key=>$value)
                                            {
                                                if($key==0)
                                                {
                                                    $additionalQuestions[$countOptionAdditionalQuestion][$countAddQuestion]['additional_answer']=$value;
                                                }
                                                if($key==1)
                                                {
                                                    $additionalQuestions[$countOptionAdditionalQuestion][$countAddQuestion]['additional_answer_type']=$value;
                                                    
                                                }
                                                
                                                if($key==2)
                                                {
                                                    $additionalQuestions[$countOptionAdditionalQuestion][$countAddQuestion]['additional_answer_mandatory']=$value;
                                                    
                                                    
                                                }
                                                
                                            }
                                        }
                                        $countAddQuestion++;
                                    }
                                    
                                }
                                
                                $countOptionAdditionalQuestion++;
                            }
                            
                            
                            
                            
                        }
                        
                        //setting array
                        $formArray[$id][]=array(
                            'form' => array(
                                'id'=>$id,
                                'name' => $checklist_name,
                                'description' => $checklist_description,
                                'status' => $status
                            ),
                            'checklist' => array(
                                'question_id'=>$question_id,
                                'question' => $question,
                                'question_order' => $question_order,
                                'question_manatory' => $question_mandatory,
                                'question_type' => $question_type,
                                'question_status' => $question_status,
                                'optionsID'=>$optionsIDList,
                                'options' => $optionsList,
                                'optionsStatuses' => $optionsListStatuses,
                                'additional_answers' => $additionalQuestions
                            )
                            
                        );
                    }
                    break;
                }
            }
        }
        $formDetails=[];
        $formChecklists=[];
        $countFormsNum=0;
        foreach ($formArray as $formChecklist)
        {
            foreach($formChecklist as $form)
            {
                $countFormsNum++;
                $formDetails=$form['form'];
                $formChecklists[]=$form['checklist'];
            }
        }
        //update form
        $checklist_details= Checklist::model()->findByPk($formDetails['id']);
        
        if(count($formDetails)>0)
        {
            
            $model = $this->loadModel($formDetails['id']);
            $model->checklist_name = $formDetails['name'];
            $model->description = $formDetails['description'];
            $model->status_id = $formDetails['status'];
            $model->updated = date("Y-m-d H:i:s");
            $model->updated_by = Yii::app()->user->id;
            if($checklist_details['request_approval_by']>0)
            {
                $model->status_id=0;
                $status=0;
                $comment='Form Updated';
            }
            else {
                $status=$model->status_id;
                $comment='Form Updated';
            }
            $model->save();
            $SQL = "INSERT INTO `checklist_reviews_logs`(`date_created`,`status_id`, `checklist_id`, `employee_id`, `reason`)
                     VALUES ('".date('Y-m-d H:i:s')."',".$status.",". $formDetails['id'].",".Yii::app()->user->id.",'".$comment."')";
            $insert_command = Yii::app()->db->createCommand($SQL);
            $insert_command->execute();
            $con = Yii::app()->db;
            if(count($formChecklists)>0)
            {
                $sql = "UPDATE `checklist_items` SET `status_id`=0 WHERE `checklist_id` = ".$formDetails['id'];
                $cmd = $con->createCommand($sql);
                $res = $cmd->execute();
                
                $sql = "DELETE FROM `checklist_additional_options` WHERE `checklist_id` = ".$formDetails['id'];
                $cmd = $con->createCommand($sql);
                $res = $cmd->execute();
                foreach($formChecklists as $checklists)
                {
                    
                    
                    if($checklists['question_id'])
                    {
                        $status_id=1;
                        $sql1 = "UPDATE `checklist_items` SET `description`=:description,`ordering`=:ordering,`mandatory` =:mandatory,`status_id`=:status_id  WHERE `id` = :id";
                        $command = $con->createCommand($sql);
                        $command->bindValue(':description', $checklists['question'], PDO::PARAM_STR);
                        $command->bindValue(':ordering', $checklists['question_order'], PDO::PARAM_INT);
                        $command->bindValue(':mandatory', $checklists['question_manatory'], PDO::PARAM_INT);
                        $command->bindValue(':status_id', $checklists['question_status'], PDO::PARAM_INT);
                        $command->bindValue(':id', $checklists['question_id'], PDO::PARAM_INT);
                        $command->execute();
                        
                        if($checklists['question_type']==2 || $checklists['question_type']==3 || $checklists['question_type']==7)
                        {
                            if(count($checklists['options'])>0)
                            {
                                foreach($checklists['options'] as $key =>$value)
                                {
                                    $option_status=$checklists['optionsStatuses'][$key];
                                    if(isset($checklists['optionsID'][$key]))
                                    {
                                        $sql = "UPDATE `checklist_options` SET `description`=:description,option_type_id=:option_type, `status_id`=:status_id  WHERE `id` = :id";
                                        $command = $con->createCommand($sql);
                                        $command->bindValue(':description', $value, PDO::PARAM_STR);
                                        $command->bindValue(':option_type', $checklists['question_type'], PDO::PARAM_STR);
                                        $command->bindValue(':status_id', $option_status, PDO::PARAM_INT);
                                        $command->bindValue(':id', $checklists['optionsID'][$key], PDO::PARAM_INT);
                                        $command->execute();
                                        if(isset($checklists['additional_answers'][$key]))
                                        {
                                            if(count($checklists['additional_answers'][$key]))
                                            {
                                                foreach($checklists['additional_answers'][$key] as $addAnswers)
                                                {
                                                    if($addAnswers['additional_answer']!='' && $addAnswers['additional_answer_type']!='')
                                                    {
                                                        $SQL = "INSERT INTO `checklist_additional_options`(`checklist_id`,`checklist_item_id`, `option_type_id`, `status_id`, `checklist_options_id`, `mandatory`,`description`)
                                                                         VALUES (".$formDetails['id'].",".$checklists['question_id'].",".$addAnswers['additional_answer_type'].",'1',".$checklists['optionsID'][$key].",".$addAnswers['additional_answer_mandatory'].",'".$addAnswers['additional_answer']."')";
                                                        $insert_command = Yii::app()->db->createCommand($SQL);
                                                        $insert_command->execute();
                                                    }
                                                    
                                                }
                                                
                                                
                                            }
                                        }
                                    }
                                    else{
                                        $SQL = "INSERT INTO `checklist_options`(`checklist_items_id`,`description`, `option_type_id`, `ordering`,`status_id`)
                                                        VALUES (".$checklists['question_id'].",'".$value."',". $checklists['question_type'].",1,".$option_status.")";
                                        $insert_command = Yii::app()->db->createCommand($SQL);
                                        $insert_command->execute();
                                        $optionID = Yii::app()->db->getLastInsertId();
                                        if(isset($checklists['additional_answers'][$key]) && count($checklists['additional_answers'][$key]))
                                        {
                                            foreach($checklists['additional_answers'][$key] as $addAnswers)
                                            {
                                                if($addAnswers['additional_answer']!='' && $addAnswers['additional_answer_type']!='')
                                                {
                                                    $SQL = "INSERT INTO `checklist_additional_options`(`checklist_id`,`checklist_item_id`, `option_type_id`, `status_id`, `checklist_options_id`, `mandatory`,`description`)
                                                                         VALUES (".$formDetails['id'].",".$checklists['question_id'].",".$addAnswers['additional_answer_type'].",'1',".$optionID.",".$addAnswers['additional_answer_mandatory'].",'".$addAnswers['additional_answer']."')";
                                                    $insert_command = Yii::app()->db->createCommand($SQL);
                                                    $insert_command->execute();
                                                }
                                                
                                            }
                                            
                                            
                                        }
                                        
                                        
                                        
                                    }
                                }
                            }
                        }
                        else
                        {
                            $description='';
                            $option_status=$checklists['optionsStatuses'][0];
                            if(isset($checklists['optionsID'][0]))
                            {
                                
                                $sql = "UPDATE `checklist_options` SET `description`=:description, `option_type_id`=:option_type, `status_id`=:status_id  WHERE `id` = :id";
                                $command = $con->createCommand($sql);
                                $command->bindValue(':description', '', PDO::PARAM_STR);
                                $command->bindValue(':option_type', $checklists['question_type'], PDO::PARAM_STR);
                                $command->bindValue(':status_id', $option_status, PDO::PARAM_INT);
                                $command->bindValue(':id', $checklists['optionsID'][0], PDO::PARAM_INT);
                                $command->execute();
                                
                                if(isset($checklists['additional_answers'][0]) && count($checklists['additional_answers'][0]))
                                {
                                    foreach($checklists['additional_answers'][0] as $addAnswers)
                                    {
                                        if($addAnswers['additional_answer']!='' && $addAnswers['additional_answer_type']!='')
                                        {
                                            $SQL = "INSERT INTO `checklist_additional_options`(`checklist_id`,`checklist_item_id`, `option_type_id`, `status_id`, `checklist_options_id`, `mandatory`,`description`)
                                                                         VALUES (".$formDetails['id'].",".$checklists['question_id'].",".$addAnswers['additional_answer_type'].",'1',".$checklists['optionsID'][0].",".$addAnswers['additional_answer_mandatory'].",'".$addAnswers['additional_answer']."')";
                                            $insert_command = Yii::app()->db->createCommand($SQL);
                                            $insert_command->execute();
                                        }
                                        
                                    }
                                    
                                    
                                }
                                
                                
                            }
                            
                            else {
                                $SQL = "INSERT INTO `checklist_options`(`checklist_items_id`,`description`, `option_type_id`, `ordering`,`status_id`)
                                                        VALUES (".$checklists['question_id'].",'',". $checklists['question_type'].",1,".$option_status.")";
                                $insert_command = Yii::app()->db->createCommand($SQL);
                                $insert_command->execute();
                                $optionID = Yii::app()->db->getLastInsertId();
                                if(isset($checklists['additional_answers'][0]) && count($checklists['additional_answers'][0]))
                                {
                                    foreach($checklists['additional_answers'][0] as $addAnswers)
                                    {
                                        if($addAnswers['additional_answer']!='' && $addAnswers['additional_answer_type']!='')
                                        {
                                            $SQL = "INSERT INTO `checklist_additional_options`(`checklist_id`,`checklist_item_id`, `option_type_id`, `status_id`, `checklist_options_id`, `mandatory`,`description`)
                                                                         VALUES (".$formDetails['id'].",".$checklists['question_id'].",".$addAnswers['additional_answer_type'].",'1',".$optionID.",".$addAnswers['additional_answer_mandatory'].",'".$addAnswers['additional_answer']."')";
                                            $insert_command = Yii::app()->db->createCommand($SQL);
                                            $insert_command->execute();
                                        }
                                        
                                    }
                                    
                                    
                                }
                            }
                            
                        }
                    }
                    else{
                        $SQL = "INSERT INTO `checklist_items`(`checklist_id`,`description`, `ordering`, `mandatory`,`status_id`)
                                                      VALUES (".$formDetails['id'].",'".$checklists['question']."',".$checklists['question_order'].",'".$checklists['question_manatory']."',".$checklists['question_status'].")";
                        $insert_command = Yii::app()->db->createCommand($SQL);
                        $insert_command->execute();
                        $checklists['question_id'] = Yii::app()->db->getLastInsertId();
                        if($checklists['question_type']==2 || $checklists['question_type']==3 || $checklists['question_type']==7)
                        {
                            if(count($checklists['options'])>0)
                            {
                                foreach($checklists['options'] as $key =>$value)
                                {
                                    $option_status=$checklists['optionsStatuses'][$key];
                                    $SQL = "INSERT INTO `checklist_options`(`checklist_items_id`,`description`, `option_type_id`, `ordering`,`status_id`)
                                                        VALUES (:checklist_items_id,:description,:option_type_id,:ordering,:status_id)";
                                    $command = $con->createCommand($SQL);
                                    $command->bindValue(':checklist_items_id', $checklists['question_id'], PDO::PARAM_STR);
                                    $command->bindValue(':description', $value, PDO::PARAM_STR);
                                    $command->bindValue(':option_type_id', $checklists['question_type'], PDO::PARAM_INT);
                                    $command->bindValue(':ordering', 1, PDO::PARAM_INT);
                                    $command->bindValue(':status_id', $option_status, PDO::PARAM_INT);
                                    
                                    $command->execute();
                                    $optionID = Yii::app()->db->getLastInsertId();
                                    if(isset($checklists['additional_answers'][$key]) && count($checklists['additional_answers'][$key]))
                                    {
                                        foreach($checklists['additional_answers'][$key] as $addAnswers)
                                        {
                                            if($addAnswers['additional_answer']!='' && $addAnswers['additional_answer_type']!='')
                                            {
                                                $SQL = "INSERT INTO `checklist_additional_options`(`checklist_id`,`checklist_item_id`, `option_type_id`, `status_id`, `checklist_options_id`, `mandatory`,`description`)
                                                                         VALUES (".$formDetails['id'].",".$checklists['question_id'].",".$addAnswers['additional_answer_type'].",'1',".$optionID.",".$addAnswers['additional_answer_mandatory'].",'".$addAnswers['additional_answer']."')";
                                                $insert_command = Yii::app()->db->createCommand($SQL);
                                                $insert_command->execute();
                                            }
                                            
                                        }
                                        
                                        
                                    }
                                    
                                }
                            }
                        }
                        else
                        {
                            $description='';
                            
                            $option_status=$checklists['optionsStatuses'][0];
                            $SQL = "INSERT INTO `checklist_options`(`checklist_items_id`,`description`, `option_type_id`, `ordering`,`status_id`)
                                                        VALUES (:checklist_items_id,:description,:option_type_id,:ordering,:status_id)";
                            $command = $con->createCommand($SQL);
                            $command->bindValue(':checklist_items_id', $checklists['question_id'], PDO::PARAM_STR);
                            $command->bindValue(':description', '', PDO::PARAM_STR);
                            $command->bindValue(':option_type_id', $checklists['question_type'], PDO::PARAM_INT);
                            $command->bindValue(':ordering', 1, PDO::PARAM_INT);
                            $command->bindValue(':status_id', $option_status, PDO::PARAM_INT);
                            $command->execute();
                            $optionID = Yii::app()->db->getLastInsertId();
                            if(isset($checklists['additional_answers'][0]) && count($checklists['additional_answers'][0]))
                            {
                                foreach($checklists['additional_answers'][0] as $addAnswers)
                                {
                                    if($addAnswers['additional_answer']!='' && $addAnswers['additional_answer_type']!='')
                                    {
                                        $SQL = "INSERT INTO `checklist_additional_options`(`checklist_id`,`checklist_item_id`, `option_type_id`, `status_id`, `checklist_options_id`, `mandatory`,`description`)
                                                                         VALUES (".$formDetails['id'].",".$checklists['question_id'].",".$addAnswers['additional_answer_type'].",'1',".$optionID.",".$addAnswers['additional_answer_mandatory'].",'".$addAnswers['additional_answer']."')";
                                        $insert_command = Yii::app()->db->createCommand($SQL);
                                        $insert_command->execute();
                                    }
                                    
                                }
                                
                                
                            }
                            
                        }
                        
                        
                        
                        
                    }
                }
            }
            
        }
        echo '<div class="alert alert-success"><span class="fa fa-check-circle fa-fw"></span>&nbsp;'.$countFormsNum.' Records processed</div>';
        die;
    }
    
    public function checkDuplicates($duplicateArray,$form_id,$question_order,$questionStatus,$row,$key)
    {
        if(count($duplicateArray)>0)
        {
            foreach($duplicateArray as $nextKey =>$duplicate)
            {
                if($duplicate['form_id'] == $form_id && $duplicate['sequence'] == $question_order && $duplicate['status'] == $questionStatus &&  $duplicate['row'] != $row && $nextKey<$key)
                {
                    return $duplicate['row'];
                }
            }
        }
        return false;
        
    }
}


?>